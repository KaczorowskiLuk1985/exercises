package com.company;

public class Rekrutacja1 {
    public static void main(String[] args) {
        long a = 6000;
        long b = 7000;
        System.out.println(howManyTimesSquare(a, b));

    }

    public static int howManyTimesSquare(long A, long B) {
        int maxSquare = 0;

        for (long number = A; number <= B; number++) {
            int counter = suareRootCounter(number);
            if (counter > maxSquare) {
                maxSquare = counter;
            }
        }
        return maxSquare;
    }

    static int suareRootCounter(long number) {
        int maxHowManyTimesSqet = 0;

        double sqrt = Math.sqrt(number);
        while (sqrt % 1 == 0) {
            maxHowManyTimesSqet++;
            sqrt = Math.sqrt(sqrt);
        }
        //tutaj zostawiamy pustą linijkę
        return maxHowManyTimesSqet;
    }
}
