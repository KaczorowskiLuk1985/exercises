package com.company;

import com.company.zadania.zadanie165.Document;
import com.company.zadania.zadanie165.DocumentGenerator;

public class HtmlDocument extends Document {

    public HtmlDocument(DocumentGenerator.DocumentType type, String content) {
        super(type, formatText(content));
    }

    private static String formatText(String text) {
        String extraH1 = String.format("<h1>%s</h1", text);
        String extraBr = String.format("<h1>%s</h1", text);
        extraBr = "meta charset=\"utf-8\">" + extraBr;
        return extraBr;

    }

}
