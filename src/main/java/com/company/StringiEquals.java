package com.company;
/*
chodzi o to zeby napisy porownywac za pomoca equals
hasla nie powinno sie przechowywac w java poniewaz sa zbierane w "String pool" i możne je pozniej wydobywac
 */

import java.net.SocketTimeoutException;

public class StringiEquals {
    public static void main(String[] args) {
        String a = "kot";
        String b = "kot";
        System.out.println(a == b);

        String c = new String("kot");
        System.out.println(a == c);
        System.out.println(a.equals(c));

        System.out.println(a.hashCode());
        System.out.println(b.hashCode());
        System.out.println(c.hashCode());

        String d = "k";
        System.out.println(d.hashCode());
        d += "ot";
        System.out.println(d.hashCode());
    }

}
