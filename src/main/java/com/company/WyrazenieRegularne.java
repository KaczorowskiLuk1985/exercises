package com.company;

public class WyrazenieRegularne {
    public static void main(String[] args) {
//        String imie = "Lukasz Kaczorowski";
//        System.out.println(takCZyNie(imie));
//        System.out.println(takCZyNie("Jan Marcin Nowak"));
//        System.out.println(takCZyNie("Jan Marcin Nowak Czarek"));
//        System.out.println("lu_kac@o2.pl");
//        System.out.println(czyToMaile("lu.kac@o2.pl"));
        System.out.println(czyToNumerTelefonu("555333444"));
        System.out.println(czyToNumerTelefonu("555-333-444"));
        System.out.println(czyToNumerTelefonu("555-333444"));

    }

    public static boolean takCZyNie(String imie) {
        return imie
                .toLowerCase()
                .matches("[a-z]{3,}( [a-z]{3,})? ([a-z]{3,})");
    }

    public static boolean czyToMaile(String mail) { // + >0 , ? 0 lub 1 , * >= 0 , {}
        return mail.matches("[a-z][.-_a-z19]+@[a-z1-9]+\\.[a-z]{2,}");
    }
    public static boolean czyToNumerTelefonu (String telefon){
        return telefon.matches("[0-9]{9}|([0-9]{3}-){2}[0-9]{3}");
    }
}
