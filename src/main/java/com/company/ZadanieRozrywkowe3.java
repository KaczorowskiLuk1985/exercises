package com.company;

public class ZadanieRozrywkowe3 {
    public static void main(String[] args) {
        System.out.println(findNextSquare(625));
    }

    public static long findNextSquare(long sq) {
        long max = Long.MAX_VALUE;

        if (sq < 1 || sq % Math.sqrt(sq) != 0) {
            return -1;
        }
        for (long i = sq; i < max; i++) {
            i++;
            if (i % Math.sqrt(i) == 0 && i > sq) {
                return i;
            }
        }
        return -1;
    }
}
//expected:<15241630849> but was:<-1>