package com.company;

import java.util.Arrays;

public class ZadanieRozrywkowe4 {
    public static void main(String[] args) {
        System.out.println(isIsogram("and"));

    }

    public static boolean isIsogram(String str) {
        str = str.toLowerCase();
        int dlugoscWyrazu = str.length();
        char[] arr = str.toCharArray();
        Arrays.sort(arr);

        for (int i = 0; i < dlugoscWyrazu -1; i++) {
            if (arr[i] == arr[i + 1]) {
                return false;
            }
        }
        return true;
    }
}
