package com.company.zadania;

public class Zadanie108 {
    private final static int LICZBA_WYKONAN = 100_000;
    private final static String NAPIS_TESTOWY = "PIES";

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        laczenie();
        long koniec = System.currentTimeMillis();
        System.out.println("czas +=: "+ (koniec - start));

        start = System.currentTimeMillis();
        laczenieStringBulider();
        koniec = System.currentTimeMillis();
        System.out.println("czas builider +=: "+ (koniec - start));

        start = System.currentTimeMillis();
        laczenieBuffer();
        koniec = System.currentTimeMillis();
        System.out.println("czas buffer+=: "+ (koniec - start));

    }
    static String laczenie (){
        String napis = "";


        for (int i = 0; i < LICZBA_WYKONAN ; i++) {
            napis+=NAPIS_TESTOWY;
        }
        return napis;
    }
    static String laczenieStringBulider(){
        StringBuilder napis = new StringBuilder();

        for (int i = 0; i < LICZBA_WYKONAN; i++) {
            napis.append(NAPIS_TESTOWY);
        }
        return napis.toString();
    }
    static String laczenieBuffer(){
        StringBuffer napis = new StringBuffer();

        for (int i = 0; i < LICZBA_WYKONAN; i++) {
            napis.append(NAPIS_TESTOWY);
        }
        return napis.toString();
    }
}
