package com.company.zadania;
/*
Utwórz metodę, która przyjmuje wartość produktu oraz procent podatku jaki należy naliczyć, a następnie zwraca kwotę po opodatkowaniu.

> Dla przekazanych parametrów `100, 23`,
>powinno zwrócić `123`
 */

public class Zadanie146 {
    public static void main(String[] args) {

    }

    public static double zwrocPoPodatku(int suma, int podatek) {
        if (podatek < 0 || podatek > 100) {
            return suma;
        }

        return suma * (podatek / 100.0) + suma;
    }
}
