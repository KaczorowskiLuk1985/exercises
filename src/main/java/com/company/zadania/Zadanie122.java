package com.company.zadania;
/*

 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Zadanie122 {
    public static void main(String[] args) {
        System.out.println(czyMozemyZamienic("AABBCC", "ACBBCA"));
        System.out.println(chngePosible("AABBCCDD", "ACBDBDCA"));

    }

    private static boolean czyMozemyZamienic(String chain1, String chain2) {
        if (chain1.length() != chain2.length()) {
            return false;
        }

        char[] chainArrr1 = chain1.toCharArray();
        char[] chainArrr2 = chain2.toCharArray();

        List<Integer> listPosition = new LinkedList<>();

        for (int i = 0; i < chainArrr1.length; i++) {
            if (chainArrr1[i] != chainArrr2[i]) {
                listPosition.add(i);

                if (listPosition.size() > 2) {
                    return false;
                }
            }

        }
        if (listPosition.size() == 1) {
            return false;

        }
        char bufor = chainArrr2[listPosition.get(0)];
        chainArrr2[listPosition.get(0)] = chainArrr2[listPosition.get(1)];
        chainArrr2[listPosition.get(1)] = bufor;

        return Arrays.equals(chainArrr2, chainArrr1);

    }

    private static boolean chngePosible(String chainA, String chainB) {
        char[] a = chainA.toCharArray(); //ctrl + alt + v tworzy obiekt
        Arrays.sort(a);
        char[] b = chainB.toCharArray();
        Arrays.sort(b);

        return Arrays.equals(a, b);
    }
}
