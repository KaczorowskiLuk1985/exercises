package com.company.zadania;
/*
*ZADANIE #41*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
> Dla `4` zwróci: `[10, 11, 12, 13]`
>
> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */

import java.util.Arrays;

public class Zadanie41 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrocTablice(6)));

    }
    static int [] zwrocTablice (int rozmiar){
        int wartosc = 10;
        int [] tablica = new int [rozmiar];
        for (int pozycja = 0; pozycja < rozmiar ; pozycja++) {
            tablica [pozycja] = wartosc;
            wartosc++;
        }
        return tablica;
    }
}
