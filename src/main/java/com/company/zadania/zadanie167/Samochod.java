package com.company.zadania.zadanie167;

public class Samochod extends Pojazdy {
    final static Integer KOD_KRAJU = 999;
    // pole statyczne piszemy z duzych liter. odwolujemy sie przez nazwe klasy
    {
        System.out.println("blok inicjalizacyjny samochod 2");
    }

    static {
        System.out.println("STATYCZNY blok inicjalizacyjny samochód");
    }

    {
        System.out.println("blok inicjalizacyjny samochod 1");

    }

    Samochod() {
        System.out.println("konstruktor Samochod");
    }
}
