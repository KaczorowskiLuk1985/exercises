package com.company.zadania;

/*
Utwórz metodę, która przyjmuje wyraz, a następnie zwraca informacje czy zawiera on samogłoski czy nie.
 */
public class Zadanie83 {
    public static void main(String[] args) {
        String wyraz = "kAJAk";
        System.out.println(czyZawieraSamogloski(wyraz));
        System.out.println(czyZawieraSamogloski2(wyraz));
//jezeli metoda jest statyczna to mozna w niej uzywac tylko metod statycznych
    }

    static boolean czyZawieraSamogloski(String wyraz) {
        String[] samogloski = new String[]{"a", "e", "o", "u", "i", "y"};

        for (String litera : samogloski) {
            if (wyraz.toLowerCase().contains(litera)) { //zmniejszenie możemy ogarnac przed petlą
                return true;
            }
        }
        return false;
    }

    static boolean czyZawieraSamogloski2(String wyraz) {
        String nowyWyraz = wyraz.toLowerCase();
        return nowyWyraz.contains("a")
                || nowyWyraz.contains("e")
                || nowyWyraz.contains("u")
                || nowyWyraz.contains("i")
                || nowyWyraz.contains("o")
                || nowyWyraz.contains("y");
    }
}
