package com.company.zadania;
/*
Utwórz drugą metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć wszystkie elementy równe podanemu parametrowi
(tzn. gdy podamy `11` to ma usunąć z tablicy wszystkie `11`-ki)
Dla `([1, 2, 2, 4, 5],  2)`
zwróci `[1, 4, 5]`
 */

import java.util.Arrays;

public class Zadanie50 {
    public static void main(String[] args) {
        int[] tablicaMain = {1, 2, 3, 4, 5, 6, 6, 6, 6, 7};
        System.out.println(Arrays.toString(usunPodanaLiczbe(tablicaMain, 6)));

    }

    static int zwrocIleRazyWystepuje(int[] tablica, int poszLiczba) {
        int liczbaWystapien = 0;

        for (int index = 0; index < tablica.length; index++) {
            if (poszLiczba == tablica[index]) {             //if - instrukcja sprawdzajaca
                liczbaWystapien++;
            }

        }

        return liczbaWystapien;


    }

    static int[] usunPodanaLiczbe(int[] tablica, int usuwanaLiczba) {
        int iliscWystapien = zwrocIleRazyWystepuje(tablica, usuwanaLiczba);
        int[] tablicaNowa = new int[tablica.length - iliscWystapien];
        int indexWNowejTablicy = 0;//inicjujemy podstawianie indeksu;

        for (int index = 0; index < tablica.length; index++) {
            if (usuwanaLiczba != tablica[index]) {
                tablicaNowa[indexWNowejTablicy] = tablica[index];
                indexWNowejTablicy++;

            }

        }


        return tablicaNowa;

    }
}
