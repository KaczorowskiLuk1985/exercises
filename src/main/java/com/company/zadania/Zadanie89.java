//package com.company;
///*
//Utwórz metodę, która przyjmuje ciąg w formie `"<nazwa operacji> <operacja>"`. Na bazie nazwy operacji należy rozpoznać separator operacji.
//> Dla `"add 5+4+99"` zwróci `108`
//> Dla `"subtract 100-20"` zwróci `80`
//> Dla `"multiply 8*17"` zwróci `136`x
//> Dla `"divide 200/25"` zwróci `8`
// */
//
//public class Zadanie89 {
//    private static final String  OPERAION_ADDING = "add";
//    private static final String OPERATION_ADDING_SYMBOL = "+";
//    private static final String  OPERAION_SUBSTRACT = "substract";
//    private static final String  OPERATION_SUBSTRACTING_SYMBOL = "-";
//    private static final String  OPERAION_MULTIPLY = "multiply";
//    private static final String  OPERAION_MULTIPLY_SYMBOL = "*";
//    private static final String  OPERAION_DIVIDE = "divide";
//    private static final String  OPERAION_DIVIDE_SYMBOL = "/";
//
//    public static void main(String[] args) {
//        System.out.println(zwrocWynik("add 5+4+99"));
//        System.out.println(zwrocWynik("substract 100-20-5"));
//        System.out.println(zwrocWynik("multiply 8*17"));
//        System.out.println(zwrocWynik("divide 200/25"));
//    }
//
//    static Integer zwrocWynik(String napis) {
//        String[] tablica = napis.split(" ");
//        Integer suma = 0;
//
//        switch (tablica[0]) {//switch jest instrukcja warunkową
//            case OPERAION_ADDING :
//                String[] elementy = tablica[1].split(OPERAION_ADDING);
//                for (String element : elementy) {
//                    suma += Integer.parseInt(element); // napis przedstaiany jest jako warosc liczbowa
//                }
//                break;
//            case OPERAION_SUBSTRACT:
//                String[] elementy2 = tablica[1].split(OPERAION_SUBSTRACT_S);
//                suma = Integer.parseInt(elementy2[0]);
//                //for (String element2 : elementy2) { w tym przypadku nie da sie zrobic foriczem
//                for (int i = 1; i < elementy2.length; i++) {
//
//                    suma -= Integer.parseInt(elementy2[i]);
//
//                }
//                break;
//            case OPERAION_MULTIPLY:
//                String[] elementy3 = tablica[1].split(OPERAION_MULTIPLY_SYMBOL);
//                suma = Integer.parseInt(elementy3[0]);
//
//                for (int i = 1; i < elementy3.length; i++) {
//
//                    suma *= Integer.parseInt(elementy3[i]);
//                }
//                break;
//            case OPERAION_DIVIDE:
//                String[] elementy4 = tablica[1].split("/");
//                suma = Integer.parseInt(elementy4[0]);
//
//                for (int i = 1; i < elementy4.length; i++) {
//
//
//                    suma /= Integer.parseInt(elementy4[i]);
//
//                }
//                break;
//            default:
//                suma = null;
//        }
//        return suma;
//    }
//}
