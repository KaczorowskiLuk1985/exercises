package com.company.zadania;
/*
Utwórz metodę, która przyjmuje imię i nazwisko a następnie zwraca w formie napisu login, który składa się z:
>- 3 liter imienia
>- 3 liter nazwiska
>- liczby, która jest sumą długości imienia i nazwiska

> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
> Dla `"Anna Nowak"`, zwróci `"annnow9"`
> Dla `"Jan Kowalski"`, zwróci `"jankow11"`
 */

public class Zadanie85 {
    public static void main(String[] args) {
        System.out.println(zwrocLogin("Łukasz Kaczorowski"));

    }
    static String zwrocLogin (String nazwa){
        String [] nowaNazwa =  nazwa.split(" ");
        return (nowaNazwa[0].substring(0,2)
                +nowaNazwa[1].substring(0,3)
                +(nowaNazwa[0]+nowaNazwa[1]).length()).toLowerCase();
    }
}
