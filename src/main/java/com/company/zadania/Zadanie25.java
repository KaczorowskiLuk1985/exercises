package com.company.zadania;
/*
*ZADANIE #25*
Utwórz metodę, do której przekazujesz trzy parametry (np `start`, `amount`, `jump`).
Następujące parametry odpowiadają za:
pierwszy - początkowa (pierwsza) wyświetlona wartość
drugi  - liczba wyświetlonych elementów
trzeci (`double`) - różnica pomiędzy kolejnymi elementami (“skok”)
> Dla `0, 5, 5` wyświetli `0, 5, 10, 15, 20` (zaczynamy od `0`, wyświetlamy `5` elementów a elementy różnią się od siebie o `5`)
> Dla `0, 6, 2.5` wyświetli `0.0, 2.5, 5.0, 7.5, 10.0, 12.5` (zaczynamy od `0`, wyświetlamy `6` elementów a elementy różnią się od siebie o `2.5`)
 */

public class Zadanie25 {
    public static void main(String[] args) {
        warunek1(0,6,2.5);
        System.out.println();
        warunek2(0,6,2.5);

    }

    static void warunek1(int start, int amoun, double jump) {
        for (int krok = start; krok < amoun; krok++) {
            System.out.print(krok * jump + ",");

        }
    }
    static void warunek2(int start, int amoun, double jump){
        int step = start;
        while (step<amoun){
            System.out.print(step * jump + ",");
            step++;
        }
    }
}
