package com.company.zadania;
/*
*ZADANIE #30*
Utwórz metodę, która przyjmuje *jeden* parametr (który jest liczbą wierszy i kolumn) oraz wyświetla tabliczkę mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
Dla `3` wyświetli:

1   2   3
2   4   6
3   6   9

> Dla `6` wyświetli:
> ```
1   2   3   4   5   6
2   4   6   8  10  12
3   6   9  12  15  18
4   8  12  16  20  24
5  10  15  20  25  30
6  12  18  24  30  36 (edited)
 */
public class Zadanie30 {
    public static void main(String[] args) {
        kolumnyWiersze(6);

    }
    static void kolumnyWiersze (int liczba){
        for (int i = 1; i < liczba; i++) {
            for (int j = 1; j < liczba ; j++) {
                System.out.print(i*j + "\t");

            }
            System.out.println();
        }

    }
}
