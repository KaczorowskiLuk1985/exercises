package com.company.zadania;
/*
Utwórz metodę, która przyjmuje trzy parametry - listę liczb (np. `ArrayList`), początek zakresu (`int`) oraz koniec zakresu (`int`) a zwrócić liczbę.
 Metoda ma policzyć różnicę pomiędzy największym i najmniejszym elementem w tym zakresie.
Policzona różnica jest indeksem elementu z tablicy wejściowej który należy zwrócić (jeśli wartość będzie poza zakresem indeksów metoda ma zwrócić `-1`)
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie102 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1,2,3,4,5,6,7,8,9
        ));
        System.out.println(zwrociRozniceZakresu(lista, 2, 4));
        System.out.println("szukana liczba :"+pokazIndeksRoznicy(lista, 3, 6));
    }

    static Integer pokazIndeksRoznicy(List<Integer> lista, int start, int stop) {
        Integer index = zwrociRozniceZakresu(lista,start,stop);
        if (index <0 && index >lista.size()-1){
            return -1;
        }
        return lista.get(index);
    }

    static Integer zwrociRozniceZakresu(List<Integer> lista, int start, int stop) {
        Integer max = lista.get(start);
        Integer min = lista.get(stop);

        for (int i = start; i <= stop; i++) {
            if (lista.get(i) > max) {
                max = lista.get(i);
            }
            if (lista.get(i) < min) {
                min = lista.get(i);
            }

        }
        return max - min;
    }

}
