package com.company.zadania;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*
Utwórz metodę która przyjmuje liczbę, a następnie tyle razy losuje liczbę (przy wykorzystaniu klasy `Random`).
Metoda ma zwrócić mapę, która będzie przechowywać informację ile razy wystąpiła każda z liczb. (edited)
 */
public class Zadanie113 {
    public static void main(String[] args) {
        System.out.println(sprawdzIleRazy(220000));
    }

    static Map<Integer, Integer> sprawdzIleRazy(int liczbaLosowan) {
        Random random = new Random();
        Map<Integer, Integer> mapa = new HashMap<>();

        for (int i = 0; i < liczbaLosowan; i++) {
            int wylosowanaLiczba = random.nextInt(11);
//            if (mapa.containsKey(wylosowanaLiczba)) {
//                int staraLiczbaWystapien = mapa.get(wylosowanaLiczba);
//                mapa.put(wylosowanaLiczba,staraLiczbaWystapien + 1);
//                mapa.put(wylosowanaLiczba, mapa.get(wylosowanaLiczba) + 1);
//            } else {
//                mapa.put(wylosowanaLiczba, 1); //gdy mapa nie zawiera
            int liczbaWystapien = mapa.getOrDefault(wylosowanaLiczba, 0); // jesli znajdzie w tej mapie wartosc to zostanie zwrocona, jesli nie to zwroci domyslna
            mapa.put(wylosowanaLiczba, liczbaWystapien + 1);
        }
        return mapa;
    }
}

