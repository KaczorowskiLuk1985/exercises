package com.company.zadania;

/*
Policz sumę cyfr przekazanej liczby przy wykorzystaniu *rekurencji*.
 */
public class Zadanie142 {
    public static void main(String[] args) {
        System.out.println(sumaCyfr(50004));

    }

    private static int sumaCyfr(int numter) {
        System.out.println("numter = [" + numter + "]");

        if (numter == 0) {
            return 0;
        } else {
            return (numter % 10) + sumaCyfr(numter / 10);
        }
    }
}
