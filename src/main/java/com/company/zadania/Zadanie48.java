package com.company.zadania;
/*
Utwórz metodę, która przyjmuje trzy parametry
- tablicę oraz początek i koniec przedziału.
Metoda ma zwrócić nową tablicę, która będzie zawierać elementy z wybranego przedziału.
 */

import java.util.Arrays;

public class Zadanie48 {
    public static void main(String[] args) {

        System.out.println(Arrays.toString(zwrocTablice(new int[]{-1, 0, 1, 2, 2, 3, 4, 6, 5, 7, 8}, 0, 5)));
    }

    static int[] zwrocTablice(int[] tablica, int start, int stop) {
        int[] tablicaZmniejszona = new int[stop - start + 1];
        for (int index = start; index <= stop; index++) {
            tablicaZmniejszona[index - start] = tablica[index];

        }
        return tablicaZmniejszona;

    }
}
