package com.company.zadania.zadanie97;

public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private Plec plec;

    public Osoba(String imie, String nazwisko, int wiek, Plec plec) {//konstruktor trzyprametrowy
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String toString() { //ta metoda zwraca a nie wyswietla
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wiek=" + wiek +
                ", plec=" + plec +
                '}';
    }
}
