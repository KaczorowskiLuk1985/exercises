package com.company.zadania.zadanie97;
/*
Utwórz typ wyliczeniowy `Plec` (zawierająca dwie opcje). Utwórz klasę `Osoba`,
gdzie jednym z pól będzie ten typ.
Utwórz tablicę obiektów klasy `Osoba` oraz metodę która zwróci nam
liczbę pracowników wybranej (przez parametr) płci.
 */

public class Zadanie97 {
    public static void main(String[] args) {
        Osoba [] osoby = new Osoba []{
                new Osoba("Jan", "Nowak", 8, Plec.MEZCZYZNA),
                new Osoba("Janusz","Somsiad",45,Plec.MEZCZYZNA),
                new Osoba("Dżesika","Smith",16,Plec.KOBIETA)
        };
        for (Osoba osoba:osoby){

            System.out.println(osoba);
        }
        System.out.println(Plec.MEZCZYZNA);

    }

}
