package com.company.zadania;
/*
*ZADANIE #63*
Utwórz metodę, która przyjmuje dwuwymiarową tablicę.
Metoda ma zwracać tablicę która będzie zawierała sumy wszystkich kolumn.
 */

import java.util.Arrays;

public class Zadanie63 {
    public static void main(String[] args) {
        int[][] tablica = {
                {1, 2, 3},
                {4, 5, 6},
                {4, 5, 6},
                {4, 5, 6},
                {4, 5, 6},
                {7, 8, 9}
        };
        System.out.println(Arrays.toString(zwocSumeKolumn(tablica)));

    }

    static int[] zwocSumeKolumn(int[][] tablica) {
        int[] sumaKolumn = new int[tablica[0].length];

        for (int kolumna = 0; kolumna < tablica.length; kolumna++) {
            for (int wiersz = 0; wiersz < tablica[0].length; wiersz++) {
                sumaKolumn[wiersz] += tablica[kolumna][wiersz];
            }

        }
        return sumaKolumn;
    }
}
