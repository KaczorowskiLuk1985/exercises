package com.company.zadania.zadanie120;

public class Konto {
    protected double stanKonta;
    private String wlasciciel;
    private int numerKonta;
    private static int idKonta = 1;

    public Konto(String wlasciciel) {
        this.wlasciciel = wlasciciel;
        numerKonta = idKonta++; //czyli +1
    }

    @Override
    public String toString() {
        return String.format("Właścicielem konta (nr ID: %s) jest : %s . Stan konta: %s"
                , numerKonta, wlasciciel, stanKonta);
    }

    double pokazStanKonta() {
        return stanKonta;
    }

    void wplataNaKonto(double wplata) {
        stanKonta += wplata;

    }

    boolean wyplataZKonta(double wyplata) {
        if (stanKonta >= wyplata) {
            stanKonta -= wyplata;
            return true;
        }
        return false;
    }

    static void przelewanieSrodkow(Konto nadawca, Konto odbiorca, double kwotaPrzelania) {
        if (nadawca.wyplataZKonta(kwotaPrzelania)) {
            odbiorca.wplataNaKonto(kwotaPrzelania);
        }
    }

    public int getNumerKonta() {
        return numerKonta;
    }
}

