package com.company.zadania.zadanie120;

public class KontoFirmowe extends Konto {
    private double oplataTransakcyjna;

    public KontoFirmowe(String wlasciciel, double opłataTransakcyjna) {
        super(wlasciciel);
        this.oplataTransakcyjna = opłataTransakcyjna;
    }

    @Override
    boolean wyplataZKonta(double wyplata) {
        double sumaPoOplacie = wyplata + wyplata * oplataTransakcyjna;

        if (stanKonta >= sumaPoOplacie) {
            stanKonta -= sumaPoOplacie;
            return true;
        }
        return false;
    }
}
