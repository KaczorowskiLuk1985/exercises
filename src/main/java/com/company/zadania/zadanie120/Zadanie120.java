package com.company.zadania.zadanie120;

import java.util.Arrays;
import java.util.List;

public class Zadanie120 {
    public static void main(String[] args) {
        Konto konto = new Konto("Janusz");
        Konto konto1 = new Konto("Witek");
        Konto konto2 = new Konto("Halina");
        System.out.println(konto);
        System.out.println(konto1);
        System.out.println(konto2);

        konto.wplataNaKonto(40.0);
        System.out.println(konto);

        boolean result = konto.wyplataZKonta(50);
        if (result) {
            System.out.println("operacja OK");
        } else {
            System.out.println("niepowodzenie");
        }
        System.out.println(konto);

        Konto.przelewanieSrodkow(konto, konto1, 10);
        System.out.println(konto);
        System.out.println(konto1);

        KontoFirmowe kontoFirmowe = new KontoFirmowe("Gienek", 0.2);
        kontoFirmowe.wplataNaKonto(50);
        System.out.println(kontoFirmowe);
        kontoFirmowe.wyplataZKonta(10);
        System.out.println(kontoFirmowe);

        Konto.przelewanieSrodkow(kontoFirmowe, konto, 5);
        System.out.println(kontoFirmowe);


        List<Konto> listaKont = Arrays.asList(
                konto, konto1, konto2, kontoFirmowe
        );
        for (Konto k : listaKont) {
            if (k instanceof KontoFirmowe) { // sprawdza czy przedmiot jest obiektem danej klasy
                System.out.print("konto firmowe - ");
            } else {
                System.out.print("konto - ");
            }
            System.out.println("(" + k.getNumerKonta() + ")" + k.stanKonta);
        }

    }

}
