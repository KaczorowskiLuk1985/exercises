package com.company.zadania;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Zadanie109 {
    private static final int LICZBA_OPR = 100_000;
    private static final int ELEMENT_DO_WSTAWIENIA = 999;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        ArrayList<Integer> arrayList = dodawanieDoListyNaIndexZerowy();
        long stop = System.currentTimeMillis();
        System.out.println("Wypelnienie Array Listy " + (stop - start));

        start = System.currentTimeMillis();
        LinkedList<Integer> linkedList = dodawanieDoLinkedListyNaIndexZerowy();
        stop = System.currentTimeMillis();
        System.out.println("Wypelnienie Linked Listy " + (stop - start));



        start = System.currentTimeMillis();
        kasowanieOdLinkedListyNaIndexZerowy(arrayList);
        stop = System.currentTimeMillis();
        System.out.println("kasowanie spod indexu array Listy " + (stop - start));

        start = System.currentTimeMillis();
        kasowanieOdLinkedListyNaIndexZerowy(linkedList);
        stop = System.currentTimeMillis();
        System.out.println("kasowanie spod indexu Linked Listy " + (stop - start));

    }

    static ArrayList<Integer> dodawanieDoListy() {
        ArrayList<Integer> nowaLista = new ArrayList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            nowaLista.add(ELEMENT_DO_WSTAWIENIA);
        }
        return nowaLista;
    }

    static LinkedList<Integer> dodawanieDoLinkedListy() {
        LinkedList<Integer> nowaLista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            nowaLista.add(ELEMENT_DO_WSTAWIENIA);
        }
        return nowaLista;
    }

    static ArrayList<Integer> dodawanieDoListyNaIndexZerowy() {
        ArrayList<Integer> nowaLista = new ArrayList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            nowaLista.add(0, ELEMENT_DO_WSTAWIENIA);
        }
        return nowaLista;
    }

    static LinkedList<Integer> dodawanieDoLinkedListyNaIndexZerowy() {
        LinkedList<Integer> nowaLista = new LinkedList<>();
        for (int i = 0; i < LICZBA_OPR; i++) {
            nowaLista.add(0, ELEMENT_DO_WSTAWIENIA);
        }
        return nowaLista;
    }


    private static int wybieranieSpodIndexuForeach(List<Integer> nowaLista) {
        int liczbaZlisty = 0;
        for (Integer integer : nowaLista) {
            liczbaZlisty = integer;
        }
        return liczbaZlisty;
    }

    static void kasowanieOdLinkedListyNaIndexZerowy(List<Integer> lista) {
        for (int i = 0; i < lista.size(); i++) {
            lista.remove(i);
        }
    }
}