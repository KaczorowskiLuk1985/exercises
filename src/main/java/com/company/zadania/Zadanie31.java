package com.company.zadania;
/*
*ZADANIE #31*
Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę”
Dla `4` wyświetli:

1
12
123
1234

Dla `6` wyświetli:

1
12
123
1234
12345
123456
 */

public class Zadanie31 {
    public static void main(String[] args) {
        pokazChoinke(4);
        System.out.println();
        pokazChoinke(6);

    }
    static void pokazChoinke (int liczbaWierszy){
        for (int i = 1; i <= liczbaWierszy ; i++) {
            for (int j = 1; j <= i ; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}
