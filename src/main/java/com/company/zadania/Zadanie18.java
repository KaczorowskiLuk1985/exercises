package com.company.zadania;
/*
*ZADANIE #18*
Utwórz metodę, do której przekazujesz liczbę, która jest numerem miesiąca,
a program powinien *zwrócić nazwę* tego dnia.
Wykorzystaj instrukcję warunkową *switch*
> Dla `1` zwróci `styczeń`
>
> Dla `2` zwróci `luty`
>
>`...`
>
> Dla `7` zwróci `lipiec`
 */

public class Zadanie18 {
    public static void main(String[] args) {
//        System.out.println(jakiMiesiac(2));
//        System.out.println(jakiMiesiac(5));
        System.out.println(innyMiesiac(5));
    }

    static String jakiMiesiac(int miesiac) {
        switch (miesiac) {
            case 1:
                return "styczeń";
            case 2:
                return "luty";

            default:
                return "błedny numer miesiąca";
        }

    }

    static String innyMiesiac(int miesiacKolejny) {
        String komunikat = "błędny numer";
        switch (miesiacKolejny) {
            case 3:
                komunikat = "marzec";
                break;
            case 4:
                komunikat = "kwiecień";
                break;
            case 5:
                komunikat = "maj";
                break;
        }
        return komunikat;
    }
}
