package com.company.zadania;
/*
newsy sytuacje kiedy nie należy ufać float i double
 */

public class Zadanie145 {
    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();
        metoda6();

    }

    private static void metoda6() {
        float zmiennaFloat = 0.1f;
        double zmienaDouble = 0.1d;
        System.out.printf("f= %.50f i d= %.50f ",zmiennaFloat,zmienaDouble);
        System.out.print(0.1f == 0.10000000149011612000000000000000000000000000000000);
        //liczby zmienno przecinkowe sa niedokladne

    }

    private static void metoda5() {
        double suma = 0;
        for (int i = 0; i < 100000000; i++) {
            suma += 0.1;
        }
        System.out.println(suma);
    }

    private static void metoda4() {
        for (float i = 10f; i > 0; i -= 0.1f) {
            System.out.println(i);

        }
    }

    private static void metoda3() {

        System.out.println(1d == 0.9999999999999999);
        System.out.println(1f == 0.9999999999999999);
        System.out.println(0.90d == 0.9000001);
        System.out.println(0.90f == 0.9000001);
    }

    private static void metoda2() {
        for (double i = 0; i < 1; i += 0.1d) {
            System.out.printf("%.2f i %s\n", i, i);

        }
    }

    private static void metoda1() {
        //float ma problem z wyswietlaniem 0,7
        for (float i = 0; i < 1; i += 0.1f) {
            System.out.printf(" %.2f i %s\n", i, i);
        }
    }
}
