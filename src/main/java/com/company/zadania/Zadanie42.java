package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
 */

import java.util.Arrays;

public class Zadanie42 {
    public static void main(String[] args) {
        int[] tab = {1, 2, 10, 4, 5, 6, 7};
        System.out.println(Arrays.toString(tab));
        System.out.println(Arrays.toString(tablicaOdwrotna(tab)));


    }

    static int[] tablicaOdwrotna(int[] tablica) {
        int[] nowaTablica = new int[tablica.length];
        int licznik = tablica.length - 1;              // - 1 bo w tablicy numeruje sie od 0
        for (int i = 0; i < tablica.length; i++) {
            nowaTablica[i] = tablica[licznik];
            licznik--;

        }
        return nowaTablica;
    }
}
