package com.company.zadania.zadanie162;

public class Zadanie162 {
    public static void main(String[] args) {
        User u1 = new User.UserBulider()
                .setName("marek")
                .setSurname("nowak")
                .setAge(37)
                .setGender(true)
                .build();

        System.out.println(u1);

        User u2 = new User.UserBulider()
                .setName("anna")
                .setSurname("nowak")
                .setAge(34)
                .setGender(false)
                .build();

        System.out.println(u2);
    }
}
