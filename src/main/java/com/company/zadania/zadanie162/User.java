package com.company.zadania.zadanie162;

public class User {
    private String name;
    private String surname;
    private int age;
    private boolean isMan;

    //przypisuje pola builder do User
    private User(UserBulider userBulider) {
        name = userBulider.name;
        surname = userBulider.surname;
        age = userBulider.age;
        isMan = userBulider.isMan;

    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age + '\'' +
                ", isMan=" + (isMan ? "tak" : "nie") +
                '}';
    }

    //moge utworzyc obiekt klasy wewnetrznej bez tworzeia klasy wew
    static class UserBulider { //te wartosi sa przypisywane urzytkownikowi
        private String name = "brak";
        private String surname = "brak";
        private int age;
        private boolean isMan;


        UserBulider setName(String name) {
            this.name = name; // zwraca obekt na ktorym pracuje
            return this;
        }

        UserBulider setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        UserBulider setAge(int age) {
            this.age = age;
            return this;
        }

        UserBulider setGender(boolean isMan) {
            this.isMan = isMan;
            return this;
        }


        User build() {
            return new User(this);
        }
    }
}