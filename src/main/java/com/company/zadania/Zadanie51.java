package com.company.zadania;

import java.util.Arrays;

/*
Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice są tych samych długości.
- Wartości na danej pozycji mogą być większę niż 10
 */
public class Zadanie51 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 2, 3, 4, 5};
        int[] tablica2 = new int[]{5, 4, 3, 2, 1};
        System.out.println(Arrays.toString(zwrocSumeParametrowDwochTablic(tablica1,tablica2)));


    }

    static int[] zwrocSumeParametrowDwochTablic(int[] tablica1, int[] tablica2) {
        int[] tablicaZSumami = new int[tablica1.length];
        for (int index = 0; index < tablica1.length; index++) {
            tablicaZSumami[index] = tablica1[index] + tablica2[index];


        }
        return tablicaZSumami;
    }

}
