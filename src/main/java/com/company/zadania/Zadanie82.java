package com.company.zadania;
/*
Utwórz metodę, która przyjmuje ciąg znaków (gdzie wyrazy rozdzielone są znakiem spacjami) a następnie zwraca najdłuższy z nich
> Dla `"Ala ma kota."`, zwróci `"kota"` (gdyż jest najdłuższym wyrazem)
 */

public class Zadanie82 {
    public static void main(String[] args) {
        System.out.println(zwrocNajdluzszyNapis("ala ma kota"));
        System.out.println(zwrocNajdluzszyNapis2("ala ma kota"));

    }

    static String zwrocNajdluzszyNapis(String napis) {

        String[] tablicaNapisów = napis.split(" ");
        String resoult = "";
        int max = 0;

        for (String wyraz : tablicaNapisów) {
            if (wyraz.length() > max) {
                max = wyraz.length();
                resoult = wyraz;
            }
        }

        return resoult;
    }

    static String zwrocNajdluzszyNapis2(String napis) {

        String[] tablicaNapisów = napis.split(" ");
        String resoult = "";

        for (String wyraz : tablicaNapisów) {
            if (wyraz.length() > resoult.length()) {
                resoult = wyraz;
            }
        }

        return resoult;
    }
}

