package com.company.zadania;
/*
*ZADANIE #17*
Utwórz metodę, do której przekazujesz wartość logiczną, a ona *wyświetli* napis `Tak`, w przypadku przekazania `true` oraz `Nie` w przypadku przekazania `false`
 */

public class Zadanie17 {
    public static void main(String[] args) {
        watroscLogiczna();

    }
    static void watroscLogiczna(){
        int i = 21;
        System.out.println(i>20 ? "tak" : "nie");

        String komunikat = i > 20 ? "tak" : "nie";
        System.out.println(komunikat);
    }
}
