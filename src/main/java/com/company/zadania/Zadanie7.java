package com.company.zadania;
/*
Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci ich sumę
> Dla `2` oraz `11` zwróci `13`
>
> Dla `-10` oraz `3` zwróci `-7`
 */
public class Zadanie7 {
    public static void main(String[] args) {
        int wynik = dwaParametry(2,9);
        System.out.println(2*3);

        int wynik2 = dwaParametry(7, 9);
        System.out.println("wynik: " + wynik2);

        System.out.println("wynik: " + dwaParametry(5, 9));
    }


    private static int dwaParametry(int liczbaDruga, int liczbaPierwsza) {
        return liczbaDruga + liczbaPierwsza;

    }
}
