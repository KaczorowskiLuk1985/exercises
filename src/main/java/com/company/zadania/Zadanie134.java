package com.company.zadania;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/*Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie wyświetla liczbę linii oraz liczbę znaków w pliku*/
public class Zadanie134 {
    public static void main(String[] args) {
        try {
            countLinesAndChars("files/zadanie133.txt");
        } catch (IOException e) {
            System.out.println("Błąd odczytu pliku");
        }
    }
    static void countLinesAndChars (String filePath) throws IOException {
        BufferedReader buffer = new BufferedReader(new FileReader(filePath));
        String line = buffer.readLine();
        int lineCount = 0;
        int charCount = 0;
        while(line != null){
            lineCount++;
            charCount += line.length();
            line = buffer.readLine();
        }
        System.out.printf("W pliku jest %s linii i %s znaków",lineCount,charCount);
    }
}
