package com.company.zadania.zadanie119;

public class Wykladowca extends Osoba {

    private TyułNaukowy tyułNaukowy;


    Wykladowca(String imie, int wiek, Plec plec, TyułNaukowy tyuł) {

        super(imie, wiek, plec);
        tyułNaukowy = tyuł;
    }

    enum TyułNaukowy {
        DR, MGR, INZ
    }
}
