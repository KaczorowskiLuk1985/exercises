package com.company.zadania.zadanie119;

public class Osoba {
    private String imie;
    private int wiek;
    private Plec plec;

    Osoba(String imie, int wiek, Plec plec) {
        this.imie = imie;
        this.wiek = wiek;
        this.plec = plec;
    }
    @Override
    public String toString() {
        return String.format("imię: %s, wiek %s, pleć: %s",imie,wiek,plec);
    }

    enum Plec {
        MEZCZYZNA, KOBIETA
    }
}
