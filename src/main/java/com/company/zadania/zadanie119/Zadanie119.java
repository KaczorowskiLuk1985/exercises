package com.company.zadania.zadanie119;
/*
*Utwórz klasę `Osoba` z polami:*
- `imie` [String]
- `wiek` [int]
- `plec` (jako typ wyliczeniowy).

*Utwórz klasę `Student` (dziedziczący po klasie `Osoba`) z polami*
- `uczelnia` (obiekt klasy `Academy`)
- `rokStudiow` [int]
- mapę w postaci `PrzedmiotUczelniany : Integer` (reprezentujące ocenę z danego przedmiotu)

*Utwórz klasę `PrzedmiotUczelniany` z polami*
- `nazwa` [String]
- `wykladowca` (obiekt klasy `Wykladowca`)

*Utwórz klasę `Wykladowca` z polami:*
- lista zawierając obiekty klasy `Academy`).
- tytuł naukowy (typ wyliczeniowy)

*Utwórz klasę `Academy` z polami*
-`nazwa` [String]
- `adres` [String]
- lista studentów (lista obiektów klasy `Student`)
- lista wykładowców (lista obiektów klasy `Wykladowca`) (edited)
 */

import static com.company.zadania.zadanie119.Osoba.Plec.KOBIETA;
import static com.company.zadania.zadanie119.Osoba.Plec.MEZCZYZNA;
import static com.company.zadania.zadanie119.Wykladowca.TyułNaukowy.DR;

public class Zadanie119 {
    public static void main(String[] args) {

        Wykladowca wykladowca = new Wykladowca("Janusz",68,MEZCZYZNA, DR);
        System.out.println(wykladowca);

        PrzedmiotUczelniany matematyka = new PrzedmiotUczelniany("matematyka",wykladowca);
        PrzedmiotUczelniany fizyka = new PrzedmiotUczelniany("fizyka",wykladowca);

        Student student = new Student("Brajanek",20,MEZCZYZNA,2018);
        Student student2 = new Student("Dzesika",20,KOBIETA,2018);
        student.dodajOcene(matematyka,5);
        student.dodajOcene(fizyka,4);
        student2.dodajOcene(matematyka,3);
        student2.dodajOcene(fizyka,3);

        Uczelnia uczelnia = new Uczelnia("Politechnika Łódzka ", "Limanka");
        uczelnia.dodajWykladowce(wykladowca);
        uczelnia.dodajStudenta(student);
        uczelnia.dodajStudenta(student2
        );
        uczelnia.wyswietlStudentow();

    }


}

