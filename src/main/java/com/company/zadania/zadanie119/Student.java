package com.company.zadania.zadanie119;

import java.util.HashMap;
import java.util.Map;

public class Student extends Osoba {
    private Uczelnia uczelnia;
    private int rokStudiow;
    private Map<PrzedmiotUczelniany, Integer> oceny = new HashMap<>();

    public Student(String imie, int wiek, Plec plec, int rokStudiow) {
        super(imie, wiek, plec);
        this.rokStudiow = rokStudiow;

    }
    void dodajOcene(PrzedmiotUczelniany przedmiot, Integer ocena){
        oceny.put(przedmiot,ocena);
    }

    @Override
    public String toString() {
        return super.toString()+ " a na roku " + rokStudiow + "ma oceny "+ oceny;
    }
}
