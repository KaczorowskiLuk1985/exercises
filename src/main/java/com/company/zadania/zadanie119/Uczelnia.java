package com.company.zadania.zadanie119;

import java.util.ArrayList;
import java.util.List;

public class Uczelnia {
    private String nazwa;
    private String adres;
    private List<Student> listaStudentow = new ArrayList<>();
    private List<Wykladowca> listaWykladowcow = new ArrayList<>();

    public Uczelnia(String nazwa, String adres) {
        this.nazwa = nazwa;
        this.adres = adres;
    }

    void dodajStudenta(Student student) {
        listaStudentow.add(student);
    }

    void dodajWykladowce(Wykladowca wykladowca) {
        listaWykladowcow.add(wykladowca);
    }

    void wyswietlStudentow() {
        System.out.println("na uczelni " + nazwa + "przy ul. " + adres + "sudiują : ");
        for (Student student : listaStudentow) {
            System.out.println(student);
        }
    }
}
