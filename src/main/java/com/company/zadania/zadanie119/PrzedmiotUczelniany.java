package com.company.zadania.zadanie119;

public class PrzedmiotUczelniany {
    private String uczelnia;
    private Wykladowca wykladowca;

    public PrzedmiotUczelniany(String uczelnia, Wykladowca wykladowca) {
        this.uczelnia = uczelnia;
        this.wykladowca = wykladowca;
    }

    @Override
    public String toString() {
        return uczelnia;
    }
}
