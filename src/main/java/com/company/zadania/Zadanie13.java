package com.company.zadania;
/*
Utwórz metodę, do której przekazujesz trzy parametry a ona *zwróci* największy z nich z nich.
Czyli dla

`1, 2, 3` zwróci `3`,

a dla `3, 23, 10` zwróci `23`
 */

public class Zadanie13 {
    public static void main(String[] args) {
        System.out.println(zwracaNajwieksza(28, 8, 5));
        System.out.println(zwracaNajwieksza(27, 55, 5));
        System.out.println(zwracaNajwieksza(28, 28, 3));
    }

    static int zwracaNajwieksza(int b, int c, int d) {
        if (b >= c && b >= d) {
            return b;
        } else if (c >= b && c >= d) {
            return c;
        } else {
            return d;
        }
    }
}
