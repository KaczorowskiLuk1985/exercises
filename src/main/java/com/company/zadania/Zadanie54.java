package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca średnią wartość elementów z pominięciem największego.
dla `[1, 2, 3, 10, 4, 5, 6, 11]`
zwróci `31` (ponieważ pomija `11`)
 */

public class Zadanie54 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 10, 4, 5, 6, 11};
        System.out.println(zwrocSumeElementow(tab));

    }

    static double zwrocSumeElementow(int[] tablica) {
        int max = wartoscNajwieksza(tablica); //musi wiedziec w jakim zbiorze szukac
        double liczbaElementow = 0.0;
        int sumaElementow = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != max) {
                liczbaElementow++;
                sumaElementow += tablica[i];

            }
        }
        return sumaElementow / liczbaElementow;
    }

    static int wartoscNajwieksza(int[] tablica) {
        int max = tablica[0];
        for (int element : tablica) {
            if (element > max) {
                max = element;
            }
//        }
//        for (int i = 0; i < ; i++) {             to jest ten sam zapis co petla fori z tym ze wolniej liczy
//            int element = tablica[i];
//            if (element > max){
//                max = element;
//            }

        }
        return max;

    }

}
