package com.company.zadania;
/*
*ZADANIE #126 - PRACA DOMOWA*
Utwórz metodę która przyjmuje tablicę, a następnie przy wykorzystaniu
`Scannera` użytkownik może podać indeks parametru.
W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek)
 zapytaj użytkownika o indeks ponownie.
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class Zadanie126 {
    public static void main(String[] args) {

        avoidException();


    }
    private static void avoidException(){
        int[] array = new int[]{1,13,56,889,22};
        Scanner s = new Scanner(System.in);

        while (true) {
            System.out.print("Podaj proszę indeks: ");
            try {
                int indeks = s.nextInt();
                System.out.printf(
                        "Na pozycji %s jest liczba %s\n",
                        indeks, array[indeks]);
                break;

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Podano zły indeks");
                // shows exception details
                e.printStackTrace();
            } catch (InputMismatchException e) {
                System.out.println("Nie podano liczby");
                // to clear Scanner
                s.nextLine();
            }
        }
    }
}