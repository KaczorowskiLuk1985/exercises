package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić *ile elementów* (idąc po kolei od lewej) należy zsumować by przekroczyć podany (jako drugi) parametr
dla `([1,2,3,4,5,6],  9)`
należy zwrócić 2
 */

public class Zadanie56 {
    public static void main(String[] args) {
        int[]tablica = new int[]{1,2,3,4};
        System.out.println(iloscElementow(tablica, 9));

    }
    static int iloscElementow(int [] tablica, int minimalnaSuma){
        int suma = 0;
        for (int index = tablica.length-1; index >= 0; index--) {
            suma += tablica[index];
            if (suma >= minimalnaSuma){

                return tablica.length - index;
            }

        }
        return suma;
    }

}
