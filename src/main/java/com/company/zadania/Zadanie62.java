package com.company.zadania;

import java.util.Random;

public class Zadanie62 {
    public static void main(String[] args) {
        int [][] tablicaZliczbami = zwrocTablcie(3,4);
        wyswiatlTablcie(tablicaZliczbami);


    }
    static int [] [] zwrocTablcie(int dlugosc, int szerokosc){
        int[][] nowaTablica = new int[dlugosc][szerokosc];
        Random random = new Random();
        for (int i = 0; i <dlugosc ; i++) {
            for (int j = 0; j < szerokosc; j++) {
                nowaTablica[i][j] = random.nextInt(100)-50;

            }

        }
        return nowaTablica;
    }
    static void wyswiatlTablcie(int [][]tablica){

        //jesli sa roznej dlugosci to uzywamy i zamiast 0

        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j <tablica[0].length ; j++) {
                System.out.print(tablica[i][j]+"\t");

            }
            System.out.println();

        }
    }

}
