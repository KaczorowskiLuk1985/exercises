package com.company.zadania;
/*
*ZADANIE #26*
Utwórz metodę, do której przekazujesz jeden parametr i *zwraca* sumę wszystkich elementów od `1` do podanej liczby
> dla `3` zwróci `1 + 2 + 3 = 6`
>
> dla `5` zwróci `15` bo `1 + 2 + 3 + 4 + 5 = 15`
>
> dla `11` zwróci `66` bo `1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 = 66`
 */

public class Zadanie26 {
    public static void main(String[] args) {
        int wynik = sumaElementow(11);
        System.out.println(wynik);


    }

    static int sumaElementow(int parametr) {
        int suma = 0;
        for (int x = 1; x <= parametr; x++) {
            suma += x;
        }
        return suma;
    }
}
