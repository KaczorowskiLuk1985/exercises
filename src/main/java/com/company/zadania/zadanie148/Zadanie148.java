package com.company.zadania.zadanie148;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić *ile elementów* (idąc po kolei od lewej) należy zsumować by przekroczyć podany (jako drugi) parametr
> dla `([1,2,3,4,5,6],  9)`
> należy zwrócić `4`
 */

public class Zadanie148 {
    public Integer ileElementówZsumowac(int[] tablica, int liczba) {
        if(tablica==null){
            return null;
        }
        int suma = 0;
        int count = 0;
        for (int i : tablica) {
            suma += i;
            count++;

            if (suma > liczba) {
                return count;
            }
        }
        return null;
    }
}

