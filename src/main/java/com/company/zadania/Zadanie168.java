package com.company.zadania;

import com.company.zadania.zadanie158.Person;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Scanner;
import java.util.Set;

/*
data czas
 */
public class Zadanie168 {
    public static void main(String[] args) {
        /**LocalTime*/
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
        /**LocalDate*/
//        method6();
//        method7();
//        method8();
        method9();
//        method10();
        /**LOCAL DATE TIME */
//        method11();
//        method12();
        /**ZONED DATE TIME*/
//        method13();
//        method14();
//        method15();
        /**PERIOD & DURATION */
//        method16();
//        method17();
//        method18();
    }

    private static void method18() {
        String firstDateTime = "10.05.2018 13:20";
        String secondDateTime = "01.12.2018 17:00";

        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        LocalDateTime first = LocalDateTime.parse(firstDateTime, formatter);
        LocalDateTime second = LocalDateTime.parse(secondDateTime, formatter);

        long months = ChronoUnit.MONTHS.between(first, second);
        long weeks = ChronoUnit.WEEKS.between(first, second);
        long days = ChronoUnit.DAYS.between(first, second);
        long hours = ChronoUnit.HOURS.between(first, second);
        long minutes = ChronoUnit.MINUTES.between(first, second);
        long seconds = ChronoUnit.SECONDS.between(first,second);
        long nanos = ChronoUnit.NANOS.between(first,second);

        System.out.println(months + " miesiecy");
        System.out.println(weeks + " tygodni");
        System.out.println(days + " dni");
        System.out.println(hours + " godzin");
        System.out.println(minutes + " minut");
        System.out.println(seconds + " sekund");
        System.out.println(nanos + " nanosekund ");

    }


    private static void method17() {
        String firstDate = "10.05.2018";
        String secondDate = "23.12.2018";

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate firstLocalDAte = LocalDate.parse(firstDate, dateTimeFormatter);
        LocalDate secondLocalDate = LocalDate.parse(secondDate, dateTimeFormatter);

        Period period = Period.between(firstLocalDAte, secondLocalDate);

        System.out.println("pierwsza data : " + firstDate + " druga data : " + secondDate + "\n różnica pomiędzy nimi :");
        System.out.println(period.getDays() + " dni");
        System.out.println(period.getMonths() + " miesięcy");
        System.out.println(period.getYears() + " lat");
    }

    private static void method16() {
        String firstTime = "11:10";
        String secondTime = "16:20";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        LocalTime firstLocalTime = LocalTime.parse(firstTime, formatter);
        LocalTime secondLocalTime = LocalTime.parse(secondTime, formatter);

        Duration duration = Duration.between(firstLocalTime, secondLocalTime);

        System.out.println("godziny pomiędzy czasami : " + duration.toHours());
        System.out.println("minuty pomiedzy czasami : " + duration.toMinutes());
        System.out.println("sekundy pomiedzy czasami : " + duration.getSeconds());
        System.out.println("nanosekundyy pomiedzy czasami : " + duration.toNanos());
    }

    private static void method15() {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        availableZoneIds
                .stream()
                .sorted()
                .forEach(System.out::println); //jezeli jest metoda jednoparametrowa mozemy uzyc lambda
    }

    private static void method14() {

        LocalDateTime inputLocalDateTime = LocalDateTime.now();
        ZoneId inputZone = ZoneId.of("Europe/Warsaw");

        ZoneId outPutZone = ZoneId.of("US/Hawaii");

        LocalDateTime outputLocalDateTime =
                inputLocalDateTime
                        .atZone(inputZone)
                        .withZoneSameInstant(outPutZone)
                        .toLocalDateTime();

        System.out.println(inputLocalDateTime);
        System.out.println(outputLocalDateTime);
    }

    private static void method13() {
        LocalDateTime inputDateTime = LocalDateTime.now();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(inputDateTime, ZoneOffset.UTC);

        ZoneId zoneId = ZoneId.of("Europe/Warsaw");

        ZonedDateTime outputZoneDateTime = zonedDateTime.withZoneSameInstant(zoneId);

        LocalDateTime dateTime = outputZoneDateTime.toLocalDateTime();

        System.out.println(inputDateTime);
        System.out.println(dateTime);
    }

    private static void method12() {
        String inputDate = "16.07.2018 11:02";

        DateTimeFormatter inputFormat =
                DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        LocalDateTime dateTime = LocalDateTime.parse(inputDate, inputFormat);

        System.out.println(dateTime);

        DateTimeFormatter outputFormat =
                DateTimeFormatter.ofPattern("dd MMMM yyy (D 'dzień roku'), HH:mm");
        System.out.println(dateTime.format(outputFormat));
    }

    private static void method11() {
        LocalDateTime dateTime =
                LocalDateTime.of(2018, Month.DECEMBER, 1, 14, 39, 05);
        System.out.println(dateTime.toString());


        DayOfWeek dayOfWeek = dateTime.getDayOfWeek();
        System.out.println(dayOfWeek);
        System.out.println(dayOfWeek.getValue());

        DayOfWeek plus = dayOfWeek.plus(1);
        System.out.println(plus + " = " + plus.getValue());

        Month month = dateTime.getMonth();
        System.out.println(month + " = " + month.getValue());

        int minuteOfDay = dateTime.get(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay);

        long nanoOfDay = dateTime.getLong(ChronoField.NANO_OF_DAY);
        System.out.println(nanoOfDay);
    }

    private static void method10() {
        String inputDate = "4/8/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/M/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormat);

        System.out.println("ISO_DATE ->\t\t\t" + date.format(DateTimeFormatter.ISO_DATE));
        System.out.println("BASIC_ISO_DATE ->\t" + date.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println("ISO_ORDINAR_DATE ->\t" + date.format(DateTimeFormatter.ISO_ORDINAL_DATE));
        System.out.println("ISO_WEEK_DATE ->\t" + date.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println("ISO_LOCAL_DATE ->\t" + date.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private static void method9() {
        String inputDate = "01/12/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        LocalDate date = LocalDate.parse(inputDate, inputFormat);

        int dayOfYear = date.get(ChronoField.DAY_OF_YEAR);
        System.out.println(dayOfYear);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("MM");
        System.out.println(date.format(outputFormat));

        System.out.println(date.getDayOfYear());
    }

    private static void method8() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj wzorzec : ");
        String pattern = scanner.nextLine();

        try { // zaznaczamy "obiekty" ktore chcemy wziasc w tray catch i ctrl + alt + t
            LocalDate date = LocalDate.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            System.out.println("wynik: " + date.format(formatter));
        } catch (UnsupportedTemporalTypeException e) {
            System.out.println("zły format");
        } catch (IllegalArgumentException e) {
            System.out.println("zła literka");
        }

    }

    private static void method7() {
        LocalDate date = LocalDate.of(2018, 12, 1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        String outputDate = date.format(formatter); // dalej uprościliśmy kod
        System.out.println(outputDate);

        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("yyy");
        //yy=18
        //yyy lub yyyy = caly rokd
        System.out.println(date.format(formatter2));

        DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("MMM");
        //MM = 12
        //MMM= gru
        //MMMM= grudnia
        System.out.println(date.format(formatter3));

        DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("LLLL");
        // LL = 12 (miesiac cyfrowo)
        // LLL = gru
        // LLLL = grudzień (nie odmieniony
        System.out.println(date.format(formatter4));

        DateTimeFormatter formatter5 = DateTimeFormatter.ofPattern("EEEE");
        // E = sob
        // EEEE = Sobota ( dzien w pełnej formie)
        System.out.println(date.format(formatter5));

        DateTimeFormatter formatter6 = DateTimeFormatter.ofPattern("d MMMM yyyy (EEEE)");
        System.out.println(date.format(formatter6));

    }

    private static void method6() {
        LocalDate date = LocalDate.of(2018, Month.DECEMBER, 1);

        System.out.println(date.toString());
    }

    private static void method5() {
        String inputTime = "13:03:12";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime time = LocalTime.parse(inputTime, inputFormat);

        DateTimeFormatter outPutFormat = DateTimeFormatter.ofPattern("HH_mm_ss");

        String outPuTime = time.format(outPutFormat);

        System.out.println(outPuTime);
    }

    private static void method4() {
        String currentTime = "12 + 51--28";

        DateTimeFormatter formt = DateTimeFormatter.ofPattern("HH + mm--ss");
        LocalTime time = LocalTime.parse(currentTime, formt);
        System.out.println(time);//parse - przetworz

        int hour = time.get(ChronoField.HOUR_OF_DAY);
        int minute = time.get(ChronoField.MINUTE_OF_HOUR);

        System.out.printf("mamay %s min. po godzinie %s-ej\n ",
                minute,
                hour);
    }

    private static void method3() {
        LocalTime t1 = LocalTime.now();
        System.out.println(t1);
        System.out.println(t1.getNano());
    }

    private static void method2() {
        LocalTime t1 = LocalTime.parse("12:43:25");

        System.out.println(t1.getSecond()); //24
        System.out.println(t1.getNano()); //0
        System.out.println(t1.getHour()); //12
    }

    private static void method1() {
        LocalTime t1 = LocalTime.of(12, 36, 3, 100);
        System.out.println(t1.getHour());
        System.out.printf("godzina %s, minuta %s",
                t1.getHour(),
                t1.getMinute(),
                t1.getNano());
        System.out.println(t1);
    }
}
