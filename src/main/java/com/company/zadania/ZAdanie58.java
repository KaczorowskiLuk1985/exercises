package com.company.zadania;
/*
*ZADANIE #58*
Utwórz metodę, która wykorzystuje *varargs* by przekazać do metody dowolną, większą od zera,
liczbę elementów typu `String` i zwrócić jeden napis sklejony z nich.
 */

public class ZAdanie58 {
    public static void main(String[] args) {
        System.out.println(sklejNapisy("Ala", "ma", "kota"));
        System.out.println(sklejNapisy("Ala"));

    }

    static String sklejNapisy(String czytajTo, String... przekazaneWyrazy) {
        String wszystkieWyrazy = czytajTo + " ";
        for (String wyraz : przekazaneWyrazy) {
            wszystkieWyrazy += wyraz + " ";


        }
        return wszystkieWyrazy;
    }
}
