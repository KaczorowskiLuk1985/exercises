package com.company.zadania;
/*
*ZADANIE #21*
Utwórz metodę, w której pętlą *wyświetlisz* wszystkie liczby od liczby przekazanej jako parametr do `0` (tj. w kolejności malejącej).
> Dla `9` wyświetli `9, 8, 7, 6, 5, 4, 3, 2, 1, 0`
>
> Dla `4` wyświetli `4, 3, 2, 1, 0`
 */

public class Zadanie21 {
    public static void main(String[] args) {
        wyswietlMniejsze(0);

    }

    static void wyswietlMniejsze(int stop) {
        for (int i = stop; i >= 10; i++) {
            System.out.println(i + ",");
        }
    }
}
