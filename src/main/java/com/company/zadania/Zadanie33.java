package com.company.zadania;
/*
*ZADANIE #33*
Utwórz metodę, która wyświetli prostokąt o podanych wymiarach (użytkownik podaje jego wymiary jako parametry)
XXXXXXXXXX
X        X
X        X
XXXXXXXXXX
 */

public class Zadanie33 {
    static String znak = "x";
    private static String przerwa = "  ";

    public static void main(String[] args) {
        pokaKwadrat(4);

    }

    static void pokaKwadrat(int rozmiar) {
        pokaWiersz(rozmiar);

        for (int j = 0; j < rozmiar - 2; j++) {
            System.out.print(znak);
            for (int x = 0; x < rozmiar - 2 ; x++) {
                System.out.println(" ");
            }
            System.out.println(znak);
        }

        pokaWiersz(rozmiar);

    }

    static void pokaWiersz(int rozmiar) {
        for (int i = 0; i < rozmiar; i++) {
            System.out.println(znak);

        }
        System.out.println();
    }

}
