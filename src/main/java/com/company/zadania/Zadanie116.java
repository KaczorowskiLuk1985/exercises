package com.company.zadania;
/*
Utwórz metodę, która przyjmuje jeden parametr - maksymalną liczbę. Metoda powinna zwrócić mapę,
gdzie kluczami będą kolejne liczby od `1` do podanej liczby (jako parametr).
Wartościami tej mapy będzie informacja, przez jakie liczby jest ona podzielna.
10 -> 1, 2, 5, 10
11 -> 1, 11
12 -> 1, 2, 3, 4, 6, 12
13 -> 1, 13
14 -> 1, 2, 7, 14
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Zadanie116 {
    public static void main(String[] args) {
        Map<Integer, Set<Integer>> mapka = mapaZDzielnikamiKlucza(14);
        ladnieWyswietla(mapka);

    }


    private static Map<Integer, Set<Integer>> mapaZDzielnikamiKlucza(int max){
        Map<Integer,Set<Integer>> mapka = new HashMap<>();

        for (int i = 1; i <= max; i++) {
            Set<Integer>secik = secikDzielnikow(i);
            mapka.put(i,secik);
        }
        return mapka;
    }
    private static Set<Integer> secikDzielnikow(int liczbaDoSprawdzenia){
        Set<Integer> secik = new HashSet<>();

        for (int dzielnik = 1; dzielnik <= liczbaDoSprawdzenia; dzielnik++) {
            if (liczbaDoSprawdzenia % dzielnik == 0){
                secik.add(dzielnik);
            }
        }
        return secik;
    }
    private static void ladnieWyswietla(Map<Integer, Set<Integer>>mapka){
        for (Map.Entry<Integer, Set<Integer>> entry : mapka.entrySet()) {
            System.out.print(entry.getKey()+" -> ");
            for (Integer integer : entry.getValue()) {
                System.out.print(integer+", ");

            }
            System.out.println();
        }
    }

}
