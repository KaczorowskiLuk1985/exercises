package com.company.zadania.zadanie77;
/*
Utwórz klasę `Person` (`Osoba`) posiadającą
*pola reprezentujące:*
>* imie
>* nazwisko
>* wiek
>* czyMezczyzna (lub czyKobieta)

*konstruktory oraz metody służące do:*
>* przedstawienia się (`Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
>* sprawdzenia czy osoba jest pełnoletnia
 */

public class Person { //konstruktor w klasie zawsze nazywa sie tak samo
    private String imie;
    private String nazwisko;
    private int wiek;
    private Boolean czyMezczyzna;

    Person(String imie, String nazwisko) { //konstruktor, można nic nie wpisywać
        this.imie = imie; // obiekt w tej klasie .Dobra praktyka
        this.nazwisko = nazwisko; //konstruktor tworzy obiekt w klasie

    }

    // usunelismy public bo jest nie potrzebny
    Person(String imie, String nazwisko, int wiek, Boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    boolean czyPełnolenia() {
        return wiek < 18 ? false : true;
        //return wiek >18;   <---prawidłowa praktyka
    }

    void przedstawSie() {
        System.out.printf("Witaj, jestem %s %s.", imie, nazwisko);
        //metoda wyświetlania arrg
    }

    @Override
    public String toString() {
        if (wiek == 0 || czyMezczyzna == null) {
            return String.format("%s, %s ",
                    imie,
                    nazwisko);
        } else {
            return String.format("%s %s (%s l.) - %s.",
                    imie,
                    nazwisko,
                    wiek,
                    czyMezczyzna ? "mężczyzna" : "kobieta");
        }

    }

    public String getImie() {
        return imie;
    }
}
