package com.company.zadania.zadanie77;

public class Zadanie77 {
    public static void main(String[] args) {
        Person o1 = new Person("Jamusz", "Nosacz");
        System.out.println(o1);
        Person o2 = new Person("Jan", "Kowalski", 17, true);
        System.out.println(o2);
        o2.przedstawSie();

        if(o2.czyPełnolenia()){
            System.out.println("osoba jest pełnoletnia");

        }else{
            System.out.println("osoba niepełnoletnia");
        }
    }

}
