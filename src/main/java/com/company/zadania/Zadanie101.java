package com.company.zadania;
/*
Utwórz metodę, która wczytuje liczby od użytkownika do
momentu podania `-1` a następnie zwraca ich listę (np. wykorzystując implementację `ArrayList`)
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie101 {
    private static  final int KONIEC = -1;

    public static void main(String[] args) {
        List<Integer> lista = zwrocListe();
        System.out.println(lista);
        System.out.println(średnia(lista));

    }
    static List<Integer> zwrocListe(){
        List<Integer> nowaLista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        while (true){
            int liczba;
            System.out.print("podaj liczbe :");
            liczba = scanner.nextInt();
            if (liczba==KONIEC){

                break;
            }
            nowaLista.add(liczba);
        }
        return nowaLista;
    }
    static double średnia (List<Integer> lista){
        double suma = 0;
        for (Integer elementy : lista) {
            suma+=elementy;

        }
        return suma/lista.size();
    }
}
