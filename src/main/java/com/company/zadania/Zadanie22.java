package com.company.zadania;
/*
*ZADANIE #23*
Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14` (krańcowy zakres przedziału ma być drugim parametrem metody)
> Dla `0, 4` wyświetli `0, 2, 4`
>
> Dla `6, 14` wyświetli `6, 8, 10, 12, 14`
 */

public class Zadanie22 {
    public static void main(String[] args) {
        liczbyZPrzedzialu();
        System.out.println();
        liczbyZPrzedzialuDoWhile();
        System.out.println();
        liczbyZPrzedzialuFor();
        System.out.println();

    }

    static void liczbyZPrzedzialu() {
        int i = 10;
        while (i < 21) {
            System.out.print(i + ",");
            i++;


        }

    }

    static void liczbyZPrzedzialuDoWhile() {
        int i = 10;
        do {
            System.out.print(i + ",");
            i++;

        } while (i <= 20);
    }
    static void liczbyZPrzedzialuFor(){
        for(int liczba = 10; liczba < 21; liczba++){
            System.out.print(liczba + ",");
        }
    }


}
