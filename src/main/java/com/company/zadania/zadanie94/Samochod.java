package com.company.zadania.zadanie94;

public class Samochod {

    private String nazwa;
    private int rocznik;
    private Kolor kolor;

    Samochod(String nazwa, int rocznik, Kolor kolor) {
        this.nazwa = nazwa;
        this.rocznik = rocznik;
        this.kolor = kolor;
    }


    @Override
    public String toString() {
        return String.format("%s (%s) Kolor: %s", nazwa, rocznik, kolor);
    }
}
