package com.company.zadania;
/*
Sprawdź czy wyraz jest palindromem przy wykorzystaniu *rekurencji*.
 */
public class Zadanie144 {
    public static void main(String[] args) {
        System.out.println(czyJestPalidromem("Kajak"));
        System.out.println(czyJest("Kajak"));
        System.out.println(czyJestPalidromem("turbo"));

    }

    private static boolean czyJest(String wyraz) {
        return czyJestPalidromem(wyraz.toLowerCase());
    }

    private static boolean czyJestPalidromem(String wyraz) { //tutaj nie robimy toLowerCase() bo przelicza każdorazowo gy jest uruchamiana metoda

        if (wyraz.length() == 0 || wyraz.length() == 1) {
            return true;
        } else if (wyraz.charAt(0) == wyraz.charAt(wyraz.length() - 1)) {
            return czyJestPalidromem(wyraz.substring(1, wyraz.length() - 1));

        } else {
            return false;
        }
    }
}
