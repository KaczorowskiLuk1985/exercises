package com.company.zadania;
/*
*ZADANIE #127*
Utwórz metodę która przyjmuje niepustą liste lub tablicę a następnie
w *nieskończonej* pętli je wyświetla (zaczynając od pozycji `0`).
Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru).
 */

public class Zadanie127 {
    public static void main(String[] args) {
        int[] tablica = {5, 6, 7, 5, 8, 6, 5, 4, 5};
        wyświetlaTablice(tablica);
    }

    static void wyświetlaTablice(int[] tablica) {
        int indeks = 0;

        while (true) {
            try {
                System.out.print(tablica[indeks++] + " ");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.printf("Osiągnięto koniec tablicy, na indeksie %s", indeks);
                break;
            }
        }

    }

}

