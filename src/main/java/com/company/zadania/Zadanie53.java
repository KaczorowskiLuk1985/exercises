package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca różnicę pomiędzy największym a najmniejszym elementem.
 */

public class Zadanie53 {
    public static void main(String[] args) {
        int [] tablicaX = new int[]{1,2,3,4,5,6,7};
        System.out.println(zwrocRoznice(tablicaX));


    }

    static int zwrocRoznice(int[] tablica) {
        int min = tablica[0];
        int max = tablica[0];

        for (int index = 0; index < tablica.length; index++) {
            if (tablica[index] > max){
                max = tablica[index];
            }
            if (tablica[index] < min){
                min = tablica[index];
            }
        }

        int wynik = max - min;
        return wynik;
    }
}
