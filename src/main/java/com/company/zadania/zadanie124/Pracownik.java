package com.company.zadania.zadanie124;

/*
Utwórz klasy `Osoba` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik`
(dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`,
 `wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */
public class Pracownik extends Osoba {
    private Double pensja;

    public Pracownik(String imie, String nazwisko, int wiek, Plec plec) {

        super(imie, nazwisko, wiek, plec);
    }

    @Override
    public String zwrocPelneDane() {
        if (pensja != null) {

            return super.zwrocPelneDane() + "i zarabiam " + pensja;
        } else {
            return super.zwrocPelneDane();
        }
    }

    void ustawPensje(Double pensja) {
        this.pensja = pensja;
    }

}

