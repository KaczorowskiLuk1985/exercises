package com.company.zadania.zadanie124;

import static com.company.zadania.zadanie124.Osoba.Plec.KOBIETA;
import static com.company.zadania.zadanie124.Osoba.Plec.MEZCZYZNA;

/*
Utwórz klasy `Osoba` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik`
(dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`,
 `wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */
public class Zadanie124 {
    public static void main(String[] args) {
        Osoba o1 = new Osoba("Marian","Kowalski",54, MEZCZYZNA);
        Osoba o2 = new Osoba("Anna","Nowak",64, KOBIETA);
        System.out.println(o1.zwrocPelneDane());
        System.out.println(o2.zwrocWiekDoEmerytury());

        Pracownik p1;
        p1 = new Pracownik("Kasia","Kwiatkowska",22, Osoba.Plec.KOBIETA);
        System.out.println(p1.zwrocPelneDane());
        p1.ustawPensje(5300.0); //setter
        System.out.println(p1.zwrocPelneDane());
    }

}
