package com.company.zadania.zadanie124;

/*
Utwórz klasy `Osoba` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik`
(dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`,
 `wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */
public class Osoba implements PrzedstawSie {
    private String imie;
    private String nazwisko;
    private int wiek;
    private Plec plec;

    public Osoba(String imie, String nazwisko, int wiek, Plec plec) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.plec = plec;

    }

    @Override
    public String zwrocPelneDane() {
        return String.format("Nazywam się %s %s i mam %s lat", imie, nazwisko, wiek);
    }

    @Override
    public String zwroclImie() {
        return String.format("Mam na imię %s", imie);
    }

    @Override
    public int zwrocWiekDoEmerytury() {
//        if (Plec.MEZCZYZNA==plec){
//
//            return 67-wiek;
//        }else {
//            return 65-wiek;
//        }
        return (Plec.MEZCZYZNA == plec ? 67 : 65) - wiek;
    }

    @Override
    public String toString() {
        return zwrocPelneDane();
    }

    enum Plec {
        MEZCZYZNA, KOBIETA
    }
}
