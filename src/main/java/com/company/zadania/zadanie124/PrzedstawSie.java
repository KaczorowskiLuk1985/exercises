package com.company.zadania.zadanie124;
/*
Utwórz klasy `Osoba` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik`
(dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`,
`wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */
public interface PrzedstawSie {
    String zwrocPelneDane();
    String zwroclImie();
    int zwrocWiekDoEmerytury();
}
