package com.company.zadania.zadanie152;

public class Zadanie152 {
    public static void main(String[] args) {
        String imie = "Ja";
        String nazwisko = "Placek";
        System.out.println(login(imie, nazwisko));


    }

    static String login(String imie, String nazwisko) {
        if (imie == null && nazwisko == null) {
            return null;
        } else if (imie == null) {
            return String.format("XXX%s%s",
                    nazwisko.toLowerCase().substring(0, 3),
                    nazwisko.length());

        } else if (nazwisko == null) {
            return String.format("%sYYY%s",
                    imie.toLowerCase().substring(0, 3),
                    imie.length());

        } else if (imie.length() == 2) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase(),
                    "Z",
                    nazwisko.toLowerCase().substring(0, 3),
                    imie.length() + nazwisko.length());

        } else if (imie.length() == 1) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase(),
                    "ZZ",
                    nazwisko.toLowerCase().substring(0, 3),
                    imie.length() + nazwisko.length());
        } else if (nazwisko.length() == 2) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase().substring(0, 3),
                    nazwisko.toLowerCase(),
                    "Z",
                    imie.length() + nazwisko.length());
        } else if (nazwisko.length() == 1) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase().substring(0, 3),
                    nazwisko.toLowerCase(),
                    "ZZ",
                    imie.length() + nazwisko.length());
        }

        return String.format("%s%s%s",
                imie.toLowerCase().substring(0, 3),
                nazwisko.toLowerCase().substring(0, 3),
                imie.length() + nazwisko.length());
    }
}
