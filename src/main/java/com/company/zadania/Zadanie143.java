package com.company.zadania;

/*
Policz silnię dla podanej wartości przy wykorzystaniu *rekurencji*.
 */
public class Zadanie143 {
    public static void main(String[] args) {
        System.out.println(polivxSilnie(5));

    }

    private static int polivxSilnie(int wartoscPoczatkowa) {
        System.out.println("wartoscPoczatkowa = " + wartoscPoczatkowa);
        if (wartoscPoczatkowa == 1) {
            return 1;
        } else {
            return wartoscPoczatkowa * polivxSilnie(wartoscPoczatkowa - 1);
        }
    }
}
