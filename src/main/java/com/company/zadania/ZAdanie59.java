package com.company.zadania;
/*
Utwórz metodę, która przyjmuje tablicę liczb i zwraca nową tablicę
w której wszystkie ujemne liczby będą w lewej części, a wszystkie dodanie będą w prawej części.
Liczby ujemne i dodatnie mogą być w dowolnej kolejności (tzn. `1,2,3` czy `3,2,1` są jak najbardziej poprawne)
> Dla `[1, 2, -6, -9, 11, 0, -2]`
> zwróci `[-6, -9, -2, 0, 11, 2, 1]`.
 */

import java.util.Arrays;

public class ZAdanie59 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, -8, 9, 5, -9, 8, -5};
        System.out.println(Arrays.toString(zwrocPozadek(tab)));

    }

    static int[] zwrocPozadek(int[] tablica) {
        int[] tablicaPorzadkowa = new int[tablica.length];
        int dodatnie = 0;
        int ujemne = 0;

        for (int liczby : tablica) {
            if (liczby >= 0) {
                dodatnie++;

            } else {
                ujemne++;
            }

        }
        int pozycjaWtablcy = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] >= 0) {
                tablicaPorzadkowa[ujemne] = tablica[i];
                ujemne++;
            } else {
                tablicaPorzadkowa[pozycjaWtablcy] = tablica[i];
                pozycjaWtablcy++;
            }

        }
        return tablicaPorzadkowa;
    }
}
