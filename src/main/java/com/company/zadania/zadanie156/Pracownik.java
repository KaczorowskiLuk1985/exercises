package com.company.zadania.zadanie156;

public class Pracownik implements Comparable<Pracownik> {
    private String imie;
    private int wiek;
    private int pensja;

    public Pracownik(String imie, int wiek, int pensja) {
        this.imie = imie;
        this.wiek = wiek;
        this.pensja = pensja;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    public int getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return String.format("| %-10s| %-10s| %-10s|",
                imie, wiek, pensja);
    }

    @Override
    public int compareTo(Pracownik p) { //-1 obecny jest przed przekazywanym, 0 - są takie same, 1 - obecny jest po przekazywanym
//        if (this.wiek == p.wiek) {
//            //obiekty są równe
//            return 0;
//        } else if (this.wiek < p.wiek) {
//            //obecny(this) jest przed przekazywanym
//            return 1;
//        } else {
//            //obecny jest po przekazywanym (this.wiek > p.wiek)
//            return -1;

//        return Integer.compare(this.wiek,p.wiek);

        return p.wiek-this.wiek;
        }
    }



