package com.company.zadania.zadanie156;

import java.util.Comparator;

public class NameComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Pracownik p1 = (Pracownik) o1; //"ten obiekt jest moim pracownikiem"
        Pracownik p2 = (Pracownik) o2; // to samoe jak (int) 7,5 - czyli w biegu zamienia typ

        return p1.getImie().compareTo(p2.getImie()); //tak sortuje np alegro
    }
}
