package com.company.zadania.zadanie156;

import java.util.Comparator;

public class Age implements Comparator {


    @Override
    public int compare(Object o1, Object o2) {
        Pracownik p1 = (Pracownik) o1;
        Pracownik p2 = (Pracownik) o2;

        int nameComparatorResult = p1.getImie().compareTo(p2.getImie()); // z kontrolem wchodzisz do metody

        if (nameComparatorResult == 0) {
            int porownywaniePoPensji = Integer.compare(p1.getWiek(), p2.getWiek()); // jezeli imiona sa zgodne to poworwnujemy wiek
            if (porownywaniePoPensji == 0) {
                return Integer.compare(p1.getPensja(), p2.getPensja());
            }
            return porownywaniePoPensji;
        }

        return nameComparatorResult;
    }
}

