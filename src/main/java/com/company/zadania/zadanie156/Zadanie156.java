package com.company.zadania.zadanie156;
/*

 */

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zadanie156 {
    private static List<Pracownik> listaPracownikow = Arrays.asList(
            new Pracownik("Janusz", 26, 300),
            new Pracownik("Janusz", 25, 3002),
            new Pracownik("Roman", 34, 5000),
            new Pracownik("John", 77, 30000),
            new Pracownik("Mario", 37, 400),
            new Pracownik("Janusz", 26, 3001)
    );

    public static void main(String[] args) {
//        metoda1();
//        metoda2();
        metoda3();
        metoda4();
    }

    private static void metoda4() {
        listaPracownikow.sort(new Age());
        wyswietlListe(listaPracownikow);
    }

    static void metoda3() {
        listaPracownikow.sort(new NameComparator());
        wyswietlListe(listaPracownikow);

    }

    private static void metoda2() {
        Collections.sort(listaPracownikow);
        wyswietlListe(listaPracownikow);

    }

    static void metoda1() {
        wyswietlListe(listaPracownikow);
    }

    static void wyswietlListe(List<Pracownik> Lista) {
        System.out.println("+-----------------------------------+");
        System.out.println("| IMIE      | WIEK      | PENSJA    |");
        System.out.println("|===========|===========|===========|");
        for (Pracownik pracownik : Lista) {
            System.out.println(pracownik);
        }
        System.out.println("+-----------------------------------+");
        System.out.println();
    }
}
