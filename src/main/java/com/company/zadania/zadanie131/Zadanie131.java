package com.company.zadania.zadanie131;

import java.util.Scanner;

/*
Utwórz metodę, która przyjmuje dwa parametry -
napis (`String`) oraz liczbę (`int`) wcześniej odczytaną `Scanner`-em od użytkownika.

Metoda ma wyświetlić podany napis, przekazaną liczbę razy.
W przypadku gdy liczba wystąpień będzie mniejsza bądź równa zero powinien zostać rzucony własny wyjątek
(dziedziczący po `RuntimeException`).
 */
public class Zadanie131 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Liczba wystąpień napisu:");
        int liczbaWystapien = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Podaj napis:");
        String napis = scanner.nextLine();

        try {
            wyswietlNapis(napis, liczbaWystapien);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    static void wyswietlNapis(String napis, int ileRazy) {
        if (ileRazy <= 0) {
            throw new ZbytMalaLiczbaWystapien(ileRazy + " nie jest poprawną liczbą wystąpień!!");
        }
        for (int i = 0; i < ileRazy; i++) {
            System.out.println(napis);
        }
    }
}
