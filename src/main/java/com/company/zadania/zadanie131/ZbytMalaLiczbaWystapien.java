package com.company.zadania.zadanie131;

class ZbytMalaLiczbaWystapien extends RuntimeException {
    ZbytMalaLiczbaWystapien(String message) {
        super(message);
    }
}
