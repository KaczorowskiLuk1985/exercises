package com.company.zadania;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca listę,
 która będzie zawierała dwie listy. Na pozycji `0` mają być elementy (wartości) parzyste,
  a na pozycji `1` elementy nieparzyste.
 */
public class Zadanie105 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                -1,2,3,4,5,6,6,7,7,8,9
        ));
        System.out.println(podzielListe(lista));

    }

    static List<List<Integer>> podzielListe(List<Integer> lista) { //sygnatura metody
        List<Integer> listaParzysta = new ArrayList<>();
        List<Integer> listaNieParzysta = new ArrayList<>();

        for (Integer element : lista) {
            if (element % 2 == 0) {
                listaParzysta.add(element);

            } else {
                listaNieParzysta.add(element);
            }
        }
        return new ArrayList<>(Arrays.asList(listaParzysta, listaNieParzysta));
    }
}
