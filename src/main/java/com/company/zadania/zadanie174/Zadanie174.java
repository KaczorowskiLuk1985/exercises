package com.company.zadania.zadanie174;

import java.util.TreeMap;

public class Zadanie174 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
        method5();
    }

    private static void method5() {
        System.out.println("ID wątku main: " + Thread.currentThread().getId());
        WlasnyWatek wlasnyWatek = new WlasnyWatek();
        WlasnyWatek wlasnyWatek2 = new WlasnyWatek();
        wlasnyWatek.start();
        wlasnyWatek2.start();
        try {
            wlasnyWatek.join(); // trzeba uwazac na niskonczone watki
            wlasnyWatek2.join(); // trzeba uwazac na niskonczone watki
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("zakonczeni pracy " + Thread.currentThread().getId());

    }

    private static void method4() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 21; i++) {
                    System.out.printf(" %s ", i);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private static void method3() {
        Thread watek2 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 21; i++) {
                    System.out.printf("%s.            id = %s\n", i, id);
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        Thread watek3 = new Thread() { //klasaa thread sluzy do odpalenia czegos w innym watku
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 21; i++) {
                    System.out.printf("%s. id = %s\n", i, id);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        watek2.start();
        watek3.start();


    }

    private static void method2() {
        Thread watek = new Thread() { //klasa anonimowea

            @Override
            public void run() {
                System.out.println("identyfikator nowehowatku: " + Thread.currentThread().getId());
            }
        };
        watek.start();
    }

    private static void method1() {
        System.out.println("identyfikator wątku w main: " + Thread.currentThread().getId());

    }
}
