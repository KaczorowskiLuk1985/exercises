package com.company.zadania.zadanie174;

public class WlasnyWatek extends Thread {
    @Override
    public void run() {
        System.out.println("wątek staruje: " + Thread.currentThread().getId());
        for (int i = 0; i < 8; i++) {
            System.out.printf(" %s ID = %s \n", i, Thread.currentThread().getId());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\nwatek konczy: " + Thread.currentThread().getId());
    }
}
