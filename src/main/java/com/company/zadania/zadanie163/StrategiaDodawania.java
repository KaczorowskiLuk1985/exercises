package com.company.zadania.zadanie163;

public class StrategiaDodawania implements Strategia {
    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        return liczbaDruga + liczbaPierwsza;
    }
}
