package com.company.zadania.zadanie163;

public class StrategiaOdejmowania implements Strategia {
    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        return liczbaPierwsza - liczbaDruga;
    }
}
