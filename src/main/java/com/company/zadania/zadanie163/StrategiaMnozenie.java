package com.company.zadania.zadanie163;

public class StrategiaMnozenie implements Strategia {

    private static StrategiaMnozenie instanece = null;

    StrategiaMnozenie() {

    }

    static StrategiaMnozenie getInstance() {
        if (instanece == null) {
            instanece = new StrategiaMnozenie();
        }
        return instanece;
    }

    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        return liczbaDruga * liczbaDruga;
    }

}
