package com.company.zadania.zadanie163;

public interface Strategia {

    double policz(double liczbaPierwsza, double liczbaDruga);

    default String nazwaOperacji(){
        return "jestem strategią";
    }

}
