package com.company.zadania.zadanie163;

public class SrategiaDzielenie implements Strategia {
    private Strategia obecnieWybranaStrategia;

    @Override
    public double policz(double liczbaPierwsza, double liczbaDruga) {
        if (liczbaDruga == 0) {
            return 0;
        }
        return liczbaPierwsza / liczbaDruga;
    }

    public void setObecnieWybranaStrategia(Strategia obecnieWybranaStrategia) {
        this.obecnieWybranaStrategia = obecnieWybranaStrategia;
    }

    @Override
    public String nazwaOperacji() {
        return "dzielenie.";
    }
}
