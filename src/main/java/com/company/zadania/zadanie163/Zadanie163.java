package com.company.zadania.zadanie163;

/*
ZADANIE #163* - wzorzec projektowy Strategia
 */
public class Zadanie163 {
    public static void main(String[] args) {
        Kalkulator kalkulator = new Kalkulator();
        kalkulator.setObecnieWybranePole(new StrategiaDodawania());

        System.out.println(kalkulator.policz(10, 20));

        kalkulator.setObecnieWybranePole(new StrategiaOdejmowania());
        System.out.println(kalkulator.policz(10, 20));

        kalkulator.setObecnieWybranePole(new SrategiaDzielenie());
        System.out.println(kalkulator.policz(10, 20));

        kalkulator.setObecnieWybranePole(new StrategiaMnozenie());
        System.out.println(kalkulator.policz(10, 20));
    }
}
