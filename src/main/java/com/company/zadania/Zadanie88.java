package com.company.zadania;
/*
Utwórz metodę, która przyjmuje ciąg znaków, a odwraca każdy z wyrazów i zwraca całość jako jeden napis.
> DLa `"ala ma kota"` zwróci `"ala am atok"`
> DLa `"dzisiaj jest sobota"` zwróci `"jaisizd tsej atobos"`
 */

public class Zadanie88 {
    public static void main(String[] args) {
        System.out.println(odwracaSlowaWWyrazach("ala ma kaca"));
        System.out.println(odwracaWyraz("asdf qwertui"));

    }

    static String odwracaSlowaWWyrazach(String zdanie) {
        String[] wyraz = zdanie.split(" ");
        String noweZdanie = "";
        for (int pozycjaWTablicy = 0; pozycjaWTablicy < wyraz.length; pozycjaWTablicy++) {
            noweZdanie += odwracaWyraz(wyraz[pozycjaWTablicy]) + " ";
            //a+=b  --->  a= a+b
        }  //concatenation - kontatenacja laczenie
        return noweZdanie;

        //return noweZdanie.trim(); //usunie nam spacje
    }

    static String odwracaWyraz(String wyraz) {
        String nowyWyraz = "";
        for (int pozycjaLitery = 0; pozycjaLitery < wyraz.length(); pozycjaLitery++) {
            nowyWyraz = wyraz.charAt(pozycjaLitery) + nowyWyraz;
                // a = b+a ;     odpowiednia kolejność powoduje ze najpierw bierze 'k' a pozniej dostwia 'o' przed k >>>  ok

        }
        return nowyWyraz;
    }
}
