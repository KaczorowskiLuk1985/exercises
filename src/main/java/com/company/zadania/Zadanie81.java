package com.company.zadania;
/*
Utwórz metodę, która przyjmuje zdanie (np. `"Ala ma kota."`, `"Dziś jest sobota"`),
a następnie ma zwrócić sumę długości *wszystkich* wyrazów.
 */

public class Zadanie81 {
    public static void main(String[] args) {
        System.out.println(zwracaSume("ala ma kota dziś jest sobota"));
        System.out.println(zwracaCosTam("ala ma kota dziś jest sobota"));

    }

    static int zwracaSume(String napis) {
        int suma = 0;

        String[] tablica = napis.split(" ");
        for (String s : tablica) {
            suma += s.length();
        }
        return suma;
    }

    static int zwracaCosTam(String napis) {
        return napis.replace(" ", "").length();

    }
}
