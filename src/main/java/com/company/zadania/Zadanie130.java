package com.company.zadania;
/*
Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`.
W przypadku nie przekazania żadnego parametru, metoda powinna rzucić własny wyjątek
(z komunikatem o powodzie).
W przypadku podania parametrów, metoda powinna zwrócić ich sumę.
 */
public class Zadanie130 {
    public static void main(String[] args) {
        try {
            System.out.println(zwrocSume(1, 2, 7, 9));
            System.out.println(zwrocSume());
        } catch (MojWlasnyWyjatek exception) {
            System.out.println(exception.getMessage());
        }
    }

    static int zwrocSume(int... liczba) throws MojWlasnyWyjatek {
        int suma = 0;
        if (liczba.length == 0) {
            throw new MojWlasnyWyjatek("Brak parametrów do zsumowania");
        } else {
            for (int i = 0; i < liczba.length; i++) {
                suma += liczba[i];
            }
        }
        return suma;
    }
}

class MojWlasnyWyjatek extends Exception {
    //konstruktor:
    MojWlasnyWyjatek(String message) {
        super(message);
    }
}
