package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - pierwszy datą (jako ciąg znaków), a drugi jest separatorem znaków. Metoda ma zwrócić rok z przekazanej daty.
> Dla `("12/05/2018", "/")`, zwróci `"2018"`
> Dla `("22.11.2007", ".")`, zwróci `"2007"`
 */

public class Zadanie86 {
    public static void main(String[] args) {
        System.out.println(zwrocRok("12/11/2018", "/"));
        System.out.println(zwrocRok("12.11.2018", "\\."));//kropka ma problem bo jest znakiem specjalnym. slesz go
        System.out.println(zwrocRok("12/11/2018"));

    }

    static String zwrocRok(String data, String separator) {
        String[] rocznik = data.split(separator);
        return rocznik[rocznik.length-1];
    }
    static String zwrocRok(String data){
        return zwrocRok(data,"/");
    }
}
