package com.company.zadania;
/*
*ZADANIE #115  -  PRACA DOMOWA*
Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : List<Integer>`.
Metoda ma zwrócić napis. W mapie przekazanej jako parametr,
kluczami są litery a jako wartościami - *pozycje* na jakich mają wystąpić w zwracanym napisie.
 */

import java.util.*;

public class Zadanie115 {
    public static void main(String[] args) {
        List<Integer> listK = new ArrayList<>(Arrays.asList(0, 4));
        List<Integer> listA = new ArrayList<>(Arrays.asList(1, 3));
        List<Integer> listJ = new ArrayList<>();
        listJ.add(2);

        Map<String, List<Integer>> map = new HashMap<>();
        map.put("K", listK);
        map.put("A", listA);
        map.put("J", listJ);
        System.out.println(zwrocNapis(map));

    }

    static String zwrocNapis(Map<String, List<Integer>> mapa) {
        int dlugoscNapisu = 0;

        for (List<Integer> valueList : mapa.values()) {
            dlugoscNapisu += valueList.size();

        }
        String[] napis = new String[dlugoscNapisu];

//        StringBuilder builder = new StringBuilder();

        for (Map.Entry<String, List<Integer>> zmienna : mapa.entrySet()) {
            for (int i = 0; i < napis.length; i++) {
                if (zmienna.getValue().contains(i)) {
                    napis[i] = zmienna.getKey();
                }
            }
        }
//        for (String s : napis) {
//            builder.append(s);
//        }
//        return builder.toString();
        return String.join("",napis);// dodaje elementy tablicy z "" pomiędzy
    }
}