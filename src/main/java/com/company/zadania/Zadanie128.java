package com.company.zadania;
/*
Utwórz metodę która wykonuję operację dzielenia dwóch liczb (2 x `int`)
przekazywanych jako parametry i zwraca typ `Integer`. W przypadku podzielenia przez `0`
metoda powinna zwrócić `null`.
 */

public class Zadanie128 {
    public static void main(String[] args) {
        System.out.println(dzielenie(6, 3));
        System.out.println(dzielenie(6, 0));
        System.out.println(dzielenie(6, 0) == null);
    }

    static Integer dzielenie(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            return null;
        }
    }
}

