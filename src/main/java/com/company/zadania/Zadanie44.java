package com.company.zadania;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby.
Metoda ma zwrócić nową tablicę do której na wybranej pozycji (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
Dla `([1, 2, 3, 4, 5], 2, 77)`
powinno zwrócić `[1, 2, 77, 3, 4, 5]`
 */

import java.util.Arrays; // w domu zrobic w jednej petli

public class Zadanie44 {
    public static void main(String[] args) {
        int tablicaNowa[] = {1, 2, 3, 4};
        System.out.println(Arrays.toString(insertElementNaPozycje(tablicaNowa, 2, 77)));
    }

    static int[] insertElementNaPozycje(int[] tabliczka, int pozycja, int nowyElement) {
        int[] tablicaWyjsciowa = new int[tabliczka.length + 1];

        for (int index = 0; index < pozycja; index++) {
            tablicaWyjsciowa[index] = tabliczka[index];


        }
        tablicaWyjsciowa[pozycja] = nowyElement;

        for (int i = pozycja + 1; i < tablicaWyjsciowa.length; i++) {
            tablicaWyjsciowa[i] = tabliczka[i-1];

        }
        return tablicaWyjsciowa;                 // po return nie da sie w java wykonac zadnej instrukcji, exception - wyjatek
    }                                            // dodac zmienna ktora przeskoczy i doda wartosc.

}
