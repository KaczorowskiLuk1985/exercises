package com.company.zadania;

import java.util.Arrays;

/*
Utwórz metodę, która przyjmuje tablicę liczb. Metoda ma zwrócić  nową tablicę wartości logicznych (`boolean`),
gdzie wartości dodatnie (oraz `0`) zostaną zastąpione `true`, a ujemne `false`.
>Dla  `[2, 3, -6, 0, -7, -99]`
> zwróci `[true, true, false, true, false, false]`
 */
public class Zadanie60 {
    public static void main(String[] args) {

        int[] tab = new int[]{-1, -2, 1, 2, 3, -1};
        System.out.println(Arrays.toString(zwracaTabliceTrueLubFalse(tab)));

    }

    static boolean[] zwracaTabliceTrueLubFalse(int[] tablica) {

        boolean[] nowaTablica = new boolean[tablica.length];


        for (int i = 0; i < tablica.length; i++) {

            if (tablica[i] >= 0) {
                nowaTablica[i] = true;
            }

            }
            return nowaTablica;
        }
    }
