package com.company.zadania.zadanie164;

public class Obserwwator {
    private String name;
    private int maxTemp;
    private int minTemp;

    Obserwwator(String name, int maxTemp, int minTemp) {
        this.name = name;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
    }

    String getName() {
        return name;
    }

    int getMinTemp() {
        return minTemp;
    }

    int getMaxTemp() {
        return maxTemp;
    }

    void react(int currentTemp) {
        String wiadomosc = String.format("%s twierdzi, ze %s jest za %%s" + "Dla mnie max to %%s.\n", name, currentTemp);
        if (currentTemp > maxTemp) {
            System.out.printf(wiadomosc, " ciepło. ", maxTemp);
        } else if (currentTemp < minTemp) {
            System.out.printf(wiadomosc, " zimno. ", maxTemp);

        }
    }
}
