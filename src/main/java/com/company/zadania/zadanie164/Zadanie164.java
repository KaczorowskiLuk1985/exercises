package com.company.zadania.zadanie164;

import java.util.Scanner;

/*
 *ZADANIE #164* - wzorzec projektowy Obserwator
 */
public class Zadanie164 {
    public static void main(String[] args) {
        Weather weather = new Weather();// tworzymy nową instancje klasy

        Obserwwator o1 = new Obserwwator("ala", 20, 15); //tworzymy zmienne
        Obserwwator o2 = new Obserwwator("Ola", 25, 20);
        Obserwwator o3 = new Obserwwator("ela", 26, 19);
        Obserwwator o4 = new Obserwwator("tomek", 10, 11);


        weather.addObserver(o1, o2, o3, o4);

        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            System.out.println("ustaw temp : ");
            int ustawionaTemp = scanner.nextInt();
            weather.updateTemp(ustawionaTemp);
        }
    }
}
