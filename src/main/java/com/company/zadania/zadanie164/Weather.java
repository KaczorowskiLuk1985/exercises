package com.company.zadania.zadanie164;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Weather {
    private List<Obserwwator> list = new ArrayList<>();
    private int currentTemp;

    void addObserver(Obserwwator user) {
        list.add(user);
    }

    void addObserver(Obserwwator... users) {//dostajemy tablice ktorej nie mozemy wrzocic do listy
        List<Obserwwator> asList = Arrays.asList(users);
        list.addAll(asList);
    }

    void updateTemp(int newTemp) {
        currentTemp = newTemp;
        notifyObservers();
    }

    private void notifyObservers() {
        for (Obserwwator obserwwator : list) {
            obserwwator.react(currentTemp);
        }
        list.forEach(obserwwator -> obserwwator.react(currentTemp));
    }

}

