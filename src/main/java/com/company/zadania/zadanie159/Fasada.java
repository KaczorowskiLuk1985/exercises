package com.company.zadania.zadanie159;

import java.util.Arrays;
import java.util.List;

public class Fasada {
    private final int TEMP_IN = 22;
    private final int TEMP_OUT = 18;
    private List<Windows> windowsList = Arrays.asList(
            new Windows(),
            new Windows(),
            new Windows()
    );
    private Alarm alarm = new Alarm();
    private Light light = new Light();
    private Heating heating = new Heating();

    void imOut() {
        windowsList.forEach(Windows::close);
        alarm.enable();
        light.lightOff();
        heating.setTemp(TEMP_IN);
    }

    void imBack() {
        windowsList.forEach(Windows::open);
        alarm.disable();
        light.lightOn();
        heating.setTemp(TEMP_OUT);
    }
}
