package com.company.zadania.zadanie159;

import java.util.Scanner;

public class Zadanie159 {
    final static int WEJSCIE = 1;
    final static int WYJSCIE = 2;

    public static void main(String[] args) {
        Fasada panel = new Fasada();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Możliwe opcje");
        System.out.println(WEJSCIE + ". Wejscie");
        System.out.println(WYJSCIE + ". Wyjscie");

        int decyzja = scanner.nextInt();

        switch (decyzja) {
            case WEJSCIE: //magic number - gdy zostawimy cyfre w metodzie
                panel.imBack();
                break;
            case WYJSCIE:
                panel.imOut();
                break;
            default:
                System.out.println("Nie ma takiej opcji");
        }
    }
}
