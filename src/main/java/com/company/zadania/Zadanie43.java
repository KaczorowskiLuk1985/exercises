package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
dla `[1, 2, 3, 4, 5]` zwróci `4`
 */

public class Zadanie43 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 5, 6};
        System.out.println(drugaNajiększa(tablica));
    }

    static int drugaNajiększa(int[] tablica) {
        int max = Integer.MIN_VALUE;
        int drugiMax = Integer.MIN_VALUE;
        for (int pozycja = 0; pozycja < tablica.length; pozycja++) {
            if (tablica[pozycja] > max) {
                drugiMax = max;
                max = tablica[pozycja];

            } else if (tablica[pozycja] > drugiMax && tablica[pozycja] != max) {
                drugiMax = tablica[pozycja];
            }
        }
        return drugiMax;
    }
}
