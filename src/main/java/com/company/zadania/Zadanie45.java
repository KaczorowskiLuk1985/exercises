package com.company.zadania;

import java.util.Arrays;
import java.util.Scanner;

/*
*ZADANIE #45*
Utwórz metodę, w której użytkownik podaje z klawiatury rozmiar tablicy,
a następnie podaje wszystkie elementy (jako `int`):
Podaj rozmiar tablicy:  4
Liczba[1]:  88
Liczba[2]:  12
Liczba[3]:  -7
Liczba[4]:  195
Podana tablica to: [88, 12, -7, 195]
Suma elementów wynosi: 288

>
 */
public class Zadanie45 {
    public static void main(String[] args) {
        podajRozmiarTablicy();

    }
    static void podajRozmiarTablicy(){
        System.out.print("Podaj Rozmiar Tablicy :");
        Scanner scanner = new Scanner(System.in);
        int rozmiarTablicy = scanner.nextInt();
        int [] tablica = new int[rozmiarTablicy];
        int suma = 0;

        for (int i = 0; i < rozmiarTablicy; i++) {
            System.out.print("Liczba ["+ (i+1)+"] : ");
            tablica[i] = scanner.nextInt();
            suma += tablica[i];

        }
        System.out.println("podana tablica to" + Arrays.toString(tablica));
        System.out.println("Suma wszystkich elementów wynosi :" + suma);

    }


}
