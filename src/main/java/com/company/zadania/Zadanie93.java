package com.company.zadania;
/*
Utwórz metodę, która przyjmuje ciąg znaków i zwraca informacje jak długie są ciągi znaków które stoją obok siebie
> DLa `"aaabccccbb"` zwróci `[3, 1, 4, 2]`
> DLa `"telefon"` zwróci `[1, 1, 1, 1, 1, 1, 1]`
> DLa `"anna"` zwróci `[1, 2, 1]`
 */

import java.util.Arrays;

public class Zadanie93 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(howLong("aaaaaaaaaaaaaaakssssddddd")));
        System.out.println(Arrays.toString(howLong("telefonn")));
        System.out.println(Arrays.toString(howLong("anna")));

    }

    static int[] howLong(String ciagZnaków) {
        String[] tablica = ciagZnaków.split("");
        String tenSamElement = tablica[0];
        int liczbaWystapienDanegoElementu = 0;
        int[] wynik = new int[tablica.length];
        int indexTablicyWynikowej = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i].equals(tenSamElement)) { //to samo co (tablica[i] == tenSamElement)
                liczbaWystapienDanegoElementu++;
            } else {
                tenSamElement = tablica[i];
                wynik[indexTablicyWynikowej] = liczbaWystapienDanegoElementu;
                indexTablicyWynikowej++;
                liczbaWystapienDanegoElementu = 1;
            }
        }

        wynik[indexTablicyWynikowej] = liczbaWystapienDanegoElementu;

        return przycietaTablica(wynik);

    }

    static int[] przycietaTablica(int[] tablica) {
        int i = 0;

        for (; i < tablica.length; i++) {
            if (tablica[i] == 0) {
                break;

            }
        }
        int[] krotszaTablica = new int[i];
        for (int j = 0; j < krotszaTablica.length; j++) {
            krotszaTablica[j] = tablica[j];
        }
        return krotszaTablica;

    }

}
