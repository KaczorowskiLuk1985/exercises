package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - set (np. `HashSet`) oraz poszukiwaną liczbę (jako `double`.)

Metoda ma zwrócić set elementów większych od podanego (drugiego) parametru.
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Zadanie107 {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(
                0,1,2,1,-1,1,2,3,4,5,6,-4,3  // zawsze jeden rodzaj parametrów, np 2.1 nie przejdzie
        ));
        System.out.println(zwrocWieksza(set1, 2));

    }
    static Set<Integer> zwrocWieksza(Set<Integer> mojSet, double parametr){
        Set<Integer> nowySet = new HashSet<>();

        for (Integer element : mojSet) {
            if (element > parametr){
                nowySet.add(element);
            }

        }
        return nowySet;
    }

}
