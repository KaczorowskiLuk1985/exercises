package com.company.zadania.zadanie155;
/*
*ZADANIE #155* - Klasy anonimowe
Utwórz klasą `Osoba` z polami `imie` i `wiek`
oraz dodaj metodę `przedstawSie()` która zwraca `String`-a
 */

public class Zadanie155 {
    public static void main(String[] args) {
//        metoda1();
        metoda2();
    }

    private static void metoda2() {
        String info = new Osoba("Janusz", 77) {
            @Override
            String przedstawSie() {
                return String.format("jestem %s i szukam dziewczyny w wieku %s lat",
                        getImie(),
                        getWiek());
            }
        }.przedstawSie();
        System.out.println(info);
    }

    static void metoda1() {
        Osoba osoba = new Osoba("Snurf", 33);
        String info = osoba.przedstawSie();
        System.out.println(info);
    }

}
