package com.company.zadania.zadanie155;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }
    String przedstawSie(){
        return String.format("Yo! jestem %s i mam %s lat",imie, wiek);
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }
}
