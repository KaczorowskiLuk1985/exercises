package com.company.zadania.zadanie95;

enum TypWalut {
    EURO("euro","europa"), USD("dolar","usa"), PLN("złoty","polska");
    private String opis;
    private String kraj;

    TypWalut(String opis, String kraj) {
        this.opis = opis;
        this.opis = kraj;
    }

    String getOpis() {
        return opis;
    }

    public String getKraj() {
        return kraj;
    }
}
