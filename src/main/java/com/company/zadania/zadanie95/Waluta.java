package com.company.zadania.zadanie95;

public class Waluta {

    private double kursSprzedarzy;
    private double kursKupna;
    private TypWalut typWaluta;

    Waluta (double kursSprzedarzy, double kursKupna, TypWalut typWaluta){
        this.kursSprzedarzy = kursSprzedarzy;
        this.kursKupna = kursKupna;
        this.typWaluta = typWaluta;

    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s",
                kursSprzedarzy,
                kursKupna,
                typWaluta.getOpis());

    }
    void pokazeZysk (double kwota){
        double przelicznik = kwota/kursSprzedarzy;
        System.out.printf("Za %s zł kupisz %.2f %s", // %.2f zaokragla do dwoch miejsc po przecinkach
                kwota,
                przelicznik,
                typWaluta.getOpis()); // tu cos sie nie zgadza
    }

    public TypWalut getTypWaluta() {
        return typWaluta;
    }
}
