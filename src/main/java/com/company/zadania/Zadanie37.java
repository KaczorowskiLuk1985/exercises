package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę wszystkich elementów
 */

public class Zadanie37 {
    public static void main(String[] args) {
        int [] tablica = new int []{1,2,3,4,5};
        System.out.println(zwrocSume(tablica));
        System.out.println(zwrocSume2(tablica));
        System.out.println(podajSrednią(tablica));

    }
    static int zwrocSume(int[] tablica){
        int suma = 0;                               //element neutralny dodawania
        for (int i = 0; i < tablica.length; i++) {  //tablica jest obiektem i moze wywolywac metody
            suma += tablica[i];
        }
        return suma;
    }
    static int zwrocSume2 (int [] tablica){
        int suma = 0;
        for(int liczba:tablica){
            suma += liczba;


        }
        return suma;
    }
    static double podajSrednią (int[] tablica){
        return zwrocSume2(tablica)/ (double)tablica.length; // mechanizm rzutowania
    }
}
