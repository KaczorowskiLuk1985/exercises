package com.company.zadania;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`)
oraz indeks za którym należy “przeciąć” tablicę i zwrócić drugą część.
> Dla `3` oraz `<1, 7, 8, 22, 10, -2, 33>`
> powinno zwrócić `<10, -2, 33>`
 */
public class Zadanie104 {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1, 7, 8, 22, 10, -2, 33
        ));
        System.out.println(przetnijTablice(lista, 3));

    }

    private static List<Integer> przetnijTablice(List<Integer> lista, int indeksPrzeciecia) {
        List<Integer> nowaLista = new ArrayList<>();

        for (int i = indeksPrzeciecia + 1; i < lista.size(); i++) {
            nowaLista.add(lista.get(i));
        }
        return nowaLista;
    }
}
