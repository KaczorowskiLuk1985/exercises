package com.company.zadania.zadanie154;

public class Zadanie154 {
    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
        metoda4();
    }

    private static void metoda4() {
        Komputer k = new Komputer() {
            @Override
            protected void prepare() {
                super.prepare();
                System.out.println("anon");
            }

            @Override
            void start() {
                super.start();
                System.out.println("haha 0 zł");
            }
        };
        k.start();
    }

    private static void metoda3() {
        new Komputer() {
            @Override
            protected void prepare() {//stwororzono klase anonimowa,
                super.prepare();
                System.out.println("anonimowe uruchamianie");
            }
        }.start();  // TODO klasa anonimowa
    }

    private static void metoda1() {
        Komputer komputer = new Komputer();
        komputer.start();
    }

    private static void metoda2() {
        NowyKomputer nowyKomuter = new NowyKomputer();
        nowyKomuter.start();

    }
}
