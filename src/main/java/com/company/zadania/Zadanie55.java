package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa dwie tablice.
Metoda ma zwrócić nową tablicę w której elementy będą wstawione na zmianę.
Dla `[5, 1, 10, 1, 89, -9]`
oraz `[-2, 2, 7]`
Ma zwrócić `[5, -2, 1, 2, 10, 7, 1, 89, -9]`

Dla `[1, 1, 1, 1, 1]`
oraz `[2, 2, 2]`
Ma zwrócić `[1, 2, 1, 2, 1, 2, 1, 1]`
 */

import java.util.Arrays;

public class Zadanie55 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 1, 1, 1, 1, 5, 4};
        int[] tablica2 = new int[]{2, 2, 2, 2, 2, 2, 2};
        System.out.println(Arrays.toString(polaczTablice(tablica1, tablica2)));
    }

    static int[] polaczTablice(int[] tablica, int[] tablica2) {
        int[] tablicaKoncowa = new int[tablica.length + tablica2.length];
        int[] dluzszaTablica = tablica.length > tablica2.length ? tablica : tablica2; //instrukcja warunkowa zwroc wartos dluzszej tab.
        int[] krotszaTablica = tablica.length < tablica2.length ? tablica : tablica2; //zwroc wartosc krotszej tab.
        int indexDluzszejTablicy = krotszaTablica.length;

        for (int i = 0; i < krotszaTablica.length; i++) {
            tablicaKoncowa[i * 2] = tablica[i];       //wypełniamy co druga pozycje w tablicy
        }
        for (int i = 0; i < krotszaTablica.length; i++) {
            tablicaKoncowa[i * 2 + 1] = tablica2[i];  // uzupelniamy braki w tablicy
//te dwie pętle towrzą wspólną tablice (ale z wartosciami czy bez ??)
        }

        for (int i = krotszaTablica.length * 2; i < tablicaKoncowa.length; i++) { // 2* bo krotrzy przedział potrzebuje dwukrotnie wiecej miejsca niz ma tego miejsca
            tablicaKoncowa[i] = dluzszaTablica[indexDluzszejTablicy];
            indexDluzszejTablicy++;

        }
        return tablicaKoncowa;
    }
}
