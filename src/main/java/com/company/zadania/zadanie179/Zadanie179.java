package com.company.zadania.zadanie179;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import java.util.Scanner;

public class Zadanie179 {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.metaweather.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static WeatheApi api = retrofit.create(WeatheApi.class);

    public static void main(String[] args) {
        /*
        skaner
        zapytanie o nazwe miasta
         */
        System.out.print("podaj nazwę miast: ");
        Scanner scanner = new Scanner(System.in);
        String odpowiedz = scanner.nextLine();

        Call<List<SeatchCity>> call = api.searhCities(odpowiedz);
        call.enqueue(getSearchCallback()); //wywołanie w nowym watku


    }

    private static Callback<List<SeatchCity>> getSearchCallback() {
        return new Callback<List<SeatchCity>>() {
            @Override
            public void onResponse(Call<List<SeatchCity>> call, Response<List<SeatchCity>> response) {
                if (response.isSuccessful()) {
                    prseSearchData(response.body());
                }
            }


            @Override
            public void onFailure(Call<List<SeatchCity>> call, Throwable throwable) {
                throwable.printStackTrace();
            }


        };
    }

    private static void prseSearchData(List<SeatchCity> body) {
//        final int[] counter = {0};
//        body.forEach(city -> System.out.println(counter++ + city.title));
        int counter = 1;
        for (SeatchCity city : body) {
            System.out.println(counter + " . " + city.title);
            counter++;
        }

        Scanner scanner = new Scanner(System.in);
//        System.out.println("1.New York\n2.New Delhi\n3.New Orleans\n4.Newcastle\n5.Newark\nPodaj numer miasta : ");
        System.out.println("wybrana pozycja z listy : ");
        int selectedPosition = scanner.nextInt();
        String cidyId = body.get(selectedPosition - 1).id;

        getCity(cidyId);
    }

    private static void getCity(String cityId) {
        Call<City> call = api.getCity(cityId);
        call.enqueue(getCityCallBack());
    }

    private static Callback<City> getCityCallBack() {

        return new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.isSuccessful()&& response.body() !=null)
                for (Day day : response.body().days) {
                    System.out.printf("%s - %s\n", day.date, day.temperature);
                }

            }

            @Override
            public void onFailure(Call<City> call, Throwable throwable) {
                throwable.printStackTrace();

            }
        };
    }

}
