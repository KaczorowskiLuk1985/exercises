package com.company.zadania.zadanie179;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

//musi byc interface. nie wiadomo dlaczego

public interface WeatheApi {
    //jaki typ zapytania
    @GET("location/search")
    //dopasowujemy
    Call<List<SeatchCity>> searhCities(@Query("query") String cityName);

    @GET("location/{city_id}")
    Call<City>getCity(@Path("city_id") String id);

}
