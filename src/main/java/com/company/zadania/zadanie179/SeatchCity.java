package com.company.zadania.zadanie179;

import com.google.gson.annotations.SerializedName;
import lombok.ToString;

@ToString
public class SeatchCity {

    /*
    "title": "New York",
    "location_type": "City",
    "woeid": 2459115,
    "latt_long": "40.71455,-74.007118"
     */

    @SerializedName("title")
    String title;

    @SerializedName("location_type")
    String locationType;

    @SerializedName("woeid")
    String id;

    @SerializedName("latt_long")
    String latLong;


}
