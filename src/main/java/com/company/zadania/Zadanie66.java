package com.company.zadania;
/*
*ZADANIE #66*
Utwórz metodę, która wylosuję planszę do gry Saper z zaznaczonymi bombami (wartość `true` oznacza bombe,
 wartość `false` oznacza puste pole).

Na potrzeby wizualizacji utwórz dodatkową metodę która będzie wyświetlać planszę
gdzie bomby będą zaznaczone jako `*`, a puste pola np. `_`

Kolejna metoda ma zwrócić wypełnioną planszę z numerami (gdzie `-1` oznacza bombę).

*Pierwsza metoda zwróci:*

true false true
false false true
false false false

Druga metoda wyświetli:*

 * _ *
 _ _ *
 _ _ _

*Trzecia metoda zwróci:*

 * 3 *
 1 3 *
 0 1 1
 */

import java.util.Arrays;
import java.util.Random;

public class Zadanie66 {
    public static void main(String[] args) {
        boolean[][] planszaBazowa = wylosujPlansze(5);


        int[][] cokolwiek = wypelnijLiczbami(planszaBazowa);
        wyswietlPlansze(cokolwiek);


    }


    static boolean[][] wylosujPlansze(int wymiar) {
        boolean[][] plansza = new boolean[wymiar][wymiar];
        Random random = new Random();

        for (int proba = 0; proba < wymiar * wymiar * 0.2; proba++) {
            int wiesz = random.nextInt(wymiar);
            int kolumna = random.nextInt(wymiar);
            if (plansza[wiesz][kolumna]) {
                proba--;
              //  System.out.println("bledne pole" + wiesz + ", " + kolumna);
            } else {
                plansza[wiesz][kolumna] = true;

            }

           // System.out.println("wylosowano pole " + wiesz + ", " + kolumna);
        }
        return plansza;

    }

    static void wyswietlPlansze(boolean[][] plansza) { // void metoda ktora nic nie zwraca
        for (boolean[] wiersz : plansza) {
            for (boolean elementWiersza : wiersz) {
                System.out.print(elementWiersza==true ? " X " : " O ");
                System.out.print("\t"); //tabulacja tylko dla rownych kolumn
            }
            System.out.println();
        }
    }

    static int[][] wypelnijLiczbami(boolean[][] tab) {  // w tym momencie nie jest uzywana ta metoda
        int[][] tablicaWartosci = new int[tab.length][tab.length];
        for (int wiersz = 0; wiersz < tab.length; wiersz++) {
            for (int kolumna = 0; kolumna < tab[0].length; kolumna++) {
                if (tab[wiersz][kolumna]) { // == true
                    tablicaWartosci[wiersz][kolumna] = -1;
                } else {
                    tablicaWartosci[wiersz][kolumna] = zwrocLiczbaBomb(tab, wiersz, kolumna);

                }

            }

        }
        return tablicaWartosci;
    }

    static int zwrocLiczbaBomb(boolean[][] tab, int wiersz, int kolumna) {
        int liczbaBomb = 0;

        for (int i = wiersz - 1; i <= wiersz + 1; i++) {
            for (int j = kolumna - 1; j <= kolumna + 1; j++) {
                if (j >= 0 && i >= 0 && j < tab.length && i < tab.length) {
                    if (tab[i][j]) {//== true
                        liczbaBomb++;
                    }
                }
            }
        }
        return liczbaBomb;
    }

    static void wyswietlPlansze(int[][] tablica) {

        for (int[] wiersz : tablica) {
            for (int pole : wiersz) {
                if (pole == -1) {
                    System.out.print("*\t");
                } else {
                    System.out.print(pole + "\t");

                }
            }

            System.out.println();
        }

    }
}

