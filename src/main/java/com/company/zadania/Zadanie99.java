package com.company.zadania;
/*
Utwórz metodę, która przyjmuje listę liczb, a następnie największą z nich
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie99 {
    public static void main(String[] args) {

        List<Integer> listaLiczb = new ArrayList<>(Arrays.asList(
                15, 16, -7, 1, 0, 15
        )); //zamien tablice na liste

        System.out.println(zwrocNajwieksza(listaLiczb));
        System.out.println(zwrocNajwieksza2(listaLiczb));

    }

    static int zwrocNajwieksza(List<Integer> lista) {
        int najwieksza = lista.get(0);
        for (int element = 0; element < lista.size(); element++) {
            if (lista.get(element)>najwieksza){
                najwieksza = lista.get(element);
            }

        }
        return najwieksza;

    }
    static int zwrocNajwieksza2(List<Integer> lista){
        int najwieksza = lista.get(0);
        for (Integer element : lista) {
            if (element >najwieksza){
                najwieksza = element;
            }
        }
        return najwieksza;
    }

}
