package com.company.zadania;
/*
*ZADANIE #40*
Utwórz metodę, która zwraca tablicę o podanym rozmierza wypełnioną losowymi liczbami
(użyj klasy `Random()`). Rozmiar tablicy ma być parametrem metody.
 */

import java.util.Arrays;
import java.util.Random;

public class Zadanie40 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(tablica(5)));

    }
    static int [] tablica(int rozmiarTablicy){
        int [] tablica = new int[rozmiarTablicy];
        Random r = new Random();
        for (int pozycja = 0; pozycja < rozmiarTablicy; pozycja++) {
           tablica [pozycja] = r.nextInt(21)-10;  // random to klasa ktora umozliwia losowanie


        }
        return tablica;
    }
}
