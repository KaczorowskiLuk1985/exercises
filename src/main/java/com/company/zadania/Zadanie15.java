package com.company.zadania;

/*
Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci informację (w formie wartości logicznej) czy *obydwie* są parzyste.
Oraz metodę, która zwróci informację czy *chociaż jeden z nich* jest większy od zera.
 */
public class Zadanie15 {
    public static void main(String[] args) {
        int  x = 10; int y = -33;
        System.out.println(czyObieParzyste(x, y));
        System.out.println(czyObieParzyste(x, y));
        System.out.println(czyObieParzyste(x, y));
        System.out.println(czyObieParzyste(x, y));
        System.out.println(czyObieParzyste(x, y));
        System.out.println(" ");
        System.out.println(czyWiekszeOdZera(7, 6));
        System.out.println(czyWiekszeOdZera(-7, 6));
        System.out.println(czyWiekszeOdZera(-7, -6));


    }

    static boolean czyObieParzyste(int pierwsza, int druga) {
        return pierwsza % 2 == 0 && druga % 2 == 0;

    }

    static boolean czyWiekszeOdZera(int jedna, int inna) {
        return jedna > 0 || inna > 0;

    }
}


