package com.company.zadania;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

/*
Utwórz metodę która przyjmuje tablicę liczb i szuka największej z nich.
W pliku tekstowym ma być parametr od którego zależy czy cokolwiek będzie wyświetlane na konsoli czy nie.
Jeśli *tak*, metoda powinna *wyświetlić* zawartość całej tablicy oraz *wyświetlić* znalezioną liczbę*.
Największa liczba to:  15
Zbiór: [1, 4, 2, -9, 1, 15]
Jeśli *nie*, na konsoli nie powinien pojawić się żaden napis.
 */
public class Zadanie140 {
    public static void main(String[] args) {
        int[] tablica = new int[]{2, 3, 4};
        if (czyWyswietlacInfo()) {
            System.out.println(zwracanieMax(tablica));
            System.out.println(Arrays.toString(tablica));
        }

    }

    private static int zwracanieMax(int[] tablica) {
        int max = tablica[0];
        for (int liczba : tablica) {
            if (max < liczba) {
                max = liczba;
            }
        }
        return max;
    }

    private static boolean czyWyswietlacInfo() {
        try {
            FileReader file = new FileReader("pliki/Zadanie140.txt");
            BufferedReader bufferedReader = new BufferedReader(file);
            String line = bufferedReader.readLine();
            return !line.trim().isEmpty();   // jak jest niepusta
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
