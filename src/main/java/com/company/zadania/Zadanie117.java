package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - mapę (w postaci `String : Integer`) oraz poszukiwaną liczbę.
Metoda ma zwrócić informację ile razy w mapie występuje (jako wartość) podana liczba. (edited)
 */

import java.util.HashMap;
import java.util.Map;

public class Zadanie117 {
    public static void main(String[] args) {
        Map<String, Integer> mapa = new HashMap<>();
        mapa.put("a", 22);
        mapa.put("b", 22);
        mapa.put("c", 24);
        mapa.put("v", 25);
        mapa.put("t", 26);
        mapa.put("h", 22);
        System.out.println(ileRazyWystepuje(mapa, 22));

    }

    private static int ileRazyWystepuje(Map<String, Integer> mapa, int liczba) {
        int licznik = 0;
        for (Integer value : mapa.values()) {
            if (value == liczba) {
                licznik++;
            }
        }
        return licznik;
    }
}
