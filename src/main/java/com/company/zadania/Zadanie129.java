package com.company.zadania;
/*
Utwórz metodę która wczytuje liczbę (czyli typ `int`)
od użytkownika a następnie ją wyświetla.
W przypadku podania innej wartości (np. litery),
wyświetl użytkownikowi komunikat i pobierz informację ponownie.

Zaimplementuj to na dwa sposoby
- pierwszy - obsługa wyjątku przez metodę pobierającą,
- drugi - metoda rzucająca wyjątek (
 */

import java.util.Scanner;

public class Zadanie129 {
    public static void main(String[] args) {
        //OBSLUGA WYJATKU WEWNATRZ METODY
        System.out.println(zwrocLiczbe());

        //Metoda rzucajaca wyjatek
        try {
            System.out.println(zwrocLiczbe2());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static int zwrocLiczbe() {
        Scanner s = new Scanner(System.in);
        int liczba;
        try {
            System.out.println("Podaj liczbe:");
            liczba = s.nextInt();
        } catch (Exception e) {
            liczba = 0;
        }
        return liczba;
    }

    static int zwrocLiczbe2() throws Exception {
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }

}

