package com.company.zadania.zadanie170;
/*
TYPY GENERYCZNE - poznajemy po ostrych nawiasach np List <>
 */

import com.company.zadania.zadanie170.interf.FruitInterfaceBox;
import com.company.zadania.zadanie170.interf.FruitInterfacee;
import com.company.zadania.zadanie170.methods.ReturnMethods;
import com.company.zadania.zadanie170.methods.VoidMethds;
import com.company.zadania.zadanie170.object.ObjectFruitBox;
import com.company.zadania.zadanie170.shape.*;
import com.company.zadania.zadanie170.simplegeneric.BiggerFruitBox;
import com.company.zadania.zadanie170.simplegeneric.FruitBox;
import com.company.zadania.zadanie170.without.Apple;
import com.company.zadania.zadanie170.without.AppleBox;
import com.company.zadania.zadanie170.without.Orange;
import com.company.zadania.zadanie170.without.OrangeBox;


public class Zadanie170 {
    public static void main(String[] args) {
        //dedykowana klasa opakowujaca dla azdej z klas w srodku co
        // powoduje duplikacje rozwiazan
//        method1();
        /**
         *klasa opakowujaca akceptuje obiekty klasy "Object" co powoduje
         * "zaakceptowanie obiektó klass
         * Orange i Apple(po ktorej dziedzicza)
         *
         * Akceptowane są także WSZTYSTKIE Oobiekty klas,
         * które dziedziczą po kkasie Object.
         *
         * Czyli wewnatrz klasy opakowujacej jestesmy w stanie umiescic ABSOLUTNIE
         * dowolny typ(string lub Bye)
         */
//        method2();
        /**
         * klasa opakowujaca akceptuje tylko i wylacznie
         * obiekt tych klas, ktore implementuja interface "FruitInterfacee"
         */
//        method3();
        /**
         * klasa opakowujaca jest klasa generyczna czyli przy deklarracji musimy podac
         * jaki typ elem. musi znajdowac sie w srodku;
         */
//        method4();
        /**
         *klasa opakowujaca jest kalsa generyczna.
         *
         * Przyjmuje dwa obiekty klas, ktore musimy podac przy tworzeniu obiektu
         */
//        method5();
        /**
         * sprawdzamy,że generyczne klasy są w stanie przyjąć tylko te obiekty ,
         * których typ został podany przy deklaracji obiektu
         * czyli dla "
         * List<Integer> a= new ArrayList<>();
         * mogę dodać ob. klasy Int oraz obiekty wszyttkich klas
         * dziedziczących po klasie Intiger
         *
         * Nie jesteśmy w stanei dodac obiektów zadnej " nadklasy"
         */
//        method6();
        /**
         *
         */
//        method7();
        /**
         * metody ktore w sygnaturze maja typ generyczny
         */
        method8();
    }

    private static void method8() {
        String imie = "natalia";
        ReturnMethods.method5(imie);
        ReturnMethods.method5(4);
        ReturnMethods.method5('a');

        String napis = ReturnMethods.method6("Pies");
        int liczba = ReturnMethods.method6(5);
        System.out.println(napis);
        System.out.println(liczba);

        ReturnMethods.method7(new Circle());

        ReturnMethods.method8("niedziela", false);

        System.out.println(ReturnMethods.method9(new Rectangle(), new Orange()));

    }

    private static void method7() {
        ShapeBox<Circle> circleBox = new ShapeBox<>(new Circle());
        ShapeBox<Rectangle> rectangleBox = new ShapeBox<>(new Rectangle());
        ShapeBox<Square> squareBox = new ShapeBox<>(new Square());

        ShapeBox<Shape> shapeBox = new ShapeBox<>(new Rectangle());

        //wymagana akceptowana klasa podana jest w sygnaturze metody
        VoidMethds.method1(circleBox);
//        VoidMethds.method1(rectangleBox); nie zadziała
//        VoidMethds.method1(squareBox); nie zadziałą

        /**
         * akceptowanu jest dowolny ShapeBox
         */

        VoidMethds.method2(circleBox);
        VoidMethds.method2(rectangleBox);
        VoidMethds.method2(squareBox);

        /**
         * akceptowane sa klasy ktore dziedzicza po Rectangle
         */
//        VoidMethds.method3(circleBox);
        VoidMethds.method3(rectangleBox);
        VoidMethds.method3(squareBox);

        /**
         *
         */
//        VoidMethds.method4(circleBox); nie wejdzie
        VoidMethds.method4(rectangleBox);
//        VoidMethds.method4(squareBox); nie wejdzei
    }


    private static void method6() {
        Rectangle rectangle = new Rectangle();
        //kalsa opakowujaca, przy ktorej tworzeniu "musimy okreslic typ elementow w srodku
        ShapeBox<Rectangle> box1 = new ShapeBox<>(rectangle);
        //wywolujemy metode na elemencie ktory jest "w srodku"
        System.out.println(box1.getNameOfShape());

        Circle circle = new Circle();
        ShapeBox<Circle> box2 = new ShapeBox<>(circle);
        System.out.println(box2.getNameOfShape());


    }

    private static void method5() {
        String imie = "Kuba";
        Integer wiek = 38;
        //utw. generycznej klasy opakowujacej;
        // elem nie moga byc typy prymitywne - moga byc instancje klas
        BiggerFruitBox<String, Integer> box = new BiggerFruitBox<>(imie, wiek);


        System.out.println(box.getFirst());
        System.out.println(box.getSecond());

        BiggerFruitBox<String, String> box2 = new BiggerFruitBox<>("ala", "ma kota");

    }

    private static void method4() {
        Apple apple = new Apple();

        //Utworzenie Generycznej klasy opakowujacej
        FruitBox<Apple> box = new FruitBox<>(apple);
        //pobranie obiektu ze środka obiektu opakowujaceko
        Apple fruit = box.getFruit();

        Orange orange = new Orange();
        //    box.setFruit(orange); to sie nie uda - BŁĘDNA OPERACJA, nie ma możliwości zmiany

        FruitBox<Orange> box2 = new FruitBox<>(orange);
        box2.setFruit(orange);

        boolean czyZima = true;
        FruitBox<Boolean> box3 = new FruitBox<>(czyZima);
    }

    private static void method3() {
        //
        Apple apple = new Apple();

        FruitInterfaceBox box = new FruitInterfaceBox(apple);
        System.out.println(box.getFruit().getClass().getSimpleName());

        FruitInterfacee fruit = (FruitInterfacee) box.getFruit();
        Apple appleFruit = (Apple) fruit;

        Orange orange = new Orange();
        box.setFruit(orange);

        //oiekt klasy Object nie jest akceptowalny
        //poniewaz nie implementuje wybranego interfejsu
        // box.setFruit(new Object());  <-- źle


    }

    private static void method2() {
        Apple apple = new Apple();

        ObjectFruitBox box = new ObjectFruitBox(apple);
        //sprawdzamy czy element w środku jest instatncją(obietem) klasy "Apple"
        System.out.println(box.getFruit() instanceof Apple);

        //tworzymy nowy obiekt
        Orange orange = new Orange();
        //zmieniamy element w środku
        box.setFruit(orange);
        //spr. czy obiekt w środku jest instancją klasy "Orange"
        System.out.println(box.getFruit() instanceof Orange);

        //Pobieram element "ze srodka"
        Object fruit = box.getFruit();
        //pobieram nazwę klasy elementu "ze środka"
        String className = fruit.getClass()
                .getSimpleName();
        System.out.println(className);

        //rzutowanie by zwracany obiekt był traktowany jak obiekt klasy "Orange"
        Orange orangeFruit = (Orange) box.getFruit();
        System.out.println(orangeFruit);

        //tworzymy nowa zmienna ktora umiescimy w pudeku
        String info = "Ala ma kota.";
        //zmiana elementu w środku
        box.setFruit(info);
        //pobieranie elem.ze środka
        Object fruit2 = box.getFruit();

        //pobieram nazwe klasy elem ze srodka
        String className2 = fruit2.getClass().getSimpleName();
        System.out.println(className2);

    }

    private static void method1() {
        Apple apple = new Apple();
        AppleBox box = new AppleBox(apple);
        box.getApple();

        Orange orange = new Orange();
        OrangeBox box1 = new OrangeBox(orange);
        box1.getOrange();

    }
}
