package com.company.zadania.zadanie170.methods;

import com.company.zadania.zadanie170.shape.Shape;
import com.company.zadania.zadanie170.without.Orange;

public class ReturnMethods {
    public static <A> void method5(A el) {
        System.out.println("wrzucono: " + el.getClass().getSimpleName());

    }

    public static <TYPE> TYPE method6(TYPE element) {
        return element;
    }

    public static <T extends Shape> T method7(T shape) {
        shape.hello();
        return shape;
    }

    public static <A> A method8(A first, A second) {
        System.out.print("do metody przekazano zmienną typu: ");
        System.out.println(first.getClass().getSimpleName());
        System.out.print("oraz: ");
        System.out.println(second.getClass().getSimpleName());

        return first;
    }

    public static <T extends Shape, K extends Orange> boolean method9(T first, K second) {
        return first.equals(second);
    }
}
