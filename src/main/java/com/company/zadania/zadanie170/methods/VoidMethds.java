package com.company.zadania.zadanie170.methods;

import com.company.zadania.zadanie170.shape.Circle;
import com.company.zadania.zadanie170.shape.Rectangle;
import com.company.zadania.zadanie170.shape.Shape;
import com.company.zadania.zadanie170.shape.ShapeBox;

public class VoidMethds {
    // wymagamy ShapeBox z "Circle" wśrodku
    public static void method1(ShapeBox<Circle> box) {
        System.out.println(box.getNameOfShape());

    }

    //shapeBox z "czymś w środku"
    //nie weryfikujemy
    //ograniczeniem zajmuje się klasa "ShapeBox"
    public static void method2(ShapeBox<?> box) {

    }

    public static void method3(ShapeBox<? extends Rectangle> box) {

    }
// super nie moze istniec w klasach ale moze istnieć w metodach
    //akceptujemy obiekty rypu Rectangle i wszystkie jej nadklasy
    public static void method4(ShapeBox<? super Rectangle> box){

    }
}
