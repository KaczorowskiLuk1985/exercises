package com.company.zadania.zadanie170.simplegeneric;

import org.junit.jupiter.api.Test;

/**
 * T-typ
 */

public class FruitBox <TYPE>{
    private TYPE fruit;

    public FruitBox(TYPE fruit) {
        this.fruit = fruit;
    }

    public TYPE getFruit() {
        return fruit;
    }

    public void setFruit(TYPE fruit) {
        this.fruit = fruit;
    }
}
