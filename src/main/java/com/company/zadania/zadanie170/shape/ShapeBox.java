package com.company.zadania.zadanie170.shape;

/**
 * klasa opakowujaca akceptuje tylko te klasy ktore maja relacje
 *  z klasa shape
 * tzn. po niej dziedzicza i implementuja tn interface
 */

public class ShapeBox<TYPE extends Shape> {
    private TYPE shape;

    public ShapeBox(TYPE shape) {
        this.shape = shape;
    }

    public TYPE getShape() {
        return shape;
    }

    public String getNameOfShape(){
        return shape.getName();
    }
}
