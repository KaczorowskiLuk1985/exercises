package com.company.zadania.zadanie170.shape;

public interface Shape {
    String getName();

    default void hello(){
        System.out.println("hello. i'm shape");
    }
}
