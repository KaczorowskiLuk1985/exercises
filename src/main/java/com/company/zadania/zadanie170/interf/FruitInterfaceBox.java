package com.company.zadania.zadanie170.interf;

import com.company.zadania.zadanie170.without.Apple;
import com.company.zadania.zadanie170.without.Orange;

public class FruitInterfaceBox {
    private FruitInterfacee fruit;

    public FruitInterfaceBox(Apple fruit) {
        this.fruit = fruit;
    }

    public FruitInterfacee getFruit() {
        return fruit;
    }

    public void setFruit(Orange fruit) {
        this.fruit = fruit;
    }
}
