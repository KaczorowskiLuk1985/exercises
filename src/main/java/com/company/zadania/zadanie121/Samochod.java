package com.company.zadania.zadanie121;

public class Samochod extends Pojazd {
    private int liczbaKol = 4;
    private int liczbaDrzwi = 3;

    Samochod(String nazwa, int maxPredkosc, int liczbaPasarzerow) {
        super(nazwa, maxPredkosc, liczbaPasarzerow);
    }


    @Override
    void przedstawSie() {
        System.out.println("samochód");
    }
}

