package com.company.zadania.zadanie121;

public class Zadanie121 {
    public static void main(String[] args) {
        Samochod samochod = new Samochod("volvo", 200, 4);
        Pociag pociag = new Pociag("PKP", 55, 100);
        Samochod samochod1 = new Samochod("volco", 433, 3);
        //Pojazd pojazd = new Pojazd("volvo",100,4);
        // nie jestesmy w stanie tworzyc obiektow abstrakcyjnych
        pociag.przedstawSie();
        samochod.przedstawSie();
    }

}
