package com.company.zadania.zadanie121;

abstract class Pojazd { // abstract class = nie mozna utworzyc obiektu
    //metoda musi zostac nadpisana

    private String nazwa;
    private int maxPredkosc;
    private int liczbaPasarzerow;

    Pojazd(String nazwa, int maxPredkosc, int liczbaPasarzerow) {
        this.nazwa = nazwa;
        this.maxPredkosc = maxPredkosc;
        this.liczbaPasarzerow = liczbaPasarzerow;
    }

    abstract void przedstawSie(); //jesli metoda jest abstrakcyjna kazda klasa bedzie dziedziczona

}
