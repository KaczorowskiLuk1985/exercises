package com.company.zadania;
/*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
a następnie wyświetla zawartość tego pliku na konsolę. Zrealizuj zadanie odczytując dane “znak po znaku”.
 */

import java.io.FileInputStream;
import java.io.IOException;

public class Zadanie132 {
    public static void main(String[] args) {
        try {
            odczytajPlikZnakPoZnaku("company/zadanie132.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void odczytajPlikZnakPoZnaku(String sciezkaDoPliku) throws IOException {
        FileInputStream plik = new FileInputStream(sciezkaDoPliku);
        int znak = plik.read();
        while (znak != -1) {
            System.out.print((char) znak);
            znak = plik.read();

        }
    }
}
