package com.company.zadania.Zadanie157;

import java.io.*;

public class Zadanie157 {
    public static void main(String[] args) {
        Wiadomosc157 wiadomosc = new Wiadomosc157("Ala", "Ala ma kota");
//        zapisDoPliku(wiadomosc);
        zapisDoPliku2(wiadomosc);
        Wiadomosc157 odczytanaWiadomość = odczytZPliku();
        System.out.println(odczytanaWiadomość);

    }

    static Wiadomosc157 odczytZPliku() {
        try (
                FileInputStream file = new FileInputStream("pliki/Zadanie157.txt");
                ObjectInputStream input = new ObjectInputStream(file);
        ) {
            Wiadomosc157 wiadomosc = (Wiadomosc157) input.readObject();
            return wiadomosc;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    static void zapisDoPliku(Wiadomosc157 wiadomosc) {
        try {
            FileOutputStream file = new FileOutputStream("pliki/zadanie157.txt");
            ObjectOutputStream outPut = new ObjectOutputStream(file);
            outPut.writeObject(wiadomosc);
            outPut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void zapisDoPliku2(Wiadomosc157 wiadomosc) {
        //try with resourses (obiekty sa zamykane, nie potrzebujemy close
        try (
                FileOutputStream file = new FileOutputStream("pliki/zadanie157.txt");
                ObjectOutputStream outPut = new ObjectOutputStream(file)
        ) {
            outPut.writeObject(wiadomosc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
