package com.company.zadania.Zadanie157;

import java.io.Serializable;

public class Wiadomosc157 implements Serializable {
    private String title;
    private String content;

    public Wiadomosc157(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        return "Wiadomosc157{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
