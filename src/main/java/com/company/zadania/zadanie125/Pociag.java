package com.company.zadania.zadanie125;

public class Pociag extends Pojazd {
    private int liczbaWagonow;
    private boolean czyMaWagonBarowy;

    Pociag(String nazwa, int liczbaWagonow, boolean czyMaWagonBarowy) {
        super(nazwa);
        this.liczbaWagonow = liczbaWagonow;
        this.czyMaWagonBarowy = czyMaWagonBarowy;
    }

    @Override
    public String toString() {
        return String.format("Pociąg %s ma %s wagonów %s. %s %s",
                nazwa,
                liczbaWagonow,
                czyMaWagonBarowy ? "i ma wagon barowy" : "ale nie ma wagonu barowego",
                maxPredkosc > 0 ? "Maksymalna prędkość to: " + maxPredkosc : "",
                liczbaPasazerow > 1 ? "Maksymalna liczba pasażerów to: " + liczbaPasazerow : "");
    }
}
