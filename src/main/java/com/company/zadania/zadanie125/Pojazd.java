package com.company.zadania.zadanie125;

abstract class Pojazd implements CharekterystykaPojazdu {
    protected String nazwa;
    protected int maxPredkosc;
    protected int liczbaPasazerow;

    public Pojazd(String nazwa) {
        this.nazwa = nazwa;
    }

    @Override
    // final - nie zmienia swojej wartości (po klasie final nie można dziedziczyć)
    public final int maxPredkosc() {
        return maxPredkosc;
    }

    @Override
    public final int liczbaPasazerow() {
        return liczbaPasazerow;
    }

    final void ustawParametry(int maxPredkosc, int liczbaPasazerow) {
        this.maxPredkosc = maxPredkosc;
        this.liczbaPasazerow = liczbaPasazerow;
    }
}
