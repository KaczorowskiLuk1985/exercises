package com.company.zadania.zadanie125;

import java.util.ArrayList;
import java.util.List;

/*
Utwórz klasę `Pojazd` (z polami `nazwa`) a następnie dziedziczące klasy klasy
- `Samochod` (z polami `liczbaKol` i `liczbaDrzwi`)
- `Pociag` (z polami `liczbaWagonow`, `czyMaWagonBarowy`)

Utwórz interfejs `CharakterystykaPojazdu`, zawierający metody `maxPredkosc()` oraz `liczbaPasazerow()`.
 */
public class Zadanie125 {
    public static void main(String[] args) {
        Samochod s1 = new Samochod("Volvo", 4, 5 );
        Pociag p1 = new Pociag("ICC", 17, false);
        System.out.println(s1);
        System.out.println(p1);
        s1.ustawParametry(190, 5);
        p1.ustawParametry(160, 200);
        System.out.println(s1);
        System.out.println(p1);

        List<CharekterystykaPojazdu> lista = new ArrayList<>();
        lista.add(s1);
        lista.add(p1);

        for (CharekterystykaPojazdu charekterystykaPojazdu : lista) {
            System.out.println(charekterystykaPojazdu.liczbaPasazerow());
        }


    }
}
