package com.company.zadania.zadanie125;

public class Samochod extends Pojazd {
    private int liczbaKol;
    private int liczbaDrzwi;

    Samochod(String nazwa, int liczbaKol, int liczbaDrzwi) {
        super(nazwa);
        this.liczbaKol = liczbaKol;
        this.liczbaDrzwi = liczbaDrzwi;
    }


    @Override
    public String toString() {
        return String.format("Car %s, ma %s koła i %s drzwi. %s %s",
                nazwa,
                liczbaKol,
                liczbaDrzwi,
                maxPredkosc > 0 ? "Maksymalna prędkość to: " + maxPredkosc : "",
                liczbaPasazerow > 1 ? "Maksymalna liczba pasażerów to: " + liczbaPasazerow : "");
    }
}
