package com.company.zadania.zadanie161;

public class Konfiguracja {
    private int ulubionaLiczba;

    private static Konfiguracja instance = null;

    private Konfiguracja() { //prywatny konstruktor, mega istotne

    }

    public static Konfiguracja getInstance() {
        if (instance == null) {
            instance = new Konfiguracja();
            System.out.println("jej piewszy raz");
        } else {
            System.out.println("pole juz isnieje");
        }
        System.out.println(instance.hashCode());
        return instance;
    }

    public int getUlubionaLiczba() {
        return ulubionaLiczba;
    }

    public void setUlubionaLiczba(int ulubionaLiczba) {
        this.ulubionaLiczba = ulubionaLiczba;
    }
}
