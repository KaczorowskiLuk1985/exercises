package com.company.zadania.zadanie161;
/*
singletone, prywatny konstruktor, w praktyce używa się listy pól.
 */

public class Zadanie161 {
    public static void main(String[] args) {
        Konfiguracja config = Konfiguracja.getInstance();
        config.setUlubionaLiczba(7);

        wyswietl();
        config.setUlubionaLiczba(88);
        wyswietl();
    }

    static private void wyswietl() {
        Konfiguracja config = Konfiguracja.getInstance();
        System.out.println(config.getUlubionaLiczba());
    }
}
