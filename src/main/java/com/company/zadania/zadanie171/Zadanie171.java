package com.company.zadania.zadanie171;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie171 {
    public static void main(String[] args) {
        List<GenericClass<String, Integer, Boolean>> list = Arrays.asList(
                new GenericClass<>("adam", 12, true),
                new GenericClass<>("emi", 33, false),
                new GenericClass<>("kamil", 333, true)
        );
        list.forEach(System.out::println);

        List<GenericClass<Integer, Integer, Integer>> list2 = Arrays.asList(
                new GenericClass<>(1, 12, 5),
                new GenericClass<>(3, 33, 5),
                new GenericClass<>(4, 333, 6)
        );
        list2.forEach(System.out::println);

    }
}
