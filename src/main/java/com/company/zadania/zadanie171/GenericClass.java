package com.company.zadania.zadanie171;

public class GenericClass<A, B, C> {

    private A first;
    private B sec;
    private C third;

    public GenericClass(A first, B sec, C third) {
        this.first = first;
        this.sec = sec;
        this.third = third;
    }

    @Override
    public String toString() {
        return "GenericClass{" +
                "first=" + first +
                ", sec=" + sec +
                ", third=" + third +
                '}';
    }
}
