package com.company.zadania.zadanie165;

import com.company.zadania.zadanie165.DocumentGenerator.DocumentType;

//fabryka wpływa na obiekty ktore wychodza
public class Zadanie165 {
    public static void main(String[] args) {
        DocumentGenerator generator = new DocumentGenerator();

        String text = "Jest niedziela\n ale nie pada śnieg";

        Document txt = generator.createDocument(text, DocumentType.TXT);
        txt.saveFile("zadanie_165");

        Document html = generator.createDocument(text, DocumentType.HTML);
        html.saveFile("zadanie_165");
    }
}
