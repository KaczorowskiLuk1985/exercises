package com.company.zadania.zadanie165;

public class DocumentGenerator {
    Document createDocument(String textToSave, DocumentType type) {

        Document document = null;
        switch (type) {
            case TXT:
                document = new TxtDocument(type,textToSave);
                break;
            case HTML:
                //TODO handle HTML documents
                break;
        }
        return document;
    }

    public enum DocumentType {
        TXT("txt"),
        HTML("html");

        private String extension;

        DocumentType(String extension) {
            this.extension = extension;
        }

        public String getExtension() {
            return extension;
        }
    }
}
