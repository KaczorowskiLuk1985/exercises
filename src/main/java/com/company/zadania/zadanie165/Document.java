package com.company.zadania.zadanie165;

import com.company.zadania.zadanie165.DocumentGenerator.DocumentType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Document {
    private DocumentType type;
    private String content;

    public Document(DocumentType type, String content) {
        this.type = type;
        this.content = content;
    }

    void saveFile(String fileName) {
        String outPtPath = String.format(
                "pliki/%s.%s",
                fileName,
                type.getExtension());

        File file = new File(outPtPath);

        try (FileWriter writer = new FileWriter(file)) {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
