package com.company.zadania.zadanie175;

// chodzi o to ze wykonujac wiele operacji naraz mozemy skozystac z modyfikatorow ktore nam to ograniają

public class Konto {
    private int numer;
    private String wlasciciel;
    private volatile int stanKonta; //pozwala na kozystanie tlko z jednego wątku
    //słuzy tylko do pól

    public Konto(int numer, String wlasciciel, int stanKonta) {
        this.numer = numer;
        this.wlasciciel = wlasciciel;
        this.stanKonta = stanKonta;

    }

    public int getNumer() {
        return numer;
    }

    public String getWlasciciel() {
        return wlasciciel;
    }

    public int getStanKonta() {
        return stanKonta;
    }

    @Override
    public String toString() {
        return "numer = " + numer + ", wlasciciel = " + wlasciciel + ", stanKonta = " + stanKonta;
    }

    synchronized void wlata(int kwotaWplaty) { //rozwiazuje probem. jesli jakas metoda
                                              // wykonuje operacje na polu to nie dopuszcza innych oeracji
        stanKonta += kwotaWplaty;

    }

    synchronized void wyplata(int kwotaWyplaty) {
        if (stanKonta < kwotaWyplaty) {
            throw new IllegalArgumentException("brak srodkow na koncie - do pracy"); //zły argument
        }
        stanKonta -= kwotaWyplaty;
    }
}
