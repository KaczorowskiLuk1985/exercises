package com.company.zadania.zadanie175;

import java.util.TreeMap;

public class zadanie175 {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        Konto konto = new Konto(1, "Janek", 20000);

        final int ileRazy = 1000;
        final int kwota = 25;

        Thread wplac = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wlata(kwota);
                }
            }
        });
        Thread wyplac = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy-2; i++) {
                    konto.wyplata(kwota);
                }
            }
        });
        //te operacje nie wykonuja się synchronicznie
        System.out.println(konto);
        wplac.start();
        wyplac.start();
        try {
            wplac.join();
            wyplac.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(konto);


    }
}
