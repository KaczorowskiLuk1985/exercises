package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`) oraz poszukiwana liczba.

Metoda ma za zadanie zwrócić *listę pozycji* na których znajduje się liczba (przekazana jako drugi parametr).
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie103 {
    public static void main(String[] args) {
        // Integer[] tablica = {1,2,3,4,5,6,7,5,3,3,24,3}
        //List<Integer> lista = new ArrayList<>(Arrays.asList(tablica))

        List<Integer> lista = new ArrayList<>(Arrays.asList(
                1,2,3,8,8,4,5,6,7,7565,8,87,86
        ));
        System.out.println(zwrocListePozycji(lista, 8));

    }
    public static List<Integer> zwrocListePozycji(List<Integer> lista, int poszukiwana){
        List<Integer> nowaLista = new ArrayList<>();
        for (int pozycja = 0; pozycja < lista.size(); pozycja++) {
            if (lista.get(pozycja) == poszukiwana){
                nowaLista.add(pozycja);

            }

        }
        return nowaLista;
    }
}
