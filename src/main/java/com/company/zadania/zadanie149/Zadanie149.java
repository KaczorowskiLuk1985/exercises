package com.company.zadania.zadanie149;
/*
Utwórz metodę, do której przekazujesz trzy parametry - pierwsze dwa to jest przedział.
Metoda powinna zwrócić informację czy trzeci parametr znajduje się w tym przedziale
> Dla `1, 10, 5` zwróci `true`
>
> Dla `1, 10, 15` zwróci `false`
>
> Dla `-10, 0, 3` zwróci `false`
>
> Dla `-10, 10, 0` zwróci `true`
>
> Dla `4, 8, 4` zwróci `true`
>
> Dla `3, 3, 3` zwróci `true`
 */

public class Zadanie149 {
    public boolean czyZnajdueSieWPRzedziale(int start, int stop, int liczba){
        if (liczba >=start && liczba <= stop){
            return true;
        }else {
            return false;

        }
    }
}
