package com.company.zadania.zadanie123;

public class Koło extends Kształt {
    int promien;

    public Koło(int promien) {
        this.promien = promien;
    }

    @Override
    String wyswietlInformacjeOSobie() {
        return String.format("jestem kołem o promieniu %s",
                promien);
    }

    @Override
    public double policzPole() {

        return Math.PI*promien*promien;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return policzPole()*wysokosc;
    }

    @Override
    public double policzObwod() {
        return 2 * Math.PI*promien;
    }
}
