package com.company.zadania.zadanie123;

public interface Obliczenia {
    double policzPole();
    double policzObjetosc(int wysokosc);
    double policzObwod();

}
