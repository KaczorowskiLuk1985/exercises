package com.company.zadania.zadanie123;

class Prostokat extends Kształt {
    protected int dlugosc;
    protected int szerokosc;

    public Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    @Override
        // ta metoda jest gdzies wyzej lub wymusilo jej inplementacje
    String wyswietlInformacjeOSobie() {

        return String.format(
                "jestem prostkokatem a moje boki to %s i %s",
                dlugosc,
                szerokosc);
    }

    @Override
    public double policzPole() {
        return dlugosc * szerokosc;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return policzPole() * wysokosc;
    }

    @Override
    public double policzObwod() {
        return 2 * dlugosc + 2 * szerokosc;
    }
}
