package com.company.zadania.zadanie123;
/*
Utwórz klasę `Ksztalt` oraz klasy dziedziczące po niej:
- klasę `Prostokat` (z polami `dlugosc`, `szerokosc`),
- klasę `Kwadrat` (z polem `dlugosc`),
- klasę `Koło` (z polem `promien`).
Zablokuj możliwość tworzenia obiektów klasy `Ksztalt` oraz wymuś,
by klasy dziedziczące musiały zaimplementować metodę `wyswietlInformacjeOSobie()` (zwracająca `String`).

Utwórz interfejs `Obliczenia` w którym będą metody `policzPole()`, `policzObwod()`
oraz `policzObjetosc(int wysokosc)`. Interfejs ten zaimplementuj we wcześniej utworzonych klasach.
 */

import java.util.ArrayList;
import java.util.List;

public class Zadanie123 {
    public static void main(String[] args) {
        Prostokat prostokat = new Prostokat(4,5);
       // System.out.println(prostokat.wyswietlInformacjeOSobie());
        Kwadrat kwadrat = new Kwadrat(4);
       // System.out.println(kwadrat.wyswietlInformacjeOSobie());
        Koło kolo = new Koło(5);
       // System.out.println(kolo.wyswietlInformacjeOSobie());

        List<Kształt> listaKsztaltow = new ArrayList<>();
        listaKsztaltow.add(prostokat);
        listaKsztaltow.add(kwadrat);
        listaKsztaltow.add(kolo);

        for (Kształt kształt : listaKsztaltow) {
            System.out.println(kształt.wyswietlInformacjeOSobie());
            // metoda  policzPole() jest możliwy dzieki interface
            System.out.printf("pole wynosi %.3f\n", kształt.policzPole()); //%.2f = po kropce ma zajac dwa miejsca
            // cofanie zmian w gicie - obadaj.
        }
    }
}
