package com.company.zadania.zadanie123;

public class Kwadrat extends Prostokat {
    Kwadrat(int dlugosc) {
        super(dlugosc, dlugosc); // wymasza zeby byly dwa parametry. wywolywany jest konstruktor nadklasy
    }



    @Override
    String wyswietlInformacjeOSobie() {
        return String.format("jestem kwadratem o bokach %s i %s",
                dlugosc, //protrcted dziala dla class dziedziczących i działa tak samo jak pakietowy czyli dla pakietu(?)
                dlugosc);
    }

}
