package com.company.zadania;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby które są indexami (pozycjami)
zakresu.
Metoda ma zwrócić sumę elementów w podanym przedziale.
Dla `([1, 2, 3, 4, 5], 2, 4)`
>zwróci `12`, bo `3 + 4 + 5`
 */


public class Zadanie49 {
    public static void main(String[] args) {
        int[] zakres = new int[]{1, 2, 3, 4, 5, 6, 7};
        System.out.println(zwrocSumeElementow(zakres, 1, 7));
    }

    static int zwrocSumeElementow(int[] tablica, int start, int stop) {
        int suma = 0;

        if (start > stop || start < 0 || stop > tablica.length - 1) {
            System.out.println("błędny zakres");
        } else {
            for (int index = start; index <= stop; index++) {
                suma += tablica[index];
            }
        }

        return suma;
    }
}
