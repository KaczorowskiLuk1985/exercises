package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę (wartości `short`) oraz liczbę.
 Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy.
 */

public class Zadanie39 {
    public static void main(String[] args) {
        short tablica[] = new short[]{1, 5, 10};
        short zmienna = 7;
        boolean wynik = czyZnajdujeSię(tablica, zmienna);
        System.out.println(wynik ? " jest w tablicy" :
                "nie ma w tablicy");

    }

    static boolean czyZnajdujeSię(short[] tablica, short poszukiwana) {
        for (short liczba : tablica) {
            if (liczba == poszukiwana) {
                return true;
            }
        }
        return false;
    }
}
