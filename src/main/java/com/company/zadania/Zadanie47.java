package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz wartość logiczną.
Gdy drugi parametr ma wartość `true` metoda ma zwrócić *największa* liczbę z tablicy.
Gdy parametr będzie miał wartość `false`, metoda ma zwrócić *najmniejszą* liczbę z tablicy.
 */

public class Zadanie47 {
    public static void main(String[] args) {
        int[] tablica = new int[]{2, 2, 2,2, 2};

        System.out.println(zwrociMaxLubMin(tablica, false));
        System.out.println(zwrociMaxLubMin(tablica, true));

    }

    public static int zwrociMaxLubMin(int[] table, boolean isMax) {

        int max = table[0];
        int min = table[0];
        for (int i = 1; i < table.length; i++) {
            if (table[i] > max) {
                max = table[i];

            }
            if (table[i] < min) {
                min = table[i];
            }

        }
        return isMax ? max : min; //przyda sie, jesli mamy wartos logiczna to mozna uzyc skrótu "nazwa boolean" ? "nazwa int" : "nazwa int"
    }
}
