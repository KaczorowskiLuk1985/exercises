package com.company.zadania;

public class Zadanie10 {
    public static void main(String[] args) {
        System.out.println(trueOrFalse(5));
        System.out.println(trueOrFalse(25));

        System.out.println(trueOrFalse(3));
        System.out.println(trueOrFalse(40));

        int liczba = 20;
        boolean wynik = trueOrFalse(liczba);
        if (wynik) {
            System.out.println("liczba " + liczba + " jest większa od 20");
        } else {
            System.out.println("liczba " + liczba + " jest mniejsza od 20");
        }

    }

    static boolean trueOrFalse(int x) {
        if (x >= 20) {
            return true;

        } else {
            return false;
        }


    }
}
