package com.company.zadania;

public class Zadanie6 { //metody
    public static void main(String[] args) {
        metoda(2,5);
        operacjaMatematyczna(10, 20);

    }

    static void operacjaMatematyczna(int a, int b) {
        System.out.printf("%s + %s = %s\n", a, b, a + b);
        System.out.printf("%s - %s = %s\n", a, b, a - b);
        System.out.printf("%s * %s = %s\n", a, b, a * b);
        System.out.printf("%s / %s = %s\n", a, b, a / b);
    }

    static void metoda(float a, int b) {
        System.out.println(a + " + " + b + " = " + (a + b));
        System.out.println(a + " - " + b + " = " + (a - b));
        System.out.println(a + " * " + b + " = " + (a * b));
        System.out.println(a + " / " + b + " = " + (a / b));

    }
}

