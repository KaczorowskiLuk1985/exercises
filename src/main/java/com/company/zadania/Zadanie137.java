package com.company.zadania;
/*
Utwórz metodę która odczytuje zawartości ze Scannera (do momentu podania pustej linii) i zapisuje wartości do pliku
 */

import java.io.*;
import java.util.Scanner;

public class Zadanie137 {
    public static void main(String[] args) {
        try {
            zapisDoPlikou();
        }catch (IOException e){
            e.printStackTrace();
        }

    }
    private static void zapisDoPlikou() throws IOException {
        Scanner scanner = new Scanner(System.in);

        FileWriter writer = new FileWriter("pliki/Zadanie137");
        BufferedWriter bufferedWriter = new BufferedWriter(writer);

        while (true){
            System.out.println("wprowadz cos...");
            String linia = scanner.nextLine();
            if (linia.equals("")){ //jezeli linia jest pusta.
                break;

            }else {
                bufferedWriter.write(linia);
                bufferedWriter.newLine();
            }
        }
        bufferedWriter.flush();
        bufferedWriter.close();
        System.out.println("zakonczono dodawanie danych");
    }
}
