package com.company.zadania;
/*
*ZADANIE #14*
Utwórz metodę, która zwróci informację (w formie wartości logicznej) czy przekazana liczba jest parzysta czy nie.
>Dla `12` zwroci `true`
>
>Dla `9` zwróci `false`
>
>Dla `1` zwróci `false`
>
>Dla `40` zwróci `true`
>
>Dla `123` zwróci `false`
 */

public class Zadanie14 {
    public static void main(String[] args) {
        System.out.println(czyParzysta(12));
        System.out.println(czyParzysta(13));
    }

    static boolean czyParzysta(int liczba) {
        if (liczba % 2 == 0) {
            return true;  //modny zapis
        }
        return false;
    }

    static boolean czyParzysta2(int liczba) {
        return (liczba % 2 == 0);
    }
}