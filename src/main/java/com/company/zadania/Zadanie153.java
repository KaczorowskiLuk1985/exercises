package com.company.zadania;
/*
rekrutacja zadanie 1
 */

public class Zadanie153 {
    public static void main(String[] args) {
        long a = 60000;
        long b = 5_000_000L;
        System.out.println(howManyTimesSquare(a, b));

    }

    public static int howManyTimesSquare(long A, long B) {
        int maxSquare = 0;

        for (long number = A; number <= B; number++) {
            int counter = suareRootCounter(number);
            if (counter > maxSquare) {
                maxSquare = counter;
            }
        }
        return maxSquare;
    }

    static int suareRootCounter(long number) {
        int maxHowManyTimesSqet = 0;

        double sqrt = Math.sqrt(number);
        while (sqrt % 1 == 0) {
            maxHowManyTimesSqet++;
            sqrt = Math.sqrt(sqrt);
        }
        //tutaj zostawiamy pustą linijkę
        return maxHowManyTimesSqet;
    }
}
