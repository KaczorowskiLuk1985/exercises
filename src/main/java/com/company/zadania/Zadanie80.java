package com.company.zadania;
/*
Utwórz metodę, która przyjmuje liczbę w formie napisu, a następnie ma zwrócić (jako `String`) sumę wszystkich cyfr
Dla `"536"`, zwróci `"14"` (bo 5 + 3 + 6 = 14)
Dla `"12345"`, zwróci `"15"`
 */

public class Zadanie80 {
    public static void main(String[] args) {
        System.out.println(zwrocSume("135783"));
        System.out.println(zwrocSume("11116"));

    }
    static String zwrocSume (String liczba){
       String[] tablic = liczba.split("");         //chatAt() znajduje znak w wyrazie
       int suma = 0;                    // tnie co znak

        for (String cyfra : tablic) { //iter
            suma += Integer.parseInt(cyfra);  //int typ ptosty, Integer klasa
                     // Integer metoda statyczna pareInt - plusuje się na rozmowach kwal
        }
        return String.valueOf(suma);//metoda jest statyczna bo wywoluje ją na klasie String
    }
}
