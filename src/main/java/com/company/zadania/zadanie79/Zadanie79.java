package com.company.zadania.zadanie79;

import java.util.Scanner;

/*Utwórz klasę `Rownanie` służącą do policzenia równania `a^2 + b^3 + c^4`. Klasa powinna zawierać:
*pola:*
`a`, `b`, `c`

*kostruktory:*
 bezparametrowy
 3-parametrowy

*metody:*
- liczaca wartosc rownania
- przyjmującą liczbę a następnie zwracająca informację (`boolean`)
czy wartość równania przekroczyła podaną liczbą (jako parametr)
*/
public class Zadanie79 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Rownanie rownanie = new Rownanie();//tworze rownanie a pozniej go wypelniam

        System.out.print("A :");
        rownanie.setA(scanner.nextInt());

        System.out.print("B :");
        rownanie.setB(scanner.nextInt());

        System.out.print("C :");
        rownanie.setC(scanner.nextInt());

        System.out.println(rownanie);

        System.out.println(rownanie.oblicz());

        rownanie.wyswietl();
        System.out.println();
        System.out.println(rownanie.czyPodanaLiczbaPrzektoczyłaZakres(92));

    }

}
