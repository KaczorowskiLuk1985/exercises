package com.company.zadania.zadanie79;

public class Rownanie {
    private int a;
    private int b;//shift + alt powiela kursory, alt + kursor zaznacza pole w kwadracie.
    private int c;//to jest pole, (albo pole albo zmienna)
//tylko niestatyczna metoda moze modyfikowac to pole (private)


    Rownanie() {//konstruktor podajemy "pusty" gdy nie znamy jeszcze parametrow,

    }

    Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public String toString() {//sluży do wyswietlenia naszych obiektow. z automatu
        return "Rownanie{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    double oblicz() {
        return Math.pow(a, 2) + Math.pow(b, 3) + Math.pow(c, 4);//metoda statyczna
    }// idzie poznac po tym ze jest wywoływana na klasie czyli "Math"

    void wyswietl() {
        System.out.printf("%s^%s + %s^%s + %s^%s = %s", a, 2, b, 3, c, 4, oblicz());
        //VAR ARGS - zmienne argumenty, variable arguments
    }
    boolean czyPodanaLiczbaPrzektoczyłaZakres(int liczba){
        return liczba > oblicz(); //argument lub parametr
    }
}
