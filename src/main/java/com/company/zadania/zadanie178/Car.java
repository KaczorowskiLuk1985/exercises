package com.company.zadania.zadanie178;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Setter
public class Car {
    private String name;
    @Setter(AccessLevel.NONE)
    private String colour;
    private int yerOfProd;
    @Getter(AccessLevel.NONE) // tutaj blokujemy getter dla numOfDoor
    @Setter(AccessLevel.PRIVATE)
    private int numOfDoor;
}
