package com.company.zadania;

import java.io.*;

public class Zadanie138 {
    public static void main(String[] args) throws IOException {
        try {
            scalPliki("pliki/Zadanie138a.txt", "pliki/Zadanie138b.txt");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void scalPliki(String sciezka1, String sciezka2) throws IOException {
        BufferedReader odczytPierwszegoPliku = new BufferedReader(new FileReader(sciezka1));
        BufferedReader odczytDrugiegoPliku = new BufferedReader(new FileReader(sciezka2));
        BufferedWriter zapisNowegoPliku = new BufferedWriter(new FileWriter("pliki/Zadanie138.txt")); // .txt bo inaczej szuka folderu

        while (true) {
            String liniaPierwszegoPliku = odczytPierwszegoPliku.readLine();
            String liniaDrugiegoPliku = odczytDrugiegoPliku.readLine();
            if (liniaPierwszegoPliku == null && liniaDrugiegoPliku == null) {
                break;
            }
            if (liniaPierwszegoPliku != null) {
                zapisNowegoPliku.write(liniaPierwszegoPliku);
                zapisNowegoPliku.newLine();
            }
            if (liniaDrugiegoPliku != null) {
                zapisNowegoPliku.write(liniaDrugiegoPliku);
                zapisNowegoPliku.newLine();
            }
        }
        zapisNowegoPliku.close(); //zapisywanie pliku,  flusch() zapisuje pojedynczo. close() zapisuje wszystko
    }
}
