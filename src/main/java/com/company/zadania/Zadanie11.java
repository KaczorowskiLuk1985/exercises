package com.company.zadania;

/*
*ZADANIE #11*
Utwórz metodę, która przyjmuje trzy parametry (imie `String`, wiek `int`, płeć `boolean`)
 i *wyświetli* informację (w fomie zdania). Wywołaj metodę wielokrotnie dla różnych parametrów wejściowych.
> Dla przekazanych parametrów `Maciej, 88, true`,
>
>powinno zwrócić `Cześć! Jestem Maciej, mam 88 lat i jestem mężczyzną`
 */
public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println(utworzPowitanie("Ziomek", 88, false));

    }

    static String utworzPowitanie(String imie, int wiek, boolean czyKobieta) {
        String wynik = "Cześć! Jestem " + imie + ", mam " + wiek + " lat i ";
        {
            if (czyKobieta) {
                wynik += "jestem kobietą";
            } else {
                wynik += "jestem mężczyzną";
            }

            return wynik;
        }
    }
}