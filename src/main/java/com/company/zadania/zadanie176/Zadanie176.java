package com.company.zadania.zadanie176;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
//klasa thread
//interface Runable

public class Zadanie176 {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        ExecutorService es1 = Executors.newFixedThreadPool(4);   // podajemy ile watkow ma sie wykonywac
        System.out.println("Wątek: " + Thread.currentThread().getId());
        for (int i = 0; i < 10; i++) { // ogolnie chodzi o to ze wysylamy np partiami maila po 10 szt. co minute az wysle wszystkie (np 1000000 szt)
            //na raz sie nie da bo rozsadzi serwer dlatego lecimy partiami.
            Runnable r = getRunnable();
            es1.submit(r);
        }
        es1.shutdown();
    }

    private static Runnable getRunnable() { //tworzenie metody po zaznaczeniuu calego fragmentu - ctrl + alt + m;
        return new Runnable() {
            @Override
            public void run() {
                System.out.println("Zadanie podjął wątek nr: "
                        + Thread.currentThread().getId());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
