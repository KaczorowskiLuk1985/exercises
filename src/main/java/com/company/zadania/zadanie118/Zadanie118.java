package com.company.zadania.zadanie118;

/*
*ZADANIE #118*
Utwórz klasę `Osoba`, która zawiera pola `imie`, `wiek`.
Następnie utwórz klasę `Pracownik`, która zawiera dodatkowe pola `pensja`.
Następnie utwórz klasę `Kierownik`, która rozszerza klasę `Pracownik` i posiada dodatkowe pole `premia`.
Utwórz obieky wszystkich klas oraz przetestuj działanie.
 */
public class Zadanie118 {
    public static void main(String[] args) {
        Osoba osoba = new Osoba("Janusz", 56);
        System.out.println(osoba.przedstawSie());
        System.out.println();

        Pracownik pracownik = new Pracownik("Halina", 33, 14000);
        System.out.println(pracownik);
        System.out.println(pracownik.zwrocPensjeZaXMiesiecy(5));
        System.out.println();

        Kierownik kierownik = new Kierownik("Brajan", 23, 25000);
        System.out.println(kierownik.zwrocPensjeZaXMiesiecy(3));
        kierownik.ustawPremie(5000);
        System.out.println(kierownik.zwrocPensjeZaXMiesiecy(3));
        System.out.println(kierownik.przedstawSie());
    }


}
