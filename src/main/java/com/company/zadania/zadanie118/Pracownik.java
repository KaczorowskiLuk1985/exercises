package com.company.zadania.zadanie118;

public class Pracownik extends Osoba {
    private int pensja;


    public Pracownik(String imie, int wiek, int pensja) {
        super(imie, wiek);
        this.pensja = pensja;
    }

    @Override
    String przedstawSie() {
        return super.przedstawSie() + " i zarabiam " + pensja + "PLN miesięcznie ";
    }

    @Override
    public String toString() {
        return przedstawSie();
    }

    int zwrocPensjeZaXMiesiecy(int liczbaMiesiecy) {
        return pensja * liczbaMiesiecy;
    }

}
