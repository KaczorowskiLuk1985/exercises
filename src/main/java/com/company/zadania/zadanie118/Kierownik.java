package com.company.zadania.zadanie118;

public class Kierownik extends Pracownik {
    private int premia;

    public Kierownik(String imie, int wiek, int pensja) {
        super(imie, wiek, pensja);
    }

    void ustawPremie(int premia) {
        this.premia = premia;
    }

    @Override
    String przedstawSie() {
        return super.przedstawSie()+"oraz dostałem " + premia + " premii.";
    }
}
