package com.company.zadania.zadanie118;


public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    String przedstawSie() {
        // return "mam na imie " + imie + " i mam " + wiek + "lat";
        return String.format("mam na imie %s i mam %s lat",
                imie, wiek);
    }

    @Override
    public String toString() {//to.String
        return przedstawSie();
    }
}
