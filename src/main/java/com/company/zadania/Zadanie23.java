package com.company.zadania;
/*
*ZADANIE 24*
Utwórz metodę, do której przekazujesz jeden parametr a następnie wyświetlasz tyle wielokrotności liczby `10`
> Dla `4` wyświetli `10, 20, 30, 40`
>
> Dla `7` wyświetli `10, 20, 30, 40, 50, 60, 70`
 */

public class Zadanie23 {
    public static void main(String[] args) {
        warunek6(1,40);

    }
    static void warunek6(int start, int stop){
        int i=start;
        do{
            System.out.println(i + ",");
            i+=2;
        }while(i<=stop);
    }
}
