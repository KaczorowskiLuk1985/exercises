package com.company.zadania.zadanie78;
/*
Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
*pola reprezentujące:*
* dłuższy bok
* krótszy bok

*konstruktory oraz metody służące do:*
* policzenia obwodu
* policzenia pola powierzchni
* porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same
* (zwracająca wartość `boolean`)

*Utwórz tablicę zawierającą 5 obiektów tej klasy i w pętli wyświetl zdania w formie*
`10 x 20 = 200`
 */

public class Zadanie78 {

    public static void main(String[] args) {
        Rectangle prostokat = new Rectangle(2, 4);
        System.out.println(prostokat.polaczObwod());
        System.out.println(prostokat.getDluzszyBok());//pobranie wartosci przz get
        System.out.println(prostokat.getKrotszyBok());
        System.out.println(prostokat.policzPole());

        Rectangle prostokat2 = new Rectangle(5,2);
        System.out.println(Rectangle.czyPolaSatakieSame(prostokat,prostokat2));
        //wyswietla metode statyczna;
    }

}
