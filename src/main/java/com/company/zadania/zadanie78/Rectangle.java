package com.company.zadania.zadanie78;

public class Rectangle {

    private int dluzszyBok; //private. mozna sie odwlolaac do nich tylko w tej klasie
    private int krotszyBok;

    public Rectangle(int dluzszyBok, int krotszyBok) { //to jest konstruktor!
        this.dluzszyBok = dluzszyBok; //dzieki modyfikatorowi dostepu "public" mozemy go uzyc w
        this.krotszyBok = krotszyBok; //dowolnym miejscu calego projektu
        //this. odwoluje sie do obecnego obiektu
        //szukamy "krotszyBok" w polach klasy, omija "int krotszyBok"
    }

    /**
     * metoda
     */
    int polaczObwod() {

        return 2 * dluzszyBok + 2 * krotszyBok;
    }

    int policzPole() {
        return dluzszyBok * krotszyBok;
        //return getDluzszyBok() * getKtotszyBok();
    }

    /**
     * metoda statyczna
     */
    static boolean czyPolaSatakieSame(Rectangle r1, Rectangle r2) {

        return r1.policzPole() == r2.policzPole();
    }

    //getery (2), setery i getery i setery
    public int getDluzszyBok() {

        return dluzszyBok;
    }

    public int getKrotszyBok() {

        return krotszyBok;
    }

//    public void setDluzszyBok(int dluzszyBok) {
//        this.dluzszyBok = dluzszyBok;
//    }
}
