package com.company.zadania.zadanie158;

/*
stream zamienia kolekcje na ciag elementow
 */

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class zadanie158 {
    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();
//        metoda6();
//        metoda7();
//        metoda8();//przelicza tablice
//        metoda9();//przelicza tablice
//        metoda10();
//        metoda11();
//        metoda12();
//        metoda13();
//        metoda14();
//        metoda15();
//        metoda16();
//        metoda17();
//        metoda18();
//        metoda19();
        metoda20();
//        metoda21();
    }

    private static void metoda21() {
        String imionaZListy = listaOsob.stream()
                .map(osoba -> osoba.getImie())
                .collect(Collectors.joining(","));

        System.out.println(imionaZListy);
    }

    private static void metoda20() {

        Map<Integer, List<Person>> map = listaOsob.stream()
                .collect(Collectors.groupingBy(osoba -> osoba.getWiek()));
        System.out.println(map);
    }

    static List<Person> listaOsob = Arrays.asList(
            new Person("Pawełek", 22),
            new Person("Ignacy", 32),
            new Person("Marek", 32),
            new Person("Fanta", 45)
    );

    private static void metoda19() {
        List<Integer> listaWiek = listaOsob.stream()
                .map(osoba -> osoba.getWiek())
                .sorted()
                .collect(Collectors.toList());

        System.out.println(listaWiek);

    }

    private static void metoda18() {
        List<String> listaNapisow = Arrays.asList("a1a", "3a2", "a33", "43");
        boolean wynik = listaNapisow.stream()
                .anyMatch(slowo -> slowo.length() % 2 == 0);

        System.out.println(wynik);
        List<Integer> listaLiczb = Arrays.asList(2, 4, 66, 36, 22, 16, 2);
        boolean wynik2 = listaLiczb.stream()
                .allMatch(liczba -> liczba % 2 == 0);
        System.out.println(wynik2);
    }

    private static void metoda17() {
        Stream.of("a1", "a2", "a3", "a4")
                .filter(slowo -> {
                    System.out.println("teraz filtruję: " + slowo);
                    return true;
                })
                .map(slowo -> {
                    System.out.println("traz mapuje: " + slowo);
                    return slowo;
                })
                .forEach(slowo -> System.out.println("ForEach: " + slowo));
    }

    private static void metoda16() {
        Stream.of("a1", "a2", "a3", "a4")
                .map(slowo -> slowo.substring(1))// mapowac - zamieniac jedna wartosc na druga
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(liczba -> System.out.println(liczba));

    }

    private static void metoda15() {
        List<String> listaNapisow = Arrays.asList("A1", "A2", "A3", "C4", "Q7", "W61", "AA");
        List<Integer> listaRzednycg = listaNapisow
                .stream()
                .filter(slowo -> slowo.length() < 3)
                .map(slowo -> slowo.substring(1))
                .map(slowo -> {//jeśli mamy wiecej niz jedno polecenie otwieramy klamerki {}
                    try {
//ctrl + alt + t - wywolujemy menu w ktorym jest m.in. try catch
                        return Integer.parseInt(slowo);
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        System.out.println(listaRzednycg);
    }

    private static void metoda14() {
        List<String> listaNapisow = Arrays.asList("Kotek", "Rybka", "", "Słoik", "Waszka G");
        List<Integer> dlugoscNapisow = listaNapisow
                .stream()
                .filter(slowo -> !slowo.isEmpty())
                .map(String::length)
                //od tego momentu mamy inty
                .collect(Collectors.toList());
        System.out.println(dlugoscNapisow);
    }

    private static void metoda13() {
        List<String> abc = Arrays.asList("auto", "moto", "", "słoik");
        List<String> kolekcjaSlow = abc
                .stream()
                .filter(slowo -> !slowo.isEmpty())
                .filter(slowo -> slowo.startsWith("m") || slowo.startsWith("s"))
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(kolekcjaSlow);
    }

    private static void metoda12() {
        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        IntSummaryStatistics statistics = lista.stream().mapToInt(x -> x).summaryStatistics();
        System.out.println("max to : " + statistics.getMax());
        System.out.println("srednia to : " + statistics.getAverage());
        System.out.println("ilosc elementow to : " + statistics.getCount());
        System.out.println("min to : " + statistics.getMin());
        System.out.println("suma to : " + statistics.getSum());
    }

    private static void metoda11() {
        List<Integer> list = IntStream
                .range(1, 100)
                .filter(liczba -> Math.sqrt(liczba) % 1 == 0)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(list);
    }

    private static void metoda10() {
        IntStream
                .iterate(1, liczba -> liczba + 2)
                .limit(5)
                .forEach(liczba -> System.out.println(liczba));

    }

    private static void metoda9() {
        IntStream.rangeClosed(10, 20)
                .filter(liczba -> liczba % 2 == 0)
                .average()
                .ifPresent(liczba -> System.out.println(liczba));
    }

    private static void metoda8() {
        int[] tablica = {1, 0, 5, 4, 67, 7};
        double srednia = Arrays.stream(tablica)
                .filter(liczba -> {
                    if (liczba % 3 == 0) {
                        return true;
                    }
                    return false;
                })
                .average()
                .orElse(0);
        System.out.println(srednia);

    }

    private static void metoda7() { // zwraca
        IntStream.rangeClosed(1, 25)
                .filter(liczba -> liczba % 3 == 0)
                .forEach(liczba -> System.out.println(liczba + ","));
    }

    private static void metoda6() { //zwraca tablice intów x2
        int[] tablica = {4, 6, 8, 4};
        Arrays.stream(tablica)
                .map(liczba -> liczba * 2)
                .forEach(liczba -> System.out.print(liczba + ","));
    }

    private static void metoda5() {
        int[] tablica = {3, 5, 7};
        Arrays.stream(tablica)
                .forEach(liczba -> System.out.print(liczba + ","));
    }

    private static void metoda4() {
        //nie łapie górnej granicy
        List<Integer> list = new Random().ints(-20, 20)
                .limit(10)
                .boxed()
                .collect(Collectors.toList());

        System.out.println(list);
    }

    private static void metoda3() {
        Random random = new Random();
        random.ints(-10, 20)
                .limit(5)
                .forEach(System.out::println);
    }

    private static void metoda1() {
        IntStream
                .range(1, 10)//od 1 do 9
                .forEach(liczba -> {
                    System.out.print(liczba);
                });
    }

    private static void metoda2() {
        IntStream
                .rangeClosed(1, 10) // od 1 do 10
                .forEach(System.out::print);
    }
}
