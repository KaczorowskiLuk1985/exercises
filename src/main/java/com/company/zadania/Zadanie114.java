package com.company.zadania;
/*
Utwórz metodę, która przyjmuje jeden parametr - napis (typu `String`).
Metoda ma zwrócić mapę w postaci `String : List<Integer>`,
w której jako klucze mają być litery (z napisu wejściowego)
a jako wartości - *pozycje* na jakich występują one w napisie wejściowym.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zadanie114 {
    public static void main(String[] args) {
        System.out.println(mapaPozycjiLiter("costamcosCOSTAM"));
    }
    static Map<String, List<Integer>> mapaPozycjiLiter(String slowo) {
        Map<String, List<Integer>> mapa = new HashMap<>();
        String[] tablicaZnakow = slowo.split("");

        for (int i = 0; i < tablicaZnakow.length; i++) {
            List<Integer> list;
            if (mapa.containsKey(tablicaZnakow[i])) {
                list = mapa.get(tablicaZnakow[i]);

            } else {
                list = new ArrayList<>();
            }
            list.add(i);
            mapa.put(tablicaZnakow[i], list);

        }
        return mapa;
    }
}
