package com.company.zadania;
/*
*ZADANIE #57*
Utwórz metodę, która wykorzystuje mechanizm *varargs* by przekazać do metody dowolną,
większą od zera, liczbę elementów typu `int` i zwrócić ich sumę.
 */

public class Zadanie57 {
    public static void main(String[] args) {  // int metoda (int... liczba)
        System.out.println(zwrocLiczby(5, 6, 3, 2, 4, 6));

    }

    static int zwrocLiczby(int... liczba) {
        // moze byc tylko jeden taki parametr i musi byc ostatni
        int suma = 0;

        for (int i : liczba) {
            suma += i;

        }

//        for (int i = 0; i < liczba.length; i++) {
//            suma += liczba[i];
//        }
        return suma;
    }
}
