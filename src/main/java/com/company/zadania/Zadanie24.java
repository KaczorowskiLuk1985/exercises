package com.company.zadania;

public class Zadanie24 {
    public static void main(String[] args) {
        warunek1(4);
        System.out.println();
        warunek2(7);


    }

    static void warunek1(int wielokrotnosc) {
        for (int i = 1; i <= wielokrotnosc; i++) {
            System.out.print(i * 10 + ",");
        }
    }

    static void warunek2(int wielokrotnosc) {
        int i = 1;
        while (i <= wielokrotnosc) {
            System.out.print(i * 10 + ",");
            i++;
        }
    }
}
