package com.company.zadania.zadanie150;
/*
*ZADANIE #150*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
> Dla `4` zwróci: `[10, 11, 12, 13]`
>
> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */

public class Zadanie150 {
    public int [] zwracaTablica (int parametr){
        int[] tab = new int[parametr];

        for (int i = 0; i < parametr ; i++) {
        tab[i] = i+10;
        }
        return tab;
    }
}
