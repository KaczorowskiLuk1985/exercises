package com.company.zadania;
/*
Utwórz metodę, która przyjmuje tablicę ciągów znaków.
Metoda ma zwrócić napis w formie zdania (z dużą literom na początku i kropką na końcu).
> DLa `["ala", "ma", "kota"]` zwróci `"Ala ma kota."`
> DLa `["lubię", "kolor", "żółty"]` zwróci `"Lubię kolor żółty."`
 */

public class Zadanie90 {
    public static void main(String[] args) {
        System.out.println(zwrocPelneZdanie("ala", "ma", "kota"));
        System.out.println(zwrocPelneZdanie("lubię", "kolor", "żółty"));

    }

    static String zwrocPelneZdanie(String... wyrazy) {
        String noweZdanie = "";
        wyrazy[0] = zmieniaPierwszaLitere(wyrazy);

        for (String jedenWyraz : wyrazy) {
            for (int wyraz = 0; wyraz < jedenWyraz.length(); wyraz++) {
            }
            noweZdanie += jedenWyraz + " ";
        }

        return noweZdanie.trim() + "";
    }

    private static String zmieniaPierwszaLitere(String... wyrazy) {
        char[] litery = wyrazy[0].toCharArray();
        litery[0] = Character.toUpperCase(litery[0]);
        return new String(litery);

    }
}
