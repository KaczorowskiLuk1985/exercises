package com.company.zadania;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć z podanej tablicy element o wybranym indeksie i zwrócić nową tablicę.
Zaimplementuj dodatkowo metodę służącą do wyświetlania tablicy.
Dla `([1, 2, 3, 4, 5],  2)`
zwróci `[1, 2, 4, 5]`
 */

public class Zadanie46 {
    public static void main(String[] args) {
        int[] tablica = {1, 2, 3, 4, 5};
        wyswietlTablice(tablica);
        wyswietlTablice(usunElement(tablica, 2));
    }

    static int[] usunElement(int[] tablicaWejsciowa, int indexDoUsuniecia) {
        int[] nowaTablica = new int[tablicaWejsciowa.length - 1];

        for (int pozycja = 0; pozycja < indexDoUsuniecia; pozycja++) {
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja];

        }
        for (int pozycja = indexDoUsuniecia; pozycja < nowaTablica.length; pozycja++) { // przytrzymaj ctrl nad metodą
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja + 1];

        }
        return nowaTablica;

    }

    static void wyswietlTablice(int[] tablica) {
        for (int pozycja = 0; pozycja < tablica.length; pozycja++) {
            System.out.print(tablica[pozycja] + ", ");

        }
        System.out.println();
    }

}
