package com.company.zadania;
/*
*ZADANIE #110*
Utwórz metodę, które zwróci mapę gdzie kluczem jest skrót kraju (np. “PL”, “DE”, “ES”), a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`).

Utwórz drugą metodę, która zwróci mapę gdzie kluczem jest numer kierunkowy kraju (np. “48", “47”), a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`).
 */

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class Zadanie110 {
    public static void main(String[] args) {
        Map<String, String> mapka = zwrocMapeZKluczemKraju(); //ctrl + alt +V utowrzy zmienna
        System.out.println(mapka);
        Map<Integer, String> zwrocMapeZKierunowym = zwrocMapeZKierunowym(); //ctrl + alt +V utowrzy zmienna
        System.out.println(zwrocMapeZKierunowym);
        System.out.println(zwrocKraj(zwrocMapeZKierunowym, 123));
    }

    static Map<String, String> zwrocMapeZKluczemKraju() {
        Map<String, String> mapa = new HashMap<>();
        mapa.put("PL", "Polska");
        mapa.put("DE", "Niemcy");
        mapa.put("ES", "Hiszpania");
        return mapa;
    }
    static Map<Integer,String> zwrocMapeZKierunowym(){
        Map<Integer,String>mapa = new HashMap<>();
        mapa.put(123, "Polska");
        mapa.put(124, "Niemcy");
        mapa.put(125, "Hiszpania");
        return mapa;
    }
    static String zwrocKraj(Map<Integer,String> mapa,int kierunkowy){
        return mapa.get(kierunkowy);
    }
}
