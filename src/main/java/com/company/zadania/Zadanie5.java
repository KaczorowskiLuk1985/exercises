package com.company.zadania;

public class Zadanie5 {
    public static void main(String[] args) {
        int m1 = 25;
        float m2 = 5.5f;
        double m3 = 1.3;

        System.out.println(m1 + m2 + m3);
        System.out.println(m1 + m2 / m3);
        System.out.println(m1 * m2 / m3);
        System.out.println(10f / 3);
        System.out.println(10d / 3);
        System.out.println(10.0 / 3);
    }
}
