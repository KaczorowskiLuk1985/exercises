package com.company.zadania;

import java.util.ArrayList;
import java.util.List;

/*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca ich sumę.
 */
public class Zadanie98 {
    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<>();
        myList.add(12);
        myList.add(12);
        myList.add(12);
        myList.add(12);
        myList.add(12);
        myList.add(12);

        System.out.println(zwrocSumeZListy(myList));
        System.out.println(zwroc2(myList));

    }
    static int zwrocSumeZListy (List<Integer>list){
        int sum = 0;
        for (Integer number : list) { //foreach przetrząsuje listę
            sum += number;
        }
        return sum;
    }
    static int zwroc2 (List<Integer>list){
        int suma =0;
        for (int pozycja = 0; pozycja <list.size() ; pozycja++) {  //for tylko numeruje, nie przetrzasuje listy
            suma += list.get(pozycja);
        }
        return suma;
    }
}
//do obslugiwania list lepiej uzywac pętli foricz
