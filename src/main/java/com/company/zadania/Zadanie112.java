package com.company.zadania;
/*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania wartości `-1`
a następnie zwraca mapę wartości, gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zadanie112 {
    public static void main(String[] args) {
        System.out.println(zwrocMapeWartosci());

    }
    static Map<Integer,Integer>zwrocMapeWartosci(){
        Map<Integer,Integer>mapa = new HashMap<>();
        Scanner s = new Scanner(System.in); ///tworzymy obiekt klasy skaner
        while (true){
            System.out.println("podaj liczbe : ");
            int wprowadzonaLiczba = s.nextInt();
            if (wprowadzonaLiczba==-1){
                break;
            }
            if (mapa.containsKey(wprowadzonaLiczba)){ // sprawdza czy mapa zawiera taki klucz
                //pobieramy z mapy wartosc dla danego klucza
                int staraLiczbaWystapien = mapa.get(wprowadzonaLiczba);
                //na ten sam klucz wprowadzamy inna wartosc(nadpisujemy wierz)
                mapa.put(wprowadzonaLiczba,staraLiczbaWystapien+1);
            }else {
                //jesli mapa nie zawiera wartosci pod danym kluczem wstawiamy wartosc = 1
                mapa.put(wprowadzonaLiczba,1);

            }
        }
        return mapa;
    }

}
