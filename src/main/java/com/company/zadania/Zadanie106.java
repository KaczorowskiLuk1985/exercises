package com.company.zadania;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/*
    Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania duplikatu
    a następnie zwraca set (np. `HashSet`) (z podanymi liczbami)

 */
public class Zadanie106 {
    public static void main(String[] args) {
        System.out.println(zwrocSet());
    }

    static Set<Integer> zwrocSet() { // nie ustawiają swoją kolejność
        Set<Integer> lista = new HashSet<>();
        int zmienna;
        Scanner scanner = new Scanner(System.in);

        while (true) { //pętla nieskonczona
            System.out.print("podaj liczbe :");
            zmienna = scanner.nextInt();
            boolean udaloSieDodac = !lista.add(zmienna);
            if (udaloSieDodac) {
                break;
            }
        }
        return lista;
    }
}
