package com.company.zadania;
/*
Utwórz metodę, która przyjmuje wysokość trójkąta Pascala
a następnie tworzy odpowiednią strukturę danych i ją wypełnia
dla `6`  zwróci
[1]
[1, 1]
        [1, 2, 1]
      [1, 3, 3, 1]
     [1, 4, 6, 4, 1]
   [1, 5, 10,10, 5, 1]
[1, 6, 15, 20, 15, 6, 1]
 */

import java.util.Arrays;

public class Zadanie69 {
    public static void main(String[] args) {

        wyswietlTablice(zwrocStrukture(6));

    }

    static int[][] zwrocStrukture(int wysokoscTrojkata) {
        int[][] trojkatPashala = new int[wysokoscTrojkata][];//drugi nawias to dlugosc wiersza

        for (int wiersz = 0; wiersz < wysokoscTrojkata; wiersz++) {
            //  int[]trojkatPashala = new int[wiersz+1]; odwolujac sie do wiersza nie podajemy juz kolumny
            trojkatPashala[wiersz] = new int[wiersz + 1];

            int number = 1;

            for (int kolumna = 0; kolumna <= wiersz; kolumna++) {
                trojkatPashala[wiersz][kolumna] = number;
                number = number * (wiersz - kolumna) / (kolumna + 1);
            }

        }

        return trojkatPashala;
    }

    static void wyswietlTablice(int[][] trojkatPashala) {
        for (int[] wiersz : trojkatPashala) {
            System.out.println(Arrays.toString(wiersz));

        }
    }
}
