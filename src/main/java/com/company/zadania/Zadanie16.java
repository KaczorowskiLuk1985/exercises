package com.company.zadania;
/*
*ZADANIE #16*
Utwórz metodę, do której przekazujesz trzy parametry - pierwsze dwa to jest przedział. Metoda powinna zwrócić informację czy trzeci parametr znajduje się w tym przedziale
> Dla `1, 10, 5` zwróci `true`
>
> Dla `1, 10, 15` zwróci `false`
>
> Dla `-10, 0, 3` zwróci `false`
>
> Dla `-10, 10, 0` zwróci `true`
>
> Dla `4, 8, 4` zwróci `true`
>
> Dla `3, 3, 3` zwróci `true`
 */

public class Zadanie16 {
    public static void main(String[] args) {
        System.out.println(czyZnajdujeSieWPrzedzedziale(2,4,6));
        System.out.println(czyZnajdujeSieWPrzedzedziale(-9,9,6));
        System.out.println(czyZnajdujeSieWPrzedzedziale(2,9,6));
        System.out.println(czyZnajdujeSieWPrzedzedziale(10,20,6));

    }
    static boolean czyZnajdujeSieWPrzedzedziale (int poczatek, int koniec, int liczbaSprawdzana){
        return liczbaSprawdzana <= koniec && liczbaSprawdzana >= poczatek;
    }
}
