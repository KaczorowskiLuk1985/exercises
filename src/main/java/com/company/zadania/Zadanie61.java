package com.company.zadania;
/*
Utwórz metodę, która przyjmuje conajmniej DWA parametry - boolean oraz dowolną ilość (większą od 0) liczb (typu `int`).
Gdy pierwszy parametr ma wartość `true` metoda zwraca największą przekazaną liczbą, a gdy `false` najmniejszą. (edited)
 */

public class Zadanie61 {
    public static void main(String[] args) {
        System.out.println(zwrocNajwiekszaLiczbe(true, 5, 6, 7, 2, 1, 5, 6));
        System.out.println(zwrocNajwiekszaLiczbe(false, 5, 6, 7, 2, 1, 5, 6));


    }

    static int zwrocNajwiekszaLiczbe(boolean czyNajwieksza, int... liczby) {
        int max = liczby[0];
        int min = liczby[0];

        for (int pojedynczaLiczba : liczby) {
            if (pojedynczaLiczba > max) {
                max = pojedynczaLiczba;
            }
            if (pojedynczaLiczba < min) {
                min = pojedynczaLiczba;
            }
        }
        return czyNajwieksza ? max : min;
    }
}
