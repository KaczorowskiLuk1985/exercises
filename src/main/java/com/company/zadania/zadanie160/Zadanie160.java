package com.company.zadania.zadanie160;

import java.util.Scanner;

public class Zadanie160 {
    final private static int WITHDRAW = 1;
    final private static int DEPOSIT = 2;

    public static void main(String[] args) {
        Bankomat bankomat = new Bankomat();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Podaj Pin : ");
        String pin = scanner.nextLine();
        System.out.println("Entere amount of money");
        int money = scanner.nextInt();

        System.out.println("withdraw: 1");
        System.out.println("Deposit: 2");

        int decyzja = scanner.nextInt();

        switch (decyzja) {
            case WITHDRAW:
                bankomat.withdrawMony(money, pin);
                break;
            case DEPOSIT:
                bankomat.inputMoney(money, pin);
                break;
            default:
                System.out.println("invalic option ");
        }
    }
}
