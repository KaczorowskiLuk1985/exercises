package com.company.zadania.zadanie160;

public class Konto {
    private double balanceAcc = 0;

    double getBalance() {
        return balanceAcc;
    }

    void putMoney(double wplata) {
        balanceAcc += wplata;

    }

    void withdrawnMoney(double wyplata) {
        balanceAcc -= wyplata;

    }
}
