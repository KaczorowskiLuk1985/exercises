package com.company.zadania.zadanie160;

public class Bankomat {
    private IdentyfikatorPinu identyfikatorPinu = new IdentyfikatorPinu();
    private Konto konto = new Konto();

    void withdrawMony(int money, String pin) { //wypłącanie
        if (identyfikatorPinu.isPinOK(pin)) {
            if (konto.getBalance() > money) {
                System.out.println("balance before:" + konto.getBalance());
                konto.withdrawnMoney(money);
                System.out.println("balane after: ");
            } else {
                System.out.println("nie ma kasy na koncie" + konto.getBalance());
            }

        } else {
            System.out.println(" pin nieprawidłowy ");
        }

        System.out.println("please enter PIN");
    }

    void inputMoney(int money, String pin) {
        if (identyfikatorPinu.isPinOK(pin)) {
            System.out.println("balance before:" + konto.getBalance());
            konto.putMoney(money);
            System.out.println("balane after: " + konto.getBalance());
        } else {
            System.out.println(" pin nieprawidłowy ");
        }

    }
}
