package com.company.zadania;
/*
*ZADANIE #8*
Utwórz metodę, która zwróci trzecią potęgę przekazywanej liczby.
>Dla `2` zwróci `8` (bo `2^3 = 2 * 2 * 2 = 8`)
>
>a dla `3` zwróci `27` (bo `3^3 = 3 * 3 * 3 = 27`)
 */

public class Zadanie8 {
    public static void main(String[] args) {
        System.out.println(potega(2));
        System.out.println(potega(4));
        System.out.println(potega(8));
    }

    static int potega(int a) {
        return a * a * a;
    }
}
