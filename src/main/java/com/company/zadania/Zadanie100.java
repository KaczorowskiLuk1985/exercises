package com.company.zadania;
/*
Utwórz metodę, która przyjmuje listę (`List`) np. `ArrayList` elementów typu `String` a następnie zwraca listę w odwróconej kolejności.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zadanie100 {
    public static void main(String[] args) {
        List<String> listaA = new ArrayList<>(Arrays.asList(
                "ala", "ma","kota"
        ));
        //System.out.println(listaOdwrocona(listaA));
        //System.out.println(listaOdrwocona2(listaA));
        System.out.println(listaOdrwocona3(listaA));


    }
    static List<String> listaOdwrocona (List<String> lista){
        Collections.reverse(lista); //poprostu odwraca liste.
        return lista;
    }
    static List<String> listaOdrwocona2 (List<String> lista){
        List<String> nowaLista = new ArrayList<>();

        for (int i = 0; i < lista.size(); i++) {
            nowaLista.add(0,lista.get(i)); //  add jednoparametrowe gdy wstawiamy na koniec, dwuparametrowe gdy w konkretne miejsce.

        }
        return nowaLista;
    }
    static List<String> listaOdrwocona3 (List<String> lista){
        List<String> nowaLista = new ArrayList<>();

        for (String element : lista) {
            nowaLista.add(0,element); //wstawia najpierw na pozycje 0 i przesuwa ja na pozycje 1   1 >> 21 >> 321         } {

        }
        return nowaLista;
    }
}
