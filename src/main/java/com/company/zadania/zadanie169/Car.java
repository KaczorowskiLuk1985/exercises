package com.company.zadania.zadanie169;

import java.util.Optional;

public class Car {
    private String name;
    private Integer age;

    public String getName() {
        return name;
    }

    public Optional<Integer> getAge() {
        return Optional.ofNullable(age);
    }
}
