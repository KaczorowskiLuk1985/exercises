package com.company.zadania.zadanie169;
/*
typy opcjonalne - nie wiadomo po co?.
 */

import javax.swing.text.html.Option;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Zadanie169 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
        method4();
    }

    private static void method4() {
        List<Integer> list = Arrays.asList(
                1, 2, 3, 4, 5, 6, 7, 7, 7, 7, 1222, 22, 122
        );

        Optional<Integer> first = list.stream()
                .filter(num -> num > 100)
                .findFirst();

        if (first.isPresent()) {
            System.out.println("tak, znaleziono liczbe " + first.get());

        } else {
            System.out.println("Zadna liczba nie spełnia warunku ");
        }
    }

    private static void method3() {
        Optional<Double> resoult = safeDivision(3, 4);
        System.out.println(resoult.isPresent());
        double wynik = resoult.orElse(0D);
        System.out.println(wynik);

        Optional<Double> secondResoult = safeDivision(7, 0);
        System.out.println(secondResoult.isPresent());
        double secondDividingReoult = secondResoult.orElse(0D);
        System.out.println(secondDividingReoult);


    }

    private static Optional<Double> safeDivision(double first, double second) {
        if (second == 0) {
            return Optional.empty();
        } else {
            return Optional.of(first / second);
        }

    }

    private static void method2() {
        Car car = new Car();
        Optional<Integer> age = car.getAge();
//zawsze chce miec artosc (nawet jak w srodku jest 'null')
        int realReal = age.orElse(0);
        System.out.println(car.getAge().orElse(-1));
//sprawdzam czy jakaś wartość (różna od 'null') jest w środku
        if (age.isPresent()) {
            int real = age.get();
        }
    }

    private static void method1() {
        Car car = new Car();
        System.out.println(car.getName());

        String carName = car.getName().toLowerCase();
    }
}
