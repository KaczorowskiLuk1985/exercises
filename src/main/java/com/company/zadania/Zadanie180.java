package com.company.zadania;

import com.google.common.base.CaseFormat;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zadanie180 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
        method6();
    }

    private static void method6() {
        File file = new File("pliki/ala.txt");

        try {
            List<String> list = Files.readLines(file, Charsets.UTF_8);
            System.out.println(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void method5() {
        String nic = "AlaMaKota";
        String name = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, nic);
        System.out.println(nic);
        System.out.println(name);
    }

    private static void method4() {
        //wartosci musza byc niepowtarzalne
        BiMap<Integer, String> map = HashBiMap.create();
        map.put(1, "Adam");
        map.put(2, "Kuba");
//        map.put(3, "Adam");

        System.out.println(map);
    }

    private static void method3() {
        Multimap<Integer, String> multimat = HashMultimap.create();

        multimat.put(31, "styczen");
        multimat.put(31, "marzec");
        multimat.put(31, "kwiecien");

        multimat.put(30, "kwiecien");
        multimat.put(30, "czerwiec");

        System.out.println(multimat);
        System.out.println("miesiace ktore maja 30 dni: " + multimat.get(30));
    }

    private static void method2() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "poniedzialek");
        map.put(2, "worek");
        map.put(3, "środa");
        map.put(4, "czwartek");

        String joined = Joiner
                .on(", ")
                .withKeyValueSeparator(" --> ")
                .join(map);
        System.out.println(joined);
    }

    private static void method1() {
        String[] imiona = {"ada", "ola", "magda", null, "ela", "ala", null};
        String joined = Joiner
                .on(",")
                .skipNulls()
                .join(imiona);
        System.out.println(joined);
        System.out.println(String.join(",", joined));

        String joined2 = Joiner
                .on(" | ")
                .useForNull(" - Brak -")
                .join(imiona);
        System.out.println(joined2);
    }
}

