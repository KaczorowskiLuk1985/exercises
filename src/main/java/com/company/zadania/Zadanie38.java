package com.company.zadania;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca *NAJWIĘKSZĄ* liczbę tablicy
 */


public class Zadanie38 {
    public static void main(String[] args) {
        double [] tab = new double[]{3.4,11.25,5.5};
        System.out.println(zwrocMax(tab));
        System.out.println(zwrocMax2(tab));


    }
    public static double zwrocMax(double[]tab){
        double max = tab[0];                         // max przyrównujemy do zawartosci tablicy
        for (double elementTab : tab){               // (pojedynczy element : zbiór czyli wszystkie elementy tablicy) ,
            if(elementTab > max){                    //foricz, jest na slaku zwykla petla for
                max = elementTab;

            }

        }
        return max;
    }
    public static double zwrocMax2(double[] tab) {
        double max = tab[0];
        for (int i = 1; i < tab.length; i++) {
            if (tab[i] > max) {
                max = tab[i];
            }
        }
        return max;
    }
}
