package com.company.zadania.zadanie96;

public enum Kierunkowy { // enum typ wyliczeniowy// unboxing i autoboxink
    POLSKA(48), ROSJA(7), NIEMCY(49);
    private int kierunkowy;


    Kierunkowy(int kierunkowy) {
        this.kierunkowy = kierunkowy;
    }


    String jakoText() {
        return "+" + kierunkowy;
    }

    int jakoNumer() {
        return kierunkowy;
    }
}
