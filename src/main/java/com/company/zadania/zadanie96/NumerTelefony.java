package com.company.zadania.zadanie96;

class NumerTelefony {
    private String numer;
    private Kierunkowy kierunkowy;

    NumerTelefony(String numer, Kierunkowy kierunkowy) {
        this.numer = numer;
        this.kierunkowy = kierunkowy;
    }

    void wyswietlNumer() {
        System.out.println("(" + kierunkowy.jakoText()
                + ")" + numer);
        System.out.println(kierunkowy.jakoNumer()+" "+numer);

    }

}
