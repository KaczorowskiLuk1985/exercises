package com.company.zadania;
/*
Utwórz metodę, która przyjmuje 3 parametry.
Pierwsze dwa to krańce przedziału. Trzeci to “skok” pomiędzy liczbami.
Gdy przekazany przedział będzie błędny, metoda ma wyświetlić komunikat.
 */

public class Zadanie34 {
    public static void main(String[] args) {
        wyswietlPrzedzial(11, 10, 30);

    }

    static void wyswietlPrzedzial(int start, int stop, int skok) {
        if (start > stop) {
            System.out.println("podano bledny przedzial");
            return;
        } else if (stop - start < skok) {
            System.out.println("skok jest za duży");

        }
        for (int i = start; i <= stop; i = i + skok) {
            System.out.println(i + ", ");

        }
    }
}
