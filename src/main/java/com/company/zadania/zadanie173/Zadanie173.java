package com.company.zadania.zadanie173;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zadanie173 {
    public static void main(String[] args) {
        Student student1 = new Student(1, "Paweł");
        Student student2 = new Student(1, "Paweł");

        //obiekty nie są sobie równe, maja rozne hashCody

        System.out.println("st1 hashCode = " + student1.hashCode());
        System.out.println("st2 hashCode = " + student2.hashCode());
        System.out.println("czy są równe? = " + student1.equals(student2));

        List<Student> listaStudentow = new ArrayList<>();
        listaStudentow.add(student1);
        listaStudentow.add(student2);
        System.out.println("rozmiar listy -> " + listaStudentow.size());
        System.out.println("czy lista zawiera studwnta? " + listaStudentow.contains(student1));

        Set<Student> setStudentow = new HashSet<>();
        setStudentow.add(student1);
        setStudentow.add(student2);
        System.out.println("rozmiar seta -> " + setStudentow.size());
        System.out.println("czy set zawiera studenta ? "
                + setStudentow.contains(new Student(1, "Paweł")));
        //pomimo ze obiekty sa takie same to zwraca ze nie są
        //set opiera sie na hash codach

        Student student3 = new Student(2, "Przemysłąw");
        listaStudentow.add(student3);
        System.out.println("Rozmiar listy z nowym studentem: " + listaStudentow.size());
        setStudentow.add(student3);
        System.out.println("Rozmiar seta z nowym studentem: " + setStudentow.size());
        //wszystkie konstrukcje oparte o hasze wymagaja zeby mialy takie same numery

        System.out.println(" lista  " + listaStudentow);
        System.out.println(" set  " + setStudentow);
        // na szczescie mozna generowac auomatyczie ( alt + instert )
        // equal i hashCode zawsze ida parami
    }
}
