package com.company.zadania.zadanie173;

import java.util.Objects;
import java.util.Random;

public class Student {
    private int id;
    private String imie;
    private int age;
    private int yersOfStudy;
    private boolean isStudent;

    public Student(int id, String imie) {
        this.id = id;
        this.imie = imie;
    }

    public int getId() {
        return id;
    }

    public String getImie() {
        return imie;
    }

//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 31 * hash + id; //31 jest stosowane w praktyce
////        imie moze byc nullem to trzeba sie zabezpieczyc
//        hash = 31 * hash + (imie == null ? 0 : imie.hashCode());
//        return hash;
////        return new Random().nextInt(); to powoduje problemy
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) {
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (obj instanceof Student) {
//            Student student = (Student) obj; //rzutowanie
//            return student.id == this.id
//                    && student.imie.equals(this.imie);
//        } else {
//            return false;
//        }
//    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (getId() != student.getId()) return false;
        if (age != student.age) return false;
        if (yersOfStudy != student.yersOfStudy) return false;
        if (isStudent != student.isStudent) return false;
        return getImie() != null ? getImie().equals(student.getImie()) : student.getImie() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getImie() != null ? getImie().hashCode() : 0);
        result = 31 * result + age;
        result = 31 * result + yersOfStudy;
        result = 31 * result + (isStudent ? 1 : 0);
        return result;
    }
}
