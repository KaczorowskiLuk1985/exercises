package com.company.zadania.zadanie151;
/*
*ZADANIE #151*
Utwórz klasę `Person` (`Osoba`) posiadającą
>*pola reprezentujące:*
>* imie
>* nazwisko
>* wiek
>* czyMezczyzna

>*konstruktory*

>*metody służące do:*
> - zwrócenia imienia i nazwiska (tylko jeśli są ustawione!!)
> - przedstawienia się (czyli zwraca `Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
> - sprawdzenia czy osoba jest pełnoletnia
> - zmiany wieku
> - zmiany imienia
> - zwrócenia wieku do emerytury

*ZADANIE #152*
*NAJPIERW TESTY POTEM IMPLEMENTACJA*

Utwórz metodę w klasie `Person`/`Osoba` z *ZADANIA #151*, która zwraca *login* w formie napisu, który składa się z:
>- 3 liter imienia
>- 3 liter nazwiska
>- liczby, która jest sumą długości imienia i nazwiska

> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
> Dla `"Anna Nowak"`, zwróci `"annnow9"`
> Dla `"Jan Kowalski"`, zwróci `"jankow11"`


*Najpierw napisz testy które sprawdzą zachowanie metody gdy:*
- nie jest ustawione *imię* (w loginie ma być zastąpione znakami `XXX`)
- nie jest ustawione *nazwisko* (w loginie ma być zastąpione znakami `YYY`)
- nie jest ustawione ani *imię* ani *nazwisko* (metoda ma zwrócić `null`)
- *imię* lub *nazwisko* jest krótsze niż 3 znaki (w loginie ma być “dopełnione” znakiem `Z`, czyli np. dla `xi ping` powinno zwrócić `xiZpin6`)
 */

public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private boolean czyMezczyzna;

    public Osoba() {

    }

    public Osoba(String imie, String nazwisko, int wiek, boolean czyMezczyzna) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    public String zwrocNapis() {
        if (imie == null && nazwisko == null) {
            return null;
        } else if (imie == null) {
            return nazwisko;
        } else if (nazwisko == null) {
            return imie;
        } else {
            return String.format("%s %s", imie, nazwisko);
        }
    }

    public boolean czyPelnoletnia() {
        return wiek > 17;
    }

    public int ileDoEmerytury() {
        if (wiek == 0) {
            return -1;

        }
        int liczbaLatDoEmerytury = (czyMezczyzna ? 67 : 65) - wiek;
        return liczbaLatDoEmerytury > 0 ? liczbaLatDoEmerytury : 0;

    }


    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        if (wiek > 0) {
            this.wiek = wiek;
        }
    }

    public boolean isCzyMezczyzna() {
        return czyMezczyzna;
    }

    public void setCzyMezczyzna(boolean czyMezczyzna) {
        this.czyMezczyzna = czyMezczyzna;
    }

    public String zwrocLogin() {

        if (imie == null && nazwisko == null) {
            return null;
        } else if (imie == null) {
            return String.format("XXX%s%s",
                    nazwisko.toLowerCase().substring(0, 3),
                    nazwisko.length());

        } else if (nazwisko == null) {
            return String.format("%sYYY%s",
                    imie.toLowerCase().substring(0, 3),
                    imie.length());

        } else if (imie.length() == 2) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase(),
                    "Z",
                    nazwisko.toLowerCase().substring(0, 3),
                    imie.length() + nazwisko.length());

        } else if (imie.length() == 1) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase(),
                    "ZZ",
                    nazwisko.toLowerCase().substring(0, 3),
                    imie.length() + nazwisko.length());
        } else if (nazwisko.length() == 2) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase().substring(0, 3),
                    nazwisko.toLowerCase(),
                    "Z",
                    imie.length() + nazwisko.length());
        } else if (nazwisko.length() == 1) {
            return String.format("%s%s%s%s",
                    imie.toLowerCase().substring(0, 3),
                    nazwisko.toLowerCase(),
                    "ZZ",
                    imie.length() + nazwisko.length());
        }

        return String.format("%s%s%s",
                imie.toLowerCase().substring(0, 3),
                nazwisko.toLowerCase().substring(0, 3),
                imie.length() + nazwisko.length());
    }
}
