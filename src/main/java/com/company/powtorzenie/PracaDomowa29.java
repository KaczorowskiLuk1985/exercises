package com.company.powtorzenie;

import java.util.Arrays;

/*
Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
 */
public class PracaDomowa29 {
    public static void main(String[] args) {
        int [] tab = new int[]{1,2,3,4};
        System.out.println(Arrays.toString(zwrocOdwrotnaTablice(tab)));

    }
    static int [] zwrocOdwrotnaTablice (int [] tablica){
        int [] nowaTablica = new int[tablica.length];
        int n = tablica.length-1;

        for (int index = 0; index < tablica.length; index++) {
            nowaTablica[index]=tablica[n];
            n--;

        }
        return nowaTablica;
    }
}
