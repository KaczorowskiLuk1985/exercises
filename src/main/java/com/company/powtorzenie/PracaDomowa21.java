package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje 3 parametry i wyświetla odpowiednie wartości.
Pierwsze dwa to krańce przedziału. Trzeci to “skok” pomiędzy liczbami. Gdy przekazany przedział będzie błędny, metoda ma wyświetlić komunikat.
> Dla `1, 10, 3` wyświetli `1, 4, 7, 10`
 */

public class PracaDomowa21 {
    public static void main(String[] args) {
        wyswietlCiagLiczb(-10, 10, 30);
    }

    static void wyswietlCiagLiczb(int start, int stop, int jump) {

        if (start > stop) {
            System.out.println("błędny przedział");
        }
        if (jump > stop - start){
            System.out.println("błędny skok");
        }
        else {
            for (int parametr = start; parametr <= stop; parametr += jump) {

                System.out.print(parametr+", ");
            }
        }
    }
}
