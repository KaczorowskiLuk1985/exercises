package com.company.powtorzenie;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
 */

import java.util.Arrays;

public class PracaDomowa42 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(getTab(tab)));
    }

    static int[] getTab(int[] tab) {
        int[] newTab = new int[tab.length];
        for (int i = 0; i < tab.length; i++) {
            newTab[i] = tab.length - i;
        }
        return newTab;
    }
}
