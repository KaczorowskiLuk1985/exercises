package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz trzy parametry (np `start`, `amount`, `jump`).
Następujące parametry odpowiadają za:
pierwszy - początkowa (pierwsza) wyświetlona wartość
drugi  - liczba wyświetlonych elementów
trzeci (`double`) - różnica pomiędzy kolejnymi elementami (“skok”)
> Dla `0, 5, 5` wyświetli `0, 5, 10, 15, 20` (zaczynamy od `0`, wyświetlamy `5` elementów a elementy różnią się od siebie o `5`)
> Dla `0, 6, 2.5` wyświetli `0.0, 2.5, 5.0, 7.5, 10.0, 12.5` (zaczynamy od `0`, wyświetlamy `6` elementów a elementy różnią się od siebie o `2.5`)
 */

public class PracaDomowa12 {
    public static void main(String[] args) {
        wyswietlCiag(0, 5, 5);
        System.out.println();
        warunek1(0, 6, 2.5);
        System.out.println();
        warunekWhile(0, 5, 5);
        System.out.println();
        warunekDoWhile(0, 5, 5);
    }

    static void wyswietlCiag(int start, int amount, int jump) {
        for (int i = start; i < amount * jump; i = i + jump) {
            System.out.print(i + ", ");
        }
    }

    static void warunek1(int start, int amount, double jump) {
        for (int step = start; step < amount; step++) {

            System.out.print(step * jump + ", ");
        }
    }

    static void warunekWhile(int start, int amount, double jump) {
        double i = start;
        while (i < amount * jump) {
            System.out.print(i + ", ");
            i = i + jump;

        }
    }

    static void warunekDoWhile(int start, int amount, double jump) {
        double i = start;
        do {
            System.out.print(i +", ");
            i = i + jump;

        } while (i < amount * jump);
    }
}