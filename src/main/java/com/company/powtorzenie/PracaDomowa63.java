package com.company.powtorzenie;

import java.util.Arrays;

/*
*ZADANIE #63*
Utwórz metodę, która przyjmuje dwuwymiarową tablicę.
Metoda ma zwracać tablicę która będzie zawierała sumy wszystkich kolumn.
 */
public class PracaDomowa63 {
    public static void main(String[] args) {
        int[][]tab = new int[][]{{1,2,3,4,4},{1,2,3,4},{1,2,3,4}};
        System.out.println(Arrays.toString(zwrcaSumeKolumn(tab)));

    }

    static int[] zwrcaSumeKolumn(int[][] tablica) {
        int[]sumaKolumn=new int[tablica[0].length];

        for (int i = 0; i < tablica.length ; i++) {
            for (int j = 0; j < tablica[i].length ; j++) {
                sumaKolumn[j]+=tablica[i][j];
            }

        }
        return sumaKolumn;
    }
}
