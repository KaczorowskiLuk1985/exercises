package com.company.powtorzenie;
/*
Utwórz metodę, w której pętlą (typu `FOR`) wyświetlisz wszystkie liczby od `0` do `18` (włącznie).
 */

public class PracaDomowa7 {
    public static void main(String[] args) {
        petlaFor();
    }

    static void petlaFor() {
        for (int i = 0; i <= 18; i++) {
            System.out.print(i + ",");
        }
    }
}
