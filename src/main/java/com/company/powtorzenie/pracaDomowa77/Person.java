package com.company.powtorzenie.pracaDomowa77;

public class Person {
    private String name;
    private String forname;
    private int age;
    private boolean isMan;

    public Person(String name, String forname, int wiek, boolean czyMezczyzna) {
        this.name = name;
        this.forname = forname;
        this.age = wiek;
        this.isMan = czyMezczyzna;
    }

    void przedstawSie() {
        System.out.printf("Name : %s, Forname : %s, Age : %s, Sex : %s.",
                name,
                forname,
                age,
                isMan ? "man" : "woman");
    }

    void isAdult() {
        System.out.println(age > 17 ? " Adult" : " Kid");
    }
}
