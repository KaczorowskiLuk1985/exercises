package com.company.powtorzenie;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca *NAJWIĘKSZĄ* liczbę z tej tablicy
 */

public class PracaDomowa25 {
    public static void main(String[] args) {
        double[] tab = new double[]{-35.5, -25.5, -5.5, -1.5};
        System.out.println(zwrocMax(tab));
        System.out.println(zwrocNajwieksza(tab));
    }

    static double zwrocMax(double[] tablica) {
        double max = tablica[0];

        for (int liczba = 0; liczba < tablica.length; liczba++) {
            if (tablica[liczba] > max) {
                max = tablica[liczba];
            }

        }
        return max;
    }

    static double zwrocNajwieksza(double[] tablica) {
        double max = tablica[0];

        for (double liczba : tablica) {
            if (liczba > max) {
                max = liczba;
            }

        }
        return max;
    }
}
