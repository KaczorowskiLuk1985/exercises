package com.company.powtorzenie.pracaDomowa120;

class Account {
    double stanKonta;
    private String wlasciciel;
    private int numerKonta;
    private static int idKonta = 120001;

    Account(String wlasciciel) {
        this.wlasciciel = wlasciciel;
        numerKonta = idKonta++;


    }

    @Override
    public String toString() {
        return String.format("Właścicielem konta (nr ID: %s) jest : %s . Stan konta: %s"
                , numerKonta, wlasciciel, stanKonta);
    }

    public String getWlasciciel() {
        return wlasciciel;
    }

    double pokazStanKonta() {
        return stanKonta;
    }

    void wplataNaKonto(double wplata) {
        stanKonta += wplata;

    }

    boolean wyplataZKonta(double wyplata) {
        if (stanKonta >= wyplata) {
            stanKonta -= wyplata;
            return true;
        }
        return false;
    }

    static void przelewanieSrodkow(Account nadawca, Account odbioraca, double kwotaPrzelania) {
        if (nadawca.wyplataZKonta(kwotaPrzelania)) {
            odbioraca.wplataNaKonto(kwotaPrzelania);
        }
    }
}
