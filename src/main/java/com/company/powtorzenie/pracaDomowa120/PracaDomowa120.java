package com.company.powtorzenie.pracaDomowa120;


import com.company.zadania.zadanie120.Konto;

import java.util.Arrays;
import java.util.List;

public class PracaDomowa120 {
    public static void main(String[] args) {
        Account konto1 = new Account("Pan X");
//        Account konto2 = new Account("Pan Y");
//        Account konto3 = new Account("Pan Z");
//        Account konto4 = new Account("Pan 77");

//        CompanyAccount kontoFirmowe1 = new CompanyAccount("Pan szef A", 12.0);
        CompanyAccount kontoFirmowe2 = new CompanyAccount("Pan szef B", 13.0);
//        CompanyAccount kontoFirmowe3 = new CompanyAccount("Pan szef C", 14.0);
//        CompanyAccount kontoFirmowe4 = new CompanyAccount("Pan szef GB", 15.0);

        kontoFirmowe2.wplataNaKonto(100.0);
        konto1.wplataNaKonto(100.0);

        System.out.println(kontoFirmowe2);
        System.out.println(konto1);

//        Account.przelewanieSrodkow(konto1,kontoFirmowe2,20);
        Account.przelewanieSrodkow(kontoFirmowe2, konto1, 30);
//        CompanyAccount.przelewanieSrodkow(kontoFirmowe2, konto1, 30.0);
//        CompanyAccount.przelewanieSrodkow(konto1, kontoFirmowe2, 20.0);
        System.out.println(kontoFirmowe2);
        System.out.println(konto1);


//        List<Account> listaKont = Arrays.asList(
//                konto1, konto2, konto3, konto4, kontoFirmowe1, kontoFirmowe2, kontoFirmowe3, kontoFirmowe4
//        );
//        for (Account account : listaKont) {
//            if (account instanceof Account) {
//                System.out.print("konto prywatne - ");
//            } else {
//                System.out.print("konto firmowe - ");
//            }
//            System.out.println("(" + account.getNumerKonta() + ") " + account.getWlasciciel());
//        }


//        System.out.println(konto1);
//        System.out.println(konto2);
//        System.out.println(konto3);
//
//        konto1.wplataNaKonto(50.0);
//        konto2.wplataNaKonto(100.0);
//
//        System.out.println(konto1);
//        konto1.wyplataZKonta(30.0);
//        System.out.println(konto1);
//
//        boolean result = konto1.wyplataZKonta(40.0);
//        if (result) {
//            System.out.println("ok");
//        } else {
//            System.out.println("niepowodzenie");
//        }
//        System.out.println(konto1);
//        System.out.println(konto2);
//        System.out.println("przelanie pieniędzy");
//        Account.przelewanieSrodkow(konto2, konto1, 5.0);
//        System.out.println(konto1);
//        System.out.println(konto2);

    }
}
