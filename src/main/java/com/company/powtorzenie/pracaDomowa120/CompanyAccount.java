package com.company.powtorzenie.pracaDomowa120;

class CompanyAccount extends Account {
    private double oplataTransakcyjna;

    CompanyAccount(String wlasciciel, double oplataTransakcyjna) {
        super(wlasciciel);
        this.oplataTransakcyjna = oplataTransakcyjna;
    }

    @Override
    boolean wyplataZKonta(double wyplata) {
        double sumaPoOplacie = wyplata +  oplataTransakcyjna;

        if (stanKonta >= sumaPoOplacie) {
            stanKonta -= sumaPoOplacie;
            return true;
        }
        return false;
    }

}
