package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
 Metoda ma zwrócić informację ile razy w tablicy występuje liczba podana jako drugi parametr.

Utwórz drugą metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć wszystkie elementy równe podanemu parametrowi (tzn. gdy podamy `11` to ma usunąć z tablicy wszystkie `11`-ki) i zwrócić tablicę
> Dla `([1, 2, 2, 4, 5],  2)`
> zwróci `[1, 4, 5]`
 */


import java.util.Arrays;

public class PracaDomowa50 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 2, 4, 5};
        System.out.println(zwrocInformacje(tab, 5));
        System.out.println(Arrays.toString(zwroNowaTablice(tab, 2)));

    }

    static int zwrocInformacje(int[] tablica, int liczba) {
        int liczbaWystapien = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (liczba == tablica[i]) {
                liczbaWystapien++;
            }
        }
        return liczbaWystapien;
    }
    static int [] zwroNowaTablice (int [] tablica, int usuwanaLiczba){
        int iloscWystapien = zwrocInformacje(tablica,usuwanaLiczba);
        int[] tablicaNowa = new int[tablica.length - iloscWystapien];
        int indexWNowejTablicy = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (usuwanaLiczba != tablica[i]){
                tablicaNowa[indexWNowejTablicy]=tablica[i];
                indexWNowejTablicy ++;


        }



        }
        return tablicaNowa;
    }
}