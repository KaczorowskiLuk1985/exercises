package com.company.powtorzenie;
/*
*ZADANIE #33*
Utwórz metodę, która wyświetli prostokąt o podanych wymiarach (użytkownik podaje jego wymiary jako parametry)

XXXXXXXXXX
X        X
X        X
XXXXXXXXXX

 */

public class PracaDomowa20 {
    static String znak = "x ";
    private static String przerwa = "  ";

    public static void main(String[] args) {
        pokaKwadrat(5);


    }

    static void pokaKwadrat(int rozmiar) {
        pokaWiersz(rozmiar);

        for (int j = 0; j < rozmiar ; j++) {
            System.out.print(znak);
            for (int x = 0; x < rozmiar ; x++) {

                System.out.print(przerwa);

            }
            System.out.println(znak);


        }

        pokaWiersz(rozmiar);

    }

    static void pokaWiersz(int rozmiar) {
        for (int i = 1; i <= rozmiar; i++) {
            System.out.print(znak);
        }
        System.out.println();
    }

}
