package com.company.powtorzenie.pracaDomowa121;

class Bus extends Vehicle {
    String firma;
    int numerKursu;

    Bus(String typ, int maxSpeed, int liczbaPazazerow, String model, String firma, int numerKursu) {
        super(typ, maxSpeed, liczbaPazazerow, model);
        this.firma = firma;
        this.numerKursu = numerKursu;
    }

    @Override
    void pokażDaneTechniczne() {
        System.out.printf("typ: %s, max Speed: %s, liczba pasażerów: %s, model: %s, firma: %s, numer kursu %s,"
                , typ, maxSpeed, liczbaPazazerow, model, firma, numerKursu);
    }
}
