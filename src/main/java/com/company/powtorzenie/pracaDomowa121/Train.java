package com.company.powtorzenie.pracaDomowa121;

class Train extends Bus {
    private String krajDocelowy;
    private int dlugoscSkladu;

    Train(String typ, int maxSpeed, int liczbaPazazerow, String model, String firma, int numerKursu, String krajDocelowy, int dlugoscSkladu) {
        super(typ, maxSpeed, liczbaPazazerow, model, firma, numerKursu);
        this.krajDocelowy = krajDocelowy;
        this.dlugoscSkladu = dlugoscSkladu;
    }

    void wszystkieInformacje() {
        System.out.printf("typ: %s, max Speed: %s, liczba pasażerów: %s, model: %s, firma: %s, numer kursu %s, kraj docelowy: %s, dlugość składu: %s"
                , typ, maxSpeed, liczbaPazazerow, model, firma, numerKursu, krajDocelowy, dlugoscSkladu);

    }
}
