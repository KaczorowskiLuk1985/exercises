package com.company.powtorzenie.pracaDomowa121;

class Car extends Vehicle {
    String kolor;
    int cena;

    Car(String typ, int maxSpeed, int liczbaPazazerow, String model, String kolor, int cena) {
        super(typ, maxSpeed, liczbaPazazerow, model);
        this.kolor = kolor;
        this.cena = cena;
    }

    @Override
    void pokażDaneTechniczne() {
        System.out.printf("typ: %s, max Speed: %s, liczba pasażerów: %s, model: %s, kolor: %s, cena %s,"
                , typ, maxSpeed, liczbaPazazerow, model, kolor, cena);
    }
}
