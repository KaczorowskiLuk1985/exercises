package com.company.powtorzenie.pracaDomowa121;

abstract class Vehicle {
    String typ;
    String model;
    int maxSpeed;
    int liczbaPazazerow;

    public Vehicle(String typ, int maxSpeed, int liczbaPazazerow, String model) {
        this.typ = typ;
        this.maxSpeed = maxSpeed;
        this.liczbaPazazerow = liczbaPazazerow;
        this.model = model;
    }

    abstract void pokażDaneTechniczne();
}
