package com.company.powtorzenie.pracaDomowa121;


class PracaDomowa121 {
    public static void main(String[] args) {
        Car samochod1 = new Car("Samochód", 210, 4, "Volvo", "czarny", 200000);
        samochod1.pokażDaneTechniczne();
        System.out.println();
        Bus autobus = new Bus("autobus", 90, 10, "Solaris", "PKS", 139);
        autobus.pokażDaneTechniczne();

        Train pociąg = new Train("Pociąg", 400, 240, "TGW", "PKP", 201, "Niemcy", 130);
        System.out.println();
        pociąg.wszystkieInformacje();
        System.out.println();
        pociąg.pokażDaneTechniczne();
    }
}
