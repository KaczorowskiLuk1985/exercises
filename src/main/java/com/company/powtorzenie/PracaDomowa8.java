package com.company.powtorzenie;
/*
*ZADANIE #20*
Utwórz metodę, w której pętlą wyświetlisz wszystkie liczby od `0` do wartości podanej przez użytkownika (przekazywanej jako parametr do tej metody)
> gdyż użytkownik poda `3` wyświetl `0, 1, 2, 3`
>
> gdyż użytkownik poda `9` wyświetl `0, 1, 2, 3, 4, 5, 6, 7, 8, 9`
>
> gdyż użytkownik poda `18` wyświetl `0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18`

Utwórz metodę, w której pętlą *wyświetlisz* wszystkie liczby od liczby przekazanej jako parametr do `0` (tj. w kolejności malejącej).
> Dla `9` wyświetli `9, 8, 7, 6, 5, 4, 3, 2, 1, 0`
>
> Dla `4` wyświetli `4, 3, 2, 1, 0`
 */

public class PracaDomowa8 {
    public static void main(String[] args) {
        ciąg(4);
        ciąg(12);
        ciąg(18);
        System.out.println();
        malejacyCiag(4);
        malejacyCiag(9);

    }

    static void ciąg(int parametr) {
        for (int i = 0; i <= parametr; i++) {
            System.out.print(i + ",");
        }
        System.out.println();
    }

    static void malejacyCiag(int parametr) {
        for (int i = parametr; i >= 0; i--) {
            System.out.print((i + "'"));
        }
        System.out.println();
    }
}
