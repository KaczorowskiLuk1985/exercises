package com.company.powtorzenie;
/*
Utwórz metodę, w której użytkownik podaje z klawiatury rozmiar tablicy, a następnie podaje wszystkie elementy (jako `int`):
Podaj rozmiar tablicy:  4
Liczba[1]:  88
Liczba[2]:  12
Liczba[3]:  -7
Liczba[4]:  195
Podana tablica to: [88, 12, -7, 195]
Suma elementów wynosi: 288
 */

import java.util.Arrays;
import java.util.Scanner;

public class PracaDomowa45 {
    public static void main(String[] args) {
        pokazCosTam();


    }
    static void pokazCosTam (){
        System.out.print("Podaj rozmiar tablicy : ");
        Scanner scanner = new Scanner(System.in);
        int rozmiarTablicy = scanner.nextInt();
        int [] tablica = new int[rozmiarTablicy];
        int suma = 0;

        for (int i = 0; i <rozmiarTablicy ; i++) {
            System.out.print("liczba ["+(i+1)+"]: ");
            tablica[i]= scanner.nextInt();
            suma += tablica[i];

        }
        System.out.println(Arrays.toString(tablica));
        System.out.println("suma = "+suma);
    }
}
