package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby.
Metoda ma zwrócić nową tablicę do której na wybranej pozycji (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
Dla `([1, 2, 3, 4, 5], 2, 77)`
powinno zwrócić `[1, 2, 77, 3, 4, 5]`
 */

import java.util.Arrays;

public class PracaDomowa44 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println(Arrays.toString(zwrocTablice(tab, 3, 77)));

    }

    static int[] zwrocTablice(int[] tablica, int pozycja, int nowyElement) {
        int[] nowaTablica = new int[tablica.length + 1];


        for (int i = 0; i < tablica.length; i++) {
            nowaTablica[i] = tablica[i];
            nowaTablica[pozycja] = nowyElement;


        }

        for (int i = pozycja + 1; i <= tablica.length; i++) {
            nowaTablica[i] = tablica[i - 1];
        }

        return nowaTablica;
    }
}
