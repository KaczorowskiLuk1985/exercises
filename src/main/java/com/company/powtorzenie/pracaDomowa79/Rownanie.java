package com.company.powtorzenie.pracaDomowa79;

public class Rownanie {
    private int a;
    private int b;
    private int c;

    Rownanie() {

    }

    Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    int rownanieGlowne() {
        return a * a + b * b * b + c * c * c * c;
    }

    boolean czyLiczaJestWiekszaOdWyniku(int x) {
        return x > rownanieGlowne();
    }

    @Override
    public String toString() {
        return "Rownanie{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

    double obliczWynikDodawania() {
        return a + b + c;
    }

    void wyswietl() {
        System.out.printf("%s + %s + %s = %s", a, b, c, obliczWynikDodawania());
    }

    boolean czyWynikPrzekroczylZakres(int liczba) {
        return liczba > obliczWynikDodawania();

    }
}
