package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci informację
(w formie wartości logicznej) czy *obydwie* są parzyste.
Oraz utwórz metodę, do której przekazujesz dwa parametry
a ona zwróci informację (w formie wartości logicznej) czy *chociaż jeden z nich* jest większy od zera. (edited)
 */

public class PracaDomowa3 {
    public static void main(String[] args) {
        System.out.println(obieParzyste(2, 2));
        System.out.println(obieParzyste(1, 2));
        System.out.println(obieParzyste(2, 1));
        System.out.println(obieParzyste(1, 1));
        System.out.println(" ");
        System.out.println(chociażJednaParzysta(2, 2));
        System.out.println(chociażJednaParzysta(-1, 2));
        System.out.println(chociażJednaParzysta(-2, -1));
        System.out.println(chociażJednaParzysta(1, -1));

    }static boolean obieParzyste(int liczbaA, int liczbaB) {
        if (liczbaA % 2 == 0 && liczbaB % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }static boolean chociażJednaParzysta (int a, int b){
        if (a  > 0 || b > 0){
            return true;

        }else {
            return false;
        }
    }
}
