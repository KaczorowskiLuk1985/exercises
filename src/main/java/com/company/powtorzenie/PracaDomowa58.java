package com.company.powtorzenie;
/*
Utwórz metodę, która wykorzystuje *varargs* by przekazać do metody dowolną,
 większą od zera, liczbę elementów typu `String`
 i zwrócić jeden napis sklejony z nich.
 */

public class PracaDomowa58 {
    public static void main(String[] args) {
        System.out.println(skelejTo("Ala ", "ma ", "kota."));

    }

    static String skelejTo(String... napisy) {
        String string = new String();
        for (String i : napisy) {
            string += i;

        }
        return string;
    }
}
