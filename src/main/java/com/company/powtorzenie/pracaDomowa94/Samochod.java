package com.company.powtorzenie.pracaDomowa94;

class Samochod {
    private String nazwa;
    private int rocznik;
    private Kolor kolor;

    Samochod(String nazwa, int rocznik, Kolor kolor) {
        this.nazwa = nazwa;
        this.rocznik = rocznik;
        this.kolor = kolor;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "NAZWA = '" + nazwa + '\'' +
                ", ROK = " + rocznik +
                ", KOLOR = " + kolor +
                '}';
    }
}
