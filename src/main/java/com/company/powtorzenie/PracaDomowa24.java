package com.company.powtorzenie;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę wszystkich elementów.
Utwórz także metodę, która jako parametr przyjmuje tablicę i zwraca średnią wszystkich elementów
 */

public class PracaDomowa24 {
    public static void main(String[] args) {
        int[] tab = new int[]{100, 1, 1, 1, 1, 13};
        System.out.println(sumujElementyTablicy(tab));
        System.out.println(sredniaWszystkichElementow(tab));
        System.out.println(srednia2(tab));
    }

    static int sumujElementyTablicy(int[] tablica) {
        int suma = 0;

        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        return suma;
    }

    static double sredniaWszystkichElementow(int[] tablica) {
        int sredni = 0;

        for (int i = 0; i < tablica.length; i++) {
            sredni += tablica[i];


        }
        return sredni / (double) tablica.length;
    }

    static double srednia2(int[] tablica) {
        int sredni2 = 0;

        for (int liczba:tablica){
            sredni2 += liczba;

        }
        return sredni2 / (double)tablica.length;
    }
}
