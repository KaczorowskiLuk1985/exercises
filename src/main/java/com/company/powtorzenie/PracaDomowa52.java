package com.company.powtorzenie;

import java.util.Arrays;

/*
*ZADANIE #52 - DO DOMU*
Utwórz metodę, która przyjmuje dwie tablice.
Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice mogą być różnych długości!
- Liczby sumujemy jak w “dodawaniu pisemnym”, tzn. gdy wartość jest większa od `9` to `1` “idzie dalej”.
[1,  2,  3,  4]
   [ 6,  7,  8]
[1,  9,  1,  2]
 */
public class PracaDomowa52 {
    public static void main(String[] args) {
        int[] tab1 = new int[]{1, 2, 9, 6};
        int[] tab2 = new int[]{3, 4, 5, 5};
        int[] tab3 = new int[]{1, 5, 8, 7, 9};
        int[] tab4 = new int[]{2, 5, 7, 9, 3};

        int[] wynik = sumujTablice(tab1, tab2, tab3, tab4);

        System.out.println(Arrays.toString(wynik));

        int[] skroconaTablica = usunPustePozycje(wynik);
        System.out.println(Arrays.toString(skroconaTablica));

    }

    static int[] usunPustePozycje(int[] tablica) {
        int licznikZer = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == 0) {
                licznikZer++;
            } else {
                break;

            }
        }
        int[] nowaTablica = new int[tablica.length - licznikZer];
        for (int index = 0; index < nowaTablica.length; index++) {
            nowaTablica[index] = tablica[index + licznikZer];

        }
        return nowaTablica;
    }

    static int[] sumujTablice(int[] tab1, int[] tab2, int[] tab3, int[] tab4) {
        int[] tablicaPomocnicza = {tab1.length - 1, tab2.length - 1, tab3.length - 1, tab4.length - 1};
        int[] tablicaWynikowa = new int[10];

        for (int index = tablicaWynikowa.length -1; index > 0; index--) {
            int suma = tablicaWynikowa[index];

            if (tablicaPomocnicza[0] >= 0) {
                suma += tab1[tablicaPomocnicza[0]]; //instrukcja
                tablicaPomocnicza[0]--;
            }

            if (tablicaPomocnicza[1] >= 0) {
                suma += tab2[tablicaPomocnicza[1]]; //instrukcja

                tablicaPomocnicza[1]--;
            }
            if (tablicaPomocnicza[2] >= 0) {
                suma += tab3[tablicaPomocnicza[2]]; //instrukcja

                tablicaPomocnicza[2]--;
            }
            if (tablicaPomocnicza[3] >= 0) {
                suma += tab4[tablicaPomocnicza[3]]; //instrukcja

                tablicaPomocnicza[3]--;
            }
            if (suma > 9) {
                tablicaWynikowa[index] = suma % 10;
                tablicaWynikowa[index - 1] = suma / 10;

            } else {
                tablicaWynikowa[index] = suma;
            }
        }
        return tablicaWynikowa;
    }


}
