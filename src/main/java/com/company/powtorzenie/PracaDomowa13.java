package com.company.powtorzenie;
/*
*ZADANIE #26*
Utwórz metodę, do której przekazujesz jeden parametr i *zwraca* sumę wszystkich elementów od `1` do podanej liczby
> dla `3` zwróci `1 + 2 + 3 = 6`
>
> dla `5` zwróci `15` bo `1 + 2 + 3 + 4 + 5 = 15`
>
> dla `11` zwróci `66` bo `1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 = 66`
 */

public class PracaDomowa13 {
    public static void main(String[] args) {
        System.out.println(kolejnaMetodaKtoraCosZwraca(20));
    }

    static int kolejnaMetodaKtoraCosZwraca(int parametr) {
        int wynik = 0;

        for (int x = 1; x <= parametr; x++) {
           wynik += x;
        }
        return wynik;
    }
}