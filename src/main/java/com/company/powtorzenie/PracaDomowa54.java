package com.company.powtorzenie;

/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca średnią wartość elementów z pominięciem największego.
dla `[1, 2, 3, 10, 4, 5, 6, 11]`
zwróci `31` (ponieważ pomija `11`)
 */
public class PracaDomowa54 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 10, 4, 5, 6, 11};
        int[] tab2 = new int[]{11, 4, 2, 2, 2, 2, 2, 2};
        System.out.println(zwracaNajwieksza(tab));
        System.out.println(zwracaNajwieksza(tab2));

    }

    static int zwracaNajwieksza(int[] tablica) {
        int suma = 0;
        int max = tablica[0];

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
            suma += tablica[i];

        }
        return suma;
    }
}
