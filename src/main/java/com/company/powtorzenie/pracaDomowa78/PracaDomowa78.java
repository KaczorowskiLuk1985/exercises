package com.company.powtorzenie.pracaDomowa78;

public class PracaDomowa78 {
    public static void main(String[] args) {
        Rectangle prostokat1 = new Rectangle(4, 5);
        Rectangle prostokat2 = new Rectangle(4, 4);

        System.out.println(prostokat1.policzObwod());
        System.out.println(prostokat2.policzObwod());

        System.out.println("obwody sa rowne? " + Rectangle.czyObwodTakiSam(prostokat1, prostokat2));
    }


}
