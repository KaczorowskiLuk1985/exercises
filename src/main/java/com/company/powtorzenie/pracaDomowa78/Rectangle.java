package com.company.powtorzenie.pracaDomowa78;

public class Rectangle {
    private int dluzszyBok;
    private int krotszBok;

    public Rectangle(int dluzszyBok, int krotszBok) {
        this.dluzszyBok = dluzszyBok;
        this.krotszBok = krotszBok;
    }

    int policzObwod() {
        return 2 * dluzszyBok + 2 * krotszBok;

    }

    static boolean czyObwodTakiSam(Rectangle r1, Rectangle r2) {
        return r1.policzObwod() == r2.policzObwod();
    }
}
