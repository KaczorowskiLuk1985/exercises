package com.company.powtorzenie.pracaDomowa119;
/*
*Utwórz klasę `Osoba` z polami:*
- `imie` [String]
- `wiek` [int]
- `plec` (jako typ wyliczeniowy).

*Utwórz klasę `Student` (dziedziczący po klasie `Osoba`) z polami*
- `uczelnia` (obiekt klasy `Academy`)
- `rokStudiow` [int]
- mapę w postaci `PrzedmiotUczelniany : Integer` (reprezentujące ocenę z danego przedmiotu)

*Utwórz klasę `PrzedmiotUczelniany` z polami*
- `nazwa` [String]
- `wykladowca` (obiekt klasy `Wykladowca`)

*Utwórz klasę `Wykladowca` z polami:*
- lista zawierając obiekty klasy `Academy`).
- tytuł naukowy (typ wyliczeniowy)

*Utwórz klasę `Academy` z polami*
-`nazwa` [String]
- `adres` [String]
- lista studentów (lista obiektów klasy `Student`)
- lista wykładowców (lista obiektów klasy `Wykladowca`) (edited)
 */

public class PracaDomowa119 {
    public static void main(String[] args) {
        Instructor instructor1 = new Instructor("Adam", 67, Person.Sex.MAN, Instructor.AcademicTitle.PROFESOR);
        Instructor instructor2 = new Instructor("Michał", 77, Person.Sex.MAN, Instructor.AcademicTitle.PROFESOR);
        Instructor instructor3 = new Instructor("Przemek", 87, Person.Sex.MAN, Instructor.AcademicTitle.PROFESOR);

        Academy academy1 = new Academy("Budownictwo", "ul. Politechniki");
        Academy academy2 = new Academy("Informatyka", "ul. Politechniki");
        Academy academy3 = new Academy("Filozofia", "ul. Pomorstka");

        Subject matematyka = new Subject("matematyka", instructor1);
        Subject fizyka = new Subject("fizyka", instructor2);
        Subject chemia = new Subject("chemia", instructor3);


        Student student1 = new Student("Paweł", 22, Person.Sex.MAN, 4);
        student1.dodajOcenę(matematyka, 3);
        student1.dodajOcenę(fizyka, 3);
        student1.dodajOcenę(chemia, 4);

        Student student2 = new Student("Qtas", 23, Person.Sex.MAN, 3);
        student2.dodajOcenę(matematyka, 4);
        student2.dodajOcenę(fizyka, 5);
        student2.dodajOcenę(chemia, 4);

        Student student3 = new Student("Daria", 34, Person.Sex.WOMAN, 3);
        student3.dodajOcenę(matematyka, 4);
        student3.dodajOcenę(fizyka, 4);
        student3.dodajOcenę(chemia, 5);

        Person person = new Person("Emi", 33, Person.Sex.WOMAN);
        System.out.println(person);

        System.out.println(student1.toString());
        System.out.println(student2.toString());
        System.out.println(student3.toString());

        academy1.addStudent(student1);
        academy1.addStudent(student2);
        academy1.addStudent(student3);

        academy1.printInstructor();

        academy2.addStudent(student1);
        academy2.addStudent(student2);
        academy2.addStudent(student3);

        academy2.printInstructor();


    }

}
