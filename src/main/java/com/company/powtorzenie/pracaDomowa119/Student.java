package com.company.powtorzenie.pracaDomowa119;

import java.util.HashMap;
import java.util.Map;

class Student extends Person {
    private Academy academy;
    private int studyYear;
    private Map<Subject, Integer> subjectAndRateMap = new HashMap<>();

    public Student(String name, int age, Sex sex, int studyYear) {
        super(name, age, sex);
        this.studyYear = studyYear;
    }
    void dodajOcenę(Subject subject,Integer rate){
        subjectAndRateMap.put(subject,rate);

    }

    @Override
    public String toString() {
        return super.toString() +", rok studiów =" + studyYear + ", wszystkie oceny ="+ subjectAndRateMap;
    }
}

