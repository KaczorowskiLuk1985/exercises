package com.company.powtorzenie.pracaDomowa119;

import java.util.List;

class Instructor extends Person {
    private AcademicTitle academicTitle;

    public Instructor(String name, int age, Sex sex, AcademicTitle academicTitle) {
        super(name, age, sex);
        this.academicTitle = academicTitle;
    }

    enum AcademicTitle {
        MAGISTER, DOkTOR, PROFESOR;
    }

}
