package com.company.powtorzenie.pracaDomowa119;

class Subject {
    private String name;
    Instructor instructor;

    public Subject(String name, Instructor instructor) {
        this.name = name;
        this.instructor = instructor;
    }

    @Override
    public String toString() {
        return
                "przedmiot = " + name ;
    }
}
