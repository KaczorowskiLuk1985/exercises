package com.company.powtorzenie.pracaDomowa119;

import java.util.ArrayList;
import java.util.List;

class Academy {
    private String name;
    private String addres;
    private List<Student> studentList = new ArrayList<>();
    private List<Instructor> instructorList = new ArrayList<>();

    public Academy(String name, String addres) {
        this.name = name;
        this.addres = addres;
    }

    void addStudent(Student student) {
        studentList.add(student);
    }

    void addInstructor(Instructor instructor) {
        instructorList.add(instructor);
    }

    void printInstructor() {
        System.out.println("uczelnia: " + name + " addres: " + addres + ", studiują: ");
        for (Student student : studentList) {
            System.out.println(student);
        }
    }
}
