package com.company.powtorzenie;
/*
*ZADANIE #30*
Utwórz metodę, która przyjmuje *jeden* parametr (który jest liczbą wierszy i kolumn) oraz wyświetla tabliczkę mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
> Dla `3` wyświetli:
>
1   2   3
2   4   6
3   6   9
 */

public class PracaDomowa17 {
    public static void main(String[] args) {
        tabliczkaMnożenia(3);

    }

    static void tabliczkaMnożenia(int liczbaWszystkiego) {
        for (int i = 1; i <= liczbaWszystkiego; i++) {

            for (int j = 1; j <= liczbaWszystkiego; j++) {
                System.out.print(i * j + "\t");

            }
            System.out.println();
        }
    }
}
