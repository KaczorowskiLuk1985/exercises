package com.company.powtorzenie.pracaDomowa97;

public class Osoba {
    private String imie;
    private String nazw;
    private int wiek;
    private Plec plec;

    public Osoba(String imie, String nazw, int wiek, Plec plec) {
        this.imie = imie;
        this.nazw = nazw;
        this.wiek = wiek;
        this.plec = plec;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie = '" + imie + '\'' +
                ", nazw = '" + nazw + '\'' +
                ", wiek = " + wiek +
                ", plec = " + plec +
                '}';
    }

    public String getImie() {
        return imie;
    }

    public String getNazw() {
        return nazw;
    }

    public int getWiek() {
        return wiek;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazw(String nazw) {
        this.nazw = nazw;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }
}
