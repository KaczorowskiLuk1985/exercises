package com.company.powtorzenie.pracaDomowa97;

public class PracaDomowa97 {
    public static void main(String[] args) {
        Osoba[] osoby = new Osoba[]{
                new Osoba("jan", "nowak", 33, Plec.MEZCZYZNA),
                new Osoba("kamil", "jakistam", 45, Plec.MEZCZYZNA),
                new Osoba("dzesika", "smith", 18, Plec.KOBIETA)
        };
        int licznik = 0;

        for (Osoba osoba : osoby) {
            System.out.println(osoba);
            if (osoba.getPlec() == Plec.MEZCZYZNA) {
                licznik++;
            }
        }
        System.out.println("liczba mężczyzn : " + licznik);
        System.out.println(osoby[1].getImie());
        osoby[1].setImie("JANKLOD");
        System.out.println(osoby[1]);


    }
}