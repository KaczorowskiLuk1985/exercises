package com.company.powtorzenie;
/*
*ZADANIE #22*
Utwórz metodę, która wyświetli wszystkie liczby całkowite z przedziału `10` a `20`.

Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14`
(krańcowy zakres przedziału ma być drugim parametrem metody)
 */

public class PracaDomowa9 {
    public static void main(String[] args) {
        wyswietlPrzedzial();
        System.out.println();
        wyswietlPrzedzial2();
        System.out.println();
        wyswietlPrzedzial3();

    }

    static void wyswietlPrzedzial() {
        for (int i = 10; i <= 20; i++) {
            System.out.print(i + ",");
        }
    }

    static void wyswietlPrzedzial2() { //while
        int poczatek = 10;
        while (poczatek <= 20) {
            System.out.print(poczatek + ",");
            poczatek++;
        }
    }

    static void wyswietlPrzedzial3() { //do while
        int poczatek = 10;
        do {
            System.out.print(poczatek + ",");
            poczatek++;
        }
        while (poczatek <= 20);
    }
}
