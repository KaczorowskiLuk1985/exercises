package com.company.powtorzenie;
/*
*ZADANIE #27*
Utwórz metodę, która przyjmuje jeden parametr oraz wyświetla daną “kolumnę tabliczki mnożenia”:
>Dla `15` wyświetli(!)
>`15 * 1  = 15`
>`15 * 2  = 30`
>
>`...`
>
>`15 * 9  = 135`
>`15 * 10 = 150`
 */

public class PracaDomowa14 {
    public static void main(String[] args) {
        tabliczkaMnozenia(15);
        System.out.println();
        metodaWhile(15);
        System.out.println();
        metodaDoWhile(15);
    }

    static void tabliczkaMnozenia(int liczba) {
        for (int i = 1; i <= 10; i++) {
            System.out.printf("%s * %s = %s\n", liczba, i, i * liczba);
        }
    }

    static void metodaWhile(int liczba) {
        int i = 1;

        while (i <= 10) {
            i++;
            System.out.printf("%s * %s = %s\n", liczba, i, i * liczba);
        }
    }

    static void metodaDoWhile(int liczba) {
        int i = 1;
        do {
            System.out.printf("%s * %s = %s\n", liczba, i, i * liczba);
            i++;
        }
        while (i <= 10);
    }
}
