package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma usunąć z podanej tablicy element o wybranym indeksie i zwrócić nową tablicę. Zaimplementuj dodatkowo metodę służącą do wyświetlania tablicy.
> Dla `([1, 2, 3, 4, 5],  2)`
> zwróci `[1, 2, 4, 5]`
 */

import java.util.Arrays;

public class PracaDomowa46 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5,6};
        int pozycja = 3;
        System.out.println(Arrays.toString(zwracaTablice(tab, pozycja)));


    }

    static int[] zwracaTablice(int[] tablica, int pozycja) {
        int[] nowaTablica = new int[tablica.length - 1];

        for (int i = 0; i < pozycja; i++) {
            nowaTablica[i] = tablica[i];
        }
        for (int i = pozycja; i < nowaTablica.length; i++) {
            nowaTablica[i] = tablica[i+1];

        }


        return nowaTablica;
    }

}

