package com.company.powtorzenie;
/*
Utwórz metodę, która zwraca tablicę o podanym rozmierza wypełnioną
losowymi liczbami (użyj klasy `Random()`). Rozmiar tablicy ma być parametrem metody.
 */

import java.util.Arrays;
import java.util.Random;

public class PracaDomowa27 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrocTablice(10)));

    }
    static int [] zwrocTablice (int rozmiarTablicy){
        int [] tablica = new int[rozmiarTablicy];
        Random r = new Random();
        for (int index = 0; index < rozmiarTablicy; index++) {
            tablica[index] = r.nextInt(20)-10;
        }
        return tablica;
    }


}
