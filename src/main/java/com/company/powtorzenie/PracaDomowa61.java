package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje conajmniej DWA parametry - boolean oraz dowolną ilość (większą od 0) liczb (typu `int`).
Gdy pierwszy parametr ma wartość `true` metoda zwraca największą przekazaną liczbą, a gdy `false` najmniejszą. (edited)
 */
public class PracaDomowa61 {
    public static void main(String[] args) {
        System.out.println(zwrocNajwiekszaLiczbe(false, 3, 5, 6, 7, 8, 5, 4));
        System.out.println(zwrocNajwiekszaLiczbe(true, 3, 5, 6, 7, 8, 5, 4));

    }

    static int zwrocNajwiekszaLiczbe(boolean wieksza, int... liczba) {
        int max = liczba[0];
        int min = liczba[0];

        for (int i : liczba) {
            if (i > max){
                max = i;
            }

        }

        return wieksza? max:min;
    }
}
