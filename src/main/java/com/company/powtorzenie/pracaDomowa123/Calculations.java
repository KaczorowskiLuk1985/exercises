package com.company.powtorzenie.pracaDomowa123;

public interface Calculations {
    double policzPole();

    double policzObwod();

    double policzObjęość(int wysokosc);
}
