package com.company.powtorzenie.pracaDomowa123;

public class Square extends Rectangle {
    public Square(int dlugi) {
        super(dlugi, dlugi);
    }

    @Override
    String wyświetlInfoOSobie() {
        return "jestem Kwadratem o długosci boku " + dlugi;
    }

}
