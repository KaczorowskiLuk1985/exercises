package com.company.powtorzenie.pracaDomowa123;

public class Rectangle extends Shape {
    int dlugi;
    int krotki;

    public Rectangle(int dlugi, int krotki) {
        this.dlugi = dlugi;
        this.krotki = krotki;
    }

    @Override
    String wyświetlInfoOSobie() {
        return "jestem prostokatem o bokach " + dlugi + " i " + krotki;
    }

    @Override
    public double policzPole() {
        return dlugi * krotki;
    }

    @Override
    public double policzObwod() {
        return 2 * dlugi + 2 * krotki;
    }

    @Override
    public double policzObjęość(int wysokosc) {
        return policzPole() * wysokosc;
    }
}
