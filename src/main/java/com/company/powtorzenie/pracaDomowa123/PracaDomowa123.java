package com.company.powtorzenie.pracaDomowa123;
/*
Utwórz klasę `Ksztalt` oraz klasy dziedziczące po niej:
- klasę `Prostokat` (z polami `dlugosc`, `szerokosc`),
- klasę `Kwadrat` (z polem `dlugosc`),
- klasę `Koło` (z polem `promien`).
Zablokuj możliwość tworzenia obiektów klasy `Ksztalt` oraz wymuś,
by klasy dziedziczące musiały zaimplementować metodę `wyswietlInformacjeOSobie()` (zwracająca `String`).

Utwórz interfejs `Obliczenia` w którym będą metody `policzPole()`, `policzObwod()`
oraz `policzObjetosc(int wysokosc)`. Interfejs ten zaimplementuj we wcześniej utworzonych klasach.
 */


import java.util.ArrayList;
import java.util.List;

public class PracaDomowa123 {
    public static void main(String[] args) {
        Rectangle prostokat = new Rectangle(3, 2);
        Square kwadrat = new Square(5);
        Circle kolo = new Circle(4);

//        System.out.println(prostokat.wyświetlInfoOSobie());
//        System.out.println(kwadrat.wyświetlInfoOSobie());
//        System.out.println(kolo.wyświetlInfoOSobie());

        List<Shape> listaKształtow = new ArrayList<>();
        listaKształtow.add(prostokat);
        listaKształtow.add(kolo);
        listaKształtow.add(kwadrat);

        for (Shape shape : listaKształtow) {
            System.out.println(shape.wyświetlInfoOSobie());
            System.out.printf("moja objetosc wynosi %.2f\n", shape.policzObjęość(3));
        }

    }
}
