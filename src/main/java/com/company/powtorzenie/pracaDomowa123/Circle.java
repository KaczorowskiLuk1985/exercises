package com.company.powtorzenie.pracaDomowa123;

public class Circle extends Shape {
    int promien;

    public Circle(int promien) {
        this.promien = promien;
    }

    @Override
    String wyświetlInfoOSobie() {
        return "jestem kołęm o promieniu " + promien;
    }

    @Override
    public double policzPole() {
        return promien * promien * Math.PI;
    }

    @Override
    public double policzObwod() {
        return 2 * promien * Math.PI;
    }

    @Override
    public double policzObjęość(int wysokosc) {
        return policzPole() * wysokosc;
    }
}
