package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy,
a następnie zwraca nowo utworzoną, dwuwymiarową tablicę wypełnioną losowymi wartościami.
Utwórz drugą metodę do wyświetlania zwróconej tablicy.
 */

import java.util.Arrays;
import java.util.Random;

public class PracaDomowa62 {
    public static void main(String[] args) {
        int [][] tab = zwracaTabliceRandom(3,6);
        wyswietlTablice(tab);


    }static int[][]zwracaTabliceRandom(int dlugosc, int szerokosc){
        int [][]nowaTablica = new int[dlugosc][szerokosc];
        Random random = new Random();

        for (int i = 0; i <dlugosc ; i++) {
            for (int j = 0; j < szerokosc; j++) {
                nowaTablica[i][j]= random.nextInt(100)-50;
            }
        }
        return nowaTablica;
    }
    static void wyswietlTablice(int[][] tablicJebana){

        for (int i = 0; i < tablicJebana.length; i++) {
            for (int j = 0; j <tablicJebana[0].length ; j++) {
                System.out.print(tablicJebana[i][j]+"\t");

            }
            System.out.println();

        }

    }

}
