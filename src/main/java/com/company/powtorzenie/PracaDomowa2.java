package com.company.powtorzenie;
/*
Utwórz metodę, która zwróci informację (w formie wartości logicznej)
czy przekazana liczba jest parzysta czy nie.
>Dla `12` zwroci `true`
>
>Dla `9` zwróci `false`
>
>Dla `1` zwróci `false`
>
>Dla `40` zwróci `true`
>
>Dla `123` zwróci `false`
 */

public class PracaDomowa2 {
    public static void main(String[] args) {
        System.out.println(czyParzysta(3));
        System.out.println(czyParzysta(6));
        System.out.println(czyParzysta(1));
        System.out.println(czyParzysta(2));
    }
    static boolean czyParzysta(int x) {

        if (x % 2 == 0 ) {
            return true;
        }
        return false;
    }
}
