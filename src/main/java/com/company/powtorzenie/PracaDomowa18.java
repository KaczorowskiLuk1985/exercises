package com.company.powtorzenie;
/*
*ZADANIE #31*
Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę”
Dla `4` wyświetli:

1
12
123
1234
 */

public class PracaDomowa18 {
    public static void main(String[] args) {
        pokazChoinke(6);

    }

    static void pokazChoinke(int liczbaWierszy) {
        int obecnyWynik = 1;
        for (int i = 1; i<=liczbaWierszy ; i++) {

            for (int j = 1; j <= i; j++) {
                System.out.print(obecnyWynik++ +"\t");

            }
            System.out.println();


        }
    }
}
