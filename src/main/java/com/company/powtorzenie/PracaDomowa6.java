package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz liczbę,
która jest numerem miesiąca, a metoda powinien *zwrócić nazwę* tego miesiąca.
Wykorzystaj instrukcję warunkową *switch*
 */

public class PracaDomowa6 {
    public static void main(String[] args) {
        System.out.println(jakiToMiesiac(13));
        System.out.println();
        System.out.println(miesiac(4));

    }

    static String jakiToMiesiac(int a) {
        switch (a) {
            case 1:
                return "styczeń";
            case 2:
                return "luty";
            case 3:
                return "marzec";
            case 4:
                return "kwiecien";
            case 5:
                return "maj";
            case 6:
                return "czerwiec";
            case 7:
                return "lipiec";
            case 8:
                return "sierień";
            case 9:
                return "wrzesień";
            case 10:
                return "pażdziernik";
            case 11:
                return "listopad";
            case 12:
                return "grudzień";

            default:
                return "nie ma takiego miesiąca";
        }
    }
    static String miesiac (int numerMiesiaca){
        String komunikat = "błędny numer";
        switch (numerMiesiaca){
            case 1:
                komunikat = "marzec";
                break;
            case 2:
                komunikat = "kwiecień";
                break;
            case 3:
                komunikat = "maj";
                break;
        }return komunikat;
    }

}
