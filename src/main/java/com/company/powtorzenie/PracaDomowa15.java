package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz dwa parametry. Metoda ma wyświetlić wszystkie potęgi pierwszej liczby do momentu nie przekroczenia drugiej liczby.
dla `3, 100` wyświetli:

0 -> 1
1 -> 3
2 -> 9
3 -> 27
4 -> 81 (kolejny krok dałby 243, więc przekroczyłby 100)

dla `4, 300` wyświetli:

0 -> 1
1 -> 4
2 -> 14
3 -> 64
4 -> 256 (kolejny krok dałby 1024, więc przekroczyłby 300)
 */

public class PracaDomowa15 {
    public static void main(String[] args) {
        wyswietlPotegi(4, 300);

    }

    static void wyswietlPotegi(int liczba, int granica) {
        for (int i = 0; ; i++) {
            double wynik = Math.pow(liczba,i);
            if (wynik <= granica){
                System.out.printf(i + "-->" + wynik + "\n");
            }

        }
    }
}
