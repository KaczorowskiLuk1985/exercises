package com.company.powtorzenie.pracaDomowa96;

import com.company.powtorzenie.pracaDomowa94.Kolor;

class NumeryTelefonow {
    private String numer;
    private Kierunkowy kierunkowy;

    public NumeryTelefonow(String numer, Kierunkowy kierunkowy) {
        this.numer = numer;
        this.kierunkowy = kierunkowy;
    }

    @Override
    public String toString() {
        return "NumeryTelefonow{" +
                "numer = '" + numer + '\'' +
                ", kierunkowy = " + kierunkowy +
                ", kierunkowy z plusem(jako tekst) = " + kierunkowy.wyswietlText() +
                ", kierunkowy (jako int) = " + kierunkowy.wyswietlKierunkowy() +
                '}';
    }
    void wyswietlNumer() {
        System.out.println("(" + kierunkowy.wyswietlText()
                + ")" + numer);
        System.out.println(kierunkowy.wyswietlKierunkowy()+" "+numer);

    }
}
