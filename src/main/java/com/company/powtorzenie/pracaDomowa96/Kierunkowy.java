package com.company.powtorzenie.pracaDomowa96;

enum Kierunkowy {
    POLSKA(48), ROSJA(7), NIEMCY(49);
    private int kierunkowy;

    Kierunkowy(int kierunkowy) {
        this.kierunkowy = kierunkowy;
    }

    String wyswietlText() {
        return "+" + kierunkowy;
    }

    int wyswietlKierunkowy() {
        return kierunkowy;
    }
}
