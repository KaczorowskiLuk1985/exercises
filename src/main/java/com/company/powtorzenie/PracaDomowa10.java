package com.company.powtorzenie;
/*
Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14`
(krańcowy zakres przedziału ma być drugim parametrem metody)
 */

public class PracaDomowa10 {
    public static void main(String[] args) {
        coDrugaParzysta(0, 14);
        System.out.println();
        metoda2(0, 14);
        System.out.println();
        metoda3(0,14);

    }

    static void coDrugaParzysta(int start, int stop) {
        for (int i = start; i <= stop; i = i + 2) {
            System.out.print(i + ",");

        }
    }

    static void metoda2(int start, int stop) {
        int i = start;
        while (i <= stop) {
            System.out.print(i + ",");
            i = i + 2;

        }
    }

    static void metoda3(int start, int stop) {
        int i = start;
        do {
            System.out.print(i + ",");
            i = i + 2;

        } while (i <= stop);
    }
}
