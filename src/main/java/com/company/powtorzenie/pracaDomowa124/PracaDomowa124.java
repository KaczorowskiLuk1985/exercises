package com.company.powtorzenie.pracaDomowa124;

/*
Utwórz klasy `Osoba` (z polami `imie`, `nazwisko`, `wiek`) i `Pracownik`
(dziedzicząca z Osoby, z polem `pensja`)
oraz utwórz interfejs `PrzedstawSie` w którym będzie metody `wyswietlPelneDane()`,
 `wyswietlImie()` oraz `zwrocWiekDoEmerytury()`
 */

class PracaDomowa124 {
    public static void main(String[] args) {

        Person person = new Person("adam", "nowak", 59, Person.Sex.FACET);

        person.hallo();
        person.printName();
        person.timeToRetiring();

        Employee pracownik1 = new Employee("Jacek", "szpinak", 64, Person.Sex.FACET);
        pracownik1.hallo();
        pracownik1.ustawPensje(40000);
        System.out.println(pracownik1.hallo() + ", pensja : " + pracownik1.pensja());
    }
}
