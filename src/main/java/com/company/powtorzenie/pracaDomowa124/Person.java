package com.company.powtorzenie.pracaDomowa124;

class Person implements Welcome {
    private String name;
    private String surname;
    private int age;
    private Sex sex;

    Person(String name, String surname, int age, Sex sex) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public String hallo() {
        return "name: " + name +
                ", surname: " + surname +
                ", age: " + age +
                ", sex: " + sex;
    }


    @Override
    public void printName() {
        System.out.println("imię: " + name);
    }

    @Override
    public void timeToRetiring() {
        int wiekDoEmeryt = 67;

        if (age < wiekDoEmeryt) {
            System.out.println("pozostało do emerytury :" + (wiekDoEmeryt - age) + " l.");
        } else {
            System.out.println("jestś na emeryturze");
        }
    }

    enum Sex {
        FACET, FACETKA
    }
}
