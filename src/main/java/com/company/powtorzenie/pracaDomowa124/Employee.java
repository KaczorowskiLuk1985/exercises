package com.company.powtorzenie.pracaDomowa124;

class Employee extends Person {
    private double salary;

    Employee(String name, String surname, int age, Sex sex) {
        super(name, surname, age, sex);

    }

    void ustawPensje(double salary) {
        this.salary = salary;
    }
    double pensja(){
        return salary;
    }
}

