package com.company.powtorzenie;
/*
*ZADANIE #51*
Utwórz metodę, która przyjmuje dwie tablice.
Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
*Przyjęte założenia:*
- Tablice są tych samych długości.
- Wartości na danej pozycji mogą być większę niż 10

>
[1,  2,  3,  4]
[5,  6,  7,  8]
[6,  8, 10, 11]
 */

import java.util.Arrays;

public class PracaDomowa51 {
    public static void main(String[] args) {
        int[]tab1=new int[]{1,2,3,4};
        int[]tab2=new int[]{5,6,7,8};
        System.out.println(Arrays.toString(zwrocSumeTablic(tab1, tab2)));

    }

    static int[] zwrocSumeTablic(int[] tab1, int[] tab2) {
        int [] nowaTablica = new int[tab1.length];

        for (int i = 0; i < tab1.length ; i++) {
            nowaTablica[i]=tab1[i]+tab2[i];


        }
        return nowaTablica;
    }
}
