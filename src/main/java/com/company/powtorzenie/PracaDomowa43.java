package com.company.powtorzenie;

/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
> dla `[1, 2, 3, 4, 5]` zwróci `4`
 */
public class PracaDomowa43 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 7, 5, 4};
        System.out.println(dajDrugaNajwiekszaLiczb(tab));

    }

    static int dajDrugaNajwiekszaLiczb(int[] tablica) {
        int najwieksza = Integer.MIN_VALUE;
        int drugaNajw = Integer.MIN_VALUE;

        for (int i = 0; i < tablica.length; i++) {

            if (tablica[i] > najwieksza) {
                drugaNajw = najwieksza;
                najwieksza = tablica[i];
            } else if (tablica[i] > drugaNajw && tablica[i] != najwieksza) {
                drugaNajw = tablica[i];
            }

        }
        return drugaNajw;
    }
}
