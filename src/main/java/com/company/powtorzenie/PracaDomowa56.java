package com.company.powtorzenie;

/*
Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
Metoda ma zwrócić *ile elementów* (idąc po kolei od lewej) należy zsumować by przekroczyć podany (jako drugi) parametr
dla `([1,2,3,4,5,6],  9)`
należy zwrócić 2
 */
public class PracaDomowa56 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println(zwracaLiczbe(tab, 9));

    }

    static int zwracaLiczbe(int[] tablica, int liczba) {
        int suma = 0;

        for (int i = tablica.length - 1; i >= 0; i--) {
            suma += tablica[i];
            if (suma >= liczba) {
                return tablica.length - i;

            }


        }

        return suma;
    }
}

