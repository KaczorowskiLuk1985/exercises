package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz dwa parametry następnie wyświetlasz wszystkie liczby z podanego przedziału *I ICH DZIELNIKI*

Dane wyświetl w formie:
> dla `1, 6` wyświetli:
>
 1  <--  1
 2  <--  1, 2,
 3  <--  1, 3,
 4  <--  1, 2, 4,
 5  <--  1, 5,
 6  <--  1, 2, 3, 6```
 */


public class PracaDomowa23 {
    public static void main(String[] args) {
        pokazLiczbyZDzielnikami(1, 6);

    }

    static void pokazLiczbyZDzielnikami(int start, int stop) {
        if (start > stop) {
            System.out.println("błędny przedział");
        }
        for (int liczbyPrzedziału = start; liczbyPrzedziału <= stop; liczbyPrzedziału++) {
            System.out.print(liczbyPrzedziału+" <--- ");

            for (int dzielnik = 1; dzielnik <= liczbyPrzedziału; dzielnik++) {
                if (liczbyPrzedziału % dzielnik == 0) {
                    System.out.print(dzielnik+ ",");
                }
            }
            System.out.println();
        }
    }
}
