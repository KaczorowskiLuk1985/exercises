package com.company.powtorzenie;
/*
Utwórz metodę, która wykorzystuje mechanizm *varargs*
by przekazać do metody dowolną, większą od zera, liczbę elementów typu `int` i zwrócić ich sumę.
 */

public class PracaDomowa57 {
    public static void main(String[] args) {
        System.out.println(zwracaSume(5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5));

    }

    static int zwracaSume(int... liczby) {
        int suma = 0;

        for (int i : liczby) {
            suma += i;
        }
        return suma;
    }
}
