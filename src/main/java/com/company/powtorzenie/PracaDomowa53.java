package com.company.powtorzenie;
/*
Utwórz metodę, która jako parametr przyjmuje
tablicę i zwraca różnicę pomiędzy największym a najmniejszym elementem.
 */

public class PracaDomowa53 {
    public static void main(String[] args) {
        int[]tab=new int[]{-9,2,3,4,5,6};
        System.out.println(roznicaPomiedzyElementami(tab));

    }
    static int roznicaPomiedzyElementami (int[]tablica){
        int max = tablica[0];
        int min = tablica[0];

        for (int i = 0; i < tablica.length ; i++) {

            if (tablica[i] > max){
                max = tablica[i];
            }
            if (tablica[i] < min){
                min = tablica[i];
            }
        }
        int roznica = max - min;
        return roznica;
    }
}
