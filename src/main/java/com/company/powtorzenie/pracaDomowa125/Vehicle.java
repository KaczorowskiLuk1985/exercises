package com.company.powtorzenie.pracaDomowa125;

abstract class Vehicle implements Character {
    String name;
    int maxSpeed;
    int numberOfPassengers;

    public Vehicle(String name) {
        this.name = name;
    }

    @Override
    public final int maxPredkosc() {
        return maxSpeed;
    }

    @Override
    public final int liczbaPasazerow() {
        return numberOfPassengers;
    }

    final void ustawPArametry(int maxSpeed, int numberOfPassengers) {
        this.maxSpeed = maxSpeed;
        this.numberOfPassengers = numberOfPassengers;
    }
}
