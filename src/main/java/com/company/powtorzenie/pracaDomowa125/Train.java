package com.company.powtorzenie.pracaDomowa125;

public class Train extends Vehicle {
    private int liczbaWagonow;
    private boolean czyJestZombie;

    public Train(String name, int liczbaWagonow, boolean czyJestZombie) {
        super(name);
        this.liczbaWagonow = liczbaWagonow;
        this.czyJestZombie = czyJestZombie;
    }

    @Override
    public String toString() {
        return "Train{" +
                "liczbaWagonow=" + liczbaWagonow +
                ", czyJestZombie=" + czyJestZombie +
                "maxSpeed: " + maxSpeed +
                '}';
    }
}

