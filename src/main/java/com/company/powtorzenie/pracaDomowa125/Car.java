package com.company.powtorzenie.pracaDomowa125;

class Car extends Vehicle {
    private int liczbaKol;
    private int liczbadrzwi;

    public Car(String name, int liczbaKol, int liczbadrzwi) {
        super(name);
        this.liczbaKol = liczbaKol;
        this.liczbadrzwi = liczbadrzwi;
    }

    @Override
    public String toString() {
        return "Car{" +
                "liczbaKol=" + liczbaKol +
                ", liczbadrzwi=" + liczbadrzwi + "liczba pasażerów: " + numberOfPassengers +
                '}' ;
    }
}
