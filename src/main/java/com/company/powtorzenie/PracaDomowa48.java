package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz początek i koniec przedziału.
Metoda ma zwrócić nową tablicę, która będzie zawierać elementy z wybranego przedziału.
> Dla `([1, 7, 11, -4, 51],  2,  4)`
> zwróci `[11, -4, 51]`
 */

import java.util.Arrays;

public class PracaDomowa48 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 7, 11, -4, 51};
        System.out.println(Arrays.toString(zwrocPrzedział(tab, 2, 4)));

    }

    static int[] zwrocPrzedział(int[] tablica, int poczatek, int koniec) {
        int[] nowaTblica = new int[koniec - poczatek + 1];
        for (int i = poczatek; i <= koniec; i++) {
            nowaTblica[i - poczatek] = tablica[i];


        }
        return nowaTblica;
    }
}
