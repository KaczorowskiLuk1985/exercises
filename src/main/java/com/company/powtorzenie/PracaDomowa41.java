package com.company.powtorzenie;
/*
*ZADANIE #41*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
> Dla `4` zwróci: `[10, 11, 12, 13]`
>
> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */


import java.util.Arrays;

public class PracaDomowa41 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwrocTablice(8)));

    }

    private static int[] zwrocTablice(int liczbaKolumn) {
        int[] nowaTablica = new int[liczbaKolumn];
        for (int i = 0; i < liczbaKolumn; i++) {
            nowaTablica[i] += 10 + i;
        }
        return nowaTablica;
    }
}
