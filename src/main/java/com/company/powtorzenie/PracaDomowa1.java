package com.company.powtorzenie;
/*
Utwórz metodę, do której przekazujesz trzy parametry
a ona *zwróci* największy z nich z nich.
Czyli dla `1, 2, 3` zwróci `3`,

a dla `3, 23, 10` zwróci `23`
(edited)
 */

public class PracaDomowa1 {
    public static void main(String[] args) {
        największaZNich(23,23,10);
        największaZNich(50,23,10);
        największaZNich(3,23,50);
        największaZNich(3,-1,10);

    }

    static void największaZNich(int a, int b, int c) {
        if (a >= b && a >= c){
            System.out.println(a);
        }else if (b >= a && b >= c){
            System.out.println(b);
        }else {
            System.out.println(c);
        }
    }
}
