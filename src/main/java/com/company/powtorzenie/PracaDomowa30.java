package com.company.powtorzenie;
/*
Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
> dla `[1, 2, 3, 4, 5]` zwróci `4`
 */

public class PracaDomowa30 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5, 5};
        System.out.println(zwrocLiczbe(tab));

    }

    static int zwrocLiczbe(int[] tablica) {
        int max = Integer.MIN_VALUE;
        int drugiMax = Integer.MIN_VALUE;

        for (int index = 0; index < tablica.length; index++) {
            if (tablica[index] > max) {
                drugiMax = max;
                max = tablica[index];
            } else if (tablica[index] > drugiMax && tablica[index] != max) {
                drugiMax = tablica[index];
            }
        }
        return drugiMax;
    }
}
