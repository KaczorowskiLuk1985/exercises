package com.company.powtorzenie;

import java.util.Arrays;

/*
Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
Dla `4` zwróci: `[10, 11, 12, 13]`

Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
 */
public class PracaDomowa28 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(zwracaTabliceElementow(8)));
        System.out.println(Arrays.toString(zwracaTabliceElementow(4)));

    }
    static int [] zwracaTabliceElementow (int rozmiar){
        int [] tablica = new int[rozmiar];
        for (int index =0; index < rozmiar; index++) {
            tablica[index]= index+10;
        }
        return tablica;
    }
}
