package com.company.powtorzenie;
/*
Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby które są indexami (pozycjami) zakresu.
Metoda ma zwrócić sumę elementów w podanym przedziale.
> Dla `([1, 2, 3, 4, 5], 2, 4)`
> zwróci `12`, bo `3 + 4 + 5`
 */

public class PracaDomowa49 {
    public static void main(String[] args) {
        int[] tab = new int[]{1, 2, 3, 4, 5};
        System.out.println(zwrocSumęElementów(tab, 2, 4));

    }

    static int zwrocSumęElementów(int[] tablica, int poczatek, int koniec) {
        int suma = 0;
        if (poczatek > koniec || poczatek < 0 || koniec > tablica.length - 1) {
            System.out.println("bledny przedzial");

        } else {
            for (int i = poczatek; i <= koniec; i++) {
                suma += tablica[i];
            }
        }
        return suma;
    }
}