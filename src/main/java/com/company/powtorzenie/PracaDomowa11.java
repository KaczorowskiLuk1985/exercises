package com.company.powtorzenie;
/*
*ZADANIE 24*
Utwórz metodę, do której przekazujesz jeden parametr a następnie wyświetlasz tyle wielokrotności liczby `10`
> Dla `4` wyświetli `10, 20, 30, 40`
>
> Dla `7` wyświetli `10, 20, 30, 40, 50, 60, 70`
 */

public class PracaDomowa11 {
    public static void main(String[] args) {
        wielokrotnosc10(4);
        System.out.println();
        wielokrotnosc10(7);
        System.out.println();
        metodaWhile(4);
        System.out.println();
        metodaWhile(7);
        System.out.println();
        metodaDoWhile(4);
        System.out.println();
        metodaDoWhile(7);

    }

    static void wielokrotnosc10(int parametr) {
        for (int i = 10; i <= 10 * parametr; i = i + 10) {
            System.out.print(i + ",");
        }
    }

    static void metodaWhile(int parametr) {
        int i = 10;
        while (i <= 10 * parametr) {
            System.out.print(i + ",");
            i = i + 10;
        }
    }

    static void metodaDoWhile(int parametr) {
        int i = 10;
        do {
            System.out.print(i + ",");
            i = i + 10;
        } while (i <= 10 * parametr);
    }
}
