package com.company.powtorzenie.pracaDomowa95;

class PracaDomowa95 {
    public static void main(String[] args) {
        Waluta waluta = new Waluta(3.5,4.5,TypWalut.EURO);
        System.out.println(waluta);
        waluta.pokazZysk(4);
        System.out.println();

        Waluta waluta1 = new Waluta(4,5,TypWalut.PLN);
        System.out.println(waluta1);
        waluta1.pokazZysk(5);
        System.out.println();
//        System.out.println(waluta1.toString()); //nie ma sensu użyć .toString gdyz to sie juz nadpisał
        Waluta waluta2 = new Waluta(5.5,6.1,TypWalut.DOLAR);
        System.out.println(waluta2);
        waluta2.pokazZysk(70);

    }


}
