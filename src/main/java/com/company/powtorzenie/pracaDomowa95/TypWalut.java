package com.company.powtorzenie.pracaDomowa95;

enum TypWalut {
    EURO("euro", "Europa"), DOLAR("dolar", "USA"), PLN("złoty", "Polska");

    private String rodzajWaluty;
    private String kraj;

    TypWalut(String rodzajWaluty, String kraj) {
        this.rodzajWaluty = rodzajWaluty;
        this.kraj = kraj;
    }

    public String getRodzajWaluty() {
        return rodzajWaluty;
    }

    public String getKraj() {
        return kraj;
    }
}
