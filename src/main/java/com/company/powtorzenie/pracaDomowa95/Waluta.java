package com.company.powtorzenie.pracaDomowa95;

class Waluta {
    private double kursSprzedarzy;
    private double kursKupna;
    private TypWalut typWalut;

    public Waluta(double kursSprzedarzy, double kursKupna, TypWalut typWalut) {
        this.kursSprzedarzy = kursSprzedarzy;
        this.kursKupna = kursKupna;
        this.typWalut = typWalut;
    }

    @Override
    public String toString() {
        return "Waluta{" +
                "kursSprzedarzy = " + kursSprzedarzy +
                ", kursKupna = " + kursKupna +
                ", typWalut = " + typWalut.getRodzajWaluty() +
                ", miejsce  = " + typWalut.getKraj() +
                '}';
    }
    void pokazZysk(double kwota){
        double przelicznik = kursKupna/kursSprzedarzy;
        System.out.printf("Za %s kupisz %.2f %s",
                kwota,
                przelicznik,
                typWalut);

    }

    public TypWalut getTypWalut() {
        return typWalut;
    }
}
