package com.company.powtorzenie;
/*
*ZADANIE #39*
Utwórz metodę, która przyjmuje dwa parametry - tablicę (wartości `short`) oraz liczbę.
Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy
 */

public class PracaDomowa26 {
    public static void main(String[] args) {
        short [] tablica = new short[]{1,2,3,4,5};
        short szukana = 5;
        boolean wynik = liczbaWTablicy(tablica, szukana);
        boolean wynik2 = nowaLiczbaWTablicy(tablica, szukana);
        System.out.println( wynik ? "jest w tablicy" : "nie ma w tablicy");
        System.out.println( wynik2 ? "jest w tablicy" : "nie ma w tablicy");

    }
    static boolean liczbaWTablicy (short [] tab, short poszukiwana ){
        for ( short liczba:tab){
            if (poszukiwana == liczba){
                return true;
            }
        }
        return false;
    }
    static boolean nowaLiczbaWTablicy (short [] tab, short poszukiwana){
        for (int liczba = 1; liczba <= tab.length; liczba++) {
            if (liczba == poszukiwana){
                return true;
            }

        }
        return false;
    }
}
