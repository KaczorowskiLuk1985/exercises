package com.company.powtorzenie;
/*
*ZADANIE #17*
Utwórz metodę, do której przekazujesz wartość logiczną, a ona *wyświetli* napis `Tak`,
w przypadku przekazania `true` oraz `Nie` w przypadku przekazania `false`

 */

public class PracaDomowa5 {
    public static void main(String[] args) {
        System.out.println(wyswietlTakLubNie(true));
        System.out.println(wyswietlTakLubNie(false));
        System.out.println(wyswietlTakLubNie(false));

    }

    static String wyswietlTakLubNie(boolean tak) {
        if (tak == true) {
            return "tak";
        } else if (tak == false) {
            return "nie";
        }
        return "Nic";
    }
}
