package com.company.powtorzenie.pracaDomowa118;

class Supervisor extends Employee {
    private double bonus;

    Supervisor(String name, int age, double salary, double bonus) {
        super(name, age, salary);
        this.bonus = bonus;
    }


    String wszystkieInformacje() {

        return super.przedstawSię() + super.wysokoscPensji() + " Wysokość premi: " + bonus;
    }

}
