package com.company.powtorzenie.pracaDomowa118;
/*
*ZADANIE #118*
Utwórz klasę `Osoba`, która zawiera pola `imie`, `wiek`.
Następnie utwórz klasę `Pracownik`, która zawiera dodatkowe pola `pensja`.
Następnie utwórz klasę `Kierownik`, która rozszerza klasę `Pracownik` i posiada
 dodatkowe pole `premia`.
Utwórz obieky wszystkich klas oraz przetestuj działanie.
 */

public class PracaDomowa118 {
    public static void main(String[] args) {

        Person person = new Person("Emi", 33);
        Employee employee = new Employee("Kalina", 23, 3000.00);
        Supervisor supervisor = new Supervisor("Maciek", 44, 15000.00, 10000.00);

//        System.out.println(person.przedstawSię());
//        System.out.println(employee.przedstawSię() + employee.wysokoscPensji());
//        System.out.println(supervisor.wszystkieInformacje() + supervisor.miesieczneZarobki(4));

        Suma suma = new Suma();
        System.out.println(suma.dodaj(10, 20));
        System.out.println(suma.dodaj(10.0, 20.0));
        System.out.println(suma.dodaj(10, 20, 30));
        System.out.println(suma.dodaj(10.0, 20.0, 30.0));
        System.out.println(suma.dodaj(1,2,2));

    }
}
