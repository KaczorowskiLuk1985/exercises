package com.company.powtorzenie.pracaDomowa118;

class Employee extends Person {
    private double salary;

    Employee(String name, int age, double salary) {
        super(name, age);
        this.salary = salary;
    }

    String wysokoscPensji() {
        return "Wysokość Pensji: " + salary;
    }

    String miesieczneZarobki(int liczbaMiesiecy) {

        return "pensja za " + liczbaMiesiecy + " miesiecy wynosi" + liczbaMiesiecy * salary;

    }
}
