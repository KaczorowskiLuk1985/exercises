package com.company.powtorzenie.pracaDomowa118;

class Person {
    private String name;
    private int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    String przedstawSię() {

        return "nazywam się " + name + " i mam " + age + " l. ";
    }
}
