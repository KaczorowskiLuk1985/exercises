package com.company;

/*
his time no story, no theory. The examples below show you how to write function accum:

Examples:

Accumul.accum("abcd");    // "A-Bb-Ccc-Dddd"
 */
public class ZadanieRozrywkowe5 {
    private final static String SEPARATOR = "-"; // pamietac o priv final i static, wszystkie napisy robimy odrazu pod klasą

    public static void main(String[] args) {   //napisy  w kodzie powinny byc zmiennymi
        System.out.println(accum("abCd"));
    }

    public static String accum(String s) {
        StringBuilder result = new StringBuilder();
        String[] arrayString = s.toLowerCase().split(""); //tutaj zmieniamy na małe bo oferacje wykonujemy tylko raz.
        //w petli by wykonywalo za kazdym razem.
        int counter = 0;

        for (String letter : arrayString) {
            result.append(letter.toUpperCase());   //result += letter.toUpperCase()
            for (int index = 0; index < counter; index++) {
                result.append(letter);
            }
            result.append(SEPARATOR);
            counter++;
        }
        //return result.replaceFirst(".$",""); .$ oznacza statni znak w wyrazie
        return result.substring(0,result.length()-1);
    }
}