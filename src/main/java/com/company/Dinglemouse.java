package com.company;

public class Dinglemouse {
    private String firstName;
    private String lastName;

    public Dinglemouse(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFullName() {
        if (firstName.equals("")){
            return lastName;
        }
        if (lastName.equals("")){
            return firstName;
        }
        if (firstName.equals("")&& lastName.equals("")){
            return"";
        }
        return firstName + " " + lastName;
    }
}
