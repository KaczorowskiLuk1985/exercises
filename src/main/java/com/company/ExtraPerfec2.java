package com.company;
/*
zwraca elementy ktore w zapisie binarnym zaczynaja i koncza sie na ten sam znak i w srodku maja zero
 */

import java.util.Arrays;
import java.util.stream.IntStream;

public class ExtraPerfec2 {
    public static void main(String[] args) {
        int num1 = 35;
        System.out.println(Integer.toBinaryString(num1) + " <--- zapis binarny liczby " + num1);
        System.out.println(Arrays.toString(extraPerfect(num1)));

    }

    private static int[] extraPerfect(int number) {


        return IntStream.rangeClosed(1, number)
                //iltf zawsze zwraca wartosc logiczna
//                .filter(ExtraPerfec2::isBinaryStringCorrect)//metoda extraperf2 zostala utworzona dla lepszej czytelnosci
                .filter(num -> {
                    boolean resoult = isBinaryStringCorrect(num);
                    System.out.printf("%s\t -->\t %s, \twartosc logiczna: %s \n", num, Integer.toBinaryString(num), resoult);
                    return resoult;
                })
                .toArray();
    }

    private static boolean isBinaryStringCorrect(int num) {
        final char firstLast = '1'; // deklaracja i inicjalizacja
        String binaryString = Integer.toBinaryString(num);

        if (binaryString.charAt(0) == firstLast
                && binaryString.charAt(binaryString.length() - 1) == firstLast) {
            // jeżeli dlugosc jes krotsza niz 3 to nie sprawdzamy dalej bo nie wykroimy srodka)
            if (binaryString.length() < 3) {
                return true;
            } else {
                // w streamach granica zawsze sie zatrzymuje przed wskazanym miejscem. wyjatkiem jest metoda .rangeClosed
                String srodek = binaryString.substring(1, binaryString.length() - 1);
                return !srodek.contains("1");
            }
        }
        return false;
    }
}
