package com.company.codewars;
/*
https://www.codewars.com/kata/going-to-the-cinema/train/java
 */

public class CodewarsL50 {
    public static void main(String[] args) {

//TODO pętla while, Timed Out
    }

    public static int movie(int card, int ticket, double perc) {
        int counter = 0;
        double systemBCost = card;
        double systemACost = 0;
        double systemBiletCost = ticket * perc;
        while (Math.ceil(systemBCost) > systemACost) {
            systemBCost += systemBiletCost;
            systemBCost += ticket;
            systemBiletCost *= perc;
            counter++;
        }

        return counter;
    }
}
