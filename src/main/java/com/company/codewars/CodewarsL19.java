package com.company.codewars;
/*
https://www.codewars.com/kata/sorted-yes-no-how/train/java
 */

import java.util.Arrays;

public class CodewarsL19 {
    public static void main(String[] args) {
        int[] tab = {1, -15, -15, -15};
        System.out.println(isSortedAndHow(tab));

    }

    public static String isSortedAndHow(int[] array) {
        int[] sortArr = new int[array.length];
        int[] odwrocona = new int[array.length];
        int licznik = array.length - 1;

        for (int i = 0; i < array.length; i++) {
            sortArr[i] = array[i];
        }
        Arrays.sort(sortArr);
        for (int i = 0; i < sortArr.length; i++) {
            odwrocona[i] = sortArr[licznik];
            licznik--;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == sortArr[i]) {
                return "yes, ascending";
            }
            if (array[i] == odwrocona[i]) {
                return "yes, descending";
            }
            else {
                return "no";
            }
        }
        return "";
    }

}
