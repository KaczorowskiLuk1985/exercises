package com.company.codewars;
/*
https://www.codewars.com/kata/i-love-you-a-little-a-lot-passionately-dot-dot-dot-not-at-all/train/java
 */

import java.util.Arrays;

public class Codewars52 {
    public static void main(String[] args) {
        System.out.println(howMuchILoveYou(7));

    }

    public static String howMuchILoveYou(int nb_petals) {


        return Arrays.asList("not at all", "I love you",
                "a little",
                "a lot",
                "passionately",
                "madly")
                .get(nb_petals % 6);
    }
}
