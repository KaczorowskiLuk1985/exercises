package com.company.codewars;
/*
https://www.codewars.com/kata/most-digits/train/java
 */

public class CodewarsL25 {
    public static void main(String[] args) {
        int[] a = {3, 40000, 100};
        int[] b = {1, 200, 250, 100000};
        int[] c = {-10, 1, 0, 1, 10};

        System.out.println(findLongest(a));
        System.out.println(findLongest(b));
        System.out.println(findLongest(c));

    }

    public static int findLongest(int[] numbers) {
        int licznik = 0;
        int licznik2 = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            if (String.valueOf(Math.abs(numbers[i])).length() > licznik) {
                licznik = String.valueOf(Math.abs(numbers[i])).length();
                licznik2 = numbers[i];
            }
        }
        return licznik2;
    }
}
