package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/printer-errors/train/java
 */
public class CodewarsS2 {
    public static void main(String[] args) {
        System.out.println(printerError("aaaxbbbbyyhwawiwjjjwwm"));
        System.out.println(printerError("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz"));

    }

    public static String printerError(String s) {
//        String[] split = s.split("");
//        int counter = 0;
//
//        for (int i = 0; i < split.length; i++) {
//            if (!split[i].matches("[a-m]")) {
//                counter++;
//            }
//        }
//        return String.format("%s/%s",counter,s.length());

        long count = Arrays.stream(s.split(""))
                .filter(x -> !x.matches("[a-m]"))
                .count();

        return String.format("%s/%s",count,s.length());

//        return s.replaceAll("[a-m]", "").length() + "/" + s.length();
    }
}
