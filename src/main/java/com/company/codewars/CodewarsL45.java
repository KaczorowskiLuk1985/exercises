package com.company.codewars;

import java.util.Arrays;

public class CodewarsL45 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(NumbersWithDigitInside2(11, 1)));
        System.out.println(Arrays.toString(NumbersWithDigitInside2(7, 6)));
        System.out.println(Arrays.toString(NumbersWithDigitInside2(5, 6)));
        System.out.println(Arrays.toString(NumbersWithDigitInside2(20, 0)));
        System.out.println(Arrays.toString(NumbersWithDigitInside2(44, 4)));
    }

    public static long[] NumbersWithDigitInside(long x, long d) {
        long[] tablicaWynikowa = new long[3];
        String szukanaLiczba = String.valueOf(d);
        long licznik = 0;
        String[] tabString = new String[(int) x];
        long drugiLicznik = 1;

        for (int i = 0; i < x; i++) {

            tabString[i] = String.valueOf(i + 1);

            if (tabString[i].contains(szukanaLiczba)) {
                licznik++;
                drugiLicznik *= i;

                tablicaWynikowa[0] = licznik;
                tablicaWynikowa[1] += Integer.valueOf(tabString[i]);
                tablicaWynikowa[2] = drugiLicznik;
            }
        }
        System.out.println(Arrays.toString(tabString));
        return tablicaWynikowa;
    }

    public static long[] NumbersWithDigitInside2(long x, long d) {
        long liczniki = 0, suma = 0, iloczyn = 0;

        for (long i = 1; i <= x; i++) {
            if (String.valueOf(i).contains(String.valueOf(d))) {
                liczniki++;
                suma += i;
                iloczyn = iloczyn == 0 ? i : iloczyn * i;
            }
        }
        return new long[]{liczniki, suma, iloczyn};
    }
}