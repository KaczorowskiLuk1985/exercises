package com.company.codewars;
/*
https://www.codewars.com/kata/simple-beads-count/train/java
 */

import java.util.stream.IntStream;

public class cw10 {
    public static void main(String[] args) {
        System.out.println(countRedBeads(2));
        System.out.println(countRedBeads(3));
        System.out.println(countRedBeads(4));
        System.out.println(countRedBeads(5));
        System.out.println(countRedBeads(1));

    }

    public static int countRedBeads(final int nBlue) {

        return nBlue < 2 ? 0 : 2 * nBlue - 2;
    }
}