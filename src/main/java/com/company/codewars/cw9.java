package com.company.codewars;
/*
https://www.codewars.com/kata/maximum-length-difference/train/java
 */

import java.util.Arrays;

public class cw9 {
    public static void main(String[] args) {
        String[] s1 = new String[]{"hoqq", "bbllkw", "oox", "ejjuyyy", "plmiis", "xxxzgpsssa", "xxwwkktt", "znnnnfqknaz", "qqquuhii", "dvvvwz"};
        String[] s2 = new String[]{"cccooommaaqqoxii", "gggqaffhhh", "tttoowwwmmww"};
//        String[] s2 = new String[]{""};

        System.out.println(mxdiflg(s1, s2));

    }

    public static int mxdiflg(String[] a1, String[] a2) {
        int countX = 0;
        int countY = 0;
        int a = 0;


        for (int i = 0; i < a1.length; i++) {
            a+= a1[i].length();
            if (a1[i].length() > countX) {
                countX = a1[i].length();

            }

        }
        System.out.println(countX);
        System.out.println(a/9);

        for (int i = 0; i < a2.length; i++) {
            if (a2[i].length() > countY) {
                countY = a2[i].length();
            }
        }
        System.out.println(countY);

        int abs = countX / a1.length + countY / a2.length;
//        System.out.println(abs + " = abs");


        return (countX == 0 || countY == 0) ? -1 : Math.abs(countX - countY);
    }

}
