package com.company.codewars;
/*
https://www.codewars.com/kata/balanced-number-special-numbers-series-number-1/train/java
 */

import java.util.Arrays;

public class CodewarsS45 {
    public static void main(String[] args) {

        System.out.println(balancedNum(7));
        System.out.println(balancedNum(959));
        System.out.println(balancedNum(13));
        System.out.println(balancedNum(432));
        System.out.println(balancedNum(424));
        System.out.println(" + ");
        System.out.println(balancedNum(1024));
        System.out.println(balancedNum(66545));
        System.out.println(balancedNum(295591));
        System.out.println(balancedNum(1230987));
        System.out.println(balancedNum(56239814));

    }

    public static String balancedNum(long number) {
        String[] num = String.valueOf(number).split("");
        String balanced = "Balanced";
        String notBalanced = "Not Balanced";
        int left = 0;
        int right = 0;

        if (num.length < 3) {
            return balanced;

        } else if (num.length % 2 != 0) {
            for (int i = 0; i < num.length / 2; i++) {
                left += Integer.valueOf(num[i]);

            }
            for (int i = num.length / 2 + 1; i < num.length; i++) {
                right += Integer.valueOf(num[i]);
            }
//            System.out.println(left + " + " + right);
            return left == right ? balanced : notBalanced;
        } else if (num.length % 2 == 0) {
            for (int i = 0; i < num.length / 2 - 1; i++) {
                left += Integer.valueOf(num[i]);
            }
            for (int i = num.length / 2 + 1; i < num.length; i++) {
                right += Integer.valueOf(num[i]);
            }
            return left == right ? balanced : notBalanced;

        }

        return " KAC MORDERCA ";
    }
}
