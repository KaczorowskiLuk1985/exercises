package com.company.codewars;
/*
https://www.codewars.com/kata/find-the-smallest-integer-in-the-array/
 */

import java.util.Arrays;

public class CodewarsStream1 {
    public static void main(String[] args) {
        int[] tab = {0, 56, 232, 12, 11, 43};

        System.out.println(findSmallestInt(tab));

    }

    private static int findSmallestInt(int[] args) {

        return Arrays.stream(args)
                .min()
                .getAsInt();
    }
}
