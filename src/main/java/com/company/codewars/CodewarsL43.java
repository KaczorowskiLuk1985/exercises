package com.company.codewars;
/*
https://www.codewars.com/kata/sum-of-odd-cubed-numbers/train/java
 */

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

public class CodewarsL43 {
    public static void main(String[] args) {
        int[] tab = {1, 2, 3, 4};
        int[] tab2 = {-5, -5, 5, 5};
        int[] tab3 = {3, 2, -2, -3};
        int[] tab4 = {-3};
        System.out.println(cubeOdd(tab));
        System.out.println(cubeOdd(tab2));
        System.out.println(cubeOdd(tab3));
        System.out.println(cubeOdd(tab4));


    }

    public static int cubeOdd(int arr[]) {
        int licznik = 0;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                licznik += arr[i] * arr[i] * arr[i];
            }
        }
        return licznik;
    }
}
