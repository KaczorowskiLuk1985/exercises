package com.company.codewars;
/*
https://www.codewars.com/kata/are-they-the-same/train/java
 */

import java.util.Arrays;

public class cw20 {
    public static void main(String[] args) {

        int[] a = {121, 144, 19, 161, 19, 144, 19, 11};
        int[] b = {121, 14641, 20736, 361, 25921, 361, 20736, 361};
        System.out.println(comp(a, b));

        int[] c = {2, 2, 3};
        int[] d = {4, 9, 9};
        System.out.println(comp(c, d));

        int[] e = null;
        int[] f = null;
        System.out.println(comp(e, f));

        int[] g = new int[0];
        int[] h = new int[0];
        System.out.println(comp(g, h));
    }

    private static boolean comp(int[] a, int[] b) {

        if (a == null || b == null || a.length != b.length) {
            return false;
        }

        for (int i = 0; i < a.length; i++) {
            a[i] = a[i] * a[i];
        }

        Arrays.sort(a);
        Arrays.sort(b);

        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        //git
        return true;
    }
}
