package com.company.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

public class    CodewarsStream3 {
    public static void main(String[] args) {
        String napis = "jestememme ab aabbbbbbbbbbbb";
        System.out.println(spinWord(napis));

    }

    public static String spinWord(String sentence) {
        String separator = " ";
        String[] split = sentence.split(separator);
        return Arrays.stream(split)
                .map(slowo -> {
                    if (slowo.length() > 4) {
//                        return new StringBuilder(slowo).reverse().toString();
//                        odwracanie petla
                        String noweSlowo = "";
                        for (char s : slowo.toCharArray()) {
                            noweSlowo = s + noweSlowo; // odwraca wyraz
                        }
                        return noweSlowo;
                    } else {
                        return slowo;
                    }
                })
                .collect(Collectors.joining(separator));
    }
}
