package com.company.codewars;
/*
https://www.codewars.com/kata/exes-and-ohs/train/java
 */
public class cw18 {
    public static void main(String[] args) {
        System.out.println(getXO("xxxooo"));
        System.out.println(getXO("xxxXooOo"));
        System.out.println(getXO("zpzpzpp"));
        System.out.println(getXO("xooxx"));
    }

    public static boolean getXO(String str) {

//        String[] split = str.toLowerCase().split("");
//        int o = 0;
//        int x = 0;
//
//        for (int i = 0; i < split.length; i++) {
//            if (split[i].equals("o")) {
//                o++;
//            }
//            if (split[i].equals("x")) {
//                x++;
//            }
//        }
//        return x == o;

        str.toLowerCase();
        return str.replace("o", "").length() == str.replace("x", "").length();
    }
}
