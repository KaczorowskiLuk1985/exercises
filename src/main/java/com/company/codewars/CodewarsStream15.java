package com.company.codewars;
/*
https://www.codewars.com/kata/string-repeat
 */
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CodewarsStream15 {
    public static void main(String[] args) {
        System.out.println(repeatStr(0, "I"));

    }

    public static String repeatStr(final int repeat, final String string) {

        return IntStream
                .range(0, repeat)
                .mapToObj(liczba -> string)
                .collect(Collectors.joining());
    }
}
