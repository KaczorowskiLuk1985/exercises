package com.company.codewars;
/*
https://www.codewars.com/kata/count-the-digit/train/java
 */

import java.util.Arrays;

public class CodewarsL28 {
    public static void main(String[] args) {
       // System.out.println(nbDig(10, 1));
       // System.out.println(nbDIG(11011, 2));
        System.out.println(nbDig(11011, 2));
      //  System.out.println(nbDig(25, 1));

    }

    public static int nbDig(int n, int d) {
        int kwadrat = 1;
        int licznik = 0;
        String napis = "";

        for (int i = 0; i <= n; i++) {
            kwadrat = i * i;
            if (String.valueOf(kwadrat).contains(String.valueOf(d))) {
                napis += String.valueOf(kwadrat);
            }
        }
        String[] tablcia = napis.split("");
        for (String string : tablcia) {
            if (string.contains(String.valueOf(d))) {
                licznik++;
            }
        }
        return licznik;
    }
    public static int nbDIG(int n, int d) {
        char c = Integer.toString(d).charAt(0);
        int counter = 0;
        for(int i=0;i<=n;i++){
            String square = ""+i*i;
            for(int j=0;j<square.length();j++){
                if(square.charAt(j)==c){
                    counter++;
                }
            }
        }

        return counter;

    }
}