package com.company.codewars;
/*
https://www.codewars.com/kata/predict-your-age/train/java
 */

import java.util.Map;

public class CodewarsL17 {
    public static void main(String[] args) {
        System.out.println(predictAge(65, 60, 75, 55, 60, 63, 64, 45));
        System.out.println(predictAge(32, 54, 76, 65, 34, 63, 64, 45));


    }

    public static int predictAge(int age1, int age2, int age3, int age4, int age5, int age6, int age7, int age8) {
        int a1 = age1 * age1;
        int a2 = age2 * age2;
        int a3 = age3 * age3;
        int a4 = age4 * age4;
        int a5 = age5 * age5;
        int a6 = age6 * age6;
        int a7 = age7 * age7;
        int a8 = age8 * age8;
        int sum = a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8;
        double pierwiastek = Math.sqrt(sum);
        int wynik = (int) (pierwiastek / 2);
        return wynik;
    }
}
