package com.company.codewars;
/*
https://www.codewars.com/kata/grasshopper-summation/train/java
 */

import java.util.stream.IntStream;

public class CodewarsStream12 {
    public static void main(String[] args) {

        System.out.println(summation(8));
    }

    public static int summation(int n) {

        return IntStream
                .rangeClosed(1, n)
                .sum();

    }
}
