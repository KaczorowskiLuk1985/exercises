package com.company.codewars;
/*
https://www.codewars.com/kata/compare-strings-by-sum-of-chars/train/java
 */

public class CodewarsS19 {
    public static void main(String[] args) {
        System.out.println(compare("AD", "BC"));
        System.out.println(compare("AD", "DD"));
        System.out.println(compare("gf", "FG"));
        System.out.println(compare("zz1", ""));
        System.out.println(compare("ZzZz", "ffPFF"));
        System.out.println(compare("kl", "lz"));
        System.out.println(compare(null, ""));

    }

    public static boolean compare(String s1, String s2) {

        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }

        int s1hashCodeSum = 0;
        int s2hashCodeSum = 0;
        String[] tab1 = s1.toUpperCase().split("");
        String[] tab2 = s2.toUpperCase().split("");

        for (int i = 0; i < tab1.length; i++) {
            if (!tab1[i].matches("[A-Z]")) {
                s1hashCodeSum = 0;
            } else {
                s1hashCodeSum += tab1[i].hashCode();
            }

        }
        for (int i = 0; i < tab2.length; i++) {
            if (!tab2[i].matches("[A-Z]")) {
                s2hashCodeSum = 0;
            } else {
                s2hashCodeSum += tab2[i].hashCode();
            }
        }
        return s1hashCodeSum == s2hashCodeSum;

    }
}
