package com.company.codewars;
//https://www.codewars.com/kata/odd-or-even/train/java

import java.util.Arrays;

public class CodewarsStream5 {
    public static void main(String[] args) {
        final int[] tab = {};
        System.out.println(oddOrEven(tab));

    }

    public static String oddOrEven(final int[] array) {

        return Arrays.stream(array).sum() % 2 == 0 ? "even" : "odd";
    }
}
