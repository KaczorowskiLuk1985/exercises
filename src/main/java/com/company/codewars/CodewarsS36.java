package com.company.codewars;
/*
https://www.codewars.com/kata/partial-word-searching/train/java
 */

import java.util.Arrays;

public class CodewarsS36 {
    public static void main(String[] args) {
        String[] test1 = {"lemonade", "limeade", "Yoo-Hoo", "root beer", "grapeade ", "water", "Mr. Pibb"};
        System.out.println(Arrays.toString(findWord("ade", test1)));

        String[] test2results = {"Tin + Oxygen", "lox"};
        System.out.println(Arrays.toString(findWord("ox", test2results)));


    }

    static String[] findWord(String x, String[] y) {
        String[] empty = {"Empty"};
        String string = String.join(",", y);
        String[] strings = Arrays.stream(y)
                .filter(pozycja -> pozycja.toLowerCase().contains(x.toLowerCase()))
                .toArray(String[]::new);

        return !string.toLowerCase().contains(x.toLowerCase()) ? empty : strings;

    }
}
