package com.company.codewars;
/*
https://www.codewars.com/kata/inspiring-strings/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsS16 {
    public static void main(String[] args) {

        String a = "a b c d e fgh";
        String b = "one two three";
        String c = "red blue grey ac";
        System.out.println(longestWord(a));
        System.out.println(longestWord(b));
        System.out.println(longestWord(c));
    }

    public static String longestWord(String wordString) {

//        String[] s = wordString.split(" ");
//        int max = 0;
//        String y = "";
//
//        for (int i = s.length - 1; i >= 0; i--) {
//            if (s[i].length() > max) {
//                max = s[i].length();
//                y = s[i];
//            }
//        }
//
//        return y;

        String[] s = wordString.split(" ");
        String y = "";

        for (int i = s.length - 1; i >= 0; i--) {
            if (s[i].length() > y.length()) {
                y = s[i];
            }
        }

        return y;
    }

}
