package com.company.codewars;

public class CodewarsL27 {
    public static void main(String[] args) {
        int[] tab = {1, 1, 2};
        int[] tab2 = {17, 17, 3, 17, 17, 17, 17};
        System.out.println(stray(tab));
        System.out.println(stray(tab2));
    }

    static int stray(int[] numbers) {
        for (int i = 1; i < numbers.length - 1; i++) {
            if (numbers[i] != numbers[i - 1] && numbers[i] == numbers[i + 1]) {
                return numbers[i - 1];
            }
            if (numbers[i] != numbers[i + 1] && numbers[i] == numbers[i - 1]) {
                return numbers[i + 1];
            }
        }
        return 0;
    }
}
