package com.company.codewars;
/*
https://www.codewars.com/kata/ranking-nba-teams/train/java
 */

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodewarsS51 {
    public static void main(String[] args) {

        String resultSheet1 = "Los Angeles Clippers 104 Dallas Mavericks 88,New York Knicks 101 Atlanta Hawks 112,Indiana Pacers 103 Memphis Grizzlies 112,"
                + "Los Angeles Lakers 111 Minnesota Timberwolves 112,Phoenix Suns 95 Dallas Mavericks 111,Portland Trail Blazers 112 New Orleans Pelicans 94,"
                + "Sacramento Kings 104 Los Angeles Clippers 111,Houston Rockets 85 Denver Nuggets 105,Memphis Grizzlies 76 Cleveland Cavaliers 106,"
                + "Milwaukee Bucks 97 New York Knicks 122,Oklahoma City Thunder 112 San Antonio Spurs 106,Boston Celtics 112 Philadelphia 76ers 95,"
                + "Brooklyn Nets 100 Chicago Bulls 115,Detroit Pistons 92 Utah Jazz 87,Miami Heat 104 Charlotte Hornets 94,"
                + "Toronto Raptors 106 Indiana Pacers 99,Orlando Magic 87 Washington Wizards 88,Golden State Warriors 111 New Orleans Pelicans 95,"
                + "Atlanta Hawks 94 Detroit Pistons 106,Chicago Bulls 97 Cleveland Cavaliers 95,";
        String resultSheet2 = "San Antonio Spurs 111 Houston Rockets 86,Chicago Bulls 103 Dallas Mavericks 102,Minnesota Timberwolves 112 Milwaukee Bucks 108,"
                + "New Orleans Pelicans 93 Miami Heat 90,Boston Celtics 81 Philadelphia 76ers 65,Detroit Pistons 115 Atlanta Hawks 87,"
                + "Toronto Raptors 92 Washington Wizards 82,Orlando Magic 86 Memphis Grizzlies 76,Los Angeles Clippers 115 Portland Trail Blazers 109,"
                + "Los Angeles Lakers 97 Golden State Warriors 136,Utah Jazz 98 Denver Nuggets 78,Boston Celtics 99 New York Knicks 85,"
                + "Indiana Pacers 98 Charlotte Hornets 86,Dallas Mavericks 87 Phoenix Suns 99,Atlanta Hawks 81 Memphis Grizzlies 82,"
                + "Miami Heat 110 Washington Wizards 105,Detroit Pistons 94 Charlotte Hornets 99,Orlando Magic 110 New Orleans Pelicans 107,"
                + "Los Angeles Clippers 130 Golden State Warriors 95,Utah Jazz 102 Oklahoma City Thunder 113,San Antonio Spurs 84 Phoenix Suns 104,"
                + "Chicago Bulls 103 Indiana Pacers 94,Milwaukee Bucks 106 Minnesota Timberwolves 88,Los Angeles Lakers 104 Portland Trail Blazers 102,"
                + "Houston Rockets 120 New Orleans Pelicans 100,Boston Celtics 111 Brooklyn Nets 105,Charlotte Hornets 94 Chicago Bulls 86,Cleveland Cavaliers 103 Dallas Mavericks 97";
        String resultSheet3 = resultSheet1 + resultSheet2;

        System.out.println(nbaCup(resultSheet3, "Boston Celtics"));
        System.out.println(nbaCup(resultSheet3, "Boston Celt"));
    }

    public static String nbaCup(String resultSheet, String toFind) {
        if (toFind.isEmpty()) {
            return "";
        }
        String[] arr = resultSheet.split(",");
        String[] matchesArray = Arrays.stream(arr)
                .filter(s -> s.matches(".*" + toFind + " .*"))
                .toArray(String[]::new);

        if (matchesArray.length == 0) {
            return String.format("%s:This team didn't play!", toFind);
        }

        int wins = 0;
        int loses = 0;
        int draws = 0;
        int scored = 0;
        int conceded = 0; //strata

        for (String string : matchesArray) {
            if (string.contains(".")) {
                return String.format("Error(float number):%s", string);
            }

            if (!string.contains(toFind)) { //nie zawiera
                continue;
            }

            Pattern pattern = Pattern.compile("(.+)( \\d+ )(.+)( \\d+)"); //kropka = dowolny znak
            Matcher matcher = pattern.matcher(string);

            if (matcher.find()) {

                int tempScored = scoredPoints(matcher, toFind);
                int tempConceded = lostPoints(matcher, toFind);

                scored += tempScored;
                conceded += tempConceded;

                if (tempScored == tempConceded) {
                    draws++;
                } else if (tempScored > tempConceded) {
                    wins++;
                } else {
                    loses++;
                }
            }
        }
        return String.format("%s:w=%s;D=%s;L=%s;Scored=%s;Conceded=%s;Points=%s", toFind, wins, draws, loses, scored, conceded, wins * 3);
    }

    //suma zdobytych punktow
    public static int scoredPoints(Matcher matcher, String timeToFind) {
        int groupNumber;
        if (matcher.group(1).equals(timeToFind)) {
            groupNumber = 2;
        } else {
            groupNumber = 4;
        }
        return Integer.valueOf(matcher.group(groupNumber)
                .replaceAll(" ", ""));
    }

    // suma straconych punktow
    public static int lostPoints(Matcher matcher, String timeToFind) {
        int groupNumber;
        if (matcher.group(1).equals(timeToFind)) {
            groupNumber = 4;
        } else {
            groupNumber = 2;
        }
        return Integer.valueOf(matcher.group(groupNumber).replaceAll(" ", ""));
    }

    static int winsPoint;
}
