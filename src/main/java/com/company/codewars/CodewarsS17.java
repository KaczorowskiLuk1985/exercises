package com.company.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

/*
https://www.codewars.com/kata/reverse-words/train/java
 */
public class CodewarsS17 {
    public static void main(String[] args) {
        String a = "krishan";
        String b = "ultr53o?n";

        System.out.println(reverseLetter(b));
        System.out.println(reverseLetter(a));

    }

    public static String reverseLetter(final String str) {

        String collect = Arrays.stream(str.split(""))
                .filter(x -> x.matches("[a-z]"))
//                .map(slowo -> {
//                    String noewSlowo = "";
//                    for (char c : slowo.toCharArray()) {
//                        noewSlowo = c + noewSlowo;
//                    }
//                    return noewSlowo;
//                })
                .collect(Collectors.joining());
        return new StringBuilder(collect).reverse().toString();
    }
}
