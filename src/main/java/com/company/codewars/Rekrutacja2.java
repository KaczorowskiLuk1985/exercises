package com.company.codewars;

import java.util.Arrays;

public class Rekrutacja2 {
    public static void main(String[] args) {
        int[] serbia = {2, 2, 2, 2, 3};
        System.out.println(howManyCastle(serbia));

    }

    private static int howManyCastle(int[] landShape) {
        int zeroCount = 0;
        int[] arrWithoutClone = new int[landShape.length];

        for (int i = 0; i < landShape.length - 1; i++) {
            if (landShape[i] != landShape[i + 1]) {
                arrWithoutClone[i] = landShape[i];
                arrWithoutClone[arrWithoutClone.length - 1] = landShape[landShape.length - 1];
            }
        }
        for (int index : arrWithoutClone) {
            if (index == 0) {
                zeroCount++;
            }
        }
        int[] arrWithoutZero = new int[landShape.length - zeroCount];
        int indexArrWithoutZero = 0;

        for (int i = 0; i < landShape.length; i++) {
            if (arrWithoutClone[i] != 0) {
                arrWithoutZero[indexArrWithoutZero] = arrWithoutClone[i];
                indexArrWithoutZero++;
            }
        }
        int top = 0;
        int valley = 0;

        for (int i = 1; i < arrWithoutZero.length - 1; i++) {
            if (arrWithoutZero[i] > arrWithoutZero[i + 1]
                    && arrWithoutZero[i] > arrWithoutZero[i - 1]) {
                top++;
            }
            if (arrWithoutZero[i] < arrWithoutZero[i + 1]
                    && arrWithoutZero[i] < arrWithoutZero[i - 1]) {
                valley++;
            }
        }
        System.out.println(Arrays.toString(landShape));
        System.out.println(Arrays.toString(arrWithoutClone));
        System.out.println(Arrays.toString(arrWithoutZero));
        return arrWithoutZero.length == 0 ? 1 : top + valley + 2;
    }
}
