package com.company.codewars;
/*
https://www.codewars.com/kata/alternating-case-<-equals->-alternating-case
 */

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsStream4 {
    public static void main(String[] args) {
        String s = "1A2B3C4D5E";
        String ss = "HELLO world";
        String sss = "hEllO wOrld";

        System.out.println(toAlternativeString(s));
        System.out.println(toAlternativeString(ss));
        System.out.println(toAlternativeString(sss));

    }

    public static String toAlternativeString(String string) {
        String[] split = string.split("");

        return Arrays.stream(split)
                .map(x -> {
                    if (x.equals(x.toLowerCase())) {
                        return x.toUpperCase();
                    } else {
                        return x.toLowerCase();
                    }
                })
                .collect(Collectors.joining());


//        String[] split = string.split("");
//        String[] alterTab = new String[string.length()];
//
//        for (int i = 0; i < string.length(); i++) {
//            if (split[i].contains(split[i].toLowerCase())) {
//                alterTab[i] = split[i].toUpperCase();
//            }
//            if (split[i].contains(split[i].toUpperCase())) {
//                alterTab[i] = split[i].toLowerCase();
//            }
//        }
//        return String.join("", alterTab);

//        return Arrays.stream(string.split(""))
//                .map(s -> s.matches("[A-Z]") ? s.toLowerCase() : s.toUpperCase())
//                .collect(Collectors.joining(""));
    }
}
