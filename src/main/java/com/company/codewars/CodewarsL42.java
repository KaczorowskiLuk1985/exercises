package com.company.codewars;
/*
https://www.codewars.com/kata/tv-remote/train/java
 */

public class CodewarsL42 {
    private static class Punkt<FIRST, SECOND> {
        FIRST x;
        SECOND y;

        Punkt(FIRST x, SECOND y) {
            this.x = x;
            this.y = y;
        }
    }
//TODO klasa generyczna sie tu pojawia. moze sie przydac
    private static final char[][] keybord = {
            {'a', 'b', 'c', 'd', 'e', '1', '2', '3'},
            {'f', 'g', 'h', 'i', 'j', '4', '5', '6'},
            {'k', 'l', 'm', 'n', 'o', '7', '8', '9'},
            {'p', 'q', 'r', 's', 't', '.', '@', '0'},
            {'u', 'v', 'w', 'x', 'y', 'z', '_', '/'}
    };

    public static void main(String[] args) {
        System.out.println(tvRemote("words"));

    }

    public static int tvRemote(final String word) {

        // Your code here
        int counter = word.length();
        Punkt<Integer, Integer> start = new Punkt<>(0, 0);
        Punkt<Integer, Integer> koniec;

        for (int i = 0; i < word.length(); i++) {
            koniec = wspolzedneLitery(word.charAt(i));
//            System.out.println(start + " --> " + koniec);
            counter += odlegloscPomiedzyPunktami(start, koniec);
            start = koniec;

        }
        return counter;
    }

    static Punkt<Integer, Integer> wspolzedneLitery(char litera) {
        for (int wysokosc = 0; wysokosc < keybord.length; wysokosc++) {
            for (int dlugosc = 0; dlugosc < keybord[wysokosc].length; dlugosc++) {
                if (keybord[wysokosc][dlugosc] == litera) {
                    return new Punkt<>(wysokosc, dlugosc);
                }
            }
        }
        return null;
    }

    public static int odlegloscPomiedzyPunktami(Punkt<Integer, Integer> start,
                                                Punkt<Integer, Integer> koniec) {
        int wysokosc = Math.abs(start.x - koniec.x);
        int szerokosc = Math.abs(start.y - koniec.y);

        return wysokosc + szerokosc;
    }

}
