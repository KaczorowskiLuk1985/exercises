package com.company.codewars;
/*
https://www.codewars.com/kata/remove-duplicates
 */

import java.util.Arrays;


public class CodewarsS35 {
    public static void main(String[] args) {
        int[] tab = {1, 5, 2, 0, 2, -3, 1, 10};
        System.out.println(Arrays.toString(unique(tab)));
    }

    public static int[] unique(int[] integers) {

        return Arrays.stream(integers)
                .distinct()
                .toArray();
    }
}
