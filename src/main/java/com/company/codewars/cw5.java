package com.company.codewars;
/*
https://www.codewars.com/kata/composing-squared-strings/train/java
 */

import java.util.Arrays;

public class cw5 {
    public static void main(String[] args) {
        String s1 = "abcd\nefgh\nijkl\nmnop";
        String s2 = "qrst\nuvwx\nyz12\n3456";
//        System.out.println(s1);
//        System.out.println(s2);
        System.out.println(compose(s1, s2));
    }

    public static String compose(String s1, String s2) {
        String[] str1 = s1.split("\n");
        String[] str2 = s2.split("\n");
        String[] value = new String[str1.length];

        for (int i = 0; i < value.length; i++) {
            value[i] = str1[i].substring(0, i + 1) + str2[str2.length - 1 - i].substring(0, str2[str2.length - 1 - i].length() - i);
        }

        return String.join("\n", value);
    }
}
