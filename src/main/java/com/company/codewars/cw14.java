package com.company.codewars;
/*
https://www.codewars.com/kata/automorphic-number-special-numbers-series-number-6/train/java
 */

public class cw14 {
    public static void main(String[] args) {

        System.out.println(autoMorphic(1));
        System.out.println(autoMorphic(3));
        System.out.println(autoMorphic(6));
        System.out.println(autoMorphic(9));
        System.out.println(autoMorphic(13));
    }

    public static String autoMorphic(int number) {
//        String square = "" + number * number;
//        String stringNumber = "" + number;
//        String substrA = square.substring(square.length() - stringNumber.length());
//
//        return substrA.equals(stringNumber) ? "Automorphic" : "Not!!";

        return (number * number + "").endsWith("" + number) ? "Automorphic" : "Not!!";

    }
}
