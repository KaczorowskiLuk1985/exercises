package com.company.codewars;
/*
https://www.codewars.com/kata/covfefe/train/java
 */

public class CodewarsS27 {
    public static void main(String[] args) {
        System.out.println(covfefe("coverage"));
        System.out.println(covfefe("coverage coveragee"));
        System.out.println(covfefe("nothing"));
        System.out.println(covfefe("double space "));
        System.out.println(covfefe("covfefe"));

    }

    public static String covfefe(String tweet) {

        if (tweet.equals("")) {
            return "covfefe";
        }
        if (tweet.contains("coverage")) {
            return tweet.replace("coverage", "covfefe");
        }
        if (!tweet.contains("coverage")) {
            return tweet.concat(" covfefe");
        }
        return "";
    }
}
