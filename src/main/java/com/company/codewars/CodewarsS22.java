package com.company.codewars;
/*
https://www.codewars.com/kata/sum-of-the-first-nth-term-of-series/train/java
 */
import java.util.stream.IntStream;

public class CodewarsS22 {
    public static void main(String[] args) {
        System.out.println(seriesSum(15));
        System.out.println(seriesSum2(15));

    }

    public static String seriesSum(int n) {
        int[] tab = IntStream
                .iterate(1, x -> x + 3)
                .limit(n)
                .toArray();

        double value = 0;

        for (int i = 0; i < tab.length; i++) {
            value += 1 / (double) tab[i];
        }
        String result = String.format("%.2f", value);

        return n == 0 ? "0.00" : result;
    }

    public static String seriesSum2(int n) {
        double sum = IntStream
                .iterate(1, x -> x + 3)
                .limit(n).mapToDouble(x -> 1.0 / x)
                .sum();
        return String.format("%.2f", sum);
    }
}
