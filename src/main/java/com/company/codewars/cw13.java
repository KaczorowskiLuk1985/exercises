package com.company.codewars;
/*
https://www.codewars.com/kata/80-s-kids-number-2-help-alf-find-his-spaceship/train/java
 */

public class cw13 {
    public static void main(String[] args) {

        String map = "..........\n" +
                "..........\n" +
                "..........\n" +
                ".......X..\n" +
                "..........\n" +
                "..........";
        String map2 = "X";
        System.out.println(findSpaceship(map));
        System.out.println(findSpaceship(map2));

    }

    public static String findSpaceship(String map) {
        int fromLeft = 0;
        int fromBottom = 0;
        String ship = "";
        String[] split = map.split("\n");

        for (int i = 0; i < split.length; i++) {
            if (split[i].contains("X")) {
                fromBottom = split.length - i - 1;
                ship = split[i];
            }
        }
        for (int i = 0; i < ship.length(); i++) {
            if (ship.charAt(i) == 'X') {
                fromLeft = i;
            }
        }
        return !map.contains("X")?"Spaceship lost forever.":String.format("[%s, %s]", fromLeft, fromBottom);
    }
}
