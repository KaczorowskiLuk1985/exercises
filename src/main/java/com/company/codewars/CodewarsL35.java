package com.company.codewars;
/*
https://www.codewars.com/kata/check-three-and-two/train/java
 */

import java.util.Arrays;

public class CodewarsL35 {
    public static void main(String[] args) {
        char[] a = {'a', 'a', 'b', 'b', 'b'};
        char[] b = {'a', 'c', 'a', 'c', 'b'};
        char[] c = {'a', 'a', 'a', 'a', 'a'};

        System.out.println(checkThreeAndTwo(a));
        System.out.println(checkThreeAndTwo(b));
        System.out.println(checkThreeAndTwo(c));

    }

    public static boolean checkThreeAndTwo(char[] chars) {
        Arrays.sort(chars);
        if (chars[0] == chars[1] && chars[1] == chars[2] && chars[2] != chars[3] && chars[3] == chars[4]) {
            return true;
        }
        return chars[0] == chars[1] && chars[1] != chars[2] && chars[2] == chars[3] && chars[3] == chars[4];
    }
}
