package com.company.codewars;
/*
https://www.codewars.com/kata/get-the-mean-of-an-array/train/java
 */

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CodewarsStream11 {
    public static void main(String[] args) {
        int[] tab = {2,3};
        System.out.println(getAverage(tab));

    }

    public static int getAverage(int[] marks) {
//        double srednia = Arrays.stream(marks)
//                .average()
//                .getAsDouble();

//        return (int) Arrays.stream(marks)
//                .average()
//                .getAsDouble();
        return (int) IntStream.of(marks)
                .average()
                .getAsDouble();
    }
}
