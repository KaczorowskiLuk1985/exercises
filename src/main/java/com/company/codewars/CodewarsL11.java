package com.company.codewars;

/*
https://www.codewars.com/kata/growth-of-a-population/train/java
 */
public class CodewarsL11 {
    public static void main(String[] args) {
        System.out.println(nbYear(1000, 2, 50, 1200));
        System.out.println(nbYear(1500, 5, 100, 5000));
    }
    public static int nbYear(int p0, double percent, int aug, int p) {
        int howMany = 0;
        while (p0 < p) {
            p0 += p0 * percent / 100 + aug;
            howMany++;
        }
        return howMany;
//        do {
//            p0 += p0 * percent / 100 + aug;
//            howMany++;
//        } while (p0 < p);
//
//        return howMany; }}
    }
}
