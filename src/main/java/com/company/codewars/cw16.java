package com.company.codewars;
/*
https://www.codewars.com/kata/tidy-number-special-numbers-series-number-9/train/java
 */

import org.omg.PortableInterceptor.INACTIVE;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class cw16 {
    public static void main(String[] args) {

        System.out.println(tidyNumber(1234565));

    }

    public static boolean tidyNumber(int number) {
        String num = number + "";
        String string = Arrays.stream(num.split(""))
                .sorted()
                .collect(Collectors.joining(""));

//        System.out.println(string + " String");

        return string.equals(num);
    }
}
