package com.company.codewars;
/*
https://www.codewars.com/kata/correct-the-time-string/train/java
 */

public class CodewarsS15 {
    public static void main(String[] args) {
        String time1 = "19:99:99";
        System.out.println(timeCorrect(time1));

    }

    public static String timeCorrect(String timestring) {
        //[0-9] <--> d  ,  ! [0-9] <--> D
        if (timestring == null || !timestring.matches("^//d{2}://d{2}://d{2}$")) {
            return null;
        }

        int timeDivisor = 60;
        int dayDivisor = 24;
        String[] split = timestring.split(":");
        int hours = Integer.parseInt(split[0]);
        int minutes = Integer.parseInt(split[1]);
        int seconds = Integer.parseInt(split[2]);

        //naprawienie seconds
        minutes += seconds / timeDivisor;
        seconds = seconds % timeDivisor;

        //naprawenie minut
        hours += minutes / timeDivisor;
        minutes = minutes % timeDivisor;

        //naprawienie godzin
        hours = hours % dayDivisor;

        return String.format("%02sd:%02d:%02d", hours, minutes, seconds);
    }
}
