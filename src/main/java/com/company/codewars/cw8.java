package com.company.codewars;
/*
https://www.codewars.com/kata/the-fusc-function-part-1/train/java
 */

public class cw8 {
    public static void main(String[] args) {

        System.out.println(fusc(85));
    }

    public static int fusc(int n) {
        if (n == 0) {
            return 0;
        }

        int[] tab = new int[n + 1];
        tab[1] = 1;

        for (int i = 2; i <= n; i++) {

            if (i % 2 == 0) {
                tab[i] = tab[i / 2];
            } else {

                tab[i] = tab[((i - 1) / 2)] + tab[((i + 1) / 2)];
            }
        }
        return tab[n];

//        if (n == 0 || n == 1)
//            return n;
//
//        int t = n % 2;
//
//        return t == 0 ? fusc(n / 2) : fusc(n / 2) + fusc(n / 2 + 1);
    }

}


