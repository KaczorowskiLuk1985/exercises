package com.company.codewars;
/*
https://www.codewars.com/kata/fizz-buzz-cuckoo-clock/train/java
 */

import java.util.Collections;

public class CodewarsS40 {
    public static void main(String[] args) {
        String t1 = "13:34";
        String t2 = "21:00";
        String t3 = "11:15";
        String t4 = "03:03";
        String t5 = "14:30";
        String t6 = "08:55";
        String t7 = "00:00";
        String t8 = "12:00";
        String t9 = "13:00";
        System.out.println(fizzBuzzCuckooClock(t1));
        System.out.println(fizzBuzzCuckooClock(t2));
        System.out.println(fizzBuzzCuckooClock(t3));
        System.out.println(fizzBuzzCuckooClock(t4));
        System.out.println(fizzBuzzCuckooClock(t5));
        System.out.println(fizzBuzzCuckooClock(t6));
        System.out.println(fizzBuzzCuckooClock(t7));
        System.out.println(fizzBuzzCuckooClock(t8));
        System.out.println(fizzBuzzCuckooClock(t9));

    }

    public static String fizzBuzzCuckooClock(String time) {
        String cucko = "Cuckoo";
        String fizz = "Fizz";
        String buzz = "Buzz";
        String tick = "tick";
        String[] split = time.split(":");
        int m = Integer.parseInt(split[1]);
        int h = Integer.valueOf(split[0]);

        if (time.equals("00:00")) {
            return String.join(" ", Collections.nCopies(12, cucko));
        }
        if (split[1].equals("00") && h <= 12) {
//            String[] timeAM = new String[h];
//
//            for (int i = 0; i < h; i++) {
//                timeAM[i] = cucko;
//            }
            return String.join(" ", Collections.nCopies(h, cucko));
        }
        if (split[1].equals("00") && h > 12) {
//            String[] timePM = new String[h % 12];
//
//            for (int i = 0; i < h % 12; i++) {
//                timePM[i] = cucko;
//            }
            return String.join(" ", Collections.nCopies(h % 12, cucko));
        }
        if (split[1].equals("30")) {
            return cucko;
        }
        if (m % 3 == 0 && m % 5 == 0 && m != 0) {
            return String.format("%s %s", fizz, buzz);
        }
        if (m % 5 == 0 && m != 0) {
            return buzz;
        }
        if (m % 3 == 0 && m != 0) {
            return fizz;
        }
        if (h % 3 != 0 || h % 5 != 0) {
            return tick;
        }

        return "";

    }
}
