package com.company.codewars;
/*
https://www.codewars.com/kata/scaling-squared-strings
 */

import java.util.Arrays;

public class cw12 {
    public static void main(String[] args) {

        String a = "abcd\nefgh\nijkl\nmnop";
        System.out.println(scale(a, 2, 3));
//        System.out.println(muliple("abcd", 2));

    }

    public static String scale(String strng, int k, int v) {
        String[] split = strng.split("\n");
        String[] multiArr = new String[split.length * v];

        for (int i = 0; i < multiArr.length; i++) {
            multiArr[i] = muliple(split[i / v], k);
        }
        return strng.equals("") ? "" : String.join("\n", multiArr);
    }

    public static String muliple(String s, int plus) {
        String[] split = s.split("");
        String multi = "";

        for (int i = 0; i < split.length * plus; i++) {
            multi += split[i / plus];
        }
        return multi;

    }
}
