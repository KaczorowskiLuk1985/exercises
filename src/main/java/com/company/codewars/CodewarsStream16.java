package com.company.codewars;
/*
https://www.codewars.com/kata/sort-and-star/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsStream16 {
    public static void main(String[] args) {
        String[] tab = new String[]{

                "take",
                "bitcoin",
                "over",
                "the",
                "world",
                "maybe",
                "who",
                "knows",
                "perhaps"
        };
        System.out.println(twoSort(tab));

    }

    public static String twoSort(String[] s) {
//        Arrays.sort(s);
//        String[] k = s[0].split("");
//
//        return String.join("***", k);


//        String collect = Arrays.stream(s)
//                .sorted()
//                .limit(1)
//                .map(x -> x.replace("", "***"))
//                .collect(Collectors.joining());
//
//        return collect.substring(3, collect.length() - 3);

        String[] collect = Arrays.stream(s)
                .sorted()
                .findFirst()
                .orElse("")
                .split("");

        return String.join("***", collect);
    }
}
