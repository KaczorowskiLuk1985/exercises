package com.company.codewars;
/*
https://www.codewars.com/kata/find-count-of-most-frequent-item-in-an-array/train/java
 */

public class CodewarsL38 {
    public static void main(String[] args) {
        int[] tab = {3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3};
        System.out.println(mostFrequentItemCount(tab));

    }

    public static int mostFrequentItemCount(int[] collection) {
        int najwiekszyLicznik = 0;

        for (int i = 0; i < collection.length; i++) {
            int holder = collection[i];

            int licznik = 0;

            for (int element : collection) {
                if (element == holder) {
                    licznik++;
                }
            }
            if (licznik > najwiekszyLicznik) {
                najwiekszyLicznik = licznik;
            }
        }
        return najwiekszyLicznik;
    }
}
