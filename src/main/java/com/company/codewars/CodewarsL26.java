package com.company.codewars;
/*
https://www.codewars.com/kata/two-fighters-one-winner/train/java
 */

public class CodewarsL26 {
    public static void main(String[] args) {
        Fighter zawodnik1 = new Fighter("Jerry", 40, 3);
        Fighter zawodnik2 = new Fighter("Harald", 20, 5);
        System.out.println(declareWinner(zawodnik1, zawodnik2, "Harald"));

    }
// TODO petla while

    public static String declareWinner(Fighter fighter1, Fighter fighter2, String firstAttacker) {
        Fighter first;
        Fighter second;

        if (firstAttacker.equals(fighter1.name)) {
            first = fighter1;
            second = fighter2;
        } else {
            first = fighter2;
            second = fighter1;
        }
        while (true) {
            second.health -= first.damagePerAttack;
            if (second.health < 1) {
                return first.name;
            }
            first.health -= second.damagePerAttack;
            if (first.health < 1) {
                return second.name;
            }
        }
    }
}
