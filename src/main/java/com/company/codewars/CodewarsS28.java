package com.company.codewars;
/*
https://www.codewars.com/kata/driving-licence/train/java
 */

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Arrays;
import java.util.Locale;

public class CodewarsS28 {
    public static void main(String[] args) {
        String[] s10 = {"John", "James", "Smith", "01-Jan-2000", "M"};
        String[] s20 = {"Johanna", "", "Gibbs", "13-Dec-1981", "F"};
        String[] s30 = {"Andrew", "Robert", "Lee", "02-September-1981", "M"};
        String[] s40 = {"Johanna", "Eugene", "Eugene", "15-December-1947", "M"};

        System.out.println(driver(s10));
        System.out.println(driver(s20));
        System.out.println(driver(s30));
        System.out.println(driver(s40));

    }

    public static String driver(final String[] data) {
        String[] str = {"9", "9", "9", "9", "9"};
        String[] dataS1 = data[2].split("");
        String s1 = "";
        String s2 = data[3].substring(data[3].length() - 2, data[3].length() - 1);


        if (dataS1.length == 5) {
            s1 = data[2].toUpperCase();
        }
        if (dataS1.length > 5) {
            s1 = data[2].substring(0, 5).toUpperCase();
        }
        if (dataS1.length < 5) {
            for (int i = 0; i < dataS1.length; i++) {
                str[i] = dataS1[i].toUpperCase();
            }
            s1 = String.join("", str);
        }
        String month = data[3].substring(3, 6);
        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("MMM")
                .withLocale(Locale.ENGLISH);

        TemporalAccessor accessor = inputFormat.parse(month);
        int num = accessor.get(ChronoField.MONTH_OF_YEAR);

        String miesiacUrodzenia = String.format("0%s", num);
        String s3 = miesiacUrodzenia.substring(miesiacUrodzenia.length() - 2);
        String[] split = s3.split("");
        System.out.println(Arrays.toString(split));
        int f = 0;

        if (data[4].contains("F")) {
            f = Integer.parseInt(split[0]) + 5;
        }else {
            f = Integer.parseInt(split[0]);
        }
        String s4 = String.valueOf(f) + s3.substring(1);
        String s5 = data[3].substring(0, 2);
        String s6 = data[3].substring(data[3].length() - 1);
        String s7 = data[0].substring(0, 1);
        String s8 = "";
        if (data[1].equals("")) {
            s8 = "9";
        } else {
            s8 = data[1].substring(0, 1);
        }
        String s9 = "9AA";

        //System.out.println(Arrays.toString(s1));
        return s1 + "_" + s2 + "_" + s4 + "_" + s5 + "_" + s6 + "_" + s7 + "_" + s8 + "_" + s9;
    }
}
