package com.company.codewars;
/*
https://www.codewars.com/kata/fix-string-case/train/java
 */

import java.util.Arrays;

class CodewarsS24 {
    public static void main(String[] args) {
        System.out.println(solve("coDe"));
        System.out.println(solve("CODe"));
        System.out.println(solve("coDE"));

    }

    public static String solve(final String str) {
        String[] split = str.split("");

        long lo = Arrays.stream(split)
                .filter(x -> x.contains(x.toLowerCase()))
                .count();
        long up = Arrays.stream(split)
                .filter(x -> x.contains(x.toUpperCase()))
                .count();

        if (lo > up) {
            return str.toLowerCase();
        }
        if (up > lo) {
            return str.toUpperCase();
        }

        return str.equals("") ? str : str.toLowerCase();

    }

}
