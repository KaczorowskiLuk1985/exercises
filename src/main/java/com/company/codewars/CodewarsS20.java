package com.company.codewars;
/*
https://www.codewars.com/kata/reverse-words/train/java
 */

import java.util.Arrays;

public class CodewarsS20 {
    public static void main(String[] args) {
        System.out.println(reverseWords("double  spaces"));
        System.out.println(reverseWords("The quick brown fox jumps over the lazy dog."));
        System.out.println("+" + reverseWords("   ") + "+");

    }

    public static String reverseWords(final String original) {
        String[] split = original.split(" ");

        for (int i = 0; i < split.length; i++) {
            if (!split[i].equals("")) {
                split[i] = new StringBuilder(split[i]).reverse().toString();
            }
        }

        return split.length == 0 ? original : String.join(" ", split);
    }

}
