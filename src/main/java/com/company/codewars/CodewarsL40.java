package com.company.codewars;
/*
https://www.codewars.com/kata/count-all-the-sheep-on-farm-in-the-heights-of-new-zealand
 */

public class CodewarsL40 {
    public static void main(String[] args) {
        int[] tab1 = {3, 1, 2};
        int[] tab2 = {4, 5};
        int sumaOwiec = 21;

        System.out.println(lostSheeps(tab1, tab2, sumaOwiec));

    }

    public static int lostSheeps(int[] fridayNightCounting, int[] saturdayNightCounting, int sheepsTotal) {
        int piatek = 0;
        int sobota = 0;

        for (int i : fridayNightCounting) {
            piatek += i;
        }
        for (int i : saturdayNightCounting) {
            sobota += i;
        }
        return sheepsTotal - piatek - sobota;
    }
}
