package com.company.codewars;

import java.util.Arrays;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/*
https://www.codewars.com/kata/jaden-casing-strings/train/java
 */
public class CodewarsS9 {
    public static void main(String[] args) {
        String a = "How can mirrors be real if our eyes aren't real";
        String b = "";
//        String c = null;
        System.out.println(toJadenCase(a));
        System.out.println(toJadenCase(b));
        System.out.println(toJadenCase(b));

    }

    public static String toJadenCase(String phrase) {
        if (phrase == "" || phrase == null) {
            return null;
        }
//        String[] split = phrase.split(" ");
//        String[] tab = new String[split.length];
//
//        for (int i = 0; i < split.length; i++) {
//            tab[i] = split[i].substring(0, 1).toUpperCase() + split[i].substring(1);
//        }
//
//        return String.join(" ", tab);
        return Arrays.stream(phrase.split(" "))
                .map(x -> x.substring(0, 1).toUpperCase() + x.substring(1))
                .collect(Collectors.joining(" "));
    }
}
