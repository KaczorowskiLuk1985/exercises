package com.company.codewars;

/*
https://www.codewars.com/kata/reversing-fun/train/java
 */
public class CodewarsS49 {
    public static void main(String[] args) {

        System.out.println(funReverse("012345"));
        System.out.println(funReverse("012"));
        System.out.println(funReverse("Hello"));
        System.out.println(funReverse("1"));
        System.out.println(funReverse("12"));
    }

    public static String funReverse(String s) {
        String[] split = s.split("");
        String reverseString = "";
        String result = "";

        for (String string : split) {
            reverseString = string + reverseString;
        }
        for (int i = 0; i < split.length / 2; i++) {
            result += reverseString.substring(i, i + 1) + s.substring(i, i + 1);
        }
        return s.length() % 2 == 0 ? result : result + split[split.length / 2];
    }
}
//  pierwszy trop :D
//        if (s.length() % 2 == 0) {
//
//            for (int i = 0; i < split.length / 2; i++) {
//                a += reverseString.substring(i, i + 1) + s.substring(i, i + 1);
//            }
//        }
//        if (s.length() % 2 != 0) {
//            for (int i = 0; i < split.length / 2; i++) {
//                a += reverseString.substring(i, i + 1) + s.substring(i, i + 1);
//            }
//            a = a + split[split.length / 2];
//        }
