package com.company.codewars;

/*
https://www.codewars.com/kata/help-suzuki-rake-his-garden/train/java
 */
public class CodewarsS41 {
    public static void main(String[] args) {
        String garden = "gravel gravel gravel gravel gravel gravel gravel gravel gravel rock slug ant " +
                "gravel gravel snail rock gravel gravel gravel gravel gravel gravel gravel slug gravel ant " +
                "gravel gravel gravel gravel rock slug gravel gravel gravel gravel gravel snail gravel " +
                "gravel rock gravel snail slug gravel gravel spider gravel gravel gravel gravel gravel " +
                "gravel gravel gravel moss gravel gravel gravel snail gravel gravel gravel ant gravel " +
                "gravel moss gravel gravel gravel gravel snail gravel gravel gravel gravel slug gravel" +
                " rock gravel gravel rock gravel gravel gravel gravel snail gravel gravel rock gravel" +
                " gravel gravel gravel gravel spider gravel rock gravel gravel";
        System.out.println(rakeGarden(garden));
    }

    public static String rakeGarden(String garden) {
        String[] s = garden.split(" ");

        for (int i = 0; i < s.length; i++) {
            if (!s[i].matches("gravel") && !s[i].matches("rock")) {
                s[i] = "gravel";
            }
        }
        return String.join(" ", s);

    }
}
