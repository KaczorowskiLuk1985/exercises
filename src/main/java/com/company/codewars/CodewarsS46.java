package com.company.codewars;
/*
https://www.codewars.com/kata/count-the-characters/train/java
 */

import java.util.Arrays;

public class CodewarsS46 {
    public static void main(String[] args) {
        System.out.println(charCount("Fancy fifth fly aloof", 'f'));

    }

    public static int charCount(String str, char c) {
//        String[] split = str.split("");
//        int resoult = 0;
//        for (int i = 0; i < split.length; i++) {
//            if (split[i].toLowerCase().equals(String.valueOf(c).toLowerCase())) {
//                resoult++;
//            }
//        }
//        return resoult;


//        int res = 0;
//        for (String string : str.split("")) {
//            if (string.toLowerCase().equals(String.valueOf(c).toLowerCase())) {
//                res++;
//            }
//        }
//        return res;

        return (int) Arrays.stream(str.toLowerCase().split(""))
                .filter(x -> x.equals(String.valueOf(c).toLowerCase()))
                .count()
                ;
    }
}
