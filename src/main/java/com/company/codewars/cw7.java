package com.company.codewars;
/*
https://www.codewars.com/kata/jumping-number-special-numbers-series-number-4/train/java
 */

import java.util.Arrays;

public class cw7 {
    public static void main(String[] args) {
        System.out.println(jumpingNumber(556787));
        System.out.println(jumpingNumber(909000));
        System.out.println(jumpingNumber(0));

    }

    public static String jumpingNumber(int number) {
        String s = ""+number;
        String num = Integer.toString(number);
        String string = String.valueOf(number);

        String[] split = string.split("");
        int[] ints = Arrays.stream(split).mapToInt(Integer::valueOf).toArray();
        int count = 0;

        for (int i = 0; i < ints.length - 1; i++) {
            if (ints[i] == ints[i + 1] + 1
                    || ints[i] == ints[i + 1] - 1
                    || ints[i] == ints[i + 1]) {
                count++;
            }
        }
        return count == ints.length - 1 ? "Jumping!!" : "Not!!";
    }
}
