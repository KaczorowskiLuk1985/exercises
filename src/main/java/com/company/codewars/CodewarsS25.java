package com.company.codewars;
/*
https://www.codewars.com/kata/toleetspeak/train/java
 */

import java.util.Arrays;

public class CodewarsS25 {
    public static void main(String[] args) {
        System.out.println("LOREM IPSUM DOLOR SIT AMET");
        System.out.println(toLeetSpeak("LOREM IPSUM DOLOR SIT AMET"));
        System.out.println(toLeetSpeak("12345678"));
    }

    static String toLeetSpeak(final String speak) {
        String[] split = speak.split(" ");
        String nowyNapis = "";

        for (String wyrazy : split) {
            nowyNapis += coderString(wyrazy) + " ";
        }

        return nowyNapis.trim();
    }

    static String coderString(final String speak) {
        String[] code = new String[]{"A@", "B8", "C(", "DD", "E3", "FF", "G6", "H#", "I!", "JJ", "KK",
                "L1", "MM", "NN", "O0", "PP", "QQ", "RR", "S$", "T7", "UU", "VV", "WW", "XX", "YY", "Z2"};

        String[] split = speak.split("");
        StringBuilder przemowa = new StringBuilder();

        for (String litery : split) {
            if (!litery.matches("[A-Z]")) {
                przemowa.append(litery);
            }
            for (String s : code) {
//                if (litery.contains(s.substring(0, 1))) {
//                if (litery.contains(Character.toString(s.charAt(0)))) {
                if (litery.contains(String.valueOf(s.charAt(0)))) {
                    przemowa.append(s.charAt(1));
                }
            }
        }
        return przemowa.toString();
    }

}