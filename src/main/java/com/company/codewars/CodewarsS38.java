package com.company.codewars;
/*
https://www.codewars.com/kata/odd-even-string-sort/train/java
 */

public class CodewarsS38 {
    public static void main(String[] args) {
        System.out.println(sortMyString("CodeWars"));

    }

    public static String sortMyString(String s) {
        StringBuilder odd = new StringBuilder();
        StringBuilder even = new StringBuilder();
        String[] split = s.split("");

        for (int i = 0; i < split.length; i++) {
            if (i % 2 == 0) {
                even.append(split[i]);
            } else {
                odd.append(split[i]);
            }
        }
        return String.format("%s %s", even, odd);
    }
}
