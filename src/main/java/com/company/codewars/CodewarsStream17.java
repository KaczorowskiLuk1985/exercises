package com.company.codewars;
/*
https://www.codewars.com/kata/mylanguages/train/java
 */

import java.util.*;
import java.util.stream.Collectors;

public class CodewarsStream17 {
    public static void main(String[] args) {

        final Map<String, Integer> map1 = new HashMap<>();
        map1.put("Java", 10);
        map1.put("Ruby", 80);
        map1.put("Python", 65);

        final Map<String, Integer> map2 = new HashMap<>();
        map2.put("Hindi", 60);
        map2.put("Dutch", 93);
        map2.put("Greek", 71);

        final Map<String, Integer> map3 = new HashMap<>();
        map3.put("C++", 50);
        map3.put("ASM", 10);
        map3.put("Haskell", 20);

        System.out.println(myLanguages(map1));
        System.out.println(myLanguages(map2));
        System.out.println(myLanguages(map3));

    }

    public static List<String> myLanguages(final Map<String, Integer> results) {


        return results
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= 60)
                .sorted((e1, e2) -> e2.getValue() - e1.getValue())
                .map(entry -> entry.getKey())//.map(Map.Entry::getKey)
                .collect(Collectors.toList());

//        List<String> listaJezykow = new ArrayList<>();
//
//        for (Map.Entry<String, Integer> stringIntegerEntry : results.entrySet()) {
//            results.entrySet().stream().sorted(Map.Entry.comparingByValue());
//            if (stringIntegerEntry.getValue() >= 60) {
//                listaJezykow.add(stringIntegerEntry.getKey());
//            }
//        }
//        System.out.println(results);
//        return listaJezykow;
//        return listaJezykow.stream().sorted().collect(Collectors.toList());
    }

}
