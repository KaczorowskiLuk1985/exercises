package com.company.codewars;
/*
https://www.codewars.com/kata/mind-the-gap/train/java
 */

public class cw3 {
    public static void main(String[] args) {
        String abc = "ADF";
        String abc1 = "ABC";
        System.out.println(countMissingCarriages(abc));
        System.out.println(countMissingCarriages(abc1));

    }
    public static int countMissingCarriages(String train){
        String a = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        String[] split = a.split("");
        int result = 0;

        for (int i = 0; i < 25; i++) {
            if (split[i].equals(String.valueOf(train.charAt(train.length()-1)))){
                result = i;
            }
        }
        return result - train.length()+1;
    }
}
