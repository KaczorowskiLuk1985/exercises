package com.company.codewars;

import java.util.Arrays;

public class CodewarsL41 {
    public static void main(String[] args) {
        String[] rotten = new String[]{"Apple", "rottenBanana", "rottenApple", "rottenPineapple", "rottenKiwi"};
        String[] zer = {};
        System.out.println(Arrays.toString(removeRotten(rotten)));
        System.out.println(Arrays.toString(removeRotten(zer)));
    }

    public static String[] removeRotten(String[] fruitBasket) {
        String[] zerowy = {};

        if (fruitBasket == null) {
            return zerowy;
        }
        String[] nowaTablica = new String[fruitBasket.length];

        for (int i = 0; i < nowaTablica.length; i++) {
            if (fruitBasket[i].contains("rotten")) {
                nowaTablica[i] = fruitBasket[i].toLowerCase().substring(6, fruitBasket[i].length());
            } else {
                nowaTablica[i] = fruitBasket[i].toLowerCase();
            }
        }
        return nowaTablica;
    }
}
