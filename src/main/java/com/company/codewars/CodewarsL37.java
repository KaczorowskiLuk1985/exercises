package com.company.codewars;
/*
https://www.codewars.com/kata/minimize-sum-of-array-array-series-number-1/train/cpp
 */

import java.util.Arrays;

public class CodewarsL37 {
    public static void main(String[] args) {
        int[] tabi = {5, 4, 2, 3};
        int[] tabi2 = {12, 6, 10, 26, 3, 24};
        int[] tabi3 = {9, 2, 8, 7, 5, 4, 0, 6};
        System.out.println(minSum2(tabi));
        System.out.println(minSum2(tabi2));
        System.out.println(minSum2(tabi3));

    }

    static int minSum(int[] passed) {
        Arrays.sort(passed);
        //   System.out.println(Arrays.toString(passed));
        int[] min = new int[passed.length / 2];
        int[] max = new int[passed.length / 2];
        int[] rev = new int[passed.length];
        int sum = 0;

        for (int i = 0; i < passed.length; i++) {
            rev[i] = passed[passed.length - 1 - i];
        }
        for (int i = 0; i < passed.length / 2; i++) {
            min[i] = passed[i];

            for (int j = 0; j < rev.length / 2; j++) {
                max[j] = rev[j];
            }
            sum += min[i] * max[i];
        }
//        System.out.println(Arrays.toString(rev));
//        System.out.println(Arrays.toString(min));
//        System.out.println(Arrays.toString(max));
        return sum;
    }

    public static int minSum2(int[] numbers) {
        Arrays.sort(numbers);
        int suma = 0;
        for (int i = 0; i < numbers.length / 2; i++) {
            suma += numbers[i] * numbers[numbers.length - 1 - i];
        }
        return suma;
    }
}
