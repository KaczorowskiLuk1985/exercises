package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/principal-diagonal-vs-secondary-diagonal/train/java
 */
public class cw11 {
    public static void main(String[] args) {
        int[][] tab1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] tab2 = {{2, 2, 2}, {4, 2, 6}, {8, 8, 2}};
        int[][] tab3 = {{2, 2, 2}, {4, 2, 6}, {1, 8, 2}};

        System.out.println(Arrays.deepToString(tab1));
        System.out.println(Arrays.deepToString(tab2));
        System.out.println(Arrays.deepToString(tab3));
        System.out.println(diagonal(tab1));
        System.out.println(diagonal(tab2));
        System.out.println(diagonal(tab3));
    }

    public static String diagonal(int[][] matrix) {
        int sumLR = 0;
        int sumRL = 0;
//        int[] LR = new int[matrix.length];
//        int[] RL = new int[matrix.length];

        for (int i = 0; i < matrix.length; i++) {
//            LR[i] = matrix[i][i];
//            RL[i] = matrix[i][matrix.length - 1 - i];
//            sumLR += LR[i];
//            sumRL += RL[i];
            sumLR += matrix[i][i];
            sumRL += matrix[i][matrix.length - 1 - i];
        }

        return sumLR == sumRL ? "Draw!"
                : (sumLR > sumRL ? "Principal Diagonal win!"
                : "Secondary Diagonal win!");

    }
}