package com.company.codewars;
/*
https://www.codewars.com/kata/insert-dashes/train/java
 */

public class CodewarsS32 {
    public static void main(String[] args) {
        System.out.println(insertDash(454793));
    }

    public static String insertDash(int num) {
        String[] split = String.valueOf(num).split("");
        StringBuilder s = new StringBuilder(split[0]);

        for (int i = 1; i < split.length; i++) {
            if (Integer.valueOf(split[i - 1]) % 2 != 0 && Integer.valueOf(split[i]) % 2 != 0) {
                s.append("-");
            }
            s.append(split[i]);
        }
        return s.toString();
    }
}
