package com.company.codewars;
/*
https://www.codewars.com/kata/bingo-or-not/train/java
 */

import java.util.Arrays;

public class cw6 {
    public static void main(String[] args) {
        // a b c d e f g h i j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
        // 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
        // bingo = 2,9,14,7,15
        //         2,7,9,,14,15
        int[] tab = {21, 13,2, 7, 5, 14, 15, 9, 10};

        System.out.println(bingo(tab));


    }

    public static String bingo(int[] numberArray) {


        int[] sort = Arrays.stream(numberArray)
                .distinct()
                .sorted()
                .toArray();
        String bingo = "";

        for (int i = 0; i < sort.length; i++) {
            if (sort[i] == 2 || sort[i] == 7 || sort[i] == 9 || sort[i] == 14 || sort[i] == 15) {
                bingo += sort[i];
            }
        }
        return bingo.equals("2791415") ? "WIN" : "LOSE";
    }
}
