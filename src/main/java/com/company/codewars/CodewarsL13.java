package com.company.codewars;
/*
https://www.codewars.com/kata/square-every-digit/train/java
 */
public class CodewarsL13 {
    public static void main(String[] args) {

        System.out.println(squareDigits(1221));
        System.out.println(squareDigits(90119));
    }

    public static int squareDigits(int n) {
        String napis = Integer.toString(n);
        String[] napisTab = napis.split("");
        int[] tablicaLiczb = new int[napis.length()];
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < napis.length(); i++) {
            tablicaLiczb[i] = (Integer.parseInt(napisTab[i])) * (Integer.parseInt(napisTab[i]));
        }
        for (int i : tablicaLiczb) {
            builder.append(i);
        }
        return Integer.parseInt(String.valueOf(builder));
    }
}
