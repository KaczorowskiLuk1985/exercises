package com.company.codewars;

import java.util.Arrays;

public class CodewarsL16 {
    public static void main(String[] args) {
        int[] tab1 = {1, 3, 1};
        int[] tab2 = {5, 7, 5, 9, 7};
        int[] tab3 = {1, 9, 9, 1, 9, 9, 3, 1, 7, 4, 6, 6, 6, 6, 7, 7};

        System.out.println(minValue2(tab1));
        System.out.println(minValue2(tab2));
        System.out.println(minValue2(tab3));

    }

    public static int minValue(int[] values) {
        int[] nowaTablica = new int[values.length];
        Arrays.sort(values);
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < values.length - 1; i++) {
            if (values[i] != values[i + 1]) {
                nowaTablica[i] = values[i];
            }
            if (values[values.length - 1] != values[i]) {
                nowaTablica[values.length - 1] = values[values.length - 1];
            }
        }
        for (int i : nowaTablica) {
            if (i != 0) {
                builder.append(i);
            }
        }
        return Integer.parseInt(String.valueOf(builder));
    }

    public static int minValue2(int[] values) {
        String nowyNapis = "";
        Arrays.sort(values);
        for (int i = 0; i < values.length; i++) {
            if (!nowyNapis.contains(Integer.toString(values[i]))) {
                nowyNapis += Integer.toString(values[i]);
            }
        }
        return Integer.parseInt(nowyNapis);
    }
}