package com.company.codewars;
/*
https://www.codewars.com/kata/bumps-in-the-road/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsStream10 {
    public static void main(String[] args) {
        System.out.println(bumps("nnnnnnnn____nnnnnnnn"));
    }

    public static String bumps(final String road) {
        String[] str = road.split("");

//        return Arrays.stream(str)
//                .filter(lit -> lit.contains("n"))
//                .collect(Collectors.joining())
//                .length()
//                <= 15 ? "Woohoo!" : "Car Dead";
        return road.chars()
                .filter(lit -> lit == 'n')
                .count() // zwraca liczbę elementów w strumieniu
                <= 15 ? "Woohoo!" : "Car Dead";
    }

}
