package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/summy
 */
public class CodewarsS30 {
    public static void main(String[] args) {
        System.out.println(summy("1 2 3 4 "));

    }
    static long summy(String stringOfInts){
        return Arrays.stream(stringOfInts.split(" "))
                .mapToLong(Long::valueOf)
                .sum();
    }

}
