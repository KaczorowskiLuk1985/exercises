package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/sum-of-numbers-from-0-to-n/train/java
 */
public class CodewarsL18 {
    public static void main(String[] args) {
        System.out.println(showSequence(14));
    }

    public static String showSequence(int value) {
        if (value < 0) {
            return value + "<0";
        }
        if (value == 0) {
            return value + "=0";
        }
        int sum = 0;
        int[] tablicaLiczb = new int[value + 1];
        String[] tablicString = new String[tablicaLiczb.length];

        for (int i = 0; i <= value; i++) {
            sum += i;
            tablicaLiczb[i] += i;
            tablicString[i] = String.valueOf(tablicaLiczb[i]);
        }
        return String.join("+", tablicString) + " = " + sum;
    }
}
