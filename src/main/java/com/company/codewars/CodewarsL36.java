package com.company.codewars;
/*
https://www.codewars.com/kata/reverse-a-number/train/java
 */

public class CodewarsL36 {
    public static void main(String[] args) {
        System.out.println(reverse(0));

    }

    public static int reverse(int number) {
        int mniejszyOdZera = Math.abs(number);
        String[] nowyNapis = String.valueOf(mniejszyOdZera).split("");
        String s = "";

        for (int i = nowyNapis.length - 1; i >= 0; i--) {
            s += nowyNapis[i];
        }
        return number > 0 ? Integer.valueOf(s) : Integer.valueOf(s) * -1;
    }
}