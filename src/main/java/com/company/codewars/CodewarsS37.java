package com.company.codewars;
/*
https://www.codewars.com/kata/sort-the-gift-code/train/java
 */

import sun.security.krb5.internal.crypto.Aes128;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsS37 {
    public static void main(String[] args) {
        String a = "zyxwvutsrqponmlkjihgfedcba";
        String b = "kpqsuvy";
        String c = "abcdef";
        System.out.println(sortGiftCode(a));
        System.out.println(sortGiftCode(b));
        System.out.println(sortGiftCode(c));
    }

    public static String sortGiftCode(String code) {
//        return Arrays
//                .stream(code.split(""))
//                .filter(letter -> letter.matches("[a-zA-Z]"))
//                .sorted()
//                .collect(Collectors.joining());

        String[] split = code.split("");
        Arrays.sort(split);


        return String.join("", split);
    }
}
