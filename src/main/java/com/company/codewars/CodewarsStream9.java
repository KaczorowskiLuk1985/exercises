package com.company.codewars;
/*
https://www.codewars.com/kata/if-you-cant-sleep-just-count-sheep/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class CodewarsStream9 {
    public static void main(String[] args) {

        System.out.println(countingSheep(3));
    }

    public static String countingSheep(int num) {
        String napis = " sheep...";

        return IntStream
                .rangeClosed(1, num)
                .mapToObj(liczba -> liczba + napis)
                .collect(Collectors.joining()); // pusty nawias to to samo co separttor""
    }
}
