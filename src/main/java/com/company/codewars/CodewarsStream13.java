package com.company.codewars;
/*
https://www.codewars.com/kata/beginner-series-number-3-sum-of-numbers
 */

import java.util.Arrays;
import java.util.stream.IntStream;

public class CodewarsStream13 {
    public static void main(String[] args) {
        System.out.println(GetSum(1, -2));
        System.out.println(getSum(1, -2));
    }

    public static int GetSum(int a, int b) {
        if (a > b) {
            return IntStream
                    .rangeClosed(b, a)
                    .sum();
        }

        return IntStream
                .rangeClosed(a, b)
                .sum();
    }

    public static int getSum(int a, int b) {
        int sum = 0;
        if (a == b) {
            return a;
        }

        if (a < b) {
            for (int i = a; i <= b; i++) {
                sum += i;

            }
        }
        if (a > b) {
            for (int i = b; i <= a; i++) {
                sum += i;

            }
        }
        return sum;
    }
}

