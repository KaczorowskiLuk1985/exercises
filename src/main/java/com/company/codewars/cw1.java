package com.company.codewars;
/*
https://www.codewars.com/kata/collatz-conjecture-length/train/java
 */

public class cw1 {
    public static void main(String[] args) {
//        System.out.println(conjecture(120));
        System.out.println(conjecture(20));
        System.out.println(conjecture(16));
    }

    public static long conjecture(long x) {
        long length = 0;

        do {
            if (x % 2 == 0) {
                x = x / 2;
            } else {
                x = 3 * x + 1;
            }
            length++;
        } while (x != 1);

        System.out.println();
        System.out.println("return length");
        return length + 1;
    }
}