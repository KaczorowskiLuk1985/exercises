package com.company.codewars;
/*
https://www.codewars.com/kata/number-of-people-in-the-bus
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CodewarsStream7 {
    public static void main(String[] args) {
        ArrayList<int[]> list = new ArrayList<>();
        list.add(new int[]{10, 0});
        list.add(new int[]{3, 5});
        list.add(new int[]{2, 5});

        System.out.println(countPassengers(list));

    }

    public static int countPassengers(ArrayList<int[]> stops) {
//        int in = 0;
//        int out = 0;

//        for (int[] ints : stops) {
//            in += ints[0];
//            out += ints[1];
//        }
//        return in - out;

//        int wsiadajacy = stops
//                .stream()
//                .mapToInt(x -> x[0])
//                .sum();
//        int wysiadajacy = stops.stream()
//                .mapToInt(x -> x[1])
//                .sum();
//
//        return wsiadajacy - wysiadajacy;

        return stops
                .stream()
                .mapToInt(x -> x[0] - x[1])
                .sum();
    }
}
