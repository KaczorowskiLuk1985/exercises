package com.company.codewars;
/*
https://www.codewars.com/kata/vowel-count/train/java
 */

public class cw15 {
    public static void main(String[] args) {
        String s = "abracadabra";
        System.out.println(getCount(s));

    }

    public static int getCount(String str) {
        String string = str.replaceAll("[aeiou]", "");

        return str.length() - string.length();
    }
}
