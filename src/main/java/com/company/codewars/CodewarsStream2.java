package com.company.codewars;

import java.util.Arrays;

public class CodewarsStream2 {
    public static void main(String[] args) {
        int[] tab = {-1, -5};
        System.out.println(sum(tab));

    }

    public static int sum(int[] arr) {

        return Arrays.stream(arr)
                .filter(liczba -> liczba > 0)
                .sum();
    }
}
