package com.company.codewars;
/*
https://www.codewars.com/kata/sort-an-array-by-value-and-index/train/java
 */

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CodewarsL46 {
    public static void main(String[] args) {
        int[] tab = {23, 2, 3, 4, 5};
        System.out.println(Arrays.toString(sortByValueAndIndex(tab)));

    }

    public static int[] sortByValueAndIndex(int[] array) {
        int[] nowyArr = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            nowyArr[i] = array[i] * (i + 1);
        }
        for (int i = 0; i < array.length; i++) {

            for (int j = i + 1; j < array.length; j++) {

                if (nowyArr[j] < nowyArr[i]) {


                    int zmiennaNowyArr = nowyArr[i];
                    int zmiennaArr = array[i];


                    nowyArr[i] = nowyArr[j];
                    array[i] = array[j];

                     nowyArr[j] = zmiennaNowyArr;
                    array[j] = zmiennaArr;
                }
            }
        }
        System.out.println(Arrays.toString(nowyArr));
        return array;
    }
}
