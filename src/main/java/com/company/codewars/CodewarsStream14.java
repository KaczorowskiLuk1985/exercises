package com.company.codewars;

import java.util.Arrays;
import java.util.stream.IntStream;

/*
https://www.codewars.com/kata/extra-perfect-numbers-special-numbers-series-number-7/train/java
 */
public class CodewarsStream14 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(extraPerfect(28)));
    }

    public static int[] extraPerfect(int number) {

        return IntStream
                .iterate(1, liczba -> liczba + 2)
                .limit(number)
                .filter(value -> value <= number)
                .toArray();

//        List<Integer> list = IntStream
//                .iterate(1, lic -> lic + 2)
//                .limit(number)
//                .filter(value -> value < number)
//                .boxed()
//                .collect(Collectors.toList());
    }

}
