package com.company.codewars;
/*
https://www.codewars.com/kata/coloured-triangles/train/java
 */

public class CodewarsL32 {
    public static void main(String[] args) {
        System.out.println(triangle("RRGBRGBB"));

    }

    static char triangle(final String row) {
        char[] tab = row.toCharArray();
        char[] tab2 = tab;
        for (int j = 0; j < row.length(); j++) { // pierwsza tablica idzie w pionie
            if (tab.length == 1) {
                return tab2[0];
            }
            tab2 = new char[tab.length - 1];
            for (int i = 0; i < tab.length - 1; i++) {

                if (tab[i] == tab[i + 1]) {
                    tab2[i] = tab[i];
                } else {
                    tab2[i] = "RGB".replace(String.valueOf(tab[i]), "")
                            .replace(String.valueOf(tab[i + 1]), "")
                            .charAt(0);
                }
            }
            tab = tab2;

        }


        return tab2[0];
    }
}
