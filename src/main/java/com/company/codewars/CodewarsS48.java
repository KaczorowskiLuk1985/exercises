package com.company.codewars;
/*
https://www.codewars.com/kata/nickname-generator/train/java
 */

public class CodewarsS48 {
    public static void main(String[] args) {
        System.out.println(nickname("Robert"));
        System.out.println(nickname("Kimberly"));
        System.out.println(nickname("Samantha"));
        System.out.println(nickname("Sam"));
        System.out.println("+");
        System.out.println(nickname("Jeannie"));
        System.out.println(nickname("Gregory"));
        System.out.println(nickname("Kayne"));
        System.out.println(nickname("Melissa"));

    }

    public static String nickname(String name) {
        char a = name.charAt(2);

        if (name.length() < 4) {
            return "Error: Name too short";
        }
        if (a == 'a' || a == 'e' || a == 'i' || a == 'o' || a == 'u') {
            return name.substring(0, 4);
        } else {
            return name.substring(0, 3);
        }
    }
}
