package com.company.codewars;
/*
https://www.codewars.com/kata/exclamation-marks-series-number-5-remove-all-exclamation-marks-from-the-end-of-words/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;

public class CodewarsS34 {
    public static void main(String[] args) {
        String a = "!!!Hi !!hi!!! !hi";
        String b = "!!No! seriously! !Seriously!! !!Wow!";

        System.out.println(removeBang(a));
        System.out.println(removeBang(b));
//        System.out.println(bangKiller("!!hi!!!"));
//        System.out.println(bangKiller("huj"));

    }

    public static String removeBang(String str) {
//        String[] s1 = str.split(" ");
//        String[] s2 = new String[s1.length];
//
//        for (int i = 0; i < s1.length; i++) {
//            s2[i] = bangKiller(s1[i]);
//        }
//        return String.join(" ", s2);
//
//    }
//
//    static String bangKiller(String str) {
//        String[] split = str.split("");
//        StringBuilder a = new StringBuilder();
//
//        for (String string : split) {
//            a.append(string);
//        }
//        a.reverse();
////        System.out.println(a + "<--");
//        String[] splitRevese = a.toString().split("");
//        int count = 0;
//
//        for (int i = 1; i < splitRevese.length; i++) {
//            if (splitRevese[i - 1].equals("!") && !splitRevese[i].equals("!")) {
//                count += i;
//            }
//        }
////        System.out.println(count + " <-- count");
//        String string = a.reverse().toString();
////        System.out.println(string + " <-- nieruszony");
//        return string.substring(0, string.length() - count);

        return Arrays.stream(str.split(" "))
                .map(slowo-> slowo.replaceAll("!+$",""))
                .collect(Collectors.joining(" "));
    }
}
