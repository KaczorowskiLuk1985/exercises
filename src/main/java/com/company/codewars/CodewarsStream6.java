package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/calculate-average/
 */
public class CodewarsStream6 {
    public static void main(String[] args) {
        int[] tablica = {1, 0, 5, 4, 67, 7};
        System.out.println(find_average(tablica));

    }
    public static double find_average(int[] array){
        return Arrays.stream(array).average().getAsDouble();
    }
}
