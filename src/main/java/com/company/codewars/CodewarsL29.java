package com.company.codewars;

public class CodewarsL29 {
    public static void main(String[] args) {
        System.out.println(breakChocolate(5, 3));
    }

    public static int breakChocolate(int n, int m) {
        return n > 0 && m > 0 ? n * m - 1 : 0;
    }
}