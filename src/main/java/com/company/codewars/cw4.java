package com.company.codewars;
/*
https://www.codewars.com/kata/ultimate-array-reverser/train/java
 */

import java.util.Arrays;

public class cw4 {
    public static void main(String[] args) {
        String[] a = {"I", "like", "big", "butts", "and", "I", "cannot", "lie!"};

        System.out.println(Arrays.toString(reverse(a)));

    }

    public static String[] reverse(String[] a) {
        String reverseJoin = new StringBuilder(String.join("", a)).reverse().toString();
        String[] result = new String[a.length];
        int indexLenght = 0;
        int sumOfIndexLenght = 0;

        for (int i = 0; i < a.length; i++) {
            indexLenght = a[i].length();
            sumOfIndexLenght += a[i].length();
            result[i] = reverseJoin.substring(sumOfIndexLenght - indexLenght, sumOfIndexLenght);
        }

        return result;
    }

}
