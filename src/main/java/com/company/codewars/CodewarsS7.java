package com.company.codewars;
/*
https://www.codewars.com/kata/broken-sequence/train/java
 */

import java.util.Arrays;

public class CodewarsS7 {
    public static void main(String[] args) {
        String a = "1 10 100 101 102 11 12 13 14 15 16 17 18 19 2 20 21 22 23 24 25 26 27 28 29 3 30 31 32 33 34 35 36 37 38 39 4 40 41 42 43 44 45 46 47 48 49 5 50 51 52 53 54 55 56 57 58 59 6 60 61 62 63 64 65 66 67 68 69 7 70 71 72 73 74 75 76 77 78 79 8 80 81 82 83 84 85 86 87 88 89 9 91 92 93 94 95 96 97 98 99";
        String b = "1 2 4 3";
        String c = "2 1 3 a";
        String d = "1 3 2 5";
        String e = "1 5";
        System.out.println(findMissingNumber(a));
        System.out.println(findMissingNumber(b));
        System.out.println(findMissingNumber(c));
        System.out.println(findMissingNumber(d));
        System.out.println(findMissingNumber(e));

    }

    public static int findMissingNumber(String sequence) {
        if (sequence==""){
            return 0;
        }
        String[] tab = sequence.split(" ");
        for (int i = 0; i < tab.length; i++) {
            if (!tab[i].matches("[0-9]+")) {
                return 1;
            }
        }
        int[] tablicaIntow = new int[tab.length];
        for (int i = 0; i < tab.length; i++) {
            tablicaIntow[i] = Integer.parseInt(tab[i]);
        }
        Arrays.sort(tablicaIntow);
        for (int i = 0; i < tablicaIntow.length-1; i++) {
            if (tablicaIntow[i]!=tablicaIntow[i+1]-1){
                return tablicaIntow[i]+1;
            }
            if (tablicaIntow[0]>1){
                return 1;
            }
        }

        return 0;
    }
}