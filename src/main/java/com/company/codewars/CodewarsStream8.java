package com.company.codewars;
/*
https://www.codewars.com/kata/help-the-fruit-guy
 */

import java.util.Arrays;

public class CodewarsStream8 {
    public static void main(String[] args) {
        String[] rotten = new String[]{"rottenApple",
                "rottenBanana",
                "rottenApple",
                "rottenPineapple",
                "rottenKiwi",
                "kiwiiwiw"};
        System.out.println(Arrays.toString(removeRotten(rotten)));
    }


    public static String[] removeRotten(String[] fruitBasket) {


        return Arrays.stream(fruitBasket)
                .map(s -> {
                    if (s.contains("rotten")) {
                        return s.substring(6).toLowerCase();
                    } else {
                        return s.toLowerCase();
                    }
                })

                .toArray(String[]::new);

    }
}
