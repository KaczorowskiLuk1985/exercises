package com.company.codewars;
/*
https://www.codewars.com/kata/arithmetic-progression/train/java
 */

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;


public class CodewarsS43 {
    public static void main(String[] args) {

        System.out.println(arithmeticSequenceElements(1, 1, 10));


    }

    public static String arithmeticSequenceElements(int first, int step, long total) {
        int[] string = IntStream
                .iterate(first, liczba -> liczba + step)
                .limit(total)
                .toArray();

        String string1 = Arrays.toString(string);

//        return string1.substring(1,string1.length()-1);
//        return LongStream.range(0, total)
//                .map(i -> first + step * i)
//                .mapToObj(Long::toString)
//                .collect(Collectors.joining(", "));

        return IntStream
                .iterate(first, liczba -> liczba + step)
                .limit(total)
                .mapToObj(Long::toString)
                .collect(Collectors.joining(", "));
    }
}
