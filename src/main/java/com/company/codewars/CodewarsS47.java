package com.company.codewars;
/*
https://www.codewars.com/kata/determine-if-the-poker-hand-is-flush/train/java
 */

public class CodewarsS47 {
    public static void main(String[] args) {
        System.out.println(CheckIfFlush(new String[]{"AS", "3S", "9S", "KS", "4S"}));
        System.out.println(CheckIfFlush(new String[]{"AD", "4S", "7H", "KC", "5S"}));
        System.out.println(CheckIfFlush(new String[]{"AD", "4S", "10H", "KC", "5S"}));
        System.out.println(CheckIfFlush(new String[]{"QD", "4D", "10D", "KD", "5D"}));

    }

    public static boolean CheckIfFlush(String[] cards) {
        int count = 0;

        for (int i = 0; i < cards.length - 1; i++) {
            if (cards[i].charAt(cards[i].length() - 1) == cards[i + 1].charAt(cards[i + 1].length() - 1)) {
                count++;
            }
        }
        System.out.println(count);
        return count == 4;
    }

}
