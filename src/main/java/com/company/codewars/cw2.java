package com.company.codewars;
/*
https://www.codewars.com/kata/product-of-maximums-of-array-array-series-number-2/train/java
 */

import java.util.Arrays;
import java.util.stream.IntStream;

public class cw2 {
    public static void main(String[] args) {
        int[] tab = {2, 3};
        int[] tab2 = {-4, -27, -15, -6, -1};
        int[] tab3 = {14,29,-28,39,-16,-48};
        System.out.println(maxProduct(tab, 3));
        System.out.println(maxProduct(tab2, 2));
        System.out.println(maxProduct(tab3, 4));

    }

    public static long maxProduct(int[] numbers, int sub_size) {
        long resoult = 1;
        Arrays.sort(numbers);
        int[] revrsArr = IntStream
                .range(0, numbers.length)
                .map(i -> numbers[numbers.length - i - 1])
                .filter(i -> i != 0)
                .toArray();

        if (numbers.length<3 || revrsArr.length < 3) {
            return 0;
        } else {
            for (int i = 0; i < sub_size; i++) {
                resoult *= revrsArr[i];

            }
            return resoult;
        }
    }
}
