package com.company.codewars;
/*
https://www.codewars.com/kata/string-ends-with/train/java
 */

public class CodewarsS14 {
    public static void main(String[] args) {
        String a = "abc";
        String b = "abcd";
        String c = "samurai";
        String d = "ra";

        System.out.println(solution(c, d));
        System.out.println(solution(a, b));

    }

    public static boolean solution(String str, String ending) {
        if (str.length() < ending.length()) {
            return false;
        }

        System.out.println(str.substring(str.length() - ending.length()));

        return str.substring(str.length() - ending.length()).contains(ending);
    }
}
