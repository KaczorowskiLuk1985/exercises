package com.company.codewars;
/*
https://www.codewars.com/kata/lost-number-in-number-sequence/train/java
 */

import java.util.Arrays;

public class CodewarsL39 {
    public static void main(String[] args) {
        int[] tab1 = {1, 2, 3, 4, 5};
        int[] tab2 = {1, 2, 3, 4};

        int[] tab3 = {2, 3, 4};
        int[] tab4 = {3, 4};

        int[] tab5 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] tab6 = {5, 7, 6, 9, 4, 8, 1, 2, 3};

        int[] tab7 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] tab8 = {5, 7, 6, 9, 4, 1, 2, 3};

        int[] tab9 = {1, 2};
        int[] tab10 = {1};

        System.out.println(findDeletedNumber(tab1, tab2));
        System.out.println(findDeletedNumber(tab3, tab4));
        System.out.println(findDeletedNumber(tab5, tab6));
        System.out.println(findDeletedNumber(tab7, tab8));
        System.out.println(findDeletedNumber(tab9, tab10));

    }

    public static int findDeletedNumber(int[] arr, int[] mixedArr) {
     //   Arrays.sort(mixedArr);
        int licznik = 0;
        int drugiLicznik = 0;

        for (int i = 0; i < arr.length; i++) {
            licznik += arr[i];

        }
        for (int i = 0; i < mixedArr.length; i++) {
            drugiLicznik += mixedArr[i];

        }
        return licznik - drugiLicznik;
    }
}