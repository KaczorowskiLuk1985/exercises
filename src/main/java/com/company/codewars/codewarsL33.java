package com.company.codewars;
/*
https://www.codewars.com/kata/credit-card-issuer-checking/train/java
 */

import static java.util.regex.Pattern.matches;

public class codewarsL33 {
    public static void main(String[] args) {
        System.out.println(getIssuer("6011111111111117")); //discover
        System.out.println(getIssuer("378282246310005")); //amex
        System.out.println(getIssuer("4012888888881881")); //visa
        System.out.println(getIssuer("5105105105105106")); //Mastercard
        System.out.println(getIssuer("9111111111111111")); //unknown

    }

    public static String getIssuer(String cn) {
//        if (matches("^3[4|7].{13}$", cn)) return "AMEX";
        if (cn.matches("^3[4|7].{13}$")) return "a";
        if (matches("^6011.{12}$", cn)) return "Discover";
        if (matches("^5[1-5].{14}$", cn)) return "Mastercard";
        if (matches("^4.{12}(...)?$", cn)) return "VISA";
        return "Unknown";

//        int lenght = cardNumber.length();
//
//        if (cardNumber.matches("[0-9]*") ) {
//            if (cardNumber.substring(0, 4).equals("6011") && lenght == 16) {
//                return "Discover";
//            }
//            if ((cardNumber.substring(0, 2).equals("34")
//                    || cardNumber.substring(0, 2).equals("37"))
//                    && lenght == 15) {
//                return "AMEX";
//            }
//            if (cardNumber.substring(0, 1).equals("4") && (lenght == 13 || lenght == 16)) {
//                return "VISA";
//            }
//            String sub = cardNumber.substring(0, 2);
//            if (sub.equals("51")
//                    || sub.equals("52")
//                    || sub.equals("53")
//                    || sub.equals("54")
//                    || sub.equals("55")
//                    && lenght == 16) {
//                return "Mastercard";
//            }
//        }
//        return "Unknown";
    }
}
