package com.company.codewars;

import java.util.Arrays;

/*
https://www.codewars.com/kata/form-the-largest/train/java
 */
public class CodewarsL48 {
    public static void main(String[] args) {
        System.out.println(maxNumber(7389));
        System.out.println(najwiekszaLiczba(7389));

    }

    public static long maxNumber(long n) {
        String string = String.valueOf(n);
        char[] tablicaZnakow = string.toCharArray();
        Arrays.sort(tablicaZnakow);
        char[] nowaTab = new char[tablicaZnakow.length];
        int odwroc = tablicaZnakow.length - 1;

        for (int i = 0; i < tablicaZnakow.length; i++) {
            nowaTab[i] = tablicaZnakow[odwroc];
            odwroc--;
        }
        return Long.parseLong(String.valueOf(nowaTab));
    }

    private static long najwiekszaLiczba(long n) {
        String nowyNapis = "";
        char[] tabZnak = Long.toString(n).toCharArray();
        Arrays.sort(tabZnak);

        for (int i = tabZnak.length - 1; i >= 0; i--) {
            nowyNapis += tabZnak[i];
        }
        return Long.parseLong(nowyNapis);
    }
}
