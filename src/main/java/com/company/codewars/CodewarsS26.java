package com.company.codewars;
/*
https://www.codewars.com/kata/anagram-detection/train/java
 */

import java.util.Arrays;

public class CodewarsS26 {
    public static void main(String[] args) {
        System.out.println(isAnagram("foefet", "toffee"));
        System.out.println(isAnagram("apple", "pale"));
        System.out.println(isAnagram("bdelmu", "bbelmu"));
    }

    public static boolean isAnagram(String test, String original) {
//        if (test.length() != original.length()) {
//            return false;
//        }
        String[] tablicaTestowa = test.toLowerCase().split("");
        String[] tablicaOrginalna = original.toLowerCase().split("");
        Arrays.sort(tablicaTestowa);
        Arrays.sort(tablicaOrginalna);
        String a = String.join("", tablicaTestowa);
        String b = String.join("", tablicaOrginalna);
        System.out.println(a);
        System.out.println(b);

        return a.equals(b);

//        String a = Arrays.stream(test.testowy(""))
//                .sorted()
//                .collect(Collectors.joining());
//        String b = Arrays.stream(original.testowy(""))
//                .sorted()
//                .collect(Collectors.joining());
//        System.out.println(a.hashCode() + " -> " + a);
//        System.out.println(b.hashCode() + " -> " + b);
//        return a.hashCode() == b.hashCode();
    }
}
