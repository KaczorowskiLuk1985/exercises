package com.company.zadanie136;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.regex.Pattern;

/*W załączniku jest plik z loginami i hasłami. Waszym zadaniem jest zrobienie analizy tego pliku oraz napisanie metod które *ZWRÓCĄ*:

- `[double]` informację jaki % osób miało taki sam login jak hasło (np. `anka40:anka40`) - done

- `[int]` informację ilu użytkowników “wyciekło” - done

- `[int]` informację ilu użytkowników NIE miało cyfry w haśle - chyba done

- `[double]` informację jaki % użytkowników nie używało dużych liter w haśle - chyba done

- `[int]` informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody) - chyba done

- `[Map<Integer, Integer>]` informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...)

- `[List<String>]` listę 10 najpopularniejszych haseł

- `[Map<String, Integer>]`  ile razy wystąpiła każda ze skrzynek mailowych (oczywiście jeśli loginem był mail)*/
public class Zadanie136 {
    private static final String SEPARATOR = ":";
    private static final HashMap<String, String> map = new HashMap<>();

    public static void main(String[] args) {
        try {
            readFile("pliki/Zadanie136.txt");
        } catch (IOException e) {
            System.out.println("Błąd odczytu pliku");
            return;
        }
        System.out.println("Liczba użytkowników która wyciekła to: " + usersCount());
        System.out.println("Procent użytkownikow z identycznym loginem i hasłem to: " + sameLoginAndPasswd() + "%");
        System.out.println("Liczba użytkowników bez cyfry w haśle: " + noNumberInPasswd());
        System.out.println("Procent użytkowników bez wielkich liter w haśle to: " + noUpperCaseInPasswd() + "%");
        System.out.println("Liczba użytkowników z hasłem krótszym od podanej długości to: " + passwdLessThan(7));
        System.out.println("Ile razy występowały hasła o określonych długościach" + passwdLengthCount());
        System.out.println("10 najpopularniejszych haseł: " + tenCommonPasswd());
        System.out.println("Wystąpienia skrzynek mailowych: " + mailboxCount());
    }

    private static void readFile(String filePath) throws IOException {
        String line;
        BufferedReader buffer = new BufferedReader(new FileReader(filePath));
        while ((line = buffer.readLine()) != null) {
            String[] parts = line.split(SEPARATOR);
            String key = parts[0];
            String value = parts[1];
            map.put(key, value);
        }
        buffer.close();
    }

    private static int usersCount() {
        return map.size();
    }

    private static double sameLoginAndPasswd() {
        double counter = 0;
        for (String key : map.keySet()) {
            if (key.equals(map.get(key))) {
                counter++;
            }
        }
        BigDecimal percent = new BigDecimal(counter / map.size() * 100);
        percent = percent.setScale(2, RoundingMode.HALF_UP);
        return percent.doubleValue();
    }

    private static int noNumberInPasswd() {
        int counter = 0;
        for (String key : map.keySet()) {
            //         if (Pattern.compile("[0-9]").matcher(map.get(key)).find()){
            if (!map.get(key).matches(".*[0-9].*")) {
                counter++;
            }
        }
        return counter;
    }

    private static double noUpperCaseInPasswd() {
        double counter = 0;
        for (String key : map.keySet()) {
            if (Pattern.compile("[A-Z]").matcher(map.get(key)).find()) { //TODO do zmiany
                counter++;
            }
        }
        BigDecimal percent = new BigDecimal((map.size() - counter) / map.size() * 100);
        percent = percent.setScale(2, RoundingMode.HALF_UP);
        return percent.doubleValue();
    }

    private static int passwdLessThan(int length) {
        int counter = 0;
        for (String key : map.keySet()) {
            if (map.get(key).length() < length) {
                counter++;
            }
        }
        return counter;
    }

    private static List<Map.Entry<String, Integer>> tenCommonPasswd() {
        Map<String, Integer> countMap = new HashMap<>();
        for (String passwd : map.values()) {
            countMap.put(passwd, countMap.getOrDefault(passwd, 0) + 1);
        }
        List<Map.Entry<String, Integer>> entries = new ArrayList<>(countMap.entrySet());
        entries.sort(Map.Entry.comparingByValue());
        List<Map.Entry<String, Integer>> passwd = entries.subList(entries.size() - 10, entries.size());
        return passwd;
    }

    private static Map<Integer, Integer> passwdLengthCount() {
        Map<Integer, Integer> mapa = new HashMap<>();
        for (String passwd : map.values()) {
            Integer passwdLength = passwd.length();
            mapa.put(passwdLength, mapa.getOrDefault(passwdLength, 0) + 1);
        }
        return mapa;
    }

    private static Map<String, Integer> mailboxCount() {
        final String separator = "@";
        Map<String, Integer> mailMap = new HashMap<>();

        for (String login : map.keySet()) {
            if (login.contains(separator)) {
                String[] splittedMail = login.split(separator);
                if (splittedMail.length == 2) {
                    String mailbox = splittedMail[1].toLowerCase();
                    int domainCount = mailMap.getOrDefault(mailbox, 0);
                    mailMap.put(mailbox, domainCount + 1);
                }
            }
        }
        return mailMap;
    }
}