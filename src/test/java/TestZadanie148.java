import com.company.zadania.zadanie148.Zadanie148;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestZadanie148 {
    Zadanie148 sumaElementow = new Zadanie148();

    @Test
    void sprPoprawnegoZakresuLiczb() {
        int[] tab = {1, 2, 3, 4, 5, 6};
        Integer wynik = sumaElementow.ileElementówZsumowac(tab, 9);
        Assertions.assertEquals(Integer.valueOf(4), wynik);
    }

    @Test
    void sprawdzenieJesliNieZnajdzieLiczby() {
        int[] tab = {1, 2, 3, 4};
        Integer wynik = sumaElementow.ileElementówZsumowac(tab, 11);
        Assertions.assertNull(wynik);

    }

    @Test
    void sprawdzenieGdyTablicaJesPusta() {
        int[] tab = {};
        Integer wynik = sumaElementow.ileElementówZsumowac(tab, 9);
        Assertions.assertNull(wynik);

    }

    @Test
    void sprawdzenieJesliPierwszaLiczbaSpelniaWarunek() {
        int[] tab = {5, 8, 3, 6};
        Integer wynik = sumaElementow.ileElementówZsumowac(tab, 4);
        Assertions.assertEquals(Integer.valueOf(1), wynik);

    }

    @Test
    void sprawdzenieJeslitablicaJestNulem() {
        Integer wynik = sumaElementow.ileElementówZsumowac(null, 4);
        Assertions.assertNull(wynik);
    }
}
