import com.company.zadania.zadanie147.Zadanie147;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadania147 {
    Zadanie147 kalkulator = new Zadanie147();

    @Test
    void sprawdzCzySumujePoprawnie() {
        Integer wynik = kalkulator.sumowanie(3, 4);
        Assertions.assertEquals(Integer.valueOf(7), wynik);
    }

    @Test
    void sprawdzCzySumujePoprawnieGdyPierwszaJestNulem() {
        Integer wynik = kalkulator.sumowanie(null, 2);
        Assertions.assertEquals(Integer.valueOf(2), wynik);
    }

    @Test
    void sprawdzCzySumujePoprawnieGdyDrugaJestNulem() {
        Integer wynik = kalkulator.sumowanie(2, null);
        Assertions.assertEquals(Integer.valueOf(2), wynik);

    }

    @Test
    void sprawdzCzySumujePoprawnieGdyObieSaNulem() {
        Integer wynik = kalkulator.sumowanie(null, null);
        Assertions.assertNull(wynik);

    }

    @Test
    void sprawdzCzySumujePoprawnieMaksymalne() {
        Integer wynik = kalkulator.sumowanie(Integer.MAX_VALUE, Integer.MIN_VALUE);
        Assertions.assertEquals(Integer.valueOf(-2), wynik);

    }

    @Test
    public void sprawdzCzySumujePoprawnieWieleLiczb() {
        Integer wynik = kalkulator.sumowanieWieluLiczb(5, 10, 15, 20);
        Assertions.assertEquals(Integer.valueOf(50), wynik);
    }

    @Test
    public void SprzawdzGdyPusta() {
        Integer wynik = kalkulator.sumowanieWieluLiczb(null);
        Assertions.assertNull(wynik);
    }

    @Test
    public void SprzawdzGdyNicNiePrzekazuje() {
        Integer wynik = kalkulator.sumowanieWieluLiczb();
        Assertions.assertEquals(Integer.valueOf(0), wynik);
    }

    @Test
    public void sprCzyOdejmuje() {
        Integer wynik = kalkulator.odejmowanieDwochLiczb(3, 1);
        Assertions.assertEquals(Integer.valueOf(2), wynik);

    }

    @Test
    void sprGdyNull() {
        Integer wynik = kalkulator.odejmowanieDwochLiczb(null, null);
        Assertions.assertNull(wynik);
    }

    @Test
    void sprGdyL1jestNulem() {
        Integer wynik = kalkulator.odejmowanieDwochLiczb(null, -4);
        Assertions.assertEquals(Integer.valueOf(-4), wynik);
    }

    @Test
    void sprGdyL2JestNulem() {
        Integer wynik = kalkulator.odejmowanieDwochLiczb(-4, null);
        Assertions.assertEquals(Integer.valueOf(-4), wynik);
    }
}
