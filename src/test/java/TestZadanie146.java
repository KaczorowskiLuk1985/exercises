import com.company.zadania.Zadanie146;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
//ctrl + alt + o - usuwa zbedne adnotacje

public class TestZadanie146 {
    @Test
    public void czyZwracaLiczbe() {
        double wynik = Zadanie146.zwrocPoPodatku(100, 23);
        Assertions.assertEquals(123.0, wynik);
    }
    @Test
    public void CzyPodatekJestUjemny(){
        double wynik = Zadanie146.zwrocPoPodatku(100,-23);
        Assertions.assertEquals(100,wynik);
    }
    @Test
    public void czyPodatekWiekszyOd100(){
        double wynik = Zadanie146.zwrocPoPodatku(100,100);
        Assertions.assertEquals(200,wynik);

    }
    @Test
    public void czyPodatekWiekszyOd105(){
        double wynik = Zadanie146.zwrocPoPodatku(100,105);
        Assertions.assertEquals(100,wynik);
    }
    @Test
    public void gdyPodatekJestRownyZero(){
        double wynik = Zadanie146.zwrocPoPodatku(100,0);
        Assertions.assertEquals(100,wynik);
    }

}
