import com.company.zadania.zadanie150.Zadanie150;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie150 {
    Zadanie150 testowanieTablicy = new Zadanie150();

    @Test
    void czyTablicaJestOk() {
        int spodziweanaDlugosc = 4; //expLenght
        int[] tablicazorcowa = {10, 11, 12, 13};
        int[] wynik = testowanieTablicy.zwracaTablica(spodziweanaDlugosc);
        Assertions.assertEquals(spodziweanaDlugosc, wynik.length);
        Assertions.assertArrayEquals(tablicazorcowa, wynik);
    }

//    @Test
//    void czyZwrociTabliceGdyPrzegazaneJestZero() {
//        int[] tablicaworcowa = {};
//        int[] wynik = testowanieTablicy.zwracaTablica(0);
//        Assertions.assertArrayEquals(tablicaworcowa, wynik);
//    }
//
//    @Test
//    void czyzwrociTabliceGdyPrzekazanaWartoscJestUjemna() {
//
//    }

    @Test
    void czyZwrociTabliceZGranicyZakresu() {
        //TODO - uzupełnic po zajeciach z JVM (java virtual machine)
        int granica = Integer.MIN_VALUE;
        int[] tabWynikowa = testowanieTablicy.zwracaTablica(granica);
        Assertions.assertEquals(granica, tabWynikowa.length);
    }
}
