import com.company.zadania.zadanie151.Osoba;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestZadania151TDD { //podejscie tdd najpierw trorzymy testy i pozniej dopasowujemy program
    private Osoba persona;

    @BeforeEach
    void setPersona() {

        persona = new Osoba();
    }

    @Test
    void czyZwracaPelnaNazeKiedyJestImieINazwisko() {
        persona.setImie("Marian");
        persona.setNazwisko("Nowak");

        Assertions.assertEquals("Marian Nowak", persona.zwrocNapis());

    }

    @Test
    void czyZwracaImie() {
        persona.setImie("Ilona");
        Assertions.assertEquals("Ilona", persona.zwrocNapis());
    }

    @Test
    void czyZwracaNazwisko() {
        String nazwisko = "Novak";
        persona.setNazwisko(nazwisko);

        Assertions.assertEquals(nazwisko, persona.zwrocNapis());
    }

    @Test
    void czyZwociNullKiedyNePodaImieniaINazwiska() {
        Assertions.assertNull(persona.zwrocNapis());
    }

    @Test
    void czyZtwociTRUEjesliOsobaJestPelnoletnia() {
        int wiek = 19;
        persona.setWiek(wiek);
        Assertions.assertTrue(persona.czyPelnoletnia());
    }

    @Test
    void czyZwrociFALSEjesliOsobaNieJestNiepelnoletnia() {
        int wiek = 14;
        persona.setWiek(wiek);
        Assertions.assertFalse(persona.czyPelnoletnia());
    }

    @Test
    void czyZwrociaFalseJesliOsobaMaUjemnyWiek() {
        Assertions.assertEquals(0, persona.getWiek());

        int wiekPlus = 15;
        persona.setWiek((wiekPlus));
        Assertions.assertEquals(wiekPlus, persona.getWiek());

        int wiekMinus = -4;
        persona.setWiek(wiekMinus);
        Assertions.assertEquals(wiekPlus, persona.getWiek());
        Assertions.assertNotEquals(wiekMinus, persona.getWiek());
    }

    @Test
    void czyTestDlaKobietyZwrociDobryWiekDoEmerytury() {
        int spodziewanyWiek = 66;

        persona.setWiek(spodziewanyWiek);
        persona.setCzyMezczyzna(false);

        Assertions.assertEquals(0, persona.ileDoEmerytury());
    }

    @Test
    void czyTestDlaMezczyznyZwrociDobryWiekDoEmerytury() {
        int zakladamWiek = 80;

        persona.setWiek(zakladamWiek);
        persona.setCzyMezczyzna(true);

        Assertions.assertEquals(0, persona.ileDoEmerytury());
    }

    @Test
    void czyZwrociPoprawnyWiekDoEmeryturyDlaKobityPonizej65() {
        int wiekLudzia = 50;

        persona.setWiek(wiekLudzia);
        persona.setCzyMezczyzna(false);

        Assertions.assertEquals(15, persona.ileDoEmerytury());
    }

    @Test
    void czyZwrociPoprawnyWiekDoEmeryturyDlaMezczyznyPonizej67() {
        int wiekLudzia = 50;

        persona.setWiek(wiekLudzia);
        persona.setCzyMezczyzna(true);

        Assertions.assertEquals(17, persona.ileDoEmerytury());
    }

    @Test
    void gdyPodamyWiekuOsoby() {
        persona.setCzyMezczyzna(false);
        Assertions.assertEquals(-1, persona.ileDoEmerytury());

        persona.setCzyMezczyzna(true);
        Assertions.assertEquals(-1, persona.ileDoEmerytury());
    }

    @Test
    void czyZwrociNullJesliNiePodamymieniaINazwiska() {
        Assertions.assertNull(persona.zwrocLogin());
    }

    @Test
    void zwrociXXXGdyNieMaImienia(){
        String nazwisko = "Placek";

        persona.setNazwisko(nazwisko);

        Assertions.assertEquals("XXXpla6",persona.zwrocLogin());

    }

    @Test
    void zwrociYYYgdyNieMaNazwiska(){
        String imie = "Jacek";

        persona.setImie(imie);

        Assertions.assertEquals("jacYYY5",persona.zwrocLogin());

    }

    @Test
    void zwrociLoginZDopełnionymZnakiemGdyDlugoscImieniaLubNazwiskaJestKrotszaNiz3(){
        String imie = "Xi";
        String nazwisko = "Ping";

        persona.setImie(imie);
        persona.setNazwisko(nazwisko);

        Assertions.assertEquals("xiZpin6",persona.zwrocLogin());
    }
}
