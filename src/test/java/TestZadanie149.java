import com.company.zadania.zadanie149.Zadanie149;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestZadanie149 {
    Zadanie149 jakasNazwa = new Zadanie149();

    @Test
    void sprawdzaCzyLiczbaJestWPrzedziale() {
        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(3, 9, 4);
        Assertions.assertTrue(wynik);
    }

    @Test
    void sprawdzaCzyJestPrzedPrzedziałem() {
        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(3, 6, 1);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzaCzyJestZaPrzedzdiale() {

        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(3, 6, 7);
        Assertions.assertFalse(wynik);
    }

    @Test
    void spradzaCzyPrzedzialJestBledny() {
        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(5, 1, 4);
        Assertions.assertFalse(wynik);
    }

    @Test
    void sprawdzaCzyStartjestRownyStop() {
        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(4, 4, 5);
        Assertions.assertFalse(wynik);

    }

    @Test
    void sprawdzaGdyLiczbaJestTakaSamaJakPrzedzialy() {
        boolean wynik = jakasNazwa.czyZnajdueSieWPRzedziale(3, 3, 3);
        Assertions.assertTrue(wynik);
    }
}
